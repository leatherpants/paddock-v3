<?php

namespace Tests\Feature;

use Tests\TestCase;

class AppHelperTest extends TestCase
{
    /** @test */
    public function test_get_empty_title()
    {
        $this->assertEquals('Ferrari Paddock', getTitle());
    }

    /** @test */
    public function test_get_title()
    {
        $title = 'Sebastian Vettel';

        $this->assertEquals('Ferrari Paddock - Sebastian Vettel', getTitle($title));
    }
}
