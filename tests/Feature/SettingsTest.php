<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SettingsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_settings_index()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get(route('settings'));

        $response->assertStatus(200);
    }

    /** @test */
    public function test_update_settings()
    {
        $user = factory(Users::class)->create();

        $data = [
            'id' => $user->id,
            'lang' => 'en',
        ];

        $response = $this->actingAs($user)->post(route('settings'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('settings'));
    }

    /** @test */
    public function test_change_language()
    {
        $user = factory(Users::class)->create();

        $result = $this->actingAs($user)->get(route('settings.changeLanguage', ['lang' => 'en']));
        $result->assertStatus(302);
    }

    /** @test */
    public function test_logout_others()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get(route('settings.logoutOthers'));
        $response->assertStatus(302);
        $response->assertRedirect(route('settings'));
    }
}
