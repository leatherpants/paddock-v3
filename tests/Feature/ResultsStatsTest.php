<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultsStatsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_results_stats()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/results/stats/index');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_results_stats_wins()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/results/stats/wins');

        $response->assertStatus(200);
    }
}
