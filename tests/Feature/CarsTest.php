<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Cars\Models\Cars;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CarsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_cars()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/cars');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_car()
    {
        $user = factory(Users::class)->create();

        $car = factory(Cars::class)->create([
            'slug' => 'sf70h',
        ]);

        $response = $this->actingAs($user)->get('/cars/'.$car->slug);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_car_fails()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/cars/sf79h');

        $response->assertStatus(404);
    }
}
