<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Settings\Models\Settings;
use App\paddock\Seasons\Models\SeasonsRaces;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_dashboard()
    {
        $user = factory(Users::class)->create();

        $season = factory(Settings::class)->create([
            'key' => 'season',
            'value' => 2018,
        ]);

        $gp = factory(Settings::class)->create([
            'key' => 'gp_id',
            'value' => 1,
        ]);

        factory(SeasonsRaces::class)->create([
            'season' => $season->value,
            'gp_id' => $gp->value,
        ]);

        factory(GrandPrixs::class)->create([
            'id' => $gp->value,
            'full_name' => 'Swedish Grand Prix',
        ]);

        $response = $this->actingAs($user)->get('/dashboard');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_dashboard_for_racing()
    {
        $user = factory(Users::class)->create();

        $season = factory(Settings::class)->create([
            'key' => 'season',
            'value' => 2018,
        ]);

        $gp = factory(Settings::class)->create([
            'key' => 'gp_id',
            'value' => 1,
        ]);

        $race = factory(SeasonsRaces::class)->create([
            'season' => $season->value,
            'gp_id' => $gp->value,
        ]);

        factory(GrandPrixs::class)->create([
            'id' => $race->id,
            'name' => 'Sweden',
            'hashtag' => 'SwedishGP',
        ]);

        factory(Tracks::class)->create([
            'id' => $race->track_id,
            'name' => 'Orebro Street Circuit',
        ]);

        $response = $this->actingAs($user)->get('/dashboard/race');

        $response->assertStatus(200);
    }

    /** @test  */
    public function test_get_dashboard_for_testing()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/dashboard/test');

        $response->assertStatus(200);
    }
}
