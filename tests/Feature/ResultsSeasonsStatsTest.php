<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Seasons\Models\Seasons;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultsSeasonsStatsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_results_seasons_stats_wins()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => 2018,
        ]);

        $response = $this->actingAs($user)->get('/results/stats/'.$season->season.'/wins');

        $response->assertStatus(200);
    }
}
