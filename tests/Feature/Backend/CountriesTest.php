<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Countries\Models\Countries;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountriesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_countries()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/countries');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_add_backend_countries()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/countries/add');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_store_backend_countries()
    {
        $user = factory(Users::class)->create();

        $data = [
            'code' => 'IT',
            'name' => 'Italy',
        ];

        $response = $this->actingAs($user)->post('/backend/countries/add', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/countries');
    }

    /** @test */
    public function test_edit_backend_countries()
    {
        $user = factory(Users::class)->create();

        $countries = factory(Countries::class)->create();

        $response = $this->actingAs($user)->get('/backend/countries/'.$countries->code.'/edit');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_update_backend_countries()
    {
        $user = factory(Users::class)->create();

        $countries = factory(Countries::class)->create();

        $data = [
            'code' => $countries->code,
            'name' => $countries->name,
        ];

        $response = $this->actingAs($user)->post('/backend/countries/'.$countries->code.'/edit', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/countries');
    }
}
