<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_home()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend');

        $response->assertStatus(200);
    }
}
