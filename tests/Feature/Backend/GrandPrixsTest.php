<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GrandPrixsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_grand_prix()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/grandprixs');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_create_backend_grand_prix()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/grandprixs/add');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_store_backend_grand_prix()
    {
        $user = factory(Users::class)->create();

        $data = [
            'country_id' => 'sg',
            'name' => 'Singapore',
            'slug' => 'singapore',
            'full_name' => 'Singapore Grand Prix',
            'hashtag' => '#SingaporeGP',
            'emoji' => '🇸🇬',
            'active' => 'true',
        ];

        $response = $this->actingAs($user)->post('/backend/grandprixs/add', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/grandprixs');
    }

    /** @test */
    public function test_edit_backend_grand_prix()
    {
        $user = factory(Users::class)->create();

        $gp = factory(GrandPrixs::class)->create();

        $response = $this->actingAs($user)->get('/backend/grandprixs/'.$gp->slug.'/edit');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_update_backend_grand_prix()
    {
        $user = factory(Users::class)->create();

        $gp = factory(GrandPrixs::class)->create();

        $data = [
            'country_id' => 'sg',
            'name' => 'Singapore',
            'slug' => 'singapore',
            'full_name' => 'Singapore Grand Prix',
            'hashtag' => '#SingaporeGP',
            'emoji' => '🇸🇬',
            'active' => 'true',
        ];

        $response = $this->actingAs($user)->post('/backend/grandprixs/'.$gp->slug.'/edit', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/grandprixs');
    }

    /** @test */
    public function test_races_backend_grand_prix()
    {
        $user = factory(Users::class)->create();

        $gp = factory(GrandPrixs::class)->create();

        $response = $this->actingAs($user)->get('/backend/grandprixs/'.$gp->slug);

        $response->assertStatus(200);
    }
}
