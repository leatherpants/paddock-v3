<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SocialMediaTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_social_media()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/socialmedias');

        $response->assertStatus(200);
    }
}
