<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Seasons\Models\Seasons;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SeasonsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_seasons()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/seasons');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_create_backend_seasons()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/seasons/add');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_store_backend_seasons()
    {
        $user = factory(Users::class)->create();

        $data = [
            'season' => 2018,
        ];

        $response = $this->actingAs($user)->post('/backend/seasons/add', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/seasons');
    }

    /** @test */
    public function test_edit_backend_seasons()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create();

        $response = $this->actingAs($user)->get('/backend/seasons/'.$season->season.'/edit');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_update_backend_seasons()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create();

        $data = [
            'season' => 2018,
        ];

        $response = $this->actingAs($user)->post('/backend/seasons/'.$season->season.'/edit', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/seasons');
    }
}
