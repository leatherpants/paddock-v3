<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HashtagsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_hashtags()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/hashtags');

        $response->assertStatus(200);
    }
}
