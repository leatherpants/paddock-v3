<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Tracks\Models\Tracks;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrackTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_tracks()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/tracks');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_create_backend_tracks()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/tracks/add');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_store_backend_tracks()
    {
        $user = factory(Users::class)->create();

        $data = [
            'country_id' => 'de',
            'name' => 'Hockenheimring',
            'slug' => 'hockenheimring',
            'timezone' => 'Europe/Berlin',
            'active' => 'true',
        ];

        $response = $this->actingAs($user)->post('/backend/tracks/add', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/tracks');
    }

    /** @test */
    public function test_edit_backend_tracks()
    {
        $user = factory(Users::class)->create();

        $track = factory(Tracks::class)->create();

        $response = $this->actingAs($user)->get('/backend/tracks/'.$track->slug.'/edit');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_update_backend_tracks()
    {
        $user = factory(Users::class)->create();

        $track = factory(Tracks::class)->create();

        $data = [
            'country_id' => 'de',
            'name' => 'Hockenheimring',
            'slug' => 'hockenheimring',
            'timezone' => 'Europe/Berlin',
            'active' => 'true',
        ];

        $response = $this->actingAs($user)->post('/backend/tracks/'.$track->slug.'/edit', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/backend/tracks');
    }
}
