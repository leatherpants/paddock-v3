<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LanguagesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_users()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/languages');

        $response->assertStatus(200);
    }
}
