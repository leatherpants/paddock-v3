<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Seasons\Models\Seasons;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SeasonsRacesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_seasons_races()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create();

        $response = $this->actingAs($user)->get('backend/seasons/'.$season->season.'/races');

        $response->assertStatus(200);
    }
}
