<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TwitterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_twitter()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/backend/twitter');

        $response->assertStatus(200);
    }
}
