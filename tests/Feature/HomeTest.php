<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_index()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_index_logged_in()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/');
        $response->assertRedirect('/dashboard');
    }

    /** @test */
    public function test_get_activities()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/activities');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_sessions()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/sessions');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_social_media()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/socials');

        $response->assertStatus(200);
    }
}
