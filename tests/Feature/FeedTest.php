<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Feed\Models\FeedFDA;
use App\paddock\Feed\Models\FeedFerrari;
use App\paddock\Feed\Models\FeedHistory;
use App\paddock\Feed\Models\FeedMotorsport;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_feed_index()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_feed_meta()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/meta');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_fda_feed()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/fda');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_fda_feed_by_entry_en()
    {
        $user = factory(Users::class)->create([
            'lang' => 'en',
        ]);

        $feed = factory(FeedFDA::class)->create([
            'lang'       => 'en',
            'title'      => 'Formula 3 - Zhou up to second in FIA F3 championship',
            'created_at' => '2018-06-05',
        ]);

        $response = $this->actingAs($user)->get('/feed/fda/'.$feed->created_at->toDateString());

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_fda_feed_by_entry()
    {
        $user = factory(Users::class)->create([
            'lang' => 'de',
        ]);

        $feed = factory(FeedFDA::class)->create([
            'lang'       => 'de',
            'title'      => 'Formula 3 - Zhou bis auf den zweiten Platz in der FIA F3 Meisterschaft',
            'created_at' => '2018-06-05',
        ]);

        $response = $this->actingAs($user)->get('/feed/fda/'.$feed->created_at->toDateString());

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_fda_feed_by_entry_fails()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/fda/2003-05-26');

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_ferrari_feed()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/ferrari');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_ferrari_feed_by_entry_en()
    {
        $user = factory(Users::class)->create([
            'lang' => 'en',
        ]);

        $feed = factory(FeedFerrari::class)->create([
            'session'    => 2,
            'lang'       => 'en',
            'title'      => 'Swedish Grand Prix - Comeback after long time',
            'created_at' => '2018-06-12 10:03:59',
        ]);

        $response = $this->actingAs($user)->get('/feed/ferrari/'.$feed->created_at->toDateString().'/'.$feed->session);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_ferrari_feed_by_entry()
    {
        $user = factory(Users::class)->create([
            'lang' => 'de',
        ]);

        $feed = factory(FeedFerrari::class)->create([
            'session'    => 2,
            'lang'       => 'de',
            'title'      => 'Grand Prix von Schweden - Comeback nach langer Zeit',
            'created_at' => '2018-06-12 10:03:59',
        ]);

        $response = $this->actingAs($user)->get('/feed/ferrari/'.$feed->created_at->toDateString().'/'.$feed->session);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_ferrari_feed_by_entry_fails()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/ferrari/2003-03-08/2');

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_ferrari_feed_by_hashtag()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/ferrari/F1Testing');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_history_feed()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/history');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_history_feed_by_entry_en()
    {
        $user = factory(Users::class)->create([
            'lang' => 'en',
        ]);

        $feed = factory(FeedHistory::class)->create([
            'lang'  => 'en',
            'date'  => '2005-01-11',
            'title' => 'Sandstorm on the circuit at Sakhir',
        ]);

        $response = $this->actingAs($user)->get('/feed/history/'.$feed->date);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_history_feed_by_entry()
    {
        $user = factory(Users::class)->create([
            'lang' => 'de',
        ]);

        $feed = factory(FeedHistory::class)->create([
            'lang'  => 'de',
            'date'  => '2005-01-11',
            'title' => 'Sandstorm on the circuit at Sakhir',
        ]);

        $response = $this->actingAs($user)->get('/feed/history/'.$feed->date);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_history_feed_by_entry_fails()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/history/2003-03-07');

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_motorsport_feed()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/motorsport');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_motorsport_feed_by_entry_en()
    {
        $user = factory(Users::class)->create();

        $feed = factory(FeedMotorsport::class)->create([
            'lang'       => 'en',
            'title'      => 'Raikkonen\'s race engineer Greenwood leaves Ferrari',
            'created_at' => '2018-01-08',
        ]);

        $response = $this->actingAs($user)->get('/feed/motorsport/'.$feed->created_at->toDateString());

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_motorsport_feed_by_entry()
    {
        $user = factory(Users::class)->create();

        $feed = factory(FeedMotorsport::class)->create([
            'lang'       => 'de',
            'title'      => 'Kimi Räikkönens Renningenieur verlässt Ferrari',
            'created_at' => '2018-01-08',
        ]);

        $response = $this->actingAs($user)->get('/feed/motorsport/'.$feed->created_at->toDateString());

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_motorsport_feed_by_entry_fails()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/feed/motorsport/2017-01-16');

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_meta_details_by_link()
    {
        $user = factory(Users::class)->create();

        $link = 'https://formula1.ferrari.com/en/2018-f1-german-gp-race/';

        $data = get_meta_tags($link, false);

        $this->actingAs($user)->post('/feed/meta', ['url' => $link]);

        $this->assertEquals('German Grand Prix - A cold shower', html_entity_decode($data['twitter:title']));
        $this->assertEquals('Kimi 3rd but Seb goes off in the wet in an incredible race', html_entity_decode($data['twitter:description']));
        $this->assertEquals('https://static.formula1.ferrari.com/imgresize-cache/88683ec7a058889701332c8b02f6f09d.jpg', $data['twitter:image']);
        $this->assertEquals('https://formula1.ferrari.com/en/2018-f1-german-gp-race/', $link);
    }
}
