<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Events\Models\Events;
use App\paddock\Staffs\Models\Staffs;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_events()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/events');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_event_en()
    {
        $user = factory(Users::class)->create();

        $staff = factory(Staffs::class)->create();

        $event = factory(Events::class)->create([
            'staff_id' => $staff->id,
            'date' => 1945 - 01 - 01,
            'lang' => 'en',
        ]);

        $response = $this->actingAs($user)->get('/events/'.$event->date.'/'.$event->staff_id);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_event()
    {
        $user = factory(Users::class)->create();

        $staff = factory(Staffs::class)->create();

        $event = factory(Events::class)->create([
            'staff_id' => $staff->id,
            'date' => 1945 - 01 - 01,
            'lang' => 'de',
        ]);

        $response = $this->actingAs($user)->get('/events/'.$event->date.'/'.$event->staff_id);

        $response->assertStatus(200);
    }
}
