<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Results\Gaps;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GapTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function lessManyThanOneSecond()
    {
        $result = Gaps::gapintime(76372, 87356);

        $this->assertEquals('-10.984', $result);
    }

    /** @test */
    public function lessThanOneSecond()
    {
        $result = Gaps::gapintime(86372, 87356);

        $this->assertEquals('-0.984', $result);
    }

    /** @test */
    public function moreThanOneSecond()
    {
        $result = Gaps::gapintime(85936, 87356);

        $this->assertEquals('-1.420', $result);
    }

    /** @test */
    public function zeroGap()
    {
        $result = Gaps::gapintime(0, 0);

        $this->assertEquals('', $result);
    }

    /** @test */
    public function minimalGap()
    {
        $result = Gaps::gapintime(87356, 87350);
        $result1 = Gaps::gapintime(87350, 87356);
        $result2 = Gaps::gapintime(87350, 87350);

        $this->assertEquals('+0.006', $result);
        $this->assertEquals('-0.006', $result1);
        $this->assertEquals('+0.000', $result2);
    }

    /** @test */
    public function plusWithinASecond()
    {
        $result = Gaps::gapintime(87936, 87356);

        $this->assertEquals('+0.580', $result);
    }

    /** @test */
    public function plusMoreThanOneSecond()
    {
        $result = Gaps::gapintime(88936, 87356);

        $this->assertEquals('+1.580', $result);
    }

    /** @test */
    public function plusManyMoreThanOneSecond()
    {
        $result = Gaps::gapintime(98936, 87356);

        $this->assertEquals('+11.580', $result);
    }

    /** @test */
    public function zeroGapTeam()
    {
        $result = Gaps::team(0, 0);

        $this->assertEquals('', $result);
    }

    /** @test */
    public function minimalGapTeam()
    {
        $result = Gaps::team(87350, 87356);
        $result1 = Gaps::team(87350, 87350);

        $this->assertEquals('+0.006', $result);
        $this->assertEquals('0.000', $result1);
    }

    /** @test */
    public function plusWithinASecondTeam()
    {
        $result = Gaps::team(87356, 87936);

        $this->assertEquals('+0.580', $result);
    }

    /** @test */
    public function plusMoreThanOneSecondTeam()
    {
        $result = Gaps::team(87356, 88936);

        $this->assertEquals('+1.580', $result);
    }

    /** @test */
    public function plusManyMoreThanOneSecondTeam()
    {
        $result = Gaps::team(87356, 98936);

        $this->assertEquals('+11.580', $result);
    }
}
