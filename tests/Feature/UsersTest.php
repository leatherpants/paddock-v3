<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Users\Models\UsersSocials;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function test_get_profile()
    {
        $user = factory(Users::class)->create([
            'display_name' => 'Sandra Granath',
            'user_name' => 'sandra-granath',
        ]);

        factory(UsersSocials::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->actingAs($user)->get('/sandra-granath');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_fet_profile_fails()
    {
        $user = factory(Users::class)->create([
            'display_name' => 'Sandra Granath',
            'user_name' => 'sandra-granath',
        ]);

        $response = $this->actingAs($user)->get('/roman-granath');

        $response->assertStatus(404);
    }
}
