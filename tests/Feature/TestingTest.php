<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestingTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function test_get_testing_results()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/tests/result/2018/2');

        $response->assertStatus(200);
    }
}
