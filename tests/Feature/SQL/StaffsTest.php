<?php

namespace Tests\Feature\SQL;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StaffsTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function test_get_staffs()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/sql/staffs');
        $response->assertSuccessful();
    }
}
