<?php

namespace Tests\Feature\SQL;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Seasons\Models\SeasonsRaces;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_index()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/sql/results');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_grandprix()
    {
        $user = factory(Users::class)->create();

        $gp = factory(SeasonsRaces::class)->create();

        $response = $this->actingAs($user)->get('/sql/results/gp/'.$gp->id);
        $response->assertSuccessful();
    }
}
