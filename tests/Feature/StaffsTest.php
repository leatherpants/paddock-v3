<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Staffs\Models\Staffs;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StaffsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_staffs()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/staffs');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_staff()
    {
        $user = factory(Users::class)->create();

        $staff = factory(Staffs::class)->create([
            'slug' => 'michael-schumacher',
        ]);

        $response = $this->actingAs($user)->get('/staffs/'.$staff->slug);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_staff_fails()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/staffs/roman-granath');

        $response->assertStatus(404);
    }
}
