<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Laravel\Passport\Passport;
use App\paddock\Users\Models\Users;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Settings\Models\Settings;
use App\paddock\Seasons\Models\SeasonsRaces;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TimeZoneTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_away_time_zone()
    {
        $user = factory(Users::class)->create();

        Passport::actingAs($user);

        $season = factory(Settings::class)->create([
            'key' => 'season',
            'value' => 2018,
        ]);

        $gp = factory(Settings::class)->create([
            'key' => 'gp_id',
            'value' => 1,
        ]);

        factory(SeasonsRaces::class)->create([
            'season' => $season->value,
            'gp_id' => $gp->value,
        ]);

        $track = factory(Tracks::class)->create([
            'timezone' => 'Europe/Rome',
        ]);

        $data = [
            'timezone' => $track->timezone,
        ];

        $response = $this->json('get', '/api/time/away', $data);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_home_time_zone()
    {
        $user = factory(Users::class)->create();

        Passport::actingAs($user);

        $response = $this->get('/api/time/home');

        $response->assertStatus(200);
    }
}
