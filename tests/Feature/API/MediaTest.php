<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Laravel\Passport\Passport;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\paddock\Media\Youtube\Models\MediaYoutubeVideos;

class MediaTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend_latest_youtube_video()
    {
        $user = factory(Users::class)->create();

        Passport::actingAs($user);

        $video = factory(MediaYoutubeVideos::class)->create();

        $data = [
            'thumbnail' => 'https://img.youtube.com/vi/'.$video->youtube_id.'/0.jpg',
            'title' => $video->title,
        ];

        $response = $this->json('GET', '/api/media/youtube/latest', $data);

        $response->assertStatus(200);
    }
}
