<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Laravel\Passport\Passport;
use App\paddock\Users\Models\Users;
use App\paddock\Seasons\Models\Seasons;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_season()
    {
        $user = factory(Users::class)->create();

        Passport::actingAs($user);

        $season = factory(Seasons::class)->create([
            'season' => '2018',
        ]);

        $response = $this->get('/api/results/season/'.$season->season);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_total()
    {
        $user = factory(Users::class)->create();

        Passport::actingAs($user);

        $response = $this->get('/api/results/total');

        $response->assertStatus(200);
    }
}
