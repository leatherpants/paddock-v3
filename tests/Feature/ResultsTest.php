<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\Seasons\Models\SeasonsRaces;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_results()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/results');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_results_all_seasons()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/results/seasons');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_result()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => '2018',
        ]);

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'sweden',
        ]);

        $track = factory(Tracks::class)->create();

        $season = factory(SeasonsRaces::class)->create([
            'season' => $season->season,
            'gp_id' => $gp->id,
            'track_id' => $track->id,
        ]);

        $response = $this->actingAs($user)->get('/results/seasons/'.$season->season.'/'.$gp->slug);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_result_fails()
    {
        $user = factory(Users::class)->create();

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'sweden',
        ]);

        $response = $this->actingAs($user)->get('/results/seasons/2019/'.$gp->slug);

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_season()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => '2018',
        ]);

        $response = $this->actingAs($user)->get('/results/seasons/'.$season->season);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_view()
    {
        $user = factory(Users::class)->create();

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'italy',
        ]);

        $response = $this->actingAs($user)->get('/results/'.$gp->slug);

        $response->assertStatus(200);
    }
}
