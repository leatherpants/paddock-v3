<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SeasonsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_seasons()
    {
        $user = factory(Users::class)->create();

        factory(Seasons::class)->create(
            ['season' => 2018]
        );

        $response = $this->actingAs($user)->get('/seasons');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_seasons_race()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => 2018,
        ]);

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'sweden',
        ]);

        $response = $this->actingAs($user)->get('/seasons/'.$season->season.'/'.$gp->slug);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_seasons_race_files()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => 2018,
        ]);

        $gp = factory(GrandPrixs::class)->create([
            'id' => 21,
            'slug' => 'sweden',
        ]);

        $data = [
            'season' => $season->season,
            'gp_id' => $gp->id,
        ];

        $response = $this->actingAs($user)->get('/seasons/'.$season->season.'/'.$gp->slug.'/files', $data);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_seasons_race_laps()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => 2018,
        ]);

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'sweden',
        ]);

        $response = $this->actingAs($user)->get('/seasons/'.$season->season.'/'.$gp->slug.'/laps');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_seasons_race_live()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => 2018,
        ]);

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'sweden',
        ]);

        $session = [1, 2, 3, 4, 5];

        foreach ($session as $value) {
            $response = $this->actingAs($user)->get('/seasons/'.$season->season.'/'.$gp->slug.'/live/'.$value);

            $response->assertStatus(200);
        }
    }

    /** @test */
    public function test_get_seasons_race_preview()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => 2018,
        ]);

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'sweden',
        ]);

        $response = $this->actingAs($user)->get('/seasons/'.$season->season.'/'.$gp->slug.'/preview');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_seasons_tyre_report()
    {
        $user = factory(Users::class)->create();

        $season = factory(Seasons::class)->create([
            'season' => '2018',
        ]);

        $gp = factory(GrandPrixs::class)->create([
            'slug' => 'sweden',
            'full_name' => 'Swedish Grand Prix',
        ]);

        $response = $this->actingAs($user)->get('/seasons/'.$season->season.'/'.$gp->slug.'/report/tyres/76');

        $response->assertStatus(200);
    }
}
