<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_login_page()
    {
        $response = $this->get(route('login'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_login_redirect()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get(route('login'));
        $response->assertStatus(302);
        $response->assertRedirect(route('dashboard'));
    }

    /** @test */
    public function test_get_login()
    {
        $user = factory(Users::class)->create();

        $response = $this->post(route('login'), ['email' => $user->email, 'password' => 'secret']);
        $response->assertStatus(302);
        $response->assertRedirect(route('dashboard'));
    }

    /** @test */
    public function test_get_login_false()
    {
        $user = factory(Users::class)->create();

        $response = $this->post(route('login'), ['email' => $user->email, 'password' => 'xxxyyy']);
        $response->assertStatus(302);
        $response->assertRedirect(route('index'));
    }

    /** @test */
    public function test_get_logout()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get(route('logout'));
        $response->assertRedirect(route('index'));
    }

    /** @test */
    public function test_redirect_user_to_login_form()
    {
        $response = $this->get(route('dashboard'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
