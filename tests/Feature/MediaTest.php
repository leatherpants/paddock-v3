<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\paddock\Media\Gallery\Models\MediaGalleryAlbums;
use App\paddock\Media\Gallery\Models\MediaGalleryPhotos;
use App\paddock\Media\Youtube\Models\MediaYoutubeVideos;

class MediaTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_media()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/media');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_media_galleries()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/media/gallery/galleries');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_media_gallery_images()
    {
        $user = factory(Users::class)->create();

        $album = factory(MediaGalleryAlbums::class)->create([
            'name' => 'Sweden GP',
        ]);

        $response = $this->actingAs($user)->get('/media/gallery/'.$album->id.'/images');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_media_gallery_images_fails()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/media/gallery/1000/images');

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_media_youtube_videos()
    {
        $user = factory(Users::class)->create();

        $response = $this->actingAs($user)->get('/media/youtube/videos');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_media_youtube_video()
    {
        $user = factory(Users::class)->create();

        $video = factory(MediaYoutubeVideos::class)->create([
            'youtube_id' => '-gG3Sa0ReEk',
        ]);

        $response = $this->actingAs($user)->get('/media/youtube/'.$video->youtube_id);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_set_background_image_on_profile()
    {
        $user = factory(Users::class)->create();

        $image = factory(MediaGalleryPhotos::class)->create();

        $data = [
            'user_id' => $user->id,
            'background_image' => $image->id,
        ];

        $response = $this->actingAs($user)->get('/media/gallery/'.$image->id.'/set', $data);
        $response->assertStatus(302);
    }
}
