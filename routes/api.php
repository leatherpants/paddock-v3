<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API', 'middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'feed'], function () {
        Route::get('activities', ['uses' => 'FeedController@activities']);
        Route::get('fda/latest/{lang}', ['uses' => 'FeedController@fdaStories']);
        Route::get('ferrari/latest/{lang}', ['uses' => 'FeedController@ferrariStories']);
        Route::get('history/latest/{lang}', ['uses' => 'FeedController@historyStories']);
        Route::get('latestarticles', ['uses' => 'FeedController@latestArticles']);
        Route::get('motorsport/latest/{lang}', ['uses' => 'FeedController@motorsportStories']);
    });

    Route::group(['prefix' => 'media'], function () {
        Route::get('youtube/latest', ['uses' => 'MediaController@latestYouTubeVideo']);
    });

    Route::group(['prefix' => 'results'], function () {
        Route::get('season/{season}', ['uses' => 'ResultsController@season']);
        Route::get('total', ['uses' => 'ResultsController@total']);
    });

    Route::group(['prefix' => 'time'], function () {
        Route::get('away', ['uses' => 'TimeZoneController@away']);
        Route::get('home', ['uses' => 'TimeZoneController@home']);
    });
});
