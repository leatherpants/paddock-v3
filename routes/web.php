<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => 'auth'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'backend']);

    Route::group(['prefix' => 'countries'], function () {
        Route::get('', ['uses' => 'CountriesController@index', 'as' => 'backend.countries']);
        Route::get('add', ['uses' => 'CountriesController@create', 'as' => 'backend.countries.add']);
        Route::post('add', ['uses' => 'CountriesController@store']);
        Route::get('{code}/edit', ['uses' => 'CountriesController@edit', 'as' => 'backend.countries.edit']);
        Route::post('{code}/edit', ['uses' => 'CountriesController@update']);
        Route::get('{code}/delete', ['uses' => 'CountriesController@destroy', 'as' => 'backend.countries.delete']);
    });

    Route::group(['prefix' => 'grandprixs'], function () {
        Route::get('', ['uses' => 'GrandPrixsController@index', 'as' => 'backend.grandprixs']);
        Route::get('add', ['uses' => 'GrandPrixsController@create', 'as' => 'backend.grandprixs.add']);
        Route::post('add', ['uses' => 'GrandPrixsController@store']);
        Route::get('{slug}/edit', ['uses' => 'GrandPrixsController@edit', 'as' => 'backend.grandprixs.edit']);
        Route::post('{slug}/edit', ['uses' => 'GrandPrixsController@update']);
        Route::get('{slug}/delete', ['uses' => 'GrandPrixsController@destroy', 'as' => 'backend.grandprixs.delete']);
        Route::get('{slug}', ['uses' => 'GrandPrixsController@races', 'as' => 'backend.grandprixs.races']);
    });

    Route::group(['prefix' => 'hashtags'], function () {
        Route::get('', ['uses' => 'HashtagsController@index', 'as' => 'backend.hashtags']);
    });

    Route::group(['prefix' => 'languages'], function () {
        Route::get('', ['uses' => 'LanguagesController@index', 'as' => 'backend.languages']);
    });

    Route::group(['prefix' => 'seasons'], function () {
        Route::get('', ['uses' => 'SeasonsController@index', 'as' => 'backend.seasons']);
        Route::get('add', ['uses' => 'SeasonsController@create', 'as' => 'backend.seasons.add']);
        Route::post('add', ['uses' => 'SeasonsController@store']);
        Route::get('{season}/edit', ['uses' => 'SeasonsController@edit', 'as' => 'backend.seasons.edit']);
        Route::post('{season}/edit', ['uses' => 'SeasonsController@update']);
        Route::get('{season}/delete', ['uses' => 'SeasonsController@destroy', 'as' => 'backend.seasons.delete']);

        Route::get('{season}/races', ['uses' => 'SeasonsRacesController@view', 'as' => 'backend.seasons.races']);
    });

    Route::group(['prefix' => 'socialmedias'], function () {
        Route::get('', ['uses' => 'SocialMediaController@index', 'as' => 'backend.socialmedias']);
    });

    Route::group(['prefix' => 'tracks'], function () {
        Route::get('', ['uses' => 'TracksController@index', 'as' => 'backend.tracks']);
        Route::get('add', ['uses' => 'TracksController@create', 'as' => 'backend.tracks.add']);
        Route::post('add', ['uses' => 'TracksController@store']);
        Route::get('{slug}/edit', ['uses' => 'TracksController@edit', 'as' => 'backend.tracks.edit']);
        Route::post('{slug}/edit', ['uses' => 'TracksController@update']);
        Route::get('{slug}/delete', ['uses' => 'TracksController@destroy', 'as' => 'backend.tracks.delete']);
    });

    Route::group(['prefix' => 'twitter'], function () {
        Route::get('', ['uses' => 'TwitterController@index', 'as' => 'backend.twitter']);
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('', ['uses' => 'UsersController@index', 'as' => 'backend.users']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::get('login', ['uses' => 'AuthController@getLogin', 'as' => 'login']);
    Route::post('login', ['uses' => 'AuthController@postLogin']);
    Route::get('logout', ['uses' => 'AuthController@getLogout', 'as' => 'logout']);

    Route::group(['middleware' => 'auth'], function () {
        Route::get('activities', ['uses' => 'HomeController@activities', 'as' => 'activities']);
        Route::get('sessions', ['uses' => 'HomeController@sessions', 'as' => 'sessions']);
        Route::get('socials', ['uses' => 'HomeController@socialMedia', 'as' => 'socials']);

        Route::group(['prefix' => 'cars'], function () {
            Route::get('', ['uses' => 'CarsController@index', 'as' => 'cars']);
            Route::get('{slug}', ['uses' => 'CarsController@car', 'as' => 'cars.car']);
        });

        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('', ['uses' => 'DashboardController@dashboard', 'as' => 'dashboard']);
            Route::get('race', ['uses' => 'DashboardController@race', 'as' => 'dashboard.race']);
            Route::get('test', ['uses' => 'DashboardController@test', 'as' => 'dashboard.test']);
        });

        Route::group(['prefix' => 'events'], function () {
            Route::get('', ['uses' => 'EventsController@index', 'as' => 'events']);
            Route::get('{date}/{staff}', ['uses' => 'EventsController@event', 'as' => 'events.event']);
        });

        Route::group(['prefix' => 'feed'], function () {
            Route::get('', ['uses' => 'FeedController@index', 'as' => 'feed']);
            Route::get('meta', ['uses' => 'FeedController@meta', 'as' => 'feed.meta']);
            Route::post('meta', ['uses' => 'FeedController@postMeta']);

            Route::group(['prefix' => 'fda'], function () {
                Route::get('', ['uses' => 'FeedFDAController@index', 'as' => 'feed.fda']);
                Route::get('{date}', ['uses' => 'FeedFDAController@entry', 'as' => 'feed.fda.entry']);
            });

            Route::group(['prefix' => 'ferrari'], function () {
                Route::get('', ['uses' => 'FeedFerrariController@index', 'as' => 'feed.ferrari']);
                Route::get('{date}/{session}', ['uses' => 'FeedFerrariController@entry', 'as' => 'feed.ferrari.entry']);
                Route::get('{hashtag}', ['uses' => 'FeedFerrariController@hashtag', 'as' => 'feed.ferrari.hashtag']);
            });

            Route::group(['prefix' => 'history'], function () {
                Route::get('', ['uses' => 'FeedHistoryController@index', 'as' => 'feed.history']);
                Route::get('{date}', ['uses' => 'FeedHistoryController@entry', 'as' => 'feed.history.entry']);
            });

            Route::group(['prefix' => 'motorsport'], function () {
                Route::get('', ['uses' => 'FeedMotorsportController@index', 'as' => 'feed.motorsport']);
                Route::get('{date}', ['uses' => 'FeedMotorsportController@article', 'as' => 'feed.motorsport.article']);
            });
        });

        Route::group(['prefix' => 'media'], function () {
            Route::get('', ['uses' => 'MediaController@index', 'as' => 'media']);

            Route::group(['prefix' => 'gallery'], function () {
                Route::get('galleries', ['uses' => 'MediaController@galleries', 'as' => 'media.gallery.galleries']);
                Route::get('{album_id}/images', ['uses' => 'MediaController@galleriesImages', 'as' => 'media.gallery.images']);

                Route::get('{image_id}/set', ['uses' => 'MediaController@setBackgroundImage', 'as' => 'media.gallery.set']);
            });

            Route::group(['prefix' => 'youtube'], function () {
                Route::get('videos', ['uses' => 'MediaController@youtubeVideos', 'as' => 'media.youtube.videos']);
                Route::get('{youtube_id}', ['uses' => 'MediaController@youtubeVideo', 'as' => 'media.youtube.video']);
            });
        });

        Route::group(['prefix' => 'results'], function () {
            Route::get('', ['uses' => 'ResultsController@index', 'as' => 'results']);
            Route::get('seasons', ['uses' => 'ResultsController@seasons', 'as' => 'results.seasons']);
            Route::get('seasons/{season}', ['uses' => 'ResultsController@season', 'as' => 'results.season']);
            Route::get('seasons/{season}/{gp}', ['uses' => 'ResultsController@result', 'as' => 'results.result']);
            Route::get('{slug}', ['uses' => 'ResultsController@view', 'as' => 'results.gp']);

            Route::group(['prefix' => 'stats'], function () {
                Route::get('index', ['uses' => 'ResultsStatsController@index', 'as' => 'results.stats']);
                Route::get('wins', ['uses' => 'ResultsStatsController@wins', 'as' => 'results.stats.wins']);
                Route::get('{season}/wins', ['uses' => 'ResultsSeasonsStatsController@seasonWins', 'as' => 'results.stats.seasons.wins']);
            });
        });

        Route::group(['prefix' => 'seasons'], function () {
            Route::get('', ['uses' => 'SeasonsController@index', 'as' => 'seasons']);
            Route::get('{season}/{gp}', ['uses' => 'SeasonsController@race', 'as' => 'seasons.race']);
            Route::get('{season}/{gp}/files', ['uses' => 'SeasonsController@raceFiles', 'as' => 'seasons.files']);
            Route::get('{season}/{gp}/laps', ['uses' => 'SeasonsController@raceLaps', 'as' => 'seasons.raceLaps']);
            Route::get('{season}/{gp}/live/{session}', ['uses' => 'SeasonsController@raceLive', 'as' => 'seasons.raceLive']);
            Route::get('{season}/{gp}/preview', ['uses' => 'SeasonsController@racePreview', 'as' => 'seasons.racePreview']);
            Route::get('{season}/{gp}/report/tyres/{staff}', ['uses' => 'SeasonsController@reportTyres', 'as' => 'seasons.reportTyres']);
        });

        Route::group(['prefix' => 'settings'], function () {
            Route::get('', ['uses' => 'SettingsController@index', 'as' => 'settings']);
            Route::post('', ['uses' => 'SettingsController@save']);
            Route::get('language/{lang}', ['uses' => 'SettingsController@changeLanguage', 'as' => 'settings.changeLanguage']);
            Route::get('logoutOthers', ['uses' => 'SettingsController@logoutOthers', 'as' => 'settings.logoutOthers']);
        });

        Route::group(['prefix' => 'staffs'], function () {
            Route::get('', ['uses' => 'StaffsController@index', 'as' => 'staffs']);
            Route::get('{slug}', ['uses' => 'StaffsController@view', 'as' => 'staffs.view']);
        });

        Route::group(['prefix' => 'tests'], function () {
            Route::get('result/{season}/{week}', ['uses' => 'TestsController@result', 'as' => 'tests.result']);
        });

        Route::get('{user_name}', ['uses' => 'UsersController@profile', 'as' => 'user']);
    });
});

Route::group(['namespace' => 'SQL', 'prefix' => 'sql'], function () {
    Route::get('results', ['uses' => 'ResultsController@index']);
    Route::get('results/gp/{gp_id}', ['uses' => 'ResultsController@gp', 'as' => 'sql.results']);

    Route::get('staffs', ['uses' => 'StaffsController@index']);
});
