
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('activities', require('./components/Feed/Activities'));
Vue.component('feed-fda', require('./components/Feed/FDA.vue'));
Vue.component('feed-ferrari', require('./components/Feed/Ferrari.vue'));
Vue.component('feed-history', require('./components/Feed/History.vue'));
Vue.component('feed-motorsport', require('./components/Feed/Motorsport'));
Vue.component('latest-articles', require('./components/Dashboard/LatestArticles.vue'));
Vue.component('season-results', require('./components/SeasonResults.vue'));
Vue.component('total-results', require('./components/TotalResults.vue'));
Vue.component('youtube', require('./components/Dashboard/YouTube.vue'));
Vue.component('timezone', require('./components/Dashboard/TimeZone'));

new Vue({
    el: '#app',
});
