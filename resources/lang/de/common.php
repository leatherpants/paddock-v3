<?php

return [
    'languages' => 'Sprachen',
    'language' => 'Sprache',
    'logout' => 'Abmelden',
    'save' => 'Änderungen speichern',
    'settings' => 'Einstellungen',
    'standings' => 'Wertungen',
    'timetable' => 'Zeitplan',
];
