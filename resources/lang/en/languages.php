<?php

return [
    'english' => 'English',
    'german' => 'German',
    'italian' => 'Italian',
    'spanish' => 'Spanish',
    'swedish' => 'Swedish',
];
