@extends('templates.frontend')
@section('title', getTitle())
@section('content')
<div class="ui fluid main container">
    <div class="ui grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="time icon"></i>
                        <span class="content">
                            2018 F1 Season shedule
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui table">
                    <thead>
                        <tr>
                            <th>Grand Prix</th>
                            <th colspan="2">Friday</th>
                            <th colspan="2">Saturday</th>
                            <th>Sunday</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>FP1</th>
                            <th>FP2</th>
                            <th>FP3</th>
                            <th>Qualifying</th>
                            <th>Race</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sessions as $session)
                        <tr>
                            <td>{{ $session->grandprix->name }}</td>
                            <td>
                                {{ date('H:i', strtotime($session->session(1)->start->timezone(Auth::user()->timezone))) }} - {{ date('H:i', strtotime($session->session(1)->end->timezone(Auth::user()->timezone))) }}
                                <small style="display: inherit;">{{ date('d.m.Y', strtotime($session->session(1)->start->timezone(Auth::user()->timezone))) }}</small>
                            </td>
                            <td>
                                {{ date('H:i', strtotime($session->session(2)->start->timezone(Auth::user()->timezone))) }} - {{ date('H:i', strtotime($session->session(2)->end->timezone(Auth::user()->timezone))) }}
                            </td>
                            <td>
                                {{ date('H:i', strtotime($session->session(3)->start->timezone(Auth::user()->timezone))) }} - {{ date('H:i', strtotime($session->session(3)->end->timezone(Auth::user()->timezone))) }}
                                <small style="display: inherit;">{{ date('d.m.Y', strtotime($session->session(3)->start->timezone(Auth::user()->timezone))) }}</small>
                            </td>
                            <td>
                                {{ date('H:i', strtotime($session->session(4)->start->timezone(Auth::user()->timezone))) }} - {{ date('H:i', strtotime($session->session(4)->end->timezone(Auth::user()->timezone))) }}
                            </td>
                            <td>
                                {{ date('H:i', strtotime($session->session(5)->start->timezone(Auth::user()->timezone))) }}
                                <small style="display: inherit;">{{ date('d.m.Y', strtotime($session->session(5)->start->timezone(Auth::user()->timezone))) }}</small>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection