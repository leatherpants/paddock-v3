@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <table class="ui table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.place') }}</th>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.lap_time') }}</th>
                            <th>{{ trans('common.gap') }}</th>
                            <th>{{ trans('common.laps') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $r => $result)
                        <tr>
                            <td>{{ ++$r }}</td>
                            <td>{{ $result->driver->name }}</td>
                            <td>{{ $result->secintime() }}</td>
                            <td>{{ $result->gapintime() }}</td>
                            <td>{{ $result->laps }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <table class="ui table">
                    <tbody>
                    @foreach($generals as $result)
                        <tr>
                            <td>{{ $result->driver->name }}</td>
                            <td>{{ $result->day }}</td>
                            <td>{{ $result->laps }}</td>
                            <td>{{ $result->laps * 4.655 }}</td>
                            <td>{{ number_format($result->laps * 100 / 66) }}%</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="{{ route('dashboard.test') }}" class="ui right floated ferrari labeled icon button">
                    <i class="left arrow icon"></i>
                    {{ trans('common.tests') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection