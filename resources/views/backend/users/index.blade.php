@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('common.users') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.id') }}</th>
                                <th>{{ trans('common.first_name') }}</th>
                                <th>{{ trans('common.last_name') }}</th>
                                <th>{{ trans('common.created') }}</th>
                                <th>{{ trans('common.updated') }}</th>
                                <th>{{ trans('common.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->last_name }}</td>
                                <td>{{ $user->created_at->diffForHumans() }}</td>
                                <td>{{ $user->updated_at->diffForHumans() }}</td>
                                <td>
                                    <div class="mini ui ferrari icon buttons">
                                        <div class="ui button"><i class="edit icon"></i></div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection