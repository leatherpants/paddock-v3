@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.edit_track') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @include('_partials.notifications')
                <form action="{{ route('backend.tracks.edit', ['slug' => $track->slug]) }}" method="post" class="ui form">
                    @csrf
                    <input type="hidden" name="id" value="{{ $track->id }}">
                    <div class="two fields">
                        <div class="field{{ $errors->has('country_id') ? ' error' : '' }}">
                            <label for="country_id">{{ trans('common.country') }}</label>
                            <select name="country_id" id="country_id">
                            @foreach($countries as $country)
                                <option value="{{ $country->code }}"{{ $country->code == $track->country_id ? ' selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                            <label for="name">{{ trans('common.name') }}</label>
                            <input type="text" name="name" id="name" placeholder="{{ trans('common.name') }}" value="{{ $track->name }}">
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="inline field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="active" id="active" tabindex="0" {{ ($track->active == true) ? 'checked="checked"' : '' }} value="{{ $track->active }}" class="hidden">
                                <label for="active">{{ trans('common.active') }}</label>
                            </div>
                        </div>
                        <div class="field{{ $errors->has('timezone') ? ' error' : '' }}">
                            <label for="timezone">{{ trans('common.timezone') }}</label>
                            <input type="text" name="timezone" id="timezone" placeholder="{{ trans('common.timezone') }}" value="{{ $track->timezone }}">
                        </div>
                    </div>
                    <button type="submit" class="ui ferrari icon labeled button">
                        <i class="save icon"></i>
                        {{ trans('common.save') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('.ui.checkbox')
        .checkbox()
    ;
</script>
@endsection