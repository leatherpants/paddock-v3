@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.tracks') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('backend.tracks.add') }}" class="ui ferrari right floated icon labeled button">
                    <i class="add icon"></i>
                    {{ trans('common.add_track') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui ferrari unstackable table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (!$tracks->count())
                        <tr>
                            <td colspan="2" class="center aligned">{{ trans('common.no_data_available') }}</td>
                        </tr>
                    @else
                        @foreach($tracks as $track)
                            <tr>
                                <td{{ ($track->active === 0) ? ' class=disabled' : '' }}>
                                    <i class="{{ $track->country_id }} flag"></i>
                                    {{ $track->name }}
                                </td>
                                <td>
                                    <div class="ui ferrari icon buttons">
                                        <a href="{{ route('backend.tracks.edit', ['slug' => $track->slug]) }}" class="ui button">
                                            <i class="edit icon"></i>
                                        </a>
                                        <a href="#" class="ui button">
                                            <i class="trash icon"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection