@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.add_grandprix') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @include('_partials.notifications')
                <form action="{{ route('backend.grandprixs.add') }}" method="post" class="ui form">
                    @csrf
                    <div class="two fields">
                        <div class="field{{ $errors->has('country_id') ? ' error' : '' }}">
                            <label for="country_id">{{ trans('common.country') }}</label>
                            <select name="country_id" id="country_id">
                            @foreach($countries as $country)
                                <option value="{{ $country->code }}">{{ $country->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                            <label for="name">{{ trans('common.name') }}</label>
                            <input type="text" name="name" id="name" placeholder="{{ trans('common.name') }}" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="field{{ $errors->has('full_name') ? ' error' : '' }}">
                        <label for="full_name">{{ trans('common.full_name') }}</label>
                        <input type="text" name="full_name" id="full_name" placeholder="{{ trans('common.full_name') }}" value="{{ old('full_name') }}">
                    </div>
                    <div class="two fields">
                        <div class="field{{ $errors->has('hashtag') ? ' error' : '' }}">
                            <label for="hashtag">{{ trans('common.hashtag') }}</label>
                            <div class="ui left icon input">
                                <i class="hashtag icon"></i>
                                <input type="text" name="hashtag" id="hashtag" placeholder="{{ trans('common.hashtag') }}" value="{{ old('hashtag') }}">
                            </div>
                            <p>
                                {{ trans('common.add_hashtag') }}
                            </p>
                        </div>
                        <div class="field{{ $errors->has('emoji') ? ' error' : '' }}">
                            <label for="emoji">{{ trans('common.emoji') }}</label>
                            <input type="text" name="emoji" id="emoji" placeholder="{{ trans('common.emoji') }}" value="{{ old('emoji') }}">
                        </div>
                    </div>
                    <div class="inline field">
                        <div class="ui checkbox">
                            <input type="checkbox" name="active" id="active" tabindex="0" class="hidden">
                            <label for="active">{{ trans('common.active') }}</label>
                        </div>
                    </div>
                    <button type="submit" class="ui ferrari icon labeled button">
                        <i class="save icon"></i>
                        {{ trans('common.save') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('.ui.checkbox')
        .checkbox()
    ;
</script>
@endsection