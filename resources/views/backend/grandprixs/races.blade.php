@extends('templates.backend')
@section('title', config('app.name'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="checkered flag icon"></i>
                        <span class="content">
                            {{ $grandprix->full_name }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui unstackable table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('common.season') }}</th>
                            <th>{{ trans('common.date') }}</th>
                            <th>{{ trans('common.track') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($races as $race)
                        <tr>
                            <td>{{ $race->id }}</td>
                            <td>{{ $race->season }}</td>
                            <td>{{ $race->raceday->format('d.m.Y') }}</td>
                            <td>{{ $race->track->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection