@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.grandprixs') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('backend.grandprixs.add') }}" class="ui ferrari right floated icon labeled button">
                    <i class="add icon"></i>
                    {{ trans('common.add_grandprix') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui ferrari unstackable table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.grandprix') }}</th>
                            <th>{{ trans('common.full_name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (!$grandprixs->count())
                        <tr>
                            <td colspan="2" class="center aligned">{{ trans('common.no_data_available') }}</td>
                        </tr>
                    @else
                        @foreach($grandprixs as $grandprix)
                            <tr>
                                <td{{ ($grandprix->active === 0) ? ' class=disabled' : '' }}>
                                    <i class="{{ $grandprix->country_id }} flag"></i>
                                    {{ $grandprix->name }}
                                </td>
                                <td{{ ($grandprix->active === 0) ? ' class=disabled' : '' }}>
                                    {{ $grandprix->full_name }}
                                </td>
                                <td>
                                    <div class="ui ferrari icon buttons">
                                        <a href="{{ route('backend.grandprixs.edit', ['slug' => $grandprix->slug]) }}" class="ui button">
                                            <i class="edit icon"></i>
                                        </a>
                                        <a href="#" class="ui button">
                                            <i class="trash icon"></i>
                                        </a>
                                        <a href="{{ route('backend.grandprixs.races', ['slug' => $grandprix->slug]) }}" class="ui button">
                                            <i class="checkered flag icon"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection