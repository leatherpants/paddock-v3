@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.countries') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('backend.countries.add') }}" class="ui ferrari right floated icon labeled button">
                    <i class="add icon"></i>
                    {{ trans('common.add_country') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui ferrari unstackable table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.country') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (!$countries->count())
                        <tr>
                            <td colspan="2" class="center aligned">{{ trans('common.no_data_available') }}</td>
                        </tr>
                    @else
                        @foreach($countries as $country)
                            <tr>
                                <td>
                                    <i class="{{ $country->code }} flag"></i>
                                    {{ $country->name }}
                                </td>
                                <td>
                                    <div class="ui ferrari icon buttons">
                                        <a href="{{ route('backend.countries.edit', ['code' => $country->code]) }}" class="ui button">
                                            <i class="edit icon"></i>
                                        </a>
                                        <a href="#" class="ui button">
                                            <i class="trash icon"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection