@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.add_country') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @include('_partials.notifications')
                <form action="{{ route('backend.countries.add') }}" method="post" class="ui form">
                    @csrf
                    <div class="two fields">
                        <div class="field{{ $errors->has('code') ? ' error' : '' }}">
                            <label for="code">{{ trans('common.code') }}</label>
                            <input type="text" name="code" id="code" placeholder="{{ trans('common.code') }}" value="{{ old('code') }}">
                        </div>
                        <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                            <label for="name">{{ trans('common.name') }}</label>
                            <input type="text" name="name" id="name" placeholder="{{ trans('common.name') }}" value="{{ old('name') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui ferrari icon labeled button">
                        <i class="save icon"></i>
                        {{ trans('common.save') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection