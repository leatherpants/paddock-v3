@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="twitter icon"></i>
                        <span class="content">
                            {{ trans('common.twitter') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui unstackable table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.id') }}</th>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr>
                            <td>{{ $account->twitter_id }}</td>
                            <td>{{ $account->screen_name }}</td>
                            <td>
                                <div class="ui twitter selection dropdown">
                                    <i class="twitter icon"></i>
                                    {{ trans('common.menu') }}
                                    <div class="menu">
                                        <a href="https://twitter.com/{{ $account->name }}" target="_blank" rel="noopener" title="{{ $account->screen_name }}" class="item">
                                            <i class="external icon"></i>
                                            {{ $account->screen_name }}
                                        </a>
                                        <a href="#" class="item">
                                            <i class="user icon"></i>
                                            {{ $account->screen_name }}
                                        </a>
                                        <a href="#" class="item">
                                            <i class="edit icon"></i>
                                            {{ trans('common.edit') }}
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('.ui.dropdown').dropdown();
    </script>
@endsection