@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.edit_season') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @include('_partials.notifications')
                <form action="{{ route('backend.seasons.edit', ['season' => $season->season]) }}" method="post" class="ui form">
                    @csrf
                    <div class="field{{ $errors->has('season') ? ' error' : '' }}">
                        <label for="season">{{ trans('common.season') }}</label>
                        <input type="text" name="season" id="season" placeholder="{{ trans('common.season') }}" value="{{ $season->season }}">
                    </div>
                    <button type="submit" class="ui ferrari icon labeled button">
                        <i class="save icon"></i>
                        {{ trans('common.save') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection