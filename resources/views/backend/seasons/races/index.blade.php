@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.season_races:season', ['season' => $season]) }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui ferrari unstackable table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.date') }}</th>
                                <th>{{ trans('common.grandprix') }}</th>
                                <th>{{ trans('common.track') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (!$races->count())
                            <tr>
                                <td colspan="3" class="center aligned">{{ trans('common.no_data_available') }}</td>
                            </tr>
                        @else
                            @foreach($races as $race)
                                <tr>
                                    <td>
                                        {{ $race->raceday->format('d M Y') }}
                                    </td>
                                    <td>
                                        {{ $race->grandprix->full_name }}
                                    </td>
                                    <td>
                                        {{ $race->track->name }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.seasons') }}" class="ui right floated red labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.seasons') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection