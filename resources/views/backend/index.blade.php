@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="four wide column">
            <div class="ui ferrari segment">
                <h3 class="ui dividing header">
                    <i class="chart bar icon"></i>
                    <span class="content">
                        {{ trans('common.statistics') }}
                    </span>
                </h3>
                <div class="ui divided list">
                    <div class="item">
                        <i class="car ferrari middle aligned icon"></i>
                        <div class="content">
                            <div class="header">{{ trans('common.cars') }}</div>
                            <div class="description">{{ $cars }}</div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="world ferrari middle aligned icon"></i>
                        <div class="content">
                            <div class="header">{{ trans('common.countries') }}</div>
                            <div class="description">{{ $countries }}</div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="car ferrari middle aligned icon"></i>
                        <div class="content">
                            <div class="header">{{ trans('common.grandprixs') }}</div>
                            <div class="description">{{ $grandprixs }}</div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="flag checkered ferrari middle aligned icon"></i>
                        <div class="content">
                            <div class="header">{{ trans('common.season_races') }}</div>
                            <div class="description">{{ $seasonRaces }}</div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="flag checkered ferrari middle aligned icon"></i>
                        <div class="content">
                            <div class="header">{{ trans('common.seasons') }}</div>
                            <div class="description">{{ $seasons }}</div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="users ferrari middle aligned icon"></i>
                        <div class="content">
                            <div class="header">{{ trans('common.staffs') }}</div>
                            <div class="description">{{ $staffs }}</div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="road ferrari middle aligned icon"></i>
                        <div class="content">
                            <div class="header">{{ trans('common.tracks') }}</div>
                            <div class="description">{{ $tracks }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection