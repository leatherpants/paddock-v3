@extends('templates.backend')
@section('title', config('app.name').' | '.trans('common.backend'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                        {{ trans('common.languages') }}
                    </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui ferrari unstackable table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.country') }}</th>
                            <th colspan="2">{{ trans('common.active') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $lang)
                        <tr>
                            <td>
                                <i class="{{ $lang->flag() }} flag"></i> {{ trans('languages.'.$lang->name) }}
                            </td>
                            <td>
                                @if ($lang->active == 1)
                                <i class="green check circle outline icon"></i>
                                @else
                                <i class="red times circle outline icon"></i>
                                @endif
                                <small>(Output)</small>
                            </td>
                            <td>
                                @if ($lang->news == 1)
                                    <i class="green check circle outline icon"></i>
                                @else
                                    <i class="red times circle outline icon"></i>
                                @endif
                                <small>(News)</small>
                            </td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection