@extends('templates.frontend')
@section('title', getTitle(trans('common.settings')))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.settings') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                <div class="ui ferrari image segment">
                    <img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->display_name }}" class="ui fluid image">
                </div>
            </div>
            <div class="twelve wide column">
                <div class="ui ferrari segment">
                    @include('_partials.notifications')
                    <form action="{{ route('settings') }}" method="post" class="ui form">
                        @csrf
                        <div class="field{{ $errors->has('lang') ? ' error' : '' }}">
                            <label for="lang">{{ trans('common.languages') }}</label>
                            <select name="lang" id="lang">
                            @foreach($languages as $language)
                                <option value="{{ $language->code }}"{{ ($language->code == Auth::user()->lang) ? ' selected="selected"' : '' }}>{{ trans('languages.'.$language->name) }}</option>
                            @endforeach
                            </select>
                        </div>
                        <button type="submit" class="ui ferrari labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
                <div class="ui ferrari segment">
                    <form action="#" class="ui form">
                        @csrf
                        <div class="field">
                            <div class="ui disabled labeled left icon input">
                                <i class="facebook icon"></i>
                                <input type="text" name="facebook_username" placeholder="Enter your facebook account">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui disabled labeled left icon input">
                                <i class="twitter icon"></i>
                                <input type="text" name="twitter_username" placeholder="Enter your twitter account">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui disabled labeled left icon input">
                                <i class="instagram icon"></i>
                                <input type="text" name="instagram_username" placeholder="Enter your instagram account">
                            </div>
                        </div>
                        <button type="submit" class="ui disabled ferrari labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
                <div class="ui ferrari clearing segment">
                    <a href="{{ route('settings.logoutOthers') }}" class="ui right floated ferrari labeled icon button">
                        <i class="sign out icon"></i>
                        {{ trans('common.logoutOthers') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection