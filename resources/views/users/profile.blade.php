@extends('templates.frontend')
@section('title', getTitle($user->display_name))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="three wide column">
                <div class="ui ferrari image segment">
                    <div class="ui fluid image">
                        <img src="{{ $user->avatar }}" alt="{{ $user->display_name }}">
                    </div>
                    <div class="ui ferrari bottom attached label" style="position: relative;">
                        {{ $user->display_name }}
                    </div>
                </div>
                <div class="ui ferrari segment">
                    <div class="ui vertical fluid buttons">
                    @if ($user->socials->facebook_username)
                        <a href="https://www.facebook.com/{{ $user->socials->facebook_username }}" target="_blank" class="ui facebook button">
                            <i class="facebook icon"></i>
                            Facebook
                        </a>
                    @endif

                    @if ($user->socials->twitter_username)
                        <a href="https://www.twitter.com/{{ $user->socials->twitter_username }}" target="_blank" class="ui twitter button">
                            <i class="twitter icon"></i>
                            Twitter
                        </a>
                    @endif

                    @if ($user->socials->instagram_username)
                        <a href="https://www.instagram.com/{{ $user->socials->instagram_username }}" target="_blank" class="ui instagram button">
                            <i class="instagram icon"></i>
                            Instagram
                        </a>
                    @endif
                    </div>
                </div>
            </div>
            <div class="thirteen wide column">
                <div class="ui ferrari teaser segment">
                    @if ($user->backgroundImage)
                        <div style="background-image: url({{ $user->backgroundImage->url }});background-position: 50% 50%;background-size: cover;width: 100%;height: 370px;"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection