@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="four wide column">
                @include('_partials.dashboard.user')
            </div>
            <div class="four wide column"></div>
            <div class="four wide column">
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        Test week 2
                    </h3>
                    <table class="ui table">
                    @foreach($results as $result)
                        <tr>
                            <td>
                                {{ $result->driver->first_name }}
                                <small>({{ $result->day }})</small>
                            </td>
                            <td>{{ $result->secintime() }}</td>
                            <td>{{ $result->laps }}</td>
                        </tr>
                    @endforeach
                    </table>
                    <a href="{{ route('tests.result', ['season' => 2018, 'week' => 2]) }}" class="ui ferrari mini labeled icon button">
                        <i class="right arrow icon"></i>
                        {{ trans('common.result') }}
                    </a>
                </div>
            </div>
            <div class="four wide column">
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        Test week 1
                    </h3>
                    <table class="ui table">
                        @foreach($olders as $result)
                            <tr>
                                <td>
                                    {{ $result->driver->first_name }}
                                    <small>({{ $result->day }})</small>
                                </td>
                                <td>{{ $result->secintime() }}</td>
                                <td>{{ $result->laps }}</td>
                            </tr>
                        @endforeach
                    </table>
                    <a href="{{ route('tests.result', ['season' => 2018, 'week' => 1]) }}" class="ui ferrari mini labeled icon button">
                        <i class="right arrow icon"></i>
                        {{ trans('common.result') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection