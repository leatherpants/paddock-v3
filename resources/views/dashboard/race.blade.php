@extends('templates.frontend')
@section('title', getTitle($race->grandprix->full_name))
@section('content')
    <div class="ui fluid main container">
        <div class="ui stackable grid">
            <div class="sixteen wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <h1 class="ui header">
                        {{ $race->grandprix->full_name }}
                        <span class="sub header">
                            <i class="{{ $race->grandprix->country_id }} flag"></i>
                            {{ $race->grandprix->name }}, {{ $race->track->name }} |
                            {{ $race->raceday->subDay(2)->format('d.m.Y') }} -
                            {{ $race->raceday->format('d.m.Y') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="six wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <strong>{{ trans('common.selected_sets') }}</strong>
                </div>
            </div>
            <div class="six wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <h3>{{ trans('common.power_unit') }}</h3>
                    <div class="ui horizontal very relaxed list">
                        <div class="item">
                            <strong>{{ trans('common.name') }}</strong>
                        </div>
                        <div class="item">
                            <strong>{{ trans('common.ice') }}</strong>
                        </div>
                        <div class="item">
                            <strong>{{ trans('common.tc') }}</strong>
                        </div>
                        <div class="item">
                            <strong>{{ trans('common.mgu-h') }}</strong>
                        </div>
                        <div class="item">
                            <strong>{{ trans('common.mgu-k') }}</strong>
                        </div>
                        <div class="item">
                            <strong>{{ trans('common.es') }}</strong>
                        </div>
                        <div class="item">
                            <strong>{{ trans('common.ce') }}</strong>
                        </div>
                        @foreach($pu as $item)
                            <div class="item">
                                {{ $item->driver->name }}
                            </div>
                            <div class="item">
                                {{ $item->ice }}
                            </div>
                            <div class="item">
                                {{ $item->tc }}
                            </div>
                            <div class="item">
                                {{ $item->mguh }}
                            </div>
                            <div class="item">
                                {{ $item->mguk }}
                            </div>
                            <div class="item">
                                {{ $item->es }}
                            </div>
                            <div class="item">
                                {{ $item->ce }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="two wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <h3 class="ui header">
                        <i class="clipboard list icon"></i>
                        <span class="content">
                            {{ trans('common.standings') }}
                        </span>
                    </h3>
                    <div class="ui list">
                        <div class="item">
                            <span style="display: inline-block; width: 75%;">{{ $standings['driver1']['name'] }}</span>
                            {{ $standings['driver1']['points'] }}
                        </div>
                        <div class="item">
                            <span style="display: inline-block; width: 75%;">{{ $standings['driver2']['name'] }}</span>
                            {{ $standings['driver2']['points'] }}
                        </div>
                        <div class="item">
                            <span style="display: inline-block; width: 75%;">{{ $standings['team']['name'] }}</span>
                            {{ $standings['team']['points'] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="two wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <h3 class="ui header">
                        <i class="clock icon"></i>
                        <span class="content">
                            {{ trans('common.timetable') }}
                        </span>
                    </h3>
                    <div class="ui list">
                        @foreach($sessions as $session)
                            <div class="item">
                                <div class="middle aligned content">
                                    <strong style="display: inline-block; width:30%;">{{ $session->start->timezone(Auth::user()->timezone)->formatLocalized('%a') }}</strong>
                                    {{ $session->start->timezone(Auth::user()->timezone)->formatLocalized('%H:%M') }} -
                                    {{ $session->end->timezone(Auth::user()->timezone)->formatLocalized('%H:%M') }}
                                    {!! $session->live() !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="four wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <h3 class="ui header">
                        <i class="stopwatch icon"></i>
                        <span class="content">
                            {{ trans('common.results') }}
                        </span>
                    </h3>
                    <div class="ui two column grid">
                        <div class="column">
                            <div class="ui list">
                                <div class="item">
                                    <div class="header">Sebastian</div>
                                </div>
                                @foreach($times1 as $time)
                                    <div class="item">
                                        {{ $time->session }} -
                                        {{ $time->secintime() }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="column">
                            <div class="ui list">
                                <div class="item">
                                    <div class="header">Kimi</div>
                                </div>
                                @foreach($times2 as $time)
                                    <div class="item">
                                        {{ $time->session }} -
                                        {{ $time->secintime() }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('seasons.raceLive', ['season' => $race->season, 'gp' => $race->grandprix->slug, 'session' => 1]) }}" class="ui ferrari mini fluid labeled icon button">
                        <i class="time stop icon"></i>
                        {{ trans('common.lap_times') }}
                    </a>
                </div>
            </div>
            <div class="eight wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <strong>{{ trans('common.statistics') }}</strong>
                </div>
            </div>
            <div class="four wide column">
                <div class="ui ferrari segment" style="height: 100%;">
                    <h3 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ trans('common.feed_ferrari') }}
                        </span>
                    </h3>
                    <div class="ui list">
                        @if ($feeds->isEmpty())
                            <div class="item">
                                No entries available
                            </div>
                        @else
                        @foreach($feeds as $feed)
                            <div class="item">
                                <a href="{{ route('feed.ferrari.entry', ['date' => $feed->created_at->format('Y-m-d'), 'session' => $feed->session]) }}" title="{{ $feed->title }}" class="header">{{ $feed->title }}</a>
                            </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection