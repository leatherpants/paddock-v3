@extends('templates.frontend')
@section('title', getTitle())
@section('content')
    <div class="ui fluid main container">
        <div class="ui stackable grid">
            <div class="four wide column">
                @include('_partials.dashboard.user')
            </div>
            <div class="eight wide column">
                @if (!is_null($race))
                @include('_partials.dashboard.race')
                @else
                @include('_partials.dashboard.no_race')
                @endif
            </div>
            <div class="four wide column">
                <latest-articles></latest-articles>
            </div>
            <div class="twelve wide column"></div>
            <div class="four wide column">
                <youtube></youtube>
            </div>
        </div>
    </div>
@endsection