@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="ticket icon"></i>
                        <span class="content">
                            {{ trans('common.events') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui unstackable table">
                    <thead>
                    <tr>
                        <th>{{ trans('common.date') }}</th>
                        <th>{{ trans('common.type') }}</th>
                        <th>{{ trans('common.name') }}</th>
                        <th>{{ trans('common.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $event)
                        <tr>
                            <td>{{ $event->date->format('d.m.Y') }}</td>
                            <td>{{ $event->type }}</td>
                            <td>{{ $event->driver->name }}</td>
                            <td>
                                <a href="{{ route('events.event', ['date' => $event->date->format('Y-m-d'), 'staff' => $event->staff_id]) }}" class="ui twitter icon button">
                                    <i class="retweet icon"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection