@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="ticket icon"></i>
                            <span class="content">
                            {{ trans('common.event') }}
                        </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <div class="ui middle aligned divided list">
                        @foreach($tweets as $tweet)
                            <div class="item">
                                <div class="content">
                                    <i class="{!! $tweet->getFlag() !!} flag"></i>
                                    {!! nl2br($tweet->event_text) !!}
                                    <a href="{{ $tweet->getTweetLink() }}" target="_blank" title="{{ $tweet->event_text }}"><i class="ferrari retweet icon"></i></a>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection