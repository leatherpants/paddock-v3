@extends('templates.frontend')
@section('title', getTitle(trans('common.activities')))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="edit icon"></i>
                        <span class="content">
                            {{ trans('common.activities') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <activities></activities>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection