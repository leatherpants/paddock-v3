@extends('templates.blank')
@section('content')
    <table>
    @foreach($gps as $gp)
        <tr>
            <td><a href="{{ route('sql.results', ['gp_id' => $gp->gp_id]) }}">{{ $gp->full_name }}</a></td>
        </tr>
    @endforeach
    </table>
@endsection