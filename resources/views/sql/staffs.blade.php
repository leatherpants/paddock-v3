@extends('templates.blank')
@section('content')
    <table>
        @foreach($staffs as $staff)
            <tr>
                <td>{{ $staff->id }}</td>
                <td>{{ $staff->name }}</td>
                <td>{{ $staff->slug }}</td>
            </tr>
        @endforeach
    </table>
@endsection