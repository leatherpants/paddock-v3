@extends('templates.frontend')
@section('title', getTitle(trans('common.socialmedia')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="four wide column">
            @include('_partials.dashboard.user')
        </div>
        @foreach($entries as $entry)
            <div class="four wide column">
                <div class="ui ferrari dashboard segment">
                    <div class="ui two column stackable grid">
                        <div class="column">
                            <img src="{{ $entry->avatar }}" alt="{{ $entry->name }}" class="ui image">
                        </div>
                        <div class="column">
                            <div class="ui vertical fluid buttons">
                                @if ($entry->website)
                                    <a href="{{ $entry->website }}" target="_blank" class="ui ferrari icon labeled button">
                                        <i class="world icon"></i>
                                        {{ trans('common.website') }}
                                    </a>
                                @endif

                                @if ($entry->facebook)
                                    <a href="{{ $entry->facebook }}" target="_blank" class="ui ferrari icon labeled button">
                                        <i class="facebook icon"></i>
                                        {{ trans('common.facebook') }}
                                    </a>
                                @endif

                                @if ($entry->twitter)
                                    <a href="{{ $entry->twitter }}" target="_blank" class="ui ferrari icon labeled button">
                                        <i class="twitter icon"></i>
                                        {{ trans('common.twitter') }}
                                    </a>
                                @endif

                                @if ($entry->instagram)
                                    <a href="{{ $entry->instagram }}" target="_blank" class="ui ferrari icon labeled button">
                                        <i class="instagram icon"></i>
                                        {{ trans('common.instagram') }}
                                    </a>
                                @endif

                                @if ($entry->youtube)
                                    <a href="{{ $entry->youtube }}" target="_blank" class="ui ferrari icon labeled button">
                                        <i class="youtube icon"></i>
                                        {{ trans('common.youtube') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection