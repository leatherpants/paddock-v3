@extends('templates.frontend')
@section('title', getTitle($race['full_name']))
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="ticket icon"></i>
                            <span class="content">
                            {{ $race['full_name'] }}
                        </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('results.gp', ['slug' => $race['slug']]) }}" class="ui ferrari right floated labeled icon button">
                        <i class="trophy icon"></i>
                        {{ trans('common.results') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        @include('seasons._partials.menu')
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection