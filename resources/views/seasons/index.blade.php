@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.season_year', ['season' => $season->season]) }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui table">
                    <tbody>
                    @foreach($races as $race)
                        <tr>
                            <td>{{ $race->raceday->format('d.m.Y') }}</td>
                            <td>
                                <i class="{{ $race->grandprix->country_id }} flag"></i>
                                {{ $race->grandprix->full_name }}
                            </td>
                            <td>
                                <a href="{{ route('seasons.race', ['season' => $season->season, 'gp' => $race->grandprix->slug]) }}" class="ui ferrari labeled icon button">
                                    <i class="right arrow icon"></i>
                                    {{ trans('common.grandprix') }}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection