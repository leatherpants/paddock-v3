@extends('templates.frontend')
@section('title', getTitle($race['full_name']))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="ticket icon"></i>
                        <span class="content">
                        {{ $race['full_name'] }}
                    </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('seasons.race', ['season' => $race['season'], 'slug' => $race['slug']]) }}" class="ui ferrari right floated labeled icon button">
                    <i class="left arrow icon"></i>
                    {{ trans('common.race') }}
                </a>
                <a href="{{ route('results.gp', ['slug' => $race['slug']]) }}" class="ui ferrari right floated labeled icon button">
                    <i class="trophy icon"></i>
                    {{ trans('common.results') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui divided list">
                    @foreach($files as $file)
                        <div class="item">
                            <i class="file {{ $file->file_type }} middle aligned icon"></i>
                            <div class="content">
                                <a href="{{ $file->file_name }}" target="_blank" class="header">{{ $file->name }}</a>
                                <div class="description">{{ $file->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection