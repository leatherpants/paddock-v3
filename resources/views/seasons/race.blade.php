@extends('templates.frontend')
@section('title', getTitle($race['full_name']))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="ticket icon"></i>
                        <span class="content">
                            {{ $race['full_name'] }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('results.gp', ['slug' => $race['slug']]) }}" class="ui ferrari right floated labeled icon button">
                    <i class="trophy icon"></i>
                    {{ trans('common.results') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui ferrari segment">
                    @include('seasons._partials.menu')
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.power_unit') }}</th>
                                <th class="center aligned">{{ trans('common.ice') }}</th>
                                <th class="center aligned">{{ trans('common.tc') }}</th>
                                <th class="center aligned">{{ trans('common.mgu-h') }}</th>
                                <th class="center aligned">{{ trans('common.mgu-k') }}</th>
                                <th class="center aligned">{{ trans('common.es') }}</th>
                                <th class="center aligned">{{ trans('common.ce') }}</th>
                                <th class="center aligned"><i class="info circle icon"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                        @if ($techs->count() == 0)
                            <tr>
                                <td colspan="8" class="center aligned">{{ trans('common.no_content') }}</td>
                            </tr>
                        @else
                        @foreach($techs as $tech)
                            <tr>
                                <td>{{ $tech->driver->name }}</td>
                                <td class="center aligned">{{ $tech->ice }}</td>
                                <td class="center aligned">{{ $tech->tc }}</td>
                                <td class="center aligned">{{ $tech->mguh }}</td>
                                <td class="center aligned">{{ $tech->mguk }}</td>
                                <td class="center aligned">{{ $tech->es }}</td>
                                <td class="center aligned">{{ $tech->ce }}</td>
                                <td class="center aligned">
                                </td>
                            </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.selected_sets') }}</th>
                                <th class="center aligned">Ultrasoft</th>
                                <th class="center aligned">Supersoft</th>
                                <th class="center aligned">Soft</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($tyres as $tyre)
                            <tr>
                                <td>{{ $tyre->driver->name }}</td>
                                @if ($tyre->superhard !== null)<td class="center aligned">{{ $tyre->superhard }}</td>@endif
                                @if ($tyre->hard !== null)<td class="center aligned">{{ $tyre->hard }}</td>@endif
                                @if ($tyre->medium !== null)<td class="center aligned">{{ $tyre->medium }}</td>@endif
                                @if ($tyre->soft !== null)<td class="center aligned">{{ $tyre->soft }}</td>@endif
                                @if ($tyre->supersoft !== null)<td class="center aligned">{{ $tyre->supersoft }}</td>@endif
                                @if ($tyre->ultrasoft !== null)<td class="center aligned">{{ $tyre->ultrasoft }}</td>@endif
                                @if ($tyre->hypersoft !== null)<td class="center aligned">{{ $tyre->hypersoft }}</td>@endif
                                <td class="center aligned">
                                    <a href="{{ route('seasons.reportTyres', ['season' => $tyre->season, 'gp' => $race['slug'], 'staff' => $tyre->staff_id]) }}" class="mini ui twitter labeled icon button">
                                        <i class="twitter icon"></i>
                                        {{ trans('common.twitter') }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="four wide column">
                <div class="ui ferrari segment">
                    <h4 class="ui dividing header">
                        <i class="file outline icon"></i>
                        <span class="content">
                            {{ trans('common.files') }}
                        </span>
                    </h4>
                    <div class="ui divided list">
                    @if ($files->count() == 0)
                        <div class="item">
                            <div class="content">
                                <div class="header">No files available</div>
                            </div>
                        </div>
                    @else
                    @foreach($files as $file)
                        <div class="item">
                            <i class="file {{ $file->file_type }} middle aligned icon"></i>
                            <div class="content">
                                <a href="{{ $file->file_name }}" target="_blank" class="header">{{ $file->name }}</a>
                                <div class="description">{{ $file->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                    @endforeach
                        <div class="item">
                            <div class="right floated content">
                                <a href="{{ route('seasons.files', ['season' => $race->season, 'gp' => $race['slug']]) }}" class="ui ferrari mini labeled icon button">
                                    <i class="file icon"></i>
                                    {{ trans('common.files') }}
                                </a>
                            </div>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection