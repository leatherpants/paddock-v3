@extends('templates.frontend')
@section('title', getTitle($race['full_name']))
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="ticket icon"></i>
                            <span class="content">
                            {{ $race['full_name'] }}
                        </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('results.gp', ['slug' => $race['slug']]) }}" class="ui ferrari right floated labeled icon button">
                        <i class="trophy icon"></i>
                        {{ trans('common.results') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        @include('seasons._partials.menu')
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <div class="ui five item secondary pointing menu">
                            <a href="{{ route('seasons.raceLive', ['season' => 2018, 'gp' => $race['slug'], 'session' => 1]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/live/1') ? 'active ' : '' }}item">
                                FP1
                            </a>
                            <a href="{{ route('seasons.raceLive', ['season' => 2018, 'gp' => $race['slug'], 'session' => 2]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/live/2') ? 'active ' : '' }}item">
                                FP2
                            </a>
                            <a href="{{ route('seasons.raceLive', ['season' => 2018, 'gp' => $race['slug'], 'session' => 3]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/live/3') ? 'active ' : '' }}item">
                                FP3
                            </a>
                            <a href="{{ route('seasons.raceLive', ['season' => 2018, 'gp' => $race['slug'], 'session' => 4]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/live/4') ? 'active ' : '' }}item">
                                Quali
                            </a>
                            <a href="{{ route('seasons.raceLive', ['season' => 2018, 'gp' => $race['slug'], 'session' => 5]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/live/5') ? 'active ' : '' }}item">
                                Race
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="four wide column">
                    <div class="ui ferrari segment">
                        <form action="#" method="post" class="ui form">
                            @csrf
                            <div class="field">
                                <label for="driver1">Sebastian Vettel</label>
                                <div class="ui left icon input">
                                    <i class="stopwatch icon"></i>
                                    <input type="text" id="driver1" name="time[1]" value="{{ $data['driver1']['this_year'] }}">
                                </div>
                            </div>
                            <div class="field">
                                <label for="driver2">Kimi Räikkönen</label>
                                <div class="ui left icon input">
                                    <i class="stopwatch icon"></i>
                                    <input type="text" id="driver2" name="time[2]" value="{{ $data['driver2']['this_year'] }}">
                                </div>
                            </div>
                            <div class="two ui buttons">
                                <button class="ui ferrari labeled icon disabled button">
                                    <i class="save icon"></i>
                                    {{ trans('common.save') }}
                                </button>
                                <a href="https://www.formula1.com/en/f1-live.html" target="_blank" class="ui ferrari labeled icon button">
                                    <i class="time icon"></i>
                                    Formula1.com
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="twelve wide column">
                    <div class="ui ferrari segment">
                        <table class="ui table">
                            <tbody>
                                <tr>
                                    <td class="top aligned">
                                        <h4 class="ui dividing header">{{ $data['driver1']['part'] }}</h4>
                                        <p>
                                            {{ $data['driver1']['string'] }}<br>
                                            2017<br>
                                            {{ $data['driver1']['last_year'] }}<br><br>
                                            2018<br>
                                            {{ $data['driver1']['this_year'] }} {{ $data['driver1']['gap'] }}<br>
                                            #ForzaFerrari
                                        </p>
                                    </td>
                                    <td class="top aligned">
                                        <h4 class="ui dividing header">{{ $data['driver2']['part'] }}</h4>
                                        <p>
                                            {{ $data['driver2']['string'] }}<br>
                                            2017<br>
                                            {{ $data['driver2']['last_year'] }}<br><br>
                                            2018<br>
                                            {{ $data['driver2']['this_year'] }} {{ $data['driver2']['gap'] }}<br>
                                            #ForzaFerrari
                                        </p>
                                    </td>
                                    <td class="top aligned">
                                        <h4 class="ui dividing header">{{ $data['drivers']['part'] }}</h4>
                                        <p>
                                            {{ $data['drivers']['string'] }}<br>
                                            Seb {{ $data['drivers']['time1'] }}<br>
                                            Kimi {{ $data['drivers']['time2'] }} {{ $data['drivers']['gap'] }}<br>
                                            #ForzaFerrari
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection