<div class="ui four item secondary pointing menu">
    <a href="{{ route('seasons.race', ['season' => 2018, 'gp' => $race['slug']]) }}" class="{{ Request::is('seasons/2018/'.$race['slug']) ? 'active ' : '' }}item">
        <i class="archive icon"></i>
        {{ trans('common.reports') }}
    </a>
    <a href="{{ route('seasons.raceLive', ['season' => 2018, 'gp' => $race['slug'], 'session' => 3]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/live/*') ? 'active ' : '' }}item">
        <i class="stopwatch icon"></i>
        {{ trans('common.live_timing') }}
        <div class="ui green mini label">LIVE</div>
    </a>
    <a href="{{ route('seasons.raceLaps', ['season' => 2018, 'gp' => $race['slug']]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/laps') ? 'active ' : '' }}item">
        <i class="clock icon"></i>
        {{ trans('common.lap_times') }}
    </a>
    <a href="{{ route('seasons.racePreview', ['season' => 2018, 'gp' => $race['slug']]) }}" class="{{ Request::is('seasons/2018/'.$race['slug'].'/preview') ? 'active ' : '' }}item">
        <i class="twitter icon"></i>
        {{ trans('common.preview') }}
    </a>
</div>