@extends('templates.frontend')
@section('title', getTitle($staff->name))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        {{ $staff->name }}
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                <div class="ui ferrari segment">
                    <div class="ui list">
                        <div class="item">
                            <div class="header">
                                {{ trans('common.dateofbirth') }}
                            </div>
                            <div class="description">
                                {{ $staff->dateofbirth->format('d.m.Y') }}
                            </div>
                        </div>
                        @if ($staff->dateofdeath !== null)
                        <div class="item">
                            <div class="header">
                                {{ trans('common.dateofdeath') }}
                            </div>
                            <div class="description">
                                {{ $staff->dateofdeath->format('d.m.Y') }}
                            </div>
                        </div>
                        @endif
                        <div class="item">
                            <div class="header">
                                {{ trans('common.nationality') }}
                            </div>
                            <div class="description">
                                <i class="{{ $staff->country_id }} flag"></i>
                            </div>
                        </div>
                        @if ($staff->hashtag)
                        <div class="item">
                            <div class="header">
                                {{ trans('common.hashtag') }}
                            </div>
                            <div class="description">
                                <a href="https://twitter.com/hashtag/{{ urldecode(substr($staff->hashtag, 1)) }}?src=hash" target="_blank">{{ $staff->hashtag }}</a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="ui ferrari segment">
                    <div class="ui list">
                        <div class="item">
                            <div class="header">
                                F1 Stats
                            </div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.wins') }}
                            </div>
                            <div class="description">{{ $wins }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.podiums') }}
                            </div>
                            <div class="description">{{ $podium }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.points') }}
                            </div>
                            <div class="description">{{ $points }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.polepositions') }}
                            </div>
                            <div class="description">{{ $poles }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.grandprix') }}
                            </div>
                            <div class="description">{{ $races }}</div>
                        </div>
                    </div>
                </div>
                <div class="ui ferrari segment">
                    <div class="ui list">
                        <div class="item">
                            <div class="header">
                                Ferrari Stats
                            </div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.wins') }}
                            </div>
                            <div class="description">{{ $twins }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.podiums') }}
                            </div>
                            <div class="description">{{ $tpodium }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.points') }}
                            </div>
                            <div class="description">{{ $tpoints }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.polepositions') }}
                            </div>
                            <div class="description">{{ $tpoles }}</div>
                        </div>
                        <div class="item">
                            <div class="header">
                                {{ trans('common.grandprix') }}
                            </div>
                            <div class="description">{{ $traces }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection