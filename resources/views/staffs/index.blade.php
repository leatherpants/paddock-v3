@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="users icon"></i>
                        <span class="content">
                        {{ trans('common.staffs') }}
                    </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="#" class="ui right floated red labeled icon button">
                    <i class="checkered flag icon"></i>
                    {{ trans('common.add_staff') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui unstackable table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($staffs as $staff)
                        <tr>
                            <td>
                                <i class="{{ $staff->country_id }} flag"></i>
                                {{ $staff->name }}
                            </td>
                            <td>
                                <div class="ui red buttons">
                                    <a href="" title="{{ $staff->name }}" class="ui icon button">
                                        <i class="edit icon"></i>
                                    </a>
                                    <a href="{{ route('staffs.view', ['slug' => $staff->slug]) }}" title="{{ $staff->name }}" class="ui icon button">
                                        <i class="unhide icon"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection