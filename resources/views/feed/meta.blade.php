@extends('templates.frontend')
@section('title', getTitle(trans('common.feed')))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <form action="{{ route('feed.meta') }}" method="post" class="ui form">
                        @csrf
                        <div class="field">
                            <div class="ui labeled input">
                                <div class="ui ferrari label">
                                    {{ trans('common.url') }}
                                </div>
                                <input type="url" name="url" id="url" placeholder="{{ trans('common.url') }}" value="{{ old('url') }}">
                            </div>
                        </div>
                        <button type="submit" class="ui ferrari labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
                @if (Session::has('tags'))
                <div class="ui ferrari segment">
                    <div class="ui list">
                        <div class="item">
                            <div class="content">
                                <div class="header">{{ trans('common.title') }}</div>
                                {{ Session::get('tags')['title'] }}
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">{{ trans('common.description') }}</div>
                                {{ Session::get('tags')['description'] }}
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">{{ trans('common.image') }}</div>
                                {{ Session::get('tags')['image'] }}
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">{{ trans('common.url') }}</div>
                                {{ Session::get('tags')['url'] }}
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection