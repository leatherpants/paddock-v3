@extends('templates.frontend')
@section('title', getTitle($entry->title))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="ten wide column">
                <div class="ui ferrari segment">
                    <img src="{{ $entry->image }}" alt="{{ $entry->title }}" class="ui fluid image">
                </div>
            </div>
            <div class="six wide column">
                <div class="ui ferrari clearing segment">
                    <div class="ui ferrari label">
                        <i class="time icon"></i>
                        {{ $entry->created_at->timezone(Auth::user()->timezone)->format('d.m.Y H:i') }}
                    </div>
                    @if ($entry->hashtags)
                    <div class="ui ferrari label">
                        {{ $entry->hashtags }}
                    </div>
                    @endif
                </div>
                <div class="ui ferrari clearing segment">
                    <div class="ui divided items">
                    @foreach($entries as $entry)
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    <i class="{!! $entry->getFlag() !!} flag"></i>
                                    {{ $entry->title }}
                                </div>
                                <div class="description">
                                    @if ($entry->description)
                                    <p>{{ $entry->description }}</p>
                                    @else
                                    <p>{{ trans('common.no_description') }}</p>
                                    @endif
                                </div>
                                @if ($entry->link)
                                <div class="extra">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($entry->link) }}" target="_blank" rel="noopener" title="{{ $entry->title }}">
                                        <div class="ui facebook mini labeled icon button">
                                            <i class="share icon"></i>
                                            {{ trans('common.share') }}
                                        </div>
                                    </a>
                                    <a href="{{ $entry->getTweetLink() }}" target="_blank" rel="noopener" title="{{ $entry->title }}">
                                        <div class="ui twitter mini labeled icon button">
                                            <i class="retweet icon"></i>
                                            {{ trans('common.tweet') }}
                                        </div>
                                    </a>
                                    <a href="{{ $entry->link }}" target="_blank" rel="noopener" title="{{ $entry->title }}">
                                        <div class="ui ferrari mini labeled icon button">
                                            <i class="info icon"></i>
                                            {{ trans('common.info') }}
                                        </div>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <a href="{{ route('feed.ferrari') }}" class="ui ferrari right floated labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.feed_ferrari') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection