@extends('templates.frontend')
@section('title', getTitle(trans('common.feed')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui unstackable five column grid">
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.ferrari') }}">
                                Ferrari
                            </a>
                        </div>
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.fda') }}">
                                FDA
                            </a>
                        </div>
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.history') }}">
                                History
                            </a>
                        </div>
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.motorsport') }}">
                                Motorsport
                            </a>
                        </div>
                        <div class="column">
                            <a href="{{ route('feed.meta') }}" class="ui ferrari right floated labeled icon button">
                                <i class="info circle icon"></i>
                                Meta
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui four stackable cards">
                    <div class="ui ferrari card">
                        <a href="{{ route('feed.ferrari') }}" class="image">
                            <img src="{{ $ferrari['image'] }}" alt="{{ $ferrari['title'] }}">
                            <div class="ui ferrari top right attached label">Ferrari</div>
                        </a>
                        <div class="content">
                            {{ $ferrari['title'] }}
                        </div>
                    </div>
                    <div class="ui ferrari card">
                        <a href="{{ route('feed.fda') }}" class="image">
                            <img src="{{ $fda['image'] }}" alt="{{ $fda['title'] }}">
                            <div class="ui ferrari top right attached label">FDA</div>
                        </a>
                        <div class="content">
                            {{ $fda['title'] }}
                        </div>
                    </div>
                    @if ($history !== null)
                    <div class="ui ferrari card">
                        <a href="{{ route('feed.history') }}" class="image">
                            <img src="{{ $history['image'] }}" alt="{{ $history['title'] }}">
                            <div class="ui ferrari top right attached label">History</div>
                        </a>
                        <div class="content">
                            {{ $history['title'] }}
                        </div>
                    </div>
                    @endif
                    <div class="ui ferrari card">
                        <a href="{{ route('feed.motorsport') }}" class="image">
                            <img src="{{ $motorsport['image'] }}" alt="{{ $motorsport['title'] }}" style="max-height: 255px;">{{--TODO:decimal number--}}
                            <div class="ui ferrari top right attached label">Motorsport</div>
                        </a>
                        <div class="content">
                            {{ $motorsport['title'] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection