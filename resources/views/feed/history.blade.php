@extends('templates.frontend')
@section('title', getTitle(trans('common.feed_history')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="history icon"></i>
                        <span class="content">
                            {{ trans('common.feed_history') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui four column grid">
                        <div class="three wide middle aligned center aligned column">
                            <div class="ui ferrari label">Info</div>
                        </div>
                        <div class="three wide middle aligned center aligned column">
                            <a href="http://formula1.ferrari.com/en/home-days-en/" rel="noopener" target="_blank">
                                <i class="england flag"></i> Version
                            </a>
                        </div>
                        <div class="three wide middle aligned center aligned column">
                            <a href="http://formula1.ferrari.com/it/home-days-it/" rel="noopener" target="_blank">
                                <i class="it flag"></i> Version
                            </a>
                        </div>
                        <div class="seven wide right aligned column">
                            <div class="ui ferrari floating labeled icon dropdown button">
                                <i class="history icon"></i>
                                <span class="text">{{ trans('common.feed') }}</span>
                                <div class="menu">
                                    <a href="{{ route('feed') }}" class="item">
                                        <i class="history icon"></i>
                                        Feed
                                    </a>
                                    <div class="divider"></div>
                                    <a href="{{ route('feed.fda') }}" class="{{ Request::is('feed/fda') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        FDA
                                    </a>
                                    <a href="{{ route('feed.ferrari') }}" class="{{ Request::is('feed/ferrari') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        Ferrari
                                    </a>
                                    <a href="{{ route('feed.history') }}" class="{{ Request::is('feed/history') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        History
                                    </a>
                                    <a href="{{ route('feed.motorsport') }}" class="{{ Request::is('feed/motorsport') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        Motorsport
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <feed-history :lang="'{!! (Auth::check()) ? Auth::user()->lang : '' !!}'"></feed-history>
    </div>
</div>
@endsection