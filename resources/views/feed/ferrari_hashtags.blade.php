@extends('templates.frontend')
@section('title', getTitle($hashtag))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="hashtag icon"></i>
                        <span class="content">
                            {{ $hashtag }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari clearing segment">
                    <a href="{{ route('feed.ferrari') }}" class="ui red icon labeled right floated button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.backtofeed') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @if (!$entries->count())
                <div class="ui ferrari segment">
                    <h1 class="ui center aligned icon header">
                        <i class="massive exclamation circle ferrari icon"></i>
                        {{ trans('common.no_content') }}
                    </h1>
                </div>
                @else
                <div class="ui four stackable cards">
                    @foreach($entries as $feed)
                        <div class="card">
                            <div class="image">
                                <img src="{{ $feed->image }}" alt="{{ $feed->image }}">
                                <div class="ui bottom attached ferrari label">{{ $feed->title }}</div>
                                <div class="ui top right attached ferrari label">{{ $feed->prepareSession() }}</div>
                            </div>
                            <div class="extra content">
                                <span style="vertical-align: middle">
                                    {{ $feed->created_at->format('d.m.Y') }}
                                </span>
                                <span class="right floated">
                                    <a href="{{ route('feed.ferrari.entry', ['date' => $feed->created_at->format('Y-m-d'), 'session' => $feed->session]) }}" title="{{ $feed->title }}" class="ui ferrari mini icon button">
                                        <i class="retweet icon"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection