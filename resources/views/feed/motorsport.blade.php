@extends('templates.frontend')
@section('title', getTitle(trans('common.feed_motorsport')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="history icon"></i>
                        <span class="content">
                        {{ trans('common.feed_motorsport') }}
                    </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui three column grid">
                        <div class="three wide middle aligned center aligned column">
                            <div class="ui ferrari label">Info</div>
                        </div>
                        <div class="seven wide middle aligned column">
                            <a href="https://www.motorsport.com/f1/" rel="noopener" title="Motorsport.com" target="_blank">
                                <i class="england flag"></i>
                            </a>
                            <a href="https://de.motorsport.com/f1/" rel="noopener" title="Motorsport.com" target="_blank">
                                <i class="de flag"></i>
                            </a>
                            <a href="https://it.motorsport.com/f1/" rel="noopener" title="Motorsport.com" target="_blank">
                                <i class="it flag"></i>
                            </a>
                            <i class="se flag"></i>
                            <a href="https://es.motorsport.com/f1/" rel="noopener" title="Motorsport.com" target="_blank">
                                <i class="es flag"></i>
                            </a>
                        </div>
                        <div class="six wide right aligned column">
                            <div class="ui ferrari floating labeled icon dropdown button">
                                <i class="history icon"></i>
                                <span class="text">{{ trans('common.feed') }}</span>
                                <div class="menu">
                                    <a href="{{ route('feed') }}" class="item">
                                        <i class="history icon"></i>
                                        Feed
                                    </a>
                                    <div class="divider"></div>
                                    <a href="{{ route('feed.fda') }}" class="{{ Request::is('feed/fda') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        FDA
                                    </a>
                                    <a href="{{ route('feed.ferrari') }}" class="{{ Request::is('feed/ferrari') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        Ferrari
                                    </a>
                                    <a href="{{ route('feed.history') }}" class="{{ Request::is('feed/history') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        History
                                    </a>
                                    <a href="{{ route('feed.motorsport') }}" class="{{ Request::is('feed/motorsport') ? 'active ' : '' }}item">
                                        <i class="history icon"></i>
                                        Motorsport
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <feed-motorsport :lang="'{!! (Auth::check()) ? Auth::user()->lang : '' !!}'"></feed-motorsport>
    </div>
</div>
@endsection