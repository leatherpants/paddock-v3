@extends('templates.frontend')
@section('title', getTitle($entry->title))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="ten wide column">
                <div class="ui ferrari segment">
                    <img src="{{ $entry->image }}" alt="{{ $entry->title }}" class="ui fluid image">
                </div>
            </div>
            <div class="six wide column">
                <div class="ui ferrari clearing segment">
                    <div class="ui ferrari label">
                        <i class="time icon"></i>
                        {{ $entry->date->timezone(Auth::user()->timezone)->format('d.m.Y H:i') }}
                    </div>
                </div>
                <div class="ui ferrari clearing segment">
                    <div class="ui divided items">
                    @foreach($entries as $entry)
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    <i class="{!! $entry->getFlag() !!} flag"></i>
                                    {{ $entry->title }}
                                </div>
                                <div class="description">
                                    @if ($entry->description)
                                        <p>{{ $entry->description }}</p>
                                    @else
                                        <p>{{ trans('common.no_description') }}</p>
                                    @endif
                                </div>
                                <div class="extra">
                                    <a href="{{ $entry->getTweetLink() }}" target="_blank" rel="noopener" title="{{ $entry->event_text }}">
                                        <div class="ui twitter mini labeled icon button">
                                            <i class="retweet icon"></i>
                                            {{ trans('common.tweet') }}
                                        </div>
                                    </a>
                                    <a href="{{ $entry->link }}" target="_blank" rel="noopener" title="{{ $entry->title }}">
                                        <div class="ui ferrari icon mini button">
                                            <i class="info icon"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <a href="{{ route('feed.history') }}" class="ui ferrari right floated labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.feed_history') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection