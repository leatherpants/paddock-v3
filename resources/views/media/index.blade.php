@extends('templates.frontend')
@section('title', getTitle(trans('common.media')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="images icon"></i>
                        <span class="content">
                            {{ trans('common.media') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari clearing segment">
                    <h1 class="ui header">
                        <i class="images ferrari icon"></i>
                        <span class="content">
                            {{ trans('common.galleries') }}
                        </span>
                    </h1>
                    <div class="ui four stackable cards">
                        @foreach($galleries as $gallery)
                        <div class="card">
                            <a href="{{ route('media.gallery.images', ['id' => $gallery->album_id]) }}" class="image">
                                <img src="{{ $gallery->url }}" alt="Teaser">
                                <div class="ui bottom attached ferrari label">{{ $gallery->name }}</div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    <a href="{{ route('media.gallery.galleries') }}" class="ui ferrari right floated icon labeled button" style="margin-top: 1em">
                        <i class="images icon"></i>
                        {{ trans('common.all_galleries') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari clearing segment">
                    <h1 class="ui header">
                        <i class="youtube ferrari icon"></i>
                        <span class="content">
                            {{ trans('common.youtube') }}
                        </span>
                    </h1>
                    <div class="ui four stackable cards">
                    @foreach($videos as $video)
                        <div class="card">
                            <a href="{{ route('media.youtube.video', ['youtube_id' => $video->youtube_id]) }}" class="image">
                                <img src="https://img.youtube.com/vi/{{ $video->youtube_id }}/0.jpg" alt="thumbnail">
                                <div class="ui bottom attached ferrari label">{{ $video->titleShortner() }}</div>
                            </a>
                        </div>
                    @endforeach
                    </div>
                    <a href="{{ route('media.youtube.videos') }}" class="ui ferrari right floated icon labeled button" style="margin-top: 1em">
                        <i class="youtube icon"></i>
                        {{ trans('common.all_videos') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection