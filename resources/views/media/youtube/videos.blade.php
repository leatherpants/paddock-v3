@extends('templates.frontend')
@section('title', getTitle(trans('common.youtube')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="youtube ferrari icon"></i>
                        <span class="content">
                            {{ trans('common.videos') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari clearing segment">
                    <a href="{{ route('media') }}" class="ui ferrari labeled icon right floated button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.media') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui four stackable cards">
                    @foreach($videos as $video)
                        <div class="card">
                            <a href="{{ route('media.youtube.video', ['youtube_id' => $video->youtube_id]) }}" class="image">
                                <img src="https://img.youtube.com/vi/{{ $video->youtube_id }}/0.jpg">
                            </a>
                            <div class="content">
                                {{ $video->titleShortner() }}
                                <div class="meta">
                                    <div class="ui ferrari mini label">
                                        <i class="time icon"></i> {{ $video->created_at->diffForHumans() }}
                                    </div>
                                    @if ($video->youtubeHashtags)
                                    @foreach($video->youtubeHashtags as $tag)
                                        <div class="ui ferrari mini label">#{{ $tag->hashtag->title }}</div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection