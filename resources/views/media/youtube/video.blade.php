@extends('templates.frontend')
@section('title', getTitle($video->title))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="ten wide column">
                <div class="ui ferrari clearing segment">
                    <div style="position: relative;padding-bottom: 56.25%;padding-top: 25px;height: 0;">
                        <div class="ui embed" data-source="youtube" data-id="{{ $video->youtube_id }}"></div>
                    </div>
                    <div style="position: relative;margin: 1em 0;">
                        <p>{!! $video->description !!}</p>
                    </div>
                    <div style="position: relative;margin-top: 20px">
                        <a href="https://www.youtube.com/watch?v={{ $video->youtube_id }}" target="_blank" rel="noopener" class="ui ferrari labeled icon button">
                            <i class="youtube icon"></i>
                            Open on youtube
                        </a>
                        <a href="{{ route('media.youtube.videos') }}" class="ui ferrari right floated labeled icon button">
                            <i class="left arrow icon"></i>
                            {{ trans('common.backtovideos') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="six wide column">
                <div class="ui ferrari segment">
                    <div class="ui ferrari label">{{ trans('common.hashtags') }}</div>
                    @foreach($video->youtubeHashtags as $hashtag)
                        <div class="ui ferrari label">
                        {{ $hashtag->hashtag->title }}
                        </div>
                    @endforeach
                </div>
                <div class="ui ferrari segment">
                    <div class="ui ferrari label">
                        <i class="time icon"></i>
                        {{ $video->created_at->diffForHumans() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
$('.ui.embed').embed();
@endsection