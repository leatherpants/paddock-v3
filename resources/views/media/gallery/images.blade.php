@extends('templates.frontend')
@section('title', getTitle($album->name))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="images ferrari icon"></i>
                        <span class="content">
                            {{ $album->name }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari clearing segment">
                    <a href="{{ route('media.gallery.galleries') }}" class="ui ferrari right floated labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.backtogallery') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui four stackable cards">
                        @foreach($images as $image)
                            <div class="card">
                                <div class="image">
                                    <img src="{{ $image->url }}" alt="Teaser">
                                </div>
                                @if (auth()->user()->background_image != $image->id)
                                <a href="{{ route('media.gallery.set', ['image_id' => $image->id]) }}" class="ui ferrari bottom attached button">
                                    <i class="image icon"></i>
                                    {{ trans('common.set_background_image') }}
                                </a>
                                @else
                                <button class="disabled ui ferrari bottom attached button">
                                    <i class="image icon"></i>
                                    {{ trans('common.set_background_image') }}
                                </button>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection