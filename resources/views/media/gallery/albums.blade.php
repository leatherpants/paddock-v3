@extends('templates.frontend')
@section('title', getTitle(trans('common.galleries')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="images ferrari icon"></i>
                        <span class="content">
                            {{ trans('common.galleries') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari clearing segment">
                    <a href="{{ route('media') }}" class="ui ferrari right floated labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.media') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui four stackable cards">
                        @foreach($galleries as $gallery)
                        <div class="card">
                            <a href="{{ route('media.gallery.images', ['album_id' => $gallery->album_id]) }}" class="image">
                                <img src="{{ $gallery->url }}" alt="Teaser">
                                <div class="ui ferrari bottom attached label">{{ $gallery->name }}</div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection