@extends('templates.blank')
@section('title', getTitle())
@section('content')
@include('auth.login')
@endsection
@section('style')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
    </style>
@endsection