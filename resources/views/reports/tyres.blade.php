@extends('templates.frontend')
@section('title', getTitle($race['full_name']))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="ticket icon"></i>
                        <span class="content">
                            {{ $race['full_name'] }}
                            <span class="sub header">Selected sets for {{ $tyres['driver']['name'] }}</span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('seasons.race', ['season' => $tyres['season'], 'gp' => $race['slug']]) }}" class="ui ferrari right floated labeled icon button">
                    <i class="left arrow icon"></i>
                    {{ $race['name'] }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui horizontal segments">
                    <div class="ui ferrari segment">
                        <p>
                            <i class="england flag"></i> Selected sets for {{ $tyres['driver']['hashtag'] }}<br>
                            {!! (is_null($tyres)) ? '' : $tyres->selectedSets() !!}
                        </p>
                        <a href="https://twitter.com/intent/tweet?text={!! (is_null($tyres)) ? '' : $tyres->twitterString('en') !!}" target="_blank" class="ui twitter mini labeled icon button">
                            <i class="share icon"></i>
                            {{ trans('common.share') }}
                        </a>
                    </div>
                    <div class="ui ferrari segment">
                        <p>
                            <i class="de flag"></i> Ausgewählte Sätze für {{ $tyres['driver']['hashtag'] }}<br>
                            {!! (is_null($tyres)) ? '' : $tyres->selectedSets() !!}
                        </p>
                        <a href="https://twitter.com/intent/tweet?text={!! (is_null($tyres)) ? '' : $tyres->twitterString('de') !!}" target="_blank" class="ui twitter mini labeled icon button">
                            <i class="share icon"></i>
                            {{ trans('common.share') }}
                        </a>
                    </div>
                    <div class="ui ferrari segment">
                        <p>
                            <i class="it flag"></i> Set selezionati per {{ $tyres['driver']['hashtag'] }}<br>
                            {!! (is_null($tyres)) ? '' : $tyres->selectedSets() !!}
                        </p>
                        <a href="https://twitter.com/intent/tweet?text={!! (is_null($tyres)) ? '' : $tyres->twitterString('it') !!}" target="_blank" class="ui twitter mini labeled icon button">
                            <i class="share icon"></i>
                            {{ trans('common.share') }}
                        </a>
                    </div>
                    <div class="ui ferrari segment">
                        <p>
                            <i class="se flag"></i> Valda uppsättningar för {{ $tyres['driver']['hashtag'] }}<br>
                            {!! (is_null($tyres)) ? '' : $tyres->selectedSets() !!}
                        </p>
                        <a href="https://twitter.com/intent/tweet?text={!! (is_null($tyres)) ? '' : $tyres->twitterString('se') !!}" target="_blank" class="ui twitter mini labeled icon button">
                            <i class="share icon"></i>
                            {{ trans('common.share') }}
                        </a>
                    </div>
                    <div class="ui ferrari segment">
                        <p>
                            <i class="es flag"></i> Conjuntos seleccionados para {{ $tyres['driver']['hashtag'] }}<br>
                            {!! (is_null($tyres)) ? '' : $tyres->selectedSets() !!}
                        </p>
                        <a href="https://twitter.com/intent/tweet?text={!! (is_null($tyres)) ? '' : $tyres->twitterString('es') !!}" target="_blank" class="ui twitter mini labeled icon button">
                            <i class="share icon"></i>
                            {{ trans('common.share') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection