<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ getAppName() }}</title>
    <meta name="description" content="{{ getAppName() }}">
    <meta name="theme-color" content="#c30000">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <link rel="icon" href="{{ asset('favicon2.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('favicon2.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .main.container {
            margin-top : 7em;
            padding    : 0 1em;
        }
        @yield('style')
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <a href="{{ route('backend') }}" class="header item">
        <img class="logo" alt="logo" src="{{ asset('assets/images/logo.png') }}">
    </a>
    <a href="{{ route('backend.countries') }}" class="item{{ (Request::is('backend/countries') || Request::is('backend/countries/*')) ? ' active' : '' }}">{{ trans('common.countries') }}</a>
    <a href="{{ route('backend.languages') }}" class="item{{ (Request::is('backend/languages') || Request::is('backend/languages/*')) ? ' active' : '' }}">{{ trans('common.languages') }}</a>
    <a href="{{ route('backend.seasons') }}" class="item{{ (Request::is('backend/seasons') || Request::is('backend/seasons/*')) ? ' active' : '' }}">{{ trans('common.seasons') }}</a>
    <a href="{{ route('backend.grandprixs') }}" class="item{{ (Request::is('backend/grandprixs') || Request::is('backend/grandprixs/*')) ? ' active' : '' }}">{{ trans('common.grandprixs') }}</a>
    <a href="{{ route('backend.tracks') }}" class="item{{ Request::is('backend/tracks') || Request::is('backend/tracks/*') ? ' active' : '' }}">{{ trans('common.tracks') }}</a>
    <a href="{{ route('backend.hashtags') }}" class="item{{ Request::is('backend/hashtags') || Request::is('backend/hashtags/*') ? ' active' : '' }}">{{ trans('common.hashtags') }}</a>
    <a href="{{ route('backend.socialmedias') }}" class="item{{ Request::is('backend/socialmedias') || Request::is('backend/socialmedias/*') ? ' active' : '' }}">{{ trans('common.socialmedia') }}</a>
    <a href="{{ route('backend.users') }}" class="item{{ Request::is('backend/users') || Request::is('backend/users/*') ? ' active' : '' }}">{{ trans('common.users') }}</a>
    <a href="{{ route('backend.twitter') }}" class="item{{ Request::is('backend/twitter') || Request::is('backend/twitter/*') ? ' active' : '' }}">{{ trans('common.twitter') }}</a>
    <div class="right menu">
        <div class="ui simple dropdown item">
            <i class="code branch icon"></i>
            <i class="dropdown icon"></i>
            <div class="menu">
                <a href="{{ route('dashboard') }}" class="item">{{ trans('common.frontend') }}</a>
                <a href="/sql" target="blank" class="item">{{ trans('common.sql') }}</a>
            </div>
        </div>
        <a href="{{ route('logout') }}" class="item">{{ trans('common.logout') }}</a>
    </div>
</div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
@yield('scripts')
</body>
</html>