<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ getAppName() }}</title>
    <meta name="theme-color" content="#c30000">
    <meta name="description" content="{{ getAppName() }}">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <link rel="icon" href="{{ asset('favicon2.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('favicon2.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
@yield('style')
</head>
<body>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
</body>
</html>