<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta name="description" content="{{ getAppName() }}">
    <meta name="theme-color" content="#c30000">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <link rel="icon" href="{{ asset('favicon2.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('favicon2.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .main.container {
            margin-top: 7em;
            padding-left: 1em !important;
            padding-right: 1em !important;
        }

        .fixed.inverted.menu .toc.item {
            display: none;
        }

        @media only screen and (max-width: 700px) {
            .fixed.inverted.menu .item,
            .fixed.inverted.menu .menu {
                display: none;
            }
            .fixed.inverted.menu .toc.item {
                display: block;
            }
        }
        @yield('style')
    </style>
</head>
<body>
<div id="app">
    <div class="pusher">
        <div class="ui left vertical inverted menu sidebar">
            <a href="{{ route('dashboard') }}" class="item">Home</a>
            <a href="{{ route('media') }}" class="item">{{ trans('common.media') }}</a>
            <a href="{{ route('events') }}" class="item">{{ trans('common.events') }}</a>
            <a href="{{ route('feed') }}" class="item">{{ trans('common.feed') }}</a>
            <a href="{{ route('staffs') }}" class="item">{{ trans('common.staffs') }}</a>
            <a href="{{ route('cars') }}" class="item">{{ trans('common.cars') }}</a>
            <a href="{{ route('seasons') }}" class="item">{{ trans('common.seasons') }}</a>
            <a href="{{ route('results') }}" class="item">{{ trans('common.results') }}</a>
        </div>
        <div class="ui fixed inverted menu">
            <a class="toc icon item">
                <i class="sidebar icon"></i>
            </a>
            <a href="{{ route('dashboard') }}" class="header item">
                <img class="logo" alt="logo" src="{{ asset('assets/images/logo.png') }}">
            </a>
            <a href="{{ route('media') }}" class="item">{{ trans('common.media') }}</a>
            <a href="{{ route('events') }}" class="item">{{ trans('common.events') }}</a>
            <a href="{{ route('feed') }}" class="item">{{ trans('common.feed') }}</a>
            <a href="{{ route('staffs') }}" class="item">{{ trans('common.staffs') }}</a>
            <a href="{{ route('cars') }}" class="item">{{ trans('common.cars') }}</a>
            <a href="{{ route('seasons') }}" class="item">{{ trans('common.seasons') }}</a>
            <a href="{{ route('results') }}" class="item">{{ trans('common.results') }}</a>
            <div class="right menu">
                <a href="{{ ferrariLink() }}" target="_blank" rel="noopener" title="Scuderia Ferrari" class="icon item"><i class="external icon"></i></a>
                <a href="{{ route('backend') }}" class="item">{{ trans('common.backend') }}</a>
                <a href="{{ route('logout') }}" class="item">{{ trans('common.logout') }}</a>
            </div>
        </div>
        @yield('content')
    </div>
</div>
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
<script src="{{ mix('assets/app.js') }}"></script>
<script>
    $(document)
        .ready(function() {
            $('.ui.dropdown')
                .dropdown()
            ;

            $('.ui.sidebar')
                .sidebar({
                    context: '#app'
                })
                .sidebar('attach events', '.toc.item')
            ;
            @yield('scripts')
        })
    ;
</script>
</body>
</html>