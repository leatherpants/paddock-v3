@extends('templates.frontend')
@section('title', getTitle(trans('common.seasons')))
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="checkered flag icon"></i>
                            <span class="content">
                                {{ trans('common.seasons') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui ten stackable cards">
                    @foreach($seasons as $season)
                        <div class="card">
                            <div class="center aligned content">
                                <a href="{{ route('results.season', ['season' => $season->season]) }}" title="{{ trans('common.season:year', ['year' => $season->season]) }}{{--TODO:translation--}}">
                                    {{ $season->season }}
                                </a>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('results') }}" class="ui right floated red labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.results') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection