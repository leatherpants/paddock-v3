@extends('templates.frontend')
@section('title', getTitle(trans('common.results')))
@section('content')
<div class="ui main container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="trophy icon"></i>
                        <span class="content">
                            {{ $event->season }} {{ $event->full_name }}
                            <span class="sub header">{{ trans('common.result') }}</span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <h1 class="ui dividing header">
                    {{ trans('common.qualifying') }}
                </h1>
                <table class="ui unstackable table">
                    <thead>
                    <tr>
                        <th class="two wide">{{ trans('common.date') }}</th>
                        <th class="one wide">{{ trans('common.session') }}</th>
                        <th class="one wide">{{ trans('common.place') }}</th>
                        <th class="four wide">{{ trans('common.full_name') }}</th>
                        <th class="two wide" colspan="2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($qualiresults as $result)
                        @if ($result->team == 1)
                        <tr class="negative">
                            <td>{{ $result->dateofresult->format('d M Y') }}</td>
                            <td>{{ $result->session() }}</td>
                            <td>{{ $result->place }}</td>
                            <td>{{ $result->driver->name }}</td>
                            <td colspan="2"></td>
                        </tr>
                        @else
                        <tr>
                            <td>{{ $result->dateofresult->format('d M Y') }}</td>
                            <td>{{ $result->session() }}</td>
                            <td>{{ $result->place }}</td>
                            <td>{{ $result->driver->name }}</td>
                            <td colspan="2"></td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <h1 class="ui dividing header">
                    {{ trans('common.race') }}
                </h1>
                <table class="ui unstackable table">
                    <thead>
                        <tr>
                            <th class="two wide">{{ trans('common.date') }}</th>
                            <th class="one wide">{{ trans('common.session') }}</th>
                            <th class="one wide">{{ trans('common.place') }}</th>
                            <th class="four wide">{{ trans('common.full_name') }}</th>
                            <th class="center aligned one wide">{{ trans('common.laps') }}</th>
                            <th class="center aligned one wide">{{ trans('common.points') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($raceresults as $result)
                        @if ($result->team == 1)
                        <tr class="negative">
                            <td>{{ $result->dateofresult->format('d M Y') }}</td>
                            <td>{{ $result->session() }}</td>
                            <td>{{ $result->place }}</td>
                            <td>{{ $result->driver->name }}</td>
                            @if ($result->dnf == 1)
                                <td class="center aligned">{{ $result->laps }}</td>
                                <td class="center aligned">DNF</td>
                            @elseif ($result->dns == 0)
                                <td class="center aligned">{{ $result->laps }}</td>
                                <td class="center aligned">{{ $result->points }}</td>
                            @else
                                <td class="center aligned" colspan="2">DNS</td>
                            @endif
                        </tr>
                        @else
                        <tr>
                            <td>{{ $result->dateofresult->format('d M Y') }}</td>
                            <td>{{ $result->session() }}</td>
                            <td>{{ $result->place }}</td>
                            <td>{{ $result->driver->name }}</td>
                            @if ($result->dnf == 1)
                                <td class="center aligned">{{ $result->laps }}</td>
                                <td class="center aligned">DNF</td>
                            @elseif ($result->dns == 0)
                                <td class="center aligned">{{ $result->laps }}</td>
                                <td class="center aligned">{{ $result->points }}</td>
                            @else
                                <td class="center aligned" colspan="2">DNS</td>
                            @endif
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('results.season', ['season' => $season]) }}" class="ui red right floated labeled icon button">
                    <i class="left arrow icon"></i>
                    {{ trans('common.season') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection