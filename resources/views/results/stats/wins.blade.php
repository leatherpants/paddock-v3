@extends('templates.frontend')
@section('title', getTitle(trans('common.results')))
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="trophy icon"></i>
                            <span class="content">
                                {{ trans('common.results') }}
                                <span class="sub header">{{ trans('common.wins') }}</span>
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui unstackable table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('common.season') }}</th>
                                <th>{{ trans('common.grandprix') }}</th>
                                <th>{{ trans('common.track') }}</th>
                                <th>{{ trans('common.name') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($wins->reverse() as $w => $win)
                            <tr>
                                <td>{{ ++$w }}</td>
                                <td>{{ $win->season }}</td>
                                <td>{{ $win->grandprix->full_name }}</td>
                                <td>{{ $win->track->name }}</td>
                                <td>{{ $win->driver->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection