@extends('templates.frontend')
@section('title', getTitle(trans('common.results')))
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="trophy icon"></i>
                            <span class="content">
                                {{ trans('common.statistics') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="center aligned">{{ trans('common.wins') }}</th>
                                <th class="center aligned">{{ trans('common.polepositions') }}</th>
                                <th class="center aligned">{{ trans('common.fastestlaps') }}</th>
                                <th class="center aligned">{{ trans('common.podiums') }}</th>
                                <th class="center aligned">{{ trans('common.points') }}</th>
                                <th class="center aligned">{{ trans('common.laps') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($results as $result)
                            <tr>
                                <td>
                                    {{ $result['grandprix'] }}
                                </td>
                                <td class="center aligned">
                                    {{ $result['gp_wins'] }}
                                </td>
                                <td class="center aligned">
                                    {{ $result['gp_poles'] }}
                                </td>
                                <td class="center aligned">
                                    0
                                </td>
                                <td class="center aligned">
                                    {{ $result['gp_podiums'] }}
                                </td>
                                <td class="center aligned">
                                    {{ $result['gp_points'] }}
                                </td>
                                <td class="center aligned">
                                    {{ $result['gp_laps'] }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th class="center aligned">{{ $resultStats['wins'] }}</th>
                                <th class="center aligned">{{ $resultStats['poles'] }}</th>
                                <th class="center aligned">0</th>
                                <th class="center aligned">{{ $resultStats['podiums'] }}</th>
                                <th class="center aligned">{{ $resultStats['points'] }}</th>
                                <th class="center aligned">{{ $resultStats['laps'] }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection