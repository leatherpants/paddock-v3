@extends('templates.frontend')
@section('title', getTitle($season.' '.trans('common.season')))
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="world icon"></i>
                            <span class="content">
                                {{ trans('common.season_races:season', ['season' => $season]) }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <a href="{{ route('results.seasons') }}" class="ui ferrari right floated labeled icon button">
                            <i class="left arrow icon"></i>
                            {{ trans('common.seasons') }}
                        </a>
                        <div class="ui relaxed horizontal list">
                            <div class="item">
                                <div class="content">
                                    <div class="ui ferrari label">{{ $season }}</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    Wins <div class="ui ferrari label">{{ $stats['wins'] }}</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    Poles <div class="ui ferrari label">{{ $stats['poles'] }}</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    Fastest laps <div class="ui ferrari label">0</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    Podiums <div class="ui ferrari label">{{ $stats['podiums'] }}</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    Points <div class="ui ferrari label">{{ $stats['points'] }}</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    Laps <div class="ui ferrari label">{{ $stats['laps'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui unstackable table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.date') }}</th>
                                <th>{{ trans('common.grandprix') }}</th>
                                <th>{{ trans('common.track') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (!$races->count())
                            <tr>
                                <td colspan="3" class="center aligned">{{ trans('common.no_data_available') }}</td>
                            </tr>
                        @else
                            @foreach($races as $race)
                                <tr>
                                    <td>
                                        {{ $race->raceday->format('d M Y') }}
                                    </td>
                                    <td>
                                        {{ $race->grandprix->full_name }}
                                    </td>
                                    <td>
                                        {{ $race->track->name }}
                                    </td>
                                    <td>
                                        <a href="{{ route('results.result', ['season' => $race->season, 'gp' => $race->grandprix->slug]) }}" title="{{ trans('common.grandprix:season', ['season' => $race->season, 'grandprix' => $race->grandprix->full_name]) }}{{--TODO:translation--}}">
                                            <i class="unhide icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('results.seasons') }}" class="ui right floated red labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.seasons') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection