@extends('templates.frontend')
@section('title', getTitle(trans('common.results')))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="trophy icon"></i>
                        <span class="content">
                            {{ trans('common.results') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <a href="{{ route('results.seasons') }}" class="ui right floated ferrari labeled icon button">
                    <i class="checkered flag icon"></i>
                    {{ trans('common.seasons') }}
                </a>
                <a href="{{ route('results.stats') }}" class="ui right floated ferrari labeled icon button">
                    <i class="trophy icon"></i>
                    {{ trans('common.statistics') }}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="eight wide column">
                <season-results :season="'{!! $season !!}'"></season-results>
            </div>
            <div class="eight wide column">
                <total-results></total-results>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui unstackable table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.grandprix') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($grandprixs as $grandprix)
                        <tr>
                            <td><i class="{{ $grandprix->country_id }} flag"></i>{{ $grandprix->full_name }}</td>
                            <td>
                                <a href="{{ route('results.gp', ['slug' => $grandprix->slug]) }}" class="ui ferrari icon button">
                                    <i class="unhide icon"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection