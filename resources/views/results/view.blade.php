@extends('templates.frontend')
@section('title', getTitle($grandprix->full_name))
@section('content')
<div class="ui main fluid container">
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <h1 class="ui dividing header">
                        <i class="road icon"></i>
                        <span class="content">
                            {{ $grandprix->full_name }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui ferrari segment">
                    <div class="ui seven column stackable grid">
                        <div class="middle aligned column">
                            <strong>{{ trans('common.statistics') }}</strong>
                        </div>
                        <div class="column">
                            <div class="ui ferrari label">{{ trans('common.wins') }}</div>
                            <span style="margin-left: 5px">{{ $stats['wins'] }}</span>
                        </div>
                        <div class="column">
                            <div class="ui ferrari label">{{ trans('common.polepositions') }}</div>
                            <span style="margin-left: 5px">{{ $stats['poles'] }}</span>
                        </div>
                        <div class="column">
                            <div class="ui ferrari label">{{ trans('common.fastestlaps') }}</div>
                            <span style="margin-left: 5px">0</span>
                        </div>
                        <div class="column">
                            <div class="ui ferrari label">{{ trans('common.podiums') }}</div>
                            <span style="margin-left: 5px">{{ $stats['podiums'] }}</span>
                        </div>
                        <div class="column">
                            <div class="ui ferrari label">{{ trans('common.points') }}</div>
                            <span style="margin-left: 5px">{{ $stats['points'] }}</span>
                        </div>
                        <div class="column">
                            <div class="ui ferrari label">{{ trans('common.laps') }}</div>
                            <span style="margin-left: 5px">{{ $stats['laps'] }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        <i class="trophy icon"></i>
                        <span class="content">
                            {{ trans('common.results') }}
                        </span>
                    </h3>
                    <table class="ui unstackable table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.season') }}</th>
                                <th>{{ trans('common.name') }}</th>
                                <th class="center aligned">{{ trans('common.place') }}</th>
                                <th class="center aligned">{{ trans('common.points') }}</th>
                                <th class="center aligned">{{ trans('common.laps') }}</th>
                                <th class="center aligned">{{ trans('common.dnf') }}</th>
                                <th class="center aligned">{{ trans('common.dns') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($results as $result)
                            <tr>
                                <td>{{ $result->season }}</td>
                                <td>{{ $result->driver->name }}</td>
                                <td class="center aligned">{{ $result->place }}</td>
                                <td class="center aligned">
                                    {{ $result->points }}
                                    {!! $result->checkPoints() !!}
                                </td>
                                <td class="center aligned">{{ $result->laps }}</td>
                                <td class="center aligned">{{ $result->dnfStatus() }}</td>
                                <td class="center aligned">{{ $result->dnsStatus() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="four wide column">
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        <img src="https://www.formula1.com/content/fom-website/en/championship/drivers/sebastian-vettel/_jcr_content/image.img.1920.medium.jpg/1521204232238.jpg" alt="" class="ui circular image">
                        <span class="content">
                            Sebastian Vettel
                        </span>
                    </h3>
                    <div class="ui two horizontal buttons">
                        <div class="ui ferrari labeled icon button">
                            <i class="up arrow icon"></i>
                            Best result
                        </div>
                        <div class="ui ferrari labeled icon button">
                            <i class="down arrow icon"></i>
                            Last year's result
                        </div>
                    </div>
                </div>
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        <img src="https://www.formula1.com/content/fom-website/en/championship/drivers/kimi-raikkonen/_jcr_content/image.img.1920.medium.jpg/1521204007378.jpg" alt="" class="ui circular image">
                        <span class="content">
                            Kimi Raikkönen
                        </span>
                    </h3>
                    <div class="ui two horizontal buttons">
                        <div class="ui ferrari labeled icon button">
                            <i class="up arrow icon"></i>
                            Best result
                        </div>
                        <div class="ui ferrari labeled icon button">
                            <i class="down arrow icon"></i>
                            Last year's result
                        </div>
                    </div>
                </div>
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        Most wins as Ferrari driver
                    </h3>
                </div>
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        Most podiums as Ferrari driver
                    </h3>
                </div>
                <div class="ui ferrari segment">
                    <h3 class="ui dividing header">
                        Most poles as Ferrari driver
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection