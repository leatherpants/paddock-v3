@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="car icon"></i>
                            <span class="content">
                                {{ $car->name }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection