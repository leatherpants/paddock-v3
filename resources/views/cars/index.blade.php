@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui ferrari segment">
                        <h1 class="ui dividing header">
                            <i class="car icon"></i>
                            <span class="content">
                                {{ trans('common.cars') }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui unstackable table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cars as $car)
                            <tr>
                                <td>
                                    {{ $car->name }}
                                </td>
                                <td>
                                    <div class="ui red buttons">
                                        <a href="" title="{{ $car->name }}" class="ui icon button">
                                            <i class="edit icon"></i>
                                        </a>
                                        <a href="{{ route('cars.car', ['slug' => $car->slug]) }}" title="{{ $car->name }}" class="ui icon button">
                                            <i class="unhide icon"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection