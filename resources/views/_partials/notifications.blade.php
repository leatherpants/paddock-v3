@if ($errors->any())
<div class="ui icon error message">
    <i class="remove icon"></i>
    <ul class="list">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (session()->has('save'))
<div class="ui icon positive message">
    <i class="checkmark icon"></i>
    <div class="content">
        <div class="header">
            {{ session()->get('save') }}
        </div>
    </div>
</div>
@endif

@if(session()->has('error'))
<div class="ui icon positive message">
    <i class="remove icon"></i>
    <div class="content">
        <div class="header">
        {{ session()->get('error') }}
        </div>
    </div>
</div>
@endif