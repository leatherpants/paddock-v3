<div class="ui ferrari dashboard clearing segment" style="height: 100%">
    <div class="ui stackable grid">
        <div class="nine wide column">
            <a href="{{ route('user', ['user_name' => Auth::user()['user_name']]) }}" class="ui medium centered image">
                <img src="{{ Auth::user()['avatar'] }}" alt="{{ Auth::user()['display_name'] }}">
            </a>
            <a href="{{ route('user', ['user_name' => Auth::user()['user_name']]) }}" class="ui bottom attached ferrari small label" style="position: relative;">
                {{ Auth::user()->display_name }}
            </a>
        </div>
        <div class="seven wide column">
            <div class="ui list">
                <div class="item">
                    <div class="content">
                        <div class="ui vertical fluid buttons">
                            <a href="{{ route('settings') }}" class="ui ferrari icon labeled button">
                                <i class="settings icon"></i>
                                {{ trans('common.settings') }}
                            </a>
                            <a href="{{ route('sessions') }}" class="ui ferrari icon labeled button">
                                <i class="time icon"></i>
                                {{ trans('common.session') }}
                            </a>
                            <a href="{{ route('socials') }}" class="ui ferrari icon labeled button">
                                <i class="share icon"></i>
                                {{ trans('common.socialmedia') }}
                            </a>
                            <a href="{{ route('dashboard.test') }}" class="ui ferrari icon labeled button">
                                <i class="road icon"></i>
                                {{ trans('common.tests') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>