<div class="ui ferrari dashboard clearing segment" style="height: 100%">
    <div class="ui stackable grid">
        <div class="fourteen wide column" style="padding-bottom: 0;">
            <h1 class="ui header">
                {{ $race->grandprix->full_name }}
                <span class="sub header">
                    <i class="{{ $race->grandprix->country_id }} flag"></i>
                    {{ $race->grandprix->name }} |
                    {{ $race->raceday->subDay(2)->format('d.m.Y') }} -
                    {{ $race->raceday->format('d.m.Y') }}
                </span>
            </h1>
        </div>
        <div class="right aligned two wide column" style="padding-bottom: 0;">
            <timezone></timezone>
        </div>
        <div class="sixteen wide column" style="padding: 0.5rem 1rem;">
            <div class="ui very relaxed horizontal divided list">
                <div class="item">
                    <a href="{{ route('seasons.raceLive', ['season' => $race->season, 'gp' => $race->grandprix->slug, 'session' => 1]) }}">
                        FP1
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('seasons.raceLive', ['season' => $race->season, 'gp' => $race->grandprix->slug, 'session' => 2]) }}">
                        FP2
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('seasons.raceLive', ['season' => $race->season, 'gp' => $race->grandprix->slug, 'session' => 3]) }}">
                        FP3
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('seasons.raceLive', ['season' => $race->season, 'gp' => $race->grandprix->slug, 'session' => 4]) }}">
                        Quali
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('seasons.raceLive', ['season' => $race->season, 'gp' => $race->grandprix->slug, 'session' => 5]) }}">
                        Race
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('feed.ferrari.hashtag', ['hashtag' => $race->grandprix->hashtag]) }}">
                        <i class="newspaper outline icon"></i> {{ $race->grandprix->hashtag }}
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('seasons.race', ['season' => $race->season, 'gp' => $race->grandprix->slug]) }}">
                        <i class="calendar outline icon"></i> {{ $race->grandprix->hashtag }}
                    </a>
                </div>
            </div>
        </div>
        <div class="four wide column" style="padding-top: 0;">
            <strong>{{ trans('common.timetable') }}</strong>
            <div class="ui list">
                @foreach($sessions as $session)
                    <div class="item">
                        <div class="middle aligned content">
                            <strong style="display: inline-block; width:30%;">{{ $session->start->timezone(Auth::user()->timezone)->formatLocalized('%a') }}</strong>
                            {{ $session->start->timezone(Auth::user()->timezone)->formatLocalized('%H:%M') }} -
                            {{ $session->end->timezone(Auth::user()->timezone)->formatLocalized('%H:%M') }}
                            {!! $session->live() !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="four wide column" style="padding-top: 0;">
            <strong>{{ trans('common.standings') }}</strong>
            <div class="ui list">
                <div class="item">
                    <span style="display: inline-block; width: 75%;">{{ $standings['driver1']['name'] }}</span>
                    {{ $standings['driver1']['points'] }}
                </div>
                <div class="item">
                    <span style="display: inline-block; width: 75%;">{{ $standings['driver2']['name'] }}</span>
                    {{ $standings['driver2']['points'] }}
                </div>
                <div class="item">
                    <span style="display: inline-block; width: 75%;">{{ $standings['team']['name'] }}</span>
                    {{ $standings['team']['points'] }}
                </div>
            </div>
        </div>
    </div>
</div>