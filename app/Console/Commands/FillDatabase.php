<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FillDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill your database in and prepare Passport for use';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate:refresh', ['--seed']);
        $this->call('db:seed', ['--force' => true]);

        $this->call('passport:keys');

        $this->call('passport:client', ['--personal' => true, '--name' => config('app.name').' Personal Access Client']);
        $this->call('passport:client', ['--password' => true, '--name' => config('app.name').' Password Grant Client']);
    }
}
