<?php

namespace App\paddock\Tracks\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Tracks\Models\Tracks.
 *
 * @property int $id
 * @property string $country_id
 * @property string $name
 * @property string $slug
 * @property string $timezone
 * @property int $active
 * @mixin \Eloquent
 */
class Tracks extends Model
{
    protected $table = 'tracks';

    public $timestamps = false;
}
