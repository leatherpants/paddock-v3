<?php

namespace App\paddock\Tracks\Repositories;

use App\paddock\Tracks\Models\Tracks;

class TracksRepository
{
    /**
     * @var Tracks
     */
    private $model;

    /**
     * TracksRepository constructor.
     * @param Tracks $tracks
     */
    public function __construct(Tracks $tracks)
    {
        $this->model = $tracks;
    }

    /**
     * Create track.
     * @param array $data
     */
    public function createTrack(array $data)
    {
        $track = new Tracks();
        $track->country_id = $data['country_id'];
        $track->name = $data['name'];
        $track->slug = $data['slug'];
        $track->timezone = $data['timezone'];
        $track->active = $data['active'];
        $track->save();
    }

    /**
     * Edit track.
     * @param array $data
     */
    public function editTrack(array $data)
    {
        $track = Tracks::find($data['id']);

        if ($track) {
            $track->country_id = $data['country_id'];
            $track->name = $data['name'];
            $track->slug = $data['slug'];
            $track->timezone = $data['timezone'];
            $track->active = $data['active'];
            $track->save();
        }
    }

    /**
     * Get all track.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Count track.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count('id');
    }

    /**
     * Get track by ID.
     * @param int $id
     * @return mixed
     */
    public function getTrackByID(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->first();
    }

    /**
     * Get track by slug.
     * @param string $slug
     * @return mixed
     */
    public function getTrackBySlug(string $slug)
    {
        return $this->model
            ->where('slug', $slug)
            ->first();
    }
}
