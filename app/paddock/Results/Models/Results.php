<?php

namespace App\paddock\Results\Models;

use App\paddock\Staffs\Models\Staffs;
use App\paddock\Tracks\Models\Tracks;
use Illuminate\Database\Eloquent\Model;
use App\paddock\GrandPrixs\Models\GrandPrixs;

/**
 * App\paddock\Results\Models\Results.
 *
 * @property int $id
 * @property int $season
 * @property \Illuminate\Support\Carbon $dateofresult
 * @property int $gp_id
 * @property int $track_id
 * @property int $session_id
 * @property int $staff_id
 * @property int $team
 * @property int $place
 * @property int $dnf
 * @property int $dns
 * @property int $dsq
 * @property float $points
 * @property int $laps
 * @property-read \App\paddock\Staffs\Models\Staffs $driver
 * @property-read \App\paddock\GrandPrixs\Models\GrandPrixs $grandprix
 * @property-read \App\paddock\Tracks\Models\Tracks $track
 * @mixin \Eloquent
 */
class Results extends Model
{
    protected $table = 'results';

    public $timestamps = false;

    protected $dates = [
        'dateofresult',
    ];

    public function driver()
    {
        return $this->hasOne(Staffs::class, 'id', 'staff_id');
    }

    public function grandprix()
    {
        return $this->hasOne(GrandPrixs::class, 'id', 'gp_id');
    }

    public function track()
    {
        return $this->hasOne(Tracks::class, 'id', 'track_id');
    }

    /**
     * Output of the session of the grand prix.
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function session()
    {
        $session = '';

        switch ($this->session_id) {
            case 1:
                $session = trans('common.race');
                break;
            case 2:
                $session = trans('common.qualifying');
                break;
        }

        return $session;
    }

    public function dnfStatus()
    {
        if ($this->dnf == 1) {
            echo '<i class="red times circle icon">';
        } else {
            echo '<i class="green check circle icon">';
        }
    }

    public function dnsStatus()
    {
        if ($this->dns == 1) {
            echo '<i class="red times circle icon">';
        } else {
            echo '<i class="green check circle icon">';
        }
    }

    /**
     * @return string
     */
    public function checkPoints()
    {
        if ($this->season >= 2010) {
            return $this->points2010();
        } elseif ($this->season >= 2003 && $this->season <= 2009) {
            return $this->points2003();
        } elseif ($this->season >= 1991 && $this->season <= 2002) {
            return $this->points1991();
        } elseif ($this->season >= 1961 && $this->season <= 1990) {
            return $this->points1961();
        } elseif ($this->season == 1960) {
            return $this->points1960();
        } elseif ($this->season >= 1950 && $this->season <= 1959) {
            return $this->points1950();
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    private function points2010(): string
    {
        if ((in_array($this->place, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) && in_array($this->points, [25, 18, 15, 12, 10, 8, 6, 4, 2, 1])) ||
            ($this->place >= 11 && $this->points == 0) ||
            ($this->dnf == 1 && $this->points == 0) ||
            ($this->dns == 1 && $this->points == 0)
        ) {
            return '<i class="green check circle icon"></i>';
        } else {
            return '<i class="red times circle icon"></i>';
        }
    }

    /**
     * @return string
     */
    private function points2003(): string
    {
        if ((in_array($this->place, [1, 2, 3, 4, 5, 6, 7, 8]) && in_array($this->place, [10, 8, 6, 5, 4, 3, 2, 1])) ||
            ($this->place >= 9 && $this->points == 0) ||
            ($this->dnf == 1 && $this->points == 0) ||
            ($this->dns == 1 && $this->points == 0)
        ) {
            return '<i class="green check circle icon"></i>';
        } else {
            return '<i class="red times circle icon"></i>';
        }
    }

    /**
     * @return string
     */
    private function points1991(): string
    {
        if ((in_array($this->place, [1, 2, 3, 4, 5, 6]) && in_array($this->points, [10, 6, 4, 3, 2, 1])) ||
            ($this->place >= 7 && $this->points == 0) ||
            ($this->dnf == 1 && $this->points == 0) ||
            ($this->dns == 1 && $this->points == 0)
        ) {
            return '<i class="green check circle icon"></i>';
        } else {
            return '<i class="red times circle icon"></i>';
        }
    }

    /**
     * @return string
     */
    private function points1961(): string
    {
        if ((in_array($this->place, [1, 2, 3, 4, 5, 6]) && in_array($this->points, [9, 6, 4, 3, 2, 1])) ||
            ($this->place >= 7 && $this->points == 0) ||
            ($this->dnf == 1 && $this->points == 0) ||
            ($this->dns == 1 && $this->points == 0)
        ) {
            return '<i class="green check circle icon"></i>';
        } else {
            return '<i class="red times circle icon"></i>';
        }
    }

    /**
     * @return string
     */
    private function points1960(): string
    {
        if ((in_array($this->place, [1, 2, 3, 4, 5, 6]) && in_array($this->points, [8, 6, 4, 3, 2, 1])) ||
            ($this->place >= 7 && $this->points == 0) ||
            ($this->dnf == 1 && $this->points == 0) ||
            ($this->dns == 1 && $this->points == 0)
        ) {
            return '<i class="green check circle icon"></i>';
        } else {
            return '<i class="red times circle icon"></i>';
        }
    }

    /**
     * @return string
     */
    private function points1950(): string
    {
        if ((in_array($this->place, [1, 2, 3, 4, 5]) && in_array($this->points, [8, 6, 4, 3, 2])) ||
            ($this->place >= 6 && $this->points == 0) ||
            ($this->dnf == 1 && $this->points == 0) ||
            ($this->dns == 1 && $this->points == 0)
        ) {
            return '<i class="green check circle icon"></i>';
        } else {
            return '<i class="red times circle icon"></i>';
        }
    }
}
