<?php

namespace App\paddock\Results\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Results\Models\ResultsTimings.
 *
 * @property int $id
 * @property int $season
 * @property int $gp_id
 * @property int $session
 * @property int $staff_id
 * @property int $laptime
 * @mixin \Eloquent
 */
class ResultsTimings extends Model
{
    protected $table = 'results_timings';

    public function secintime()
    {
        if ($this->laptime !== 0) {
            $min = (int) ($this->laptime / 60000);
            $sek = ((int) ($this->laptime / 1000)) % 60;
            $ms = $this->laptime % 1000;

            return sprintf('%01d:%02d.%03d', $min, $sek, $ms);
        } else {
            return 'No time set';
        }
    }
}
