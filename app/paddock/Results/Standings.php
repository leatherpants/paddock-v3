<?php

namespace App\paddock\Results;

use App\paddock\Results\Repositories\ResultsRepository;

class Standings
{
    /**
     * @var ResultsRepository
     */
    private $resultsRepository;

    /**
     * Standings constructor.
     * @param ResultsRepository $resultsRepository
     */
    public function __construct(ResultsRepository $resultsRepository)
    {
        $this->resultsRepository = $resultsRepository;
    }

    /**
     * Get standings of the drivers and team.
     * @param int $season
     * @return array
     */
    public function getTotalStandings(int $season)
    {
        $driver1 = $this->resultsRepository->getDriverStandings($season, 76);

        $driver2 = $this->resultsRepository->getDriverStandings($season, 72);

        $team = $this->resultsRepository->getTeamStandings($season);

        $data = [
            'driver1' => [
                    'name'   => 'Sebastian Vettel',
                    'points' => number_format($driver1, 0, '.', ''),
                ],
            'driver2' => [
                    'name'   => 'Kimi Raikkönen',
                    'points' => number_format($driver2, 0, '.', ''),
                ],
            'team'    => [
                    'name'   => 'Team',
                    'points' => number_format($team, 0, '.', ''),
                ],
        ];

        return $data;
    }
}
