<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Sessions;
use App\paddock\Results\Models\Results;

class ResultsStaffsRepository
{
    /**
     * @var Results
     */
    private $results;

    /**
     * ResultsStaffsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    /**
     * Get podium by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getPodiumsByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get points by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getPointsByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->sum('points');
    }

    /**
     * Get poles by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getPolesByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::QUALIFYING)
            ->where('staff_id', $id)
            ->where('place', 1)
            ->count('id');
    }

    /**
     * Get races by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getRacesByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->count('id');
    }

    /**
     * Get team podium by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getTeamPodiumsByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->where('team', 1)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get team points by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getTeamPointsByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->where('team', 1)
            ->where('place', 1)
            ->sum('points');
    }

    /**
     * Get team poles by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getTeamPolesByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::QUALIFYING)
            ->where('staff_id', $id)
            ->where('team', 1)
            ->where('place', 1)
            ->count('id');
    }

    /**
     * Get team races by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getTeamRacesByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->where('team', 1)
            ->count('id');
    }

    /**
     * Get team wins by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getTeamWinsByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->where('team', 1)
            ->where('place', 1)
            ->count('id');
    }

    /**
     * Get wins by staff id.
     *
     * @param int $id
     * @return mixed
     */
    public function getWinsByStaffID(int $id)
    {
        return $this->results
            ->where('session_id', sessions::RACE)
            ->where('staff_id', $id)
            ->where('place', 1)
            ->count('id');
    }
}
