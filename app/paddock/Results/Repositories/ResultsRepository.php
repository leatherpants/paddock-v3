<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Sessions;
use App\paddock\Results\Models\Results;

class ResultsRepository
{
    /**
     * @var Results
     */
    private $model;

    /**
     * ResultsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->model = $results;
    }

    /**
     * Get grand prixs of results.
     * @return mixed
     */
    public function getAllResultsByGrandPrix()
    {
        $columns = [
            'grand_prixs.country_id',
            'grand_prixs.full_name',
            'grand_prixs.slug',
        ];

        return $this->model
            ->join('grand_prixs', 'grand_prixs.id', '=', 'results.gp_id')
            ->groupBy('results.gp_id')
            ->orderBy('grand_prixs.full_name', 'ASC')
            ->get($columns);
    }

    /**
     * Get driver standings.
     * @param int $season
     * @param int $driver_id
     * @return mixed
     */
    public function getDriverStandings(int $season, int $driver_id)
    {
        return $this->model
            ->where('season', $season)
            ->where('staff_id', $driver_id)
            ->where('session_id', 1)
            ->sum('points');
    }

    /**
     * Get race results of season and grand prix.
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getRaceResultsOfSeasonAndGrandPrix(int $season, int $gp)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->where('session_id', sessions::RACE)
            ->orderBy('place', 'ASC')
            ->get();
    }

    /**
     * Get quali results of season and grand prix.
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getQualiResultsOfSeasonAndGrandPrix(int $season, int $gp)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->where('session_id', sessions::QUALIFYING)
            ->orderBy('place', 'ASC')
            ->get();
    }

    /**
     * Get team standings.
     * @param int $season
     * @return mixed
     */
    public function getTeamStandings(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', 1)
            ->where('team', 1)
            ->sum('points');
    }
}
