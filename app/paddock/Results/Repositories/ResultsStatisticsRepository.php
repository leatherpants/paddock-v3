<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Sessions;
use App\paddock\Results\Models\Results;
use App\paddock\GrandPrixs\Models\GrandPrixs;

class ResultsStatisticsRepository
{
    /**
     * @var GrandPrixs
     */
    private $grandPrixs;

    /**
     * @var Results
     */
    private $model;

    /**
     * ResultsStatisticsRepository constructor.
     *
     * @param GrandPrixs $grandPrixs
     * @param Results $results
     */
    public function __construct(GrandPrixs $grandPrixs, Results $results)
    {
        $this->grandPrixs = $grandPrixs;
        $this->model = $results;
    }

    /**
     * Get grand prix stats.
     * @return array
     */
    public function getAllGrandPrixStats()
    {
        $grandprixs = [];

        $gps = $this->grandPrixs
            ->orderBy('name', 'ASC')
            ->get();

        foreach ($gps as $g => $gp) {
            $grandprixs[$g]['grandprix'] = $gp->name;
            $grandprixs[$g]['gp_wins'] = $this->getGrandPrixWins($gp->id);
            $grandprixs[$g]['gp_poles'] = $this->getGrandPrixPoles($gp->id);
            $grandprixs[$g]['gp_podiums'] = $this->getGrandPrixPodiums($gp->id);
            $grandprixs[$g]['gp_points'] = $this->getGrandPrixPoints($gp->id);
            $grandprixs[$g]['gp_laps'] = $this->getGrandPrixLaps($gp->id);
        }

        return $grandprixs;
    }

    /**
     * Get grand prix result stats.
     * @return array
     */
    public function getAllGrandPrixResultStats()
    {
        return  [
            'wins' => $this->getCountWins(),
            'poles' => $this->getCountPoles(),
            'podiums' => $this->getCountPodiums(),
            'points' => $this->getCountPoints(),
            'laps' => $this->getCountLaps(),
        ];
    }

    /**
     * Get amount of laps.
     * @return mixed
     */
    public function getCountLaps()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get number of podiums.
     * @return mixed
     */
    public function getCountPodiums()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->whereIn('place', [1, 2, 3])
            ->count();
    }

    /**
     * Get number of points.
     * @return mixed
     */
    public function getCountPoints()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get number of pole positions.
     * @return mixed
     */
    public function getCountPoles()
    {
        return $this->model
            ->where('session_id', sessions::QUALIFYING)
            ->where('team', 1)
            ->where('place', 1)
            ->count();
    }

    /**
     * Get number of wins.
     * @return mixed
     */
    public function getCountWins()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->count();
    }

    /**
     * Get all wins of the season of the team.
     * @param int $season
     * @return mixed
     */
    public function getSeasonWins(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->orderBy('dateofresult', 'ASC')
            ->get();
    }

    /**
     * Get all wins of the team.
     * @return mixed
     */
    public function getWins()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->orderBy('dateofresult', 'ASC')
            ->get();
    }

    /**
     * Get grand prix laps.
     * @param int $gp_id
     * @return mixed
     */
    public function getGrandPrixLaps(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get grand prix podiums.
     * @param int $gp_id
     * @return mixed
     */
    public function getGrandPrixPodiums(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->whereIn('place', [1, 2, 3])
            ->count();
    }

    /**
     * Get grand prix points.
     * @param int $gp_id
     * @return mixed
     */
    public function getGrandPrixPoints(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get grand prix poles.
     * @param int $gp_id
     * @return mixed
     */
    public function getGrandPrixPoles(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::QUALIFYING)
            ->where('team', 1)
            ->where('place', 1)
            ->count();
    }

    /**
     * Get grand prix wins.
     * @param int $gp_id
     * @return mixed
     */
    public function getGrandPrixWins(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->count();
    }
}
