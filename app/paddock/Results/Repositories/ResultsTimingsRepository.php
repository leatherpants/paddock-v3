<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\ResultsTimings;

class ResultsTimingsRepository
{
    /**
     * @var ResultsTimings
     */
    private $model;

    /**
     * ResultsTimingsRepository constructor.
     * @param ResultsTimings $resultsTimings
     */
    public function __construct(ResultsTimings $resultsTimings)
    {
        $this->model = $resultsTimings;
    }

    /**
     * @param int $season
     * @param int $gp
     * @param int $session
     * @param int $driver
     * @return mixed
     */
    public function getTimeBySeasonGPSessionAndDriver(int $season, int $gp, int $session, int $driver)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->where('session', $session)
            ->where('staff_id', $driver)
            ->first();
    }

    /**
     * Get laptime of the grand prix weekend by the driver.
     * @param int $season
     * @param int $gp
     * @param int $driver
     * @return mixed
     */
    public function getTimesBySeasonGPAndDriver(int $season, int $gp, int $driver)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->where('staff_id', $driver)
            ->where('laptime', '!=', '0')
            ->orderBy('laptime', 'ASC')
            ->get();
    }
}
