<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Sessions;
use App\paddock\Results\Models\Results;

class ResultsSeasonsRepository
{
    /**
     * @var Results
     */
    private $model;

    /**
     * ResultsSeasonsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->model = $results;
    }

    /**
     * Get the laps of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonLaps(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get the podiums of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonPodiums(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get the points of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonPoints(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get the poles of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonPoles(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::QUALIFYING)
            ->where('team', 1)
            ->where('place', 1)
            ->sum('place');
    }

    /**
     * Get the wins of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonWins(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }
}
