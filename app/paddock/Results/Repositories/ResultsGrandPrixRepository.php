<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Sessions;
use App\paddock\Results\Models\Results;

class ResultsGrandPrixRepository
{
    /**
     * @var Results
     */
    private $model;

    /**
     * ResultsGrandPrixRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->model = $results;
    }

    /**
     * Get the laps of the grand prix#.
     * @param int $gp_id
     * @return mixed
     */
    public function getGPLaps(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get the podiums of the grand prix.
     * @param int $gp_id
     * @return mixed
     */
    public function getGPPodiums(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get the points of the grand prix.
     * @param int $gp_id
     * @return mixed
     */
    public function getGPPoints(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get the poles of the grand prix.
     * @param int $gp_id
     * @return mixed
     */
    public function getGPPoles(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::QUALIFYING)
            ->where('team', 1)
            ->where('place', 1)
            ->sum('place');
    }

    /**
     * Get the wins of the grand prix.
     * @param int $gp_id
     * @return mixed
     */
    public function getGPWins(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }

    /**
     * Get results for race.
     * @param int $gp_id
     * @return mixed
     */
    public function getGPResultsForRace(int $gp_id)
    {
        return $this->model
            ->where('gp_id', $gp_id)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->orderBy('dateofresult', 'DESC')
            ->orderBy('place', 'ASC')
            ->orderBy('laps', 'DESC')
            ->get();
    }
}
