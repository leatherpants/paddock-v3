<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Sessions;
use App\paddock\Results\Models\Results;

class ResultsTotalRepository
{
    /**
     * @var Results
     */
    private $model;

    /**
     * ResultsTotalRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->model = $results;
    }

    /**
     * Get laps of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonLaps(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get podiums of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonPodiums(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get points of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonPoints(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get poles of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonPoles(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::QUALIFYING)
            ->where('team', 1)
            ->where('place', 1)
            ->sum('place');
    }

    /**
     * Get wins of the season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonWins(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }

    /**
     * Get total laps in F1 history.
     * @return mixed
     */
    public function getTotalLaps()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get total podiums in F1 history.
     * @return mixed
     */
    public function getTotalPodiums()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get total points in F1 history.
     * @return mixed
     */
    public function getTotalPoints()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get total poles in F1 History.
     * @return mixed
     */
    public function getTotalPoles()
    {
        return $this->model
            ->where('session_id', sessions::QUALIFYING)
            ->where('team', 1)
            ->where('place', 1)
            ->sum('place');
    }

    /**
     * Get total wins in F1 history.
     * @return mixed
     */
    public function getTotalWins()
    {
        return $this->model
            ->where('session_id', sessions::RACE)
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }
}
