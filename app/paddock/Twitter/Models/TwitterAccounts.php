<?php

namespace App\paddock\Twitter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Twitter\Models\TwitterAccounts.
 *
 * @property int $id
 * @property int $twitter_id
 * @property string $name
 * @property string $screen_name
 * @property string|null $profile_image_url_https
 * @property int $active
 * @property int $owner
 * @mixin \Eloquent
 */
class TwitterAccounts extends Model
{
    protected $table = 'twitter_accounts';

    public $timestamps = false;
}
