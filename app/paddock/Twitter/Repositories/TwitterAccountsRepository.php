<?php

namespace App\paddock\Twitter\Repositories;

use App\paddock\Twitter\Models\TwitterAccounts;

class TwitterAccountsRepository
{
    /**
     * @var TwitterAccounts
     */
    private $model;

    /**
     * TwitterAccountsRepository constructor.
     * @param TwitterAccounts $twitterAccounts
     */
    public function __construct(TwitterAccounts $twitterAccounts)
    {
        $this->model = $twitterAccounts;
    }

    /**
     * Get all twitter accounts.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('screen_name', 'ASC')
            ->get();
    }
}
