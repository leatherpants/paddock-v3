<?php

namespace App\paddock\Reports\Models;

use App\paddock\Staffs\Models\Staffs;
use Illuminate\Database\Eloquent\Model;
use App\paddock\GrandPrixs\Models\GrandPrixs;

/**
 * App\paddock\Reports\Models\ReportsTyres.
 *
 * @property int $id
 * @property int $season
 * @property int $race
 * @property int $gp_id
 * @property int $staff_id
 * @property int|null $hypersoft
 * @property int|null $ultrasoft
 * @property int|null $supersoft
 * @property int|null $soft
 * @property int|null $medium
 * @property int|null $hard
 * @property int|null $superhard
 * @property-read \App\paddock\Staffs\Models\Staffs $driver
 * @property-read \App\paddock\GrandPrixs\Models\GrandPrixs $grandprix
 * @mixin \Eloquent
 */
class ReportsTyres extends Model
{
    protected $table = 'reports_tyres';

    public function driver()
    {
        return $this->hasOne(Staffs::class, 'id', 'staff_id');
    }

    public function grandprix()
    {
        return $this->hasOne(GrandPrixs::class, 'id', 'gp_id');
    }

    /**
     * @return string
     */
    public function selectedSets()
    {
        $string = '';

        if ($this->hypersoft ||
            $this->ultrasoft ||
            $this->supersoft ||
            $this->soft ||
            $this->medium ||
            $this->hard ||
            $this->superhard > 1) {
            $string .= $this->multipleSeclectedSets();
        }

        if ($this->hypersoft ||
            $this->ultrasoft ||
            $this->supersoft ||
            $this->soft ||
            $this->medium ||
            $this->hard ||
            $this->superhard == 1) {
            $string .= $this->singleSelectedSets();
        }

        return nl2br($string);
    }

    /**
     * @return string
     */
    protected function singleSelectedSets()
    {
        $string = '';

        if ($this->hypersoft == 1) {
            $string .= $this->hypersoft.' Hypersoft<br>';
        }

        if ($this->ultrasoft == 1) {
            $string .= $this->ultrasoft.' Ultrasoft<br>';
        }

        if ($this->supersoft == 1) {
            $string .= $this->supersoft.' Supersoft<br>';
        }

        if ($this->soft == 1) {
            $string .= $this->soft.' Soft<br>';
        }

        if ($this->medium == 1) {
            $string .= $this->medium.' Medium<br>';
        }

        if ($this->hard == 1) {
            $string .= $this->hard.' Hard<br>';
        }

        if ($this->superhard == 1) {
            $string .= $this->superhard.' Superhard<br>';
        }

        return $string;
    }

    /**
     * @return string
     */
    protected function multipleSeclectedSets()
    {
        $string = '';

        if ($this->hypersoft > 1) {
            $string .= $this->hypersoft.' Hypersofts<br>';
        }

        if ($this->ultrasoft > 1) {
            $string .= $this->ultrasoft.' Ultrasofts<br>';
        }

        if ($this->supersoft > 1) {
            $string .= $this->supersoft.' Supersofts<br>';
        }

        if ($this->soft > 1) {
            $string .= $this->soft.' Softs<br>';
        }

        if ($this->medium > 1) {
            $string .= $this->medium.' Mediums<br>';
        }

        if ($this->hard > 1) {
            $string .= $this->hard.' Hard<br>';
        }

        if ($this->superhard > 1) {
            $string .= $this->superhard.' Superhard<br>';
        }

        return $string;
    }

    /**
     * @param string $lang
     * @return string
     */
    public function twitterString(string $lang)
    {
        $string = '#'.$this->grandprix->hashtag.' '.$this->grandprix->emoji.' - ';

        $string .= $this->selectedSetsHeadlineString($lang);

        $string .= $this->selectedSetsString();

        $string .= '#ForzaFerrari';

        return urlencode($string);
    }

    /**
     * @param string $lang
     * @return mixed
     */
    protected function selectedSetsHeadlineString(string $lang)
    {
        $string = '';

        if ($lang == 'en') {
            $string = 'Selected sets for '.$this->driver->hashtag.'<br>';
        } elseif ($lang == 'de') {
            $string = 'Ausgewählte Sätze für '.$this->driver->hashtag.'<br>';
        } elseif ($lang == 'it') {
            $string = 'Set selezionati per '.$this->driver->hashtag.'<br>';
        } elseif ($lang == 'se') {
            $string = 'Valda uppsättningar för '.$this->driver->hashtag.'<br>';
        } elseif ($lang == 'es') {
            $string = 'Conjuntos seleccionados para '.$this->driver->hashtag.'<br>';
        }

        return str_replace('<br>', "\r\n", $string);
    }

    /**
     * @return mixed
     */
    protected function selectedSetsString()
    {
        $string = $this->selectedSets();

        return str_replace('<br>', "\r\n", $string);
    }
}
