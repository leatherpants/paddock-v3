<?php

namespace App\paddock\Reports\Models;

use App\paddock\Staffs\Models\Staffs;
use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Reports\Models\ReportsTechnicals.
 *
 * @property int $id
 * @property int $season
 * @property int $race
 * @property int $gp_id
 * @property int $staff_id
 * @property int $ice
 * @property int $tc
 * @property int $mguh
 * @property int $mguk
 * @property int $es
 * @property int $ce
 * @property-read \App\paddock\Staffs\Models\Staffs $driver
 * @mixin \Eloquent
 */
class ReportsTechnicals extends Model
{
    protected $table = 'reports_technicals';

    public function driver()
    {
        return $this->hasOne(Staffs::class, 'id', 'staff_id');
    }
}
