<?php

namespace App\paddock\Reports\Repositories;

use App\paddock\Reports\Models\ReportsTyres;

class ReportsTyresRepository
{
    /**
     * @var ReportsTyres
     */
    private $model;

    /**
     * ReportsTyresRepository constructor.
     * @param ReportsTyres $reportsTyres
     */
    public function __construct(ReportsTyres $reportsTyres)
    {
        $this->model = $reportsTyres;
    }

    /**
     * Get selected sets by season and gp.
     * @param int $season
     * @param int $gp_id
     * @return mixed
     */
    public function getSelectedSetsBySeasonAndGP(int $season, int $gp_id)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp_id)
            ->get();
    }

    /**
     * Get selected sets by season, gp and staff id.
     * @param int $season
     * @param int $gp_id
     * @param int $staff
     * @return mixed
     */
    public function getSelectedSetsBySeasonGPAndStaff(int $season, int $gp_id, int $staff)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp_id)
            ->where('staff_id', $staff)
            ->first();
    }
}
