<?php

namespace App\paddock\Reports\Repositories;

use App\paddock\Reports\Models\ReportsTechnicals;

class ReportsTechnicalsRepository
{
    /**
     * @var ReportsTechnicals
     */
    private $model;

    /**
     * ReportsTechnicalsRepository constructor.
     * @param ReportsTechnicals $reportsTechnicals
     */
    public function __construct(ReportsTechnicals $reportsTechnicals)
    {
        $this->model = $reportsTechnicals;
    }

    /**
     * Get technical report by season and gp.
     * @param int $season
     * @param int $gp_id
     * @return mixed
     */
    public function getTechnicalReportBySeasonAndGP(int $season, int $gp_id)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp_id)
            ->get();
    }
}
