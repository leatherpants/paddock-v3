<?php

namespace App\paddock\GrandPrixs\Repositories;

use App\paddock\Seasons\Models\SeasonsRaces;
use App\paddock\GrandPrixs\Models\GrandPrixs;

class GrandPrixsRepository
{
    private $model;

    /**
     * @var SeasonsRaces
     */
    private $seasonsRaces;

    /**
     * GrandPrixsRepository constructor.
     * @param GrandPrixs $grandPrixs
     * @param SeasonsRaces $seasonsRaces
     */
    public function __construct(GrandPrixs $grandPrixs, SeasonsRaces $seasonsRaces)
    {
        $this->model = $grandPrixs;
        $this->seasonsRaces = $seasonsRaces;
    }

    /**
     * Create grand prix.
     * @param array $data
     */
    public function createGrandPrix(array $data)
    {
        $gp = new GrandPrixs();
        $gp->country_id = $data['country_id'];
        $gp->name = $data['name'];
        $gp->slug = $data['slug'];
        $gp->full_name = $data['full_name'];
        $gp->hashtag = $data['hashtag'];
        $gp->emoji = $data['emoji'];
        $gp->active = $data['active'];
        $gp->save();
    }

    /**
     * Edit grand prix.
     * @param array $data
     */
    public function editGrandPrix(array $data)
    {
        $gp = GrandPrixs::find($data['id']);

        if ($gp) {
            $gp->country_id = $data['country_id'];
            $gp->name = $data['name'];
            $gp->slug = $data['slug'];
            $gp->full_name = $data['full_name'];
            $gp->hashtag = $data['hashtag'];
            $gp->emoji = $data['emoji'];
            $gp->active = $data['active'];
            $gp->save();
        }
    }

    /**
     * Get all grand prixs.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Count grand prix.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count('id');
    }

    /**
     * Get grand prix by id.
     * @param int $id
     * @return mixed
     */
    public function getGrandPrixByID(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->first();
    }

    /**
     * Get grand prix by slug.
     * @param string $slug
     * @return mixed
     */
    public function getGrandPrixBySlug(string $slug)
    {
        return $this->model
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get list of all grand prix.
     * @param int $id
     * @return mixed
     */
    public function getGrandPrixRacesByID(int $id)
    {
        return $this->seasonsRaces
            ->where('gp_id', $id)
            ->get();
    }
}
