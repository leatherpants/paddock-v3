<?php

namespace App\paddock\GrandPrixs\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddGrandPrixRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required|string',
            'name' => 'required|string|unique:grand_prixs,name',
            'full_name' => 'required|string',
            'hashtag' => 'string',
            'emoji' => 'required|string',
            'active' => 'nullable',
        ];
    }
}
