<?php

namespace App\paddock\GrandPrixs\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\GrandPrixs\Models\GrandPrixs.
 *
 * @property int $id
 * @property string $country_id
 * @property string $name
 * @property string $slug
 * @property string $full_name
 * @property string $hashtag
 * @property string $emoji
 * @property int $active
 * @mixin \Eloquent
 */
class GrandPrixs extends Model
{
    protected $table = 'grand_prixs';

    public $timestamps = false;
}
