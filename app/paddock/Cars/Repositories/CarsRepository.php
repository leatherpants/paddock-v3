<?php

namespace App\paddock\Cars\Repositories;

use App\paddock\Cars\Models\Cars;

class CarsRepository
{
    /**
     * @var Cars
     */
    private $model;

    /**
     * CarsRepository constructor.
     * @param Cars $cars
     */
    public function __construct(Cars $cars)
    {
        $this->model = $cars;
    }

    /**
     * Get all cars.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get cars.
     * @param string $slug
     * @return mixed
     */
    public function getCar(string $slug)
    {
        return $this->model
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Count cars.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count('id');
    }
}
