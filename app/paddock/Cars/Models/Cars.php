<?php

namespace App\paddock\Cars\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Cars\Models\Cars.
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @mixin \Eloquent
 */
class Cars extends Model
{
    protected $table = 'cars';

    protected $fillable = [
        'name',
        'slug',
    ];

    public $timestamps = false;
}
