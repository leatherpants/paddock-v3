<?php

namespace App\paddock\Users\Repositories;

use App\paddock\Users\Models\UsersSocials;

class UsersSocialsRepository
{
    /**
     * @var UsersSocials
     */
    private $model;

    /**
     * UsersSocialsRepository constructor.
     * @param UsersSocials $usersSocials
     */
    public function __construct(UsersSocials $usersSocials)
    {
        $this->model = $usersSocials;
    }
}
