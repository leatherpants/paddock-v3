<?php

namespace App\paddock\Users\Repositories;

use App\paddock\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $model;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->model = $users;
    }

    /**
     * Get all users.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Get user by username.
     * @param string $username
     * @return mixed
     */
    public function getUsersByUserName(string $username)
    {
        return $this->model
            ->where('user_name', $username)
            ->first();
    }

    /**
     * Update background image.
     * @param array $data
     */
    public function updateBackgroundImage(array $data)
    {
        $info = Users::find($data['user_id']);

        if ($info) {
            $info->background_image = $data['background_image'];
            $info->save();
        }
    }

    /**
     * Update user settings.
     * @param array $data
     */
    public function updateUserSettings(array $data)
    {
        $user = Users::find($data['id']);

        if ($user) {
            $user->lang = $data['lang'];
            $user->save();
        }
    }
}
