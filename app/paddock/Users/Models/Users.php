<?php

namespace App\paddock\Users\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\paddock\Media\Gallery\Models\MediaGalleryPhotos;

/**
 * App\paddock\Users\Models\Users.
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $display_name
 * @property string $user_name
 * @property string $avatar
 * @property int $background_image
 * @property string $lang
 * @property string $timezone
 * @property int $isAdmin
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\paddock\Media\Gallery\Models\MediaGalleryPhotos $backgroundImage
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \App\paddock\Users\Models\UsersSocials $socials
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 */
class Users extends Authenticatable
{
    use HasApiTokens;

    protected $table = 'users';

    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'display_name',
        'user_name',
        'avatar',
        'background_image',
        'lang',
        'timezone',
        'isAdmin',
    ];

    /**
     * Media Gallery Photos Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function backgroundImage()
    {
        return $this->belongsTo(MediaGalleryPhotos::class, 'background_image', 'id');
    }

    public function socials()
    {
        return $this->hasOne(UsersSocials::class, 'user_id', 'id');
    }
}
