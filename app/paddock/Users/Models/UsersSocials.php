<?php

namespace App\paddock\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Users\Models\UsersSocials.
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $facebook_username
 * @property string|null $twitter_username
 * @property string|null $instagram_username
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class UsersSocials extends Model
{
    protected $table = 'users_socials';

    protected $fillable = [
        'user_id',
        'facebook_username',
        'twitter_username',
        'instagram_username',
    ];
}
