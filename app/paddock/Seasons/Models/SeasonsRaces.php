<?php

namespace App\paddock\Seasons\Models;

use App\paddock\Tracks\Models\Tracks;
use Illuminate\Database\Eloquent\Model;
use App\paddock\GrandPrixs\Models\GrandPrixs;

/**
 * App\paddock\Seasons\Models\SeasonsRaces.
 *
 * @property int $id
 * @property int $season
 * @property \Illuminate\Support\Carbon $raceday
 * @property int $gp_id
 * @property int $track_id
 * @property-read \App\paddock\GrandPrixs\Models\GrandPrixs $grandprix
 * @property-read \App\paddock\Tracks\Models\Tracks $track
 * @mixin \Eloquent
 */
class SeasonsRaces extends Model
{
    protected $table = 'seasons_races';

    public $timestamps = false;

    protected $dates = [
        'raceday',
    ];

    public function grandprix()
    {
        return $this->hasOne(GrandPrixs::class, 'id', 'gp_id');
    }

    public function track()
    {
        return $this->hasOne(Tracks::class, 'id', 'track_id');
    }
}
