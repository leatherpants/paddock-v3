<?php

namespace App\paddock\Seasons\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Seasons\Models\SeasonsFiles.
 *
 * @property int $id
 * @property int $season
 * @property int $gp_id
 * @property string $file_type
 * @property string $name
 * @property string $file_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereFileType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereGpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereSeason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\paddock\Seasons\Models\SeasonsFiles whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SeasonsFiles extends Model
{
    protected $table = 'seasons_files';
}
