<?php

namespace App\paddock\Seasons\Models;

use Illuminate\Database\Eloquent\Model;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use App\paddock\Seasons\Traits\SeasonsSessions as Session;

/**
 * App\paddock\Seasons\Models.
 *
 * @property int $id
 * @property int $season
 * @property int $gp_id
 * @property int $session_id
 * @property \Illuminate\Support\Carbon|null $start
 * @property \Illuminate\Support\Carbon|null $end
 * @mixin \Eloquent
 * @property-read \App\paddock\GrandPrixs\Models\GrandPrixs $grandprix
 */
class SeasonsSessions extends Model
{
    use Session;

    protected $table = 'seasons_sessions';

    protected $fillable = [
        'season',
        'gp_id',
        'session_id',
        'start',
        'end',
    ];

    protected $dates = [
        'start',
        'end',
    ];

    public function grandprix()
    {
        return $this->belongsTo(GrandPrixs::class, 'gp_id');
    }

    public function session($session_id)
    {
        return $this
            ->where('gp_id', $this->gp_id)
            ->where('session_id', $session_id)
            ->first();
    }
}
