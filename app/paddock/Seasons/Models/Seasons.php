<?php

namespace App\paddock\Seasons\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Seasons\Models.
 *
 * @property int $season
 * @mixin \Eloquent
 */
class Seasons extends Model
{
    protected $table = 'seasons';

    protected $fillable = [
        'season',
    ];

    protected $primaryKey = 'season';

    public $incrementing = false;

    public $timestamps = false;
}
