<?php

namespace App\paddock\Seasons\Traits;

use Carbon\Carbon;
use App\paddock\Seasons\Models\SeasonsSessions as F1Sessions;

/**
 * App\paddock\Seasons\Traits.
 *
 * @property string $timezone
 */
trait SeasonsSessions
{
    /**
     * Generate live label for sessions.
     * @return string
     */
    public function live()
    {
        $userTimezone = auth()->user()->timezone;

        $now = Carbon::now()->timezone($userTimezone);

        $session = F1Sessions::where('season', $this->season)
            ->where('gp_id', $this->gp_id)
            ->where('session_id', $this->session_id)
            ->first();

        $start = $session->start->timezone($userTimezone);
        $end = $session->end->timezone($userTimezone);

        if (($now > $start) && ($now < $end)) {
            return '<span class="ui green mini horizontal label">LIVE</span>';
        }

        return '';
    }
}
