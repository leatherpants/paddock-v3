<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Models\Seasons;

class SeasonsRepository
{
    /**
     * @var Seasons
     */
    private $model;

    /**
     * SeasonsRepository constructor.
     * @param Seasons $seasons
     */
    public function __construct(Seasons $seasons)
    {
        $this->model = $seasons;
    }

    /**
     * Create season.
     * @param array $data
     */
    public function createSeason(array $data)
    {
        $season = new Seasons();
        $season->season = $data['season'];
        $season->save();
    }

    /**
     * Edit season.
     * @param array $data
     * @param int $season
     */
    public function editSeason(array $data, int $season)
    {
        $season = Seasons::find($season);

        if ($season) {
            $season->season = $data['season'];
            $season->save();
        }
    }

    /**
     * Get all seasons.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('season', 'ASC')
            ->get();
    }

    /**
     * Count seasons.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count('season');
    }

    /**
     * Get season by season.
     * @param int $season
     * @return mixed
     */
    public function getSeasonBySeason(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->first();
    }
}
