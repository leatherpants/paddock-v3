<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Models\SeasonsSessions;

class SeasonsSessionsRepository
{
    /**
     * @var SeasonsSessions
     */
    private $model;

    /**
     * SeasonsSessionsRepository constructor.
     * @param SeasonsSessions $seasonsSessions
     */
    public function __construct(SeasonsSessions $seasonsSessions)
    {
        $this->model = $seasonsSessions;
    }

    /**
     * Get season sessions by season and grand prix.
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getSessionsBySeasonAndGP(int $season, int $gp)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->get();
    }

    /**
     * Get season sessions by season, who will be grouped by the grand prix id.
     * @param int $season
     * @return mixed
     */
    public function getSessionsBySeason(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->groupBy('gp_id')
            ->orderBy('id')
            ->get();
    }
}
