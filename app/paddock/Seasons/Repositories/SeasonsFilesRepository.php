<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Models\SeasonsFiles;

class SeasonsFilesRepository
{
    /**
     * const limit.
     */
    const LIMIT = 7;

    /**
     * @var SeasonsFiles
     */
    private $model;

    /**
     * SeasonsFilesRepository constructor.
     * @param SeasonsFiles $seasonsFiles
     */
    public function __construct(SeasonsFiles $seasonsFiles)
    {
        $this->model = $seasonsFiles;
    }

    /**
     * Get files of Grand Prix by season and its id.
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getFilesBySeasonAndGP(int $season, int $gp)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->get();
    }

    /**
     * Get files of Grand Prix by season and its id.
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getFilesBySeasonAndGPLimited(int $season, int $gp)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->take(self::LIMIT)
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
