<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Models\SeasonsRaces;

class SeasonsRacesRepository
{
    /**
     * @var SeasonsRaces
     */
    private $model;

    /**
     * SeasonsRacesRepository constructor.
     * @param SeasonsRaces $seasonsRaces
     */
    public function __construct(SeasonsRaces $seasonsRaces)
    {
        $this->model = $seasonsRaces;
    }

    /**
     * Count season races.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count('id');
    }

    /**
     * Get current race for dashboard.
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getCurrentRace(int $season, int $gp)
    {
        return $this->model
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->first();
    }

    /**
     * Get races by season.
     * @param int $season
     * @return mixed
     */
    public function getRacesBySeason(int $season)
    {
        return $this->model
            ->where('season', $season)
            ->get();
    }

    /**
     * Get race by season and grand prix.
     * @param int $season
     * @param string $gp
     * @return mixed
     */
    public function getRaceBySeasonAndGP(int $season, string $gp)
    {
        return $this->model
            ->join('grand_prixs', 'grand_prixs.id', '=', 'seasons_races.gp_id')
            ->where('seasons_races.season', $season)
            ->where('grand_prixs.slug', $gp)
            ->first();
    }

    /**
     * Get race by season and grand prix.
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getRaceBySeasonAndGPID(int $season, int $gp)
    {
        return $this->model
            ->join('grand_prixs', 'grand_prixs.id', '=', 'seasons_races.gp_id')
            ->where('seasons_races.season', $season)
            ->where('grand_prixs.id', $gp)
            ->first();
    }
}
