<?php

namespace App\paddock\Countries\Repositories;

use App\paddock\Countries\Models\Countries;

class CountriesRepository
{
    /**
     * @var Countries
     */
    private $model;

    /**
     * CountriesRepository constructor.
     * @param Countries $countries
     */
    public function __construct(Countries $countries)
    {
        $this->model = $countries;
    }

    /**
     * Create country.
     * @param array $data
     */
    public function createCountry(array $data)
    {
        $country = new Countries();
        $country->code = $data['code'];
        $country->name = $data['name'];
        $country->save();
    }

    /**
     * Edit country.
     * @param array $data
     * @param string $code
     */
    public function editCountry(array $data, string $code)
    {
        $country = Countries::find($code);

        if ($country) {
            $country->code = $data['code'];
            $country->name = $data['name'];
            $country->save();
        }
    }

    /**
     * Get all countries.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('name')
            ->get();
    }

    /**
     * Count countries.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count('code');
    }

    /**
     * Get country by code.
     * @param string $code
     * @return mixed
     */
    public function getCountryByCode(string $code)
    {
        return $this->model
            ->where('code', $code)
            ->first();
    }
}
