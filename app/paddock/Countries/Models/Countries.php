<?php

namespace App\paddock\Countries\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Countries\Models\Countries.
 *
 * @property string $code
 * @property string $name
 * @mixin \Eloquent
 */
class Countries extends Model
{
    protected $table = 'countries';

    protected $primaryKey = 'code';

    public $incrementing = false;

    public $timestamps = false;
}
