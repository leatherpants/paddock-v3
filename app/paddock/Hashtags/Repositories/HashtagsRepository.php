<?php

namespace App\paddock\Hashtags\Repositories;

use App\paddock\Hashtags\Models\Hashtags;

class HashtagsRepository
{
    /**
     * @var Hashtags
     */
    private $model;

    /**
     * HashtagsRepository constructor.
     * @param Hashtags $hashtags
     */
    public function __construct(Hashtags $hashtags)
    {
        $this->model = $hashtags;
    }

    /**
     * Get all hashtags.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('title', 'ASC')
            ->get();
    }
}
