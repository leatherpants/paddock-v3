<?php

namespace App\paddock\Hashtags\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Hashtags\Models\Hashtags.
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @mixin \Eloquent
 */
class Hashtags extends Model
{
    protected $table = 'hashtags';
}
