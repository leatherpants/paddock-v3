<?php

namespace App\paddock\Staffs\Repositories;

use App\paddock\Staffs\Models\Staffs;

class StaffsRepository
{
    /**
     * @var Staffs
     */
    private $model;

    /**
     * StaffsRepository constructor.
     * @param Staffs $staffs
     */
    public function __construct(Staffs $staffs)
    {
        $this->model = $staffs;
    }

    /**
     * Get all staff.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Count staffs.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count('id');
    }

    /**
     * Get staff by id.
     * @param int $id
     * @return mixed
     */
    public function getStaffByID(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->first();
    }

    /**
     * Get staff by slug.
     * @param string $slug
     * @return mixed
     */
    public function getStaffBySlug(string $slug)
    {
        return $this->model
            ->where('slug', $slug)
            ->first();
    }
}
