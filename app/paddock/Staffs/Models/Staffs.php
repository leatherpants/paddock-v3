<?php

namespace App\paddock\Staffs\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Staffs\Models\Staffs.
 *
 * @property int $id
 * @property string $country_id
 * @property string $name
 * @property string $slug
 * @property string $first_name
 * @property string $last_name
 * @property string|null $hashtag
 * @property \Illuminate\Support\Carbon|null $dateofbirth
 * @property \Illuminate\Support\Carbon|null $dateofdeath
 * @mixin \Eloquent
 */
class Staffs extends Model
{
    protected $table = 'staffs';

    protected $fillable = [
        'country_id',
        'name',
        'slug',
        'first_name',
        'last_name',
        'hashtag',
        'dateofbirth',
        'dateofdeath',
    ];

    public $timestamps = false;

    protected $dates = [
        'dateofbirth',
        'dateofdeath',
    ];
}
