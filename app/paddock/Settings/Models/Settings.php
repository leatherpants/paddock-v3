<?php

namespace App\paddock\Settings\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Settings\Models\Settings.
 *
 * @property string $key
 * @property string $value
 * @mixin \Eloquent
 */
class Settings extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'key',
        'value',
    ];

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;
}
