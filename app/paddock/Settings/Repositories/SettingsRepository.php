<?php

namespace App\paddock\Settings\Repositories;

use App\paddock\Settings\Models\Settings;

class SettingsRepository
{
    /**
     * @var Settings
     */
    private $model;

    /**
     * SettingsRepository constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->model = $settings;
    }

    /**
     * Get settings.
     * @param string $key
     * @return mixed
     */
    public function getSettings(string $key)
    {
        return $this->model
            ->where('key', $key)
            ->first();
    }
}
