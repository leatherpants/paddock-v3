<?php

namespace App\paddock\Languages\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Languages\Models\Languages.
 *
 * @property string $code
 * @property string $name
 * @property int $active
 * @property int $news
 * @mixin \Eloquent
 */
class Languages extends Model
{
    protected $table = 'languages';

    protected $primaryKey = 'code';

    public $incrementing = false;

    public $timestamps = false;

    public function flag()
    {
        if ($this->code == 'en') {
            return 'england';
        } else {
            return $this->code;
        }
    }
}
