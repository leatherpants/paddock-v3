<?php

namespace App\paddock\Languages\Repositories;

use App\paddock\Languages\Models\Languages;

class LanguagesRepository
{
    /**
     * @var Languages
     */
    private $model;

    /**
     * LanguagesRepository constructor.
     * @param Languages $languages
     */
    public function __construct(Languages $languages)
    {
        $this->model = $languages;
    }

    /**
     * Get all languages.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('code', 'ASC')
            ->get();
    }

    /**
     * Get only active languages for output.
     * @return mixed
     */
    public function getActive()
    {
        return $this->model
            ->where('active', 1)
            ->orderBy('code', 'ASC')
            ->get();
    }

    /**
     * Get only active languages for news.
     * @return mixed
     */
    public function getNewsActive()
    {
        return $this->model
            ->where('news', 1)
            ->orderBy('code', 'ASC')
            ->get();
    }
}
