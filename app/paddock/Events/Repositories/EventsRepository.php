<?php

namespace App\paddock\Events\Repositories;

use App\paddock\Events\Models\Events;

class EventsRepository
{
    /**
     * @var Events
     */
    private $model;

    /**
     * EventsRepository constructor.
     * @param Events $events
     */
    public function __construct(Events $events)
    {
        $this->model = $events;
    }

    /**
     * Get all events by user language.
     * @param string $lang
     * @return mixed
     */
    public function getEventsByLang(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->get();
    }

    /**
     * Get event of the day by staff id.
     * @param string $date
     * @param int $staff_id
     * @return mixed
     */
    public function getEventsByDateAndDriver(string $date, int $staff_id)
    {
        return $this->model
            ->where('date', $date)
            ->where('staff_id', $staff_id)
            ->get();
    }
}
