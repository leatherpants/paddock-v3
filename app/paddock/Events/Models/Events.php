<?php

namespace App\paddock\Events\Models;

use App\paddock\Staffs\Models\Staffs;
use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Events\Models\Events.
 *
 * @property int $id
 * @property int $staff_id
 * @property string $type
 * @property \Illuminate\Support\Carbon $date
 * @property string $lang
 * @property string $event_text
 * @property string $event_twitter
 * @property string $event_hashtags
 * @property string $event_link
 * @property-read \App\paddock\Staffs\Models\Staffs $driver
 * @mixin \Eloquent
 */
class Events extends Model
{
    protected $table = 'events';

    public $timestamps = false;

    protected $dates = [
        'date',
    ];

    public function driver()
    {
        return $this->hasOne(Staffs::class, 'id', 'staff_id');
    }

    public function getFlag()
    {
        if ($this->lang == 'en') {
            return 'england';
        } else {
            return $this->lang;
        }
    }

    /**
     * put various parts to a twitter link together.
     * @return string
     */
    public function getTweetLink()
    {
        // main part of link
        $intent = 'https://twitter.com/intent/tweet?';

        // text of tweet
        if ($this->event_twitter) {
            $intent .= 'text='.$this->event_twitter;
        }

        // hashtags of tweet
        if ($this->event_hashtags) {
            $intent .= $this->prepareHashtags();
        }

        // link of tweet
        if ($this->event_link) {
            $intent .= '&url='.$this->event_link;
        }

        return $intent;
    }

    /**
     * Preparing hashtags for tweet.
     * @return string
     */
    private function prepareHashtags()
    {
        $tags = '';
        $hashtags = $this->event_hashtags;

        $hashtag = explode(',', $hashtags);

        foreach ($hashtag as $tag) {
            $tags .= ' #'.$tag;
        }

        return urlencode($tags);
    }
}
