<?php

namespace App\paddock\Tests\Repositories;

use App\paddock\Tests\Models\TestsResults;

class TestsResultsRepository
{
    /**
     * @var TestsResults
     */
    private $model;

    /**
     * TestsResultsRepository constructor.
     * @param TestsResults $testsResults
     */
    public function __construct(TestsResults $testsResults)
    {
        $this->model = $testsResults;
    }

    /**
     * Get results by season and testweek.
     * @param int $season
     * @param int $week
     * @return mixed
     */
    public function getResultsBySeasonAndTestweek(int $season, int $week)
    {
        return $this->model
            ->where('season', $season)
            ->where('week', $week)
            ->orderBy('pos', 'ASC')
            ->get();
    }

    /**
     * Get general results by season and testweek.
     * @param int $season
     * @param int $week
     * @return mixed
     */
    public function getGeneralResultsBySeasonAndTestweek(int $season, int $week)
    {
        return $this->model
            ->where('season', $season)
            ->where('week', $week)
            ->get();
    }
}
