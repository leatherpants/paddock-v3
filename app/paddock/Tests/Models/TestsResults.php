<?php

namespace App\paddock\Tests\Models;

use App\paddock\Staffs\Models\Staffs;
use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Tests\Models\TestsResults.
 *
 * @property int $id
 * @property int $season
 * @property int $week
 * @property int $day
 * @property string $dateoftest
 * @property int $track_id
 * @property int $staff_id
 * @property int $pos
 * @property int $laptime
 * @property int $laps
 * @property-read \App\paddock\Staffs\Models\Staffs $driver
 * @mixin \Eloquent
 */
class TestsResults extends Model
{
    protected $table = 'tests_results';

    public function driver()
    {
        return $this->belongsTo(Staffs::class, 'staff_id', 'id');
    }

    public function secintime()
    {
        if ($this->laptime !== 0) {
            $min = (int) ($this->laptime / 60000);
            $sek = ((int) ($this->laptime / 1000)) % 60;
            $ms = $this->laptime % 1000;

            return sprintf('%01d:%02d.%03d', $min, $sek, $ms);
        } else {
            return 'No time set';
        }
    }

    public function gapintime()
    {
        $fastest = $this->where('season', $this->season)->where('week', $this->week)->where('laptime', '!=', '0')->min('laptime');
        $current = $this->laptime;

        if (($fastest !== $this->laptime) && ($this->laptime !== 0)) {
            $gap = $current - $fastest;

            return $this->gaptime($gap);
        }

        return '';
    }

    private function gaptime($gap)
    {
        if ($gap !== 0) {
            $min = (int) ($gap / 60000);
            $sek = ((int) ($gap / 1000)) % 60;
            $ms = $gap % 1000;

            return '+'.sprintf('%01d:%02d.%03d', $min, $sek, $ms);
        } else {
            return '0:00.000';
        }
    }
}
