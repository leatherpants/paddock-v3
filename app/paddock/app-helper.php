<?php

if (!function_exists('getAppName')) {
    /**
     * @return mixed
     */
    function getAppName()
    {
        return env('APP_TITLE', 'Ferrari Paddock');
    }
}

if (!function_exists('getTitle')) {
    /**
     * @param string|null $title
     * @return mixed|string
     */
    function getTitle(string $title = null)
    {
        if ($title !== null) {
            return getAppName().' - '.$title;
        }

        return getAppName();
    }
}

if (!function_exists('ferrariLink')) {
    /**
     * @return string
     */
    function ferrariLink()
    {
        if (auth()->user()->lang == 'it') {
            return 'http://formula1.ferrari.com/it/';
        } else {
            return 'http://formula1.ferrari.com/en/';
        }
    }
}
