<?php

namespace App\paddock\Feed\Repositories;

use Illuminate\Support\Facades\DB;
use App\paddock\Feed\Models\FeedFDA;
use App\paddock\Feed\Models\FeedFerrari;
use App\paddock\Feed\Models\FeedHistory;
use App\paddock\Feed\Models\FeedMotorsport;

class FeedRepository
{
    /**
     * Max entries on dashboard.
     */
    const LIMIT = 5;

    /**
     * Limit for titles.
     */
    const TITLELIMIT = 44;

    /**
     * @var FeedFDA
     */
    private $feedFDA;

    /**
     * @var FeedFerrari
     */
    private $feedFerrari;

    /**
     * @var FeedHistory
     */
    private $feedHistory;

    /**
     * @var FeedMotorsport
     */
    private $feedMotorsport;

    /**
     * FeedRepository constructor.
     * @param FeedFDA $feedFDA
     * @param FeedFerrari $feedFerrari
     * @param FeedHistory $feedHistory
     * @param FeedMotorsport $feedMotorsport
     */
    public function __construct(
        FeedFDA $feedFDA,
        FeedFerrari $feedFerrari,
        FeedHistory $feedHistory,
        FeedMotorsport $feedMotorsport
    ) {
        $this->feedFDA = $feedFDA;
        $this->feedFerrari = $feedFerrari;
        $this->feedHistory = $feedHistory;
        $this->feedMotorsport = $feedMotorsport;
    }

    /**
     * Show activities.
     * @param string $lang
     * @return array
     */
    public function feed(string $lang)
    {
        $data = [];

        $user = auth()->user();

        $timezone = $user->timezone;

        $fda = $this->feedFDA
            ->where('lang', $lang)
            ->select(DB::raw('\'fda\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s") as date'));

        $ferrari = $this->feedFerrari
            ->where('lang', $lang)
            ->select(DB::raw('\'ferrari\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s") as date'));

        $history = $this->feedHistory
            ->where('lang', $lang)
            ->select(DB::raw('\'history\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(CONCAT(YEAR(CURDATE()),\'-\',MONTH(date),\'-\',DAYOFMONTH(date)), "%Y-%m-%d %H:%i:%s")  as date'));

        $entries = $this->feedMotorsport
            ->where('lang', $lang)
            ->select(DB::raw('\'motorsport\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s") as date'))
            ->union($fda)
            ->union($ferrari)
            ->union($history)
            ->orderBy('date', 'DESC')
            ->get();

        foreach ($entries as $e => $entry) {
            if ($entry->type == 'fda') {
                $data[$e]['type'] = strtoupper($entry->type);
            } else {
                $data[$e]['type'] = ucfirst($entry->type);
            }
            $data[$e]['title'] = $entry->title;
            $data[$e]['image'] = $entry->image;
            $data[$e]['date'] = $entry->date->timezone($timezone)->diffForHumans();

            if ($entry->type == 'fda') {
                $data[$e]['link'] = route('feed.fda');
            } elseif ($entry->type == 'ferrari') {
                $data[$e]['link'] = route('feed.ferrari');
            } elseif ($entry->type == 'history') {
                $data[$e]['link'] = route('feed.history');
            } elseif ($entry->type == 'motorsport') {
                $data[$e]['link'] = route('feed.motorsport');
            }
        }

        return $data;
    }

    /**
     * Shows latest articles from Ferrari, History and Motorsport feed.
     * @param string $lang
     * @return array
     */
    public function latestArticles(string $lang)
    {
        $data = [];

        $user = auth()->user();

        $timezone = $user->timezone;

        $fda = $this->feedFDA
            ->where('lang', $lang)
            ->select(DB::raw('\'fda\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s") as date'));

        $ferrari = $this->feedFerrari
            ->where('lang', $lang)
            ->select(DB::raw('\'ferrari\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s") as date'));

        $history = $this->feedHistory
            ->where('lang', $lang)
            ->select(DB::raw('\'history\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(CONCAT(YEAR(CURDATE()),\'-\',MONTH(date),\'-\',DAYOFMONTH(date)), "%Y-%m-%d %H:%i:%s")  as date'));

        $entries = $this->feedMotorsport
            ->where('lang', $lang)
            ->select(DB::raw('\'motorsport\' as type'), 'title', 'image', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s") as date'))
            ->union($fda)
            ->union($ferrari)
            ->union($history)
            ->limit(self::LIMIT)
            ->orderBy('date', 'DESC')
            ->get();

        foreach ($entries as $e => $entry) {
            if ($entry->type == 'fda') {
                $data[$e]['type'] = strtoupper($entry->type);
            } else {
                $data[$e]['type'] = ucfirst($entry->type);
            }
            $data[$e]['title'] = (strlen($entry->title) <= self::TITLELIMIT) ? $entry->title : str_limit($entry->title, self::TITLELIMIT);
            $data[$e]['date'] = $entry->date->timezone($timezone)->diffForHumans();

            if ($entry->type == 'fda') {
                $data[$e]['link'] = route('feed.fda');
            } elseif ($entry->type == 'ferrari') {
                $data[$e]['link'] = route('feed.ferrari');
            } elseif ($entry->type == 'history') {
                $data[$e]['link'] = route('feed.history');
            } elseif ($entry->type == 'motorsport') {
                $data[$e]['link'] = route('feed.motorsport');
            }
        }

        return $data;
    }
}
