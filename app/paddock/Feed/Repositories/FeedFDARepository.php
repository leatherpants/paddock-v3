<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedFDA;

class FeedFDARepository
{
    /**
     * entries per page.
     */
    const PAGINATE = 12;

    /**
     * @var FeedFDA
     */
    private $model;

    /**
     * FeedFDARepository constructor.
     * @param FeedFDA $feedFDA
     */
    public function __construct(FeedFDA $feedFDA)
    {
        $this->model = $feedFDA;
    }

    /**
     * Get feed entries by lang of the logged in user.
     * @param string $lang
     * @return mixed
     */
    public function getFeedByLang(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->paginate(self::PAGINATE);
    }

    /**
     * Get entries by date.
     * @param string $date
     * @return mixed
     */
    public function getEntryByDate(string $date)
    {
        return $this->model
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->get();
    }

    /**
     * Get entry by date, lang of the logged in user.
     * @param string $date
     * @param string $lang
     * @return mixed
     */
    public function getEntryByDateAndLang(string $date, string $lang)
    {
        return $this->model
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->where('lang', $lang)
            ->first();
    }

    /**
     * Get latest feed entry of the fda feed.
     * @param string $lang
     * @return mixed
     */
    public function getLatestEntry(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->first();
    }
}
