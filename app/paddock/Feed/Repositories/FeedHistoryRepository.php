<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedHistory;

class FeedHistoryRepository
{
    /**
     * entries per page.
     */
    const PAGINATE = 12;

    /**
     * @var FeedHistory
     */
    private $model;

    /**
     * FeedHistoryRepository constructor.
     * @param FeedHistory $feedHistory
     */
    public function __construct(FeedHistory $feedHistory)
    {
        $this->model = $feedHistory;
    }

    /**
     * Get entries by date and different languages.
     * @param string $date
     * @return mixed
     */
    public function getEntriesByDate(string $date)
    {
        return $this->model
            ->where('date', $date)
            ->get();
    }

    /**
     * Get entry by date and language.
     * @param string $date
     * @param string $lang
     * @return mixed
     */
    public function getEntryByDateAndLang(string $date, string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->where('date', $date)
            ->first();
    }

    /**
     * Get entries of feed by the lang.
     * @param string $lang
     * @return mixed
     */
    public function getFeedByLang(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->paginate(self::PAGINATE);
    }

    /**
     * Get latest feed entry of the history feed.
     * @param string $date
     * @param string $lang
     * @return mixed
     */
    public function getLatestEntry(string $date, string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->where('date', 'LIKE', '%'.$date.'%')
            ->first();
    }
}
