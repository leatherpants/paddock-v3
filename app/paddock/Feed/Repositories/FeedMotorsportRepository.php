<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedMotorsport;

class FeedMotorsportRepository
{
    /**
     * entries per page.
     */
    const PAGINATE = 12;

    /**
     * @var FeedMotorsport
     */
    private $model;

    /**
     * FeedMotorsportRepository constructor.
     * @param FeedMotorsport $feedMotorsport
     */
    public function __construct(FeedMotorsport $feedMotorsport)
    {
        $this->model = $feedMotorsport;
    }

    /**
     * Get article by date.
     * @param string $date
     * @return mixed
     */
    public function getArticleByDate(string $date)
    {
        return $this->model
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->first();
    }

    /**
     * Get articles by date.
     * @param string $date
     * @return mixed
     */
    public function getArticlesByDate(string $date)
    {
        return $this->model
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->get();
    }

    /**
     * Get feed by lang.
     * @param string $lang
     * @return mixed
     */
    public function getFeedByLang(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->paginate(self::PAGINATE);
    }

    /**
     * Get latest feed entry of the motorsport feed.
     * @param string $lang
     * @return mixed
     */
    public function getLatestEntry(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->first();
    }
}
