<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedFerrari;

class FeedFerrariRepository
{
    /**
     * entries per page.
     */
    const PAGINATE = 12;

    /**
     * @var FeedFerrari
     */
    private $model;

    /**
     * FeedFerrariRepository constructor.
     * @param FeedFerrari $feedFerrari
     */
    public function __construct(FeedFerrari $feedFerrari)
    {
        $this->model = $feedFerrari;
    }

    /**
     * Get entries by date and session.
     * @param string $date
     * @param int $session
     * @return mixed
     */
    public function getEntryByDateAndSession(string $date, int $session)
    {
        return $this->model
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->where('session', $session)
            ->get();
    }

    /**
     * Get entry by date, session and lang of the logged in user.
     * @param string $date
     * @param int $session
     * @param string $lang
     * @return mixed
     */
    public function getEntryByDateAndSessionAndLang(string $date, int $session, string $lang)
    {
        return $this->model
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->where('session', $session)
            ->where('lang', $lang)
            ->first();
    }

    /**
     * Get feed entries by hashtag and lang of the logged in user.
     * @param string $hashtag
     * @param string $lang
     * @return mixed
     */
    public function getFeedByHashtagAndLang(string $hashtag, string $lang)
    {
        return $this->model
            ->where('hashtags', 'LIKE', '%'.$hashtag.'%')
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * Get feed entries by lang of the logged in user.
     * @param string $lang
     * @return mixed
     */
    public function getFeedByLang(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->paginate(self::PAGINATE);
    }

    /**
     * Get latest feed entry of the ferrari feed.
     * @param string $lang
     * @return mixed
     */
    public function getLatestEntry(string $lang)
    {
        return $this->model
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->first();
    }
}
