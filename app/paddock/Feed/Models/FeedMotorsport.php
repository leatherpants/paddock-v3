<?php

namespace App\paddock\Feed\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Feed\Models\FeedMotorsport.
 *
 * @property int $id
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $link
 * @property string $domain
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class FeedMotorsport extends Model
{
    const LINK = 'https://twitter.com/intent/tweet?';

    protected $table = 'feed_motorsport';

    protected $fillable = [
        'lang',
        'title',
        'description',
        'image',
        'link',
        'domain',
    ];

    protected $dates = [
        'date',
        'created_at',
    ];

    /**
     * Output of flag due to Semantic UI.
     * @return mixed|string
     */
    public function getFlag()
    {
        if ($this->lang == 'en') {
            return 'england';
        } else {
            return $this->lang;
        }
    }

    /**
     * put various parts to a twitter link together.
     * @return string
     */
    public function getTweetLink()
    {
        // main part of link
        $intent = self::LINK;

        // text of tweet
        if ($this->title) {
            $intent .= 'text='.$this->replaceHeadlines();
        }

        $intent .= urlencode(' #ForzaFerrari');

        // link of tweet
        if ($this->link) {
            $intent .= '&url='.$this->link;
        }

        return $intent;
    }

    /**
     * Replacing of parts of the headline to hashtags for Social Media.
     * @return string
     */
    private function replaceHeadlines()
    {
        $search = [
            '/Kimi Räikkönens\b/u',
            '/Kimi Raikkonen\b/u',
            '/Kimi\b/u',
            '/Räikkönen\b/u',
            '/Raikkonen\b/u',
            '/Raikkonens\b/u',
            '/Sebastian\b/u',
            '/Vettel\b/u',
            '/Vettels\b/u',
            '/Seb\b/u',
            '/Ferrari\b/u',
            '/Ferraris\b/u',
            '/Ferrari Driver Academy\b/u',
            '/Maurizio\b/u',
            '/Arrivabene\b/u',
            '/Free practice 1\b/u',
            '/Free practice 2\b/u',
            '/Free Practice 3\b/u',
            '/1.Freies Training\b/u',
            '/2.Freies Training\b/u',
            '/3.Freies Training\b/u',
            '/Libere 2\b/u',
            '/Libere 3\b/u',
            '/Fri träning 2\b/u',
            '/T3\b/i',
            '/Libres 2\b/u',
            '/Libres 3\b/u',
            '/Scuderia Ferrari\b/u',
        ];

        $title = [
            '#Kimi7\'s',
            '#Kimi7',
            '#Kimi7',
            '#Kimi7',
            '#Kimi7',
            '#Kimi7\'s',
            '#Seb5',
            '#Seb5',
            '#Seb5\'s',
            '#Seb5',
            '#Ferrari',
            '#Ferrari\'s',
            '#FDA',
            '#Maurizio',
            '#Arrivabene',
            '#FP1',
            '#FP2',
            '#FP3',
            '#FP1',
            '#FP2',
            '#FP3',
            '#FP2',
            '#FP3',
            '#FP2',
            '#FP3',
            '#FP2',
            '#FP3',
            '@ScuderiaFerrari',
        ];

        $headline = (string) $this->title;

        $output = preg_replace($search, $title, $headline);

        return urlencode($output);
    }
}
