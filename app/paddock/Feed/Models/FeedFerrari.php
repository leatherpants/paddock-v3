<?php

namespace App\paddock\Feed\Models;

use Illuminate\Database\Eloquent\Model;
use App\paddock\GrandPrixs\Models\GrandPrixs;

/**
 * App\paddock\Feed\Models\FeedFerrari.
 *
 * @property int $id
 * @property int $session
 * @property string $lang
 * @property string $title
 * @property string|null $description
 * @property string $image
 * @property string $link
 * @property string $domain
 * @property string|null $hashtags
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class FeedFerrari extends Model
{
    const HASHTAG = '#ForzaFerrari';

    const LINK = 'https://twitter.com/intent/tweet?';

    protected $table = 'feed_ferrari';

    protected $fillable = [
        'lang',
        'session',
        'title',
        'description',
        'image',
        'link',
        'domain',
        'hashtags',
    ];

    protected $dates = [
        'date',
        'created_at',
    ];

    /**
     * Output of flag due to Semantic UI.
     * @return mixed|string
     */
    public function getFlag()
    {
        if ($this->lang == 'en') {
            return 'england';
        } else {
            return $this->lang;
        }
    }

    /**
     * put various parts to a twitter link together.
     * @return string
     */
    public function getTweetLink()
    {
        $intent = self::LINK;

        $content = 'text='.$this->prepareGPHashtag();

        $content .= $this->prepageTitle();

        $content .= $this->prepareHashtags();

        if ($this->link) {
            $content .= '&url='.$this->link;
        }

        urlencode($content);

        return $intent.$content;
    }

    /**
     * Prepare grand prix hashtag for tweets.
     * @return string
     */
    public function prepareGPHashtag()
    {
        $hashtag = substr($this->hashtags, 1);

        if (!empty($hashtag)) {
            $grandprix = GrandPrixs::where('hashtag', 'LIKE', '%'.$hashtag.'%')->first();

            if (!is_null($grandprix)) {
                $output = '#'.$grandprix->hashtag.' '.$grandprix->emoji.' - ';
            } else {
                $output = $this->hashtags.' - ';
            }
        } else {
            $output = '';
        }

        return urlencode($output);
    }

    /**
     * Prepare title for tweets.
     * @return array|mixed
     */
    private function prepageTitle()
    {
        if (strpos($this->title, ' - ') !== false) {
            $share = explode(' - ', $this->title);
            $text = $share[1];
            $output = $this->replaceHeadlines($text);
        } else {
            $output = $this->title;
        }

        return $output;
    }

    /**
     * Prepare hashtags for tweets.
     * @return string
     */
    private function prepareHashtags()
    {
        $tags = '';
        $hashtags = [
            self::HASHTAG,
        ];

        if ($this->hashtags == null) {
            $hashtag = explode(',', self::HASHTAG);
        } else {
            $hashtag = $hashtags;
        }

        foreach ($hashtag as $tag) {
            $tags .= ' '.$tag;
        }

        return urlencode($tags);
    }

    /**
     * Replacing of parts of the headline to hashtags for Social Media.
     * @param string $text
     * @return string
     */
    private function replaceHeadlines(string $text)
    {
        $search = [
            '/Kimi\b/u',
            '/Seb\b/u',
            '/Sebastian\b/u',
            '/Vettel\b/u',
            '/Maurizio\b/u',
            '/Ferrari\b/u',
        ];

        $title = [
            '#Kimi7',
            '#Seb5',
            '#Seb5',
            '#Seb5',
            '#Maurizio',
            '#Ferrari',
        ];

        $output = preg_replace($search, $title, $text);

        return urlencode($output);
    }

    /**
     * Get various sessions.
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function prepareSession()
    {
        switch ($this->session) {
            case 1:
                return trans('common.ferrari');
            case 2:
                return trans('common.testing');
            case 3:
                return trans('common.pre-race');
            case 4:
                return trans('common.thursday');
            case 5:
                return trans('common.fp1');
            case 6:
                return trans('common.fp2');
            case 7:
                return trans('common.friday');
            case 8:
                return trans('common.fp3');
            case 9:
                return trans('common.qualifying');
            case 10:
                return trans('common.saturday');
            case 11:
                return trans('common.race');
            case 12:
                return trans('common.sebastian');
            case 13:
                return trans('common.kimi');
            case 14:
                return trans('common.maurizio');
            case 15:
                return trans('common.sergio');
            default:
                return '';
        }
    }
}
