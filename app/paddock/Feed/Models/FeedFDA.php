<?php

namespace App\paddock\Feed\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Feed\Models\FeedFDA.
 *
 * @property int $id
 * @property string $lang
 * @property string $title
 * @property string|null $description
 * @property string $image
 * @property string $link
 * @property string $domain
 * @property string|null $hashtags
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class FeedFDA extends Model
{
    const HASHTAG = '#FDA';

    const LINK = 'https://twitter.com/intent/tweet?';

    protected $table = 'feed_fda';

    protected $fillable = [
        'lang',
        'title',
        'description',
        'image',
        'link',
        'domain',
        'hashtags',
    ];

    protected $dates = [
        'date',
        'created_at',
    ];

    /**
     * Output of flag due to Semantic UI.
     * @return mixed|string
     */
    public function getFlag()
    {
        if ($this->lang == 'en') {
            return 'england';
        } else {
            return $this->lang;
        }
    }

    /**
     * put various parts to a twitter link together.
     * @return string
     */
    public function getTweetLink()
    {
        // main part of link
        $intent = self::LINK;

        // text of tweet
        if ($this->title) {
            $content = 'text='.$this->replaceHeadlines();
            $content .= $this->prepareHashtags();
        } else {
            $content = $this->prepareHashtags();
        }

        // link of tweet
        if ($this->link) {
            $content .= '&url='.$this->link;
        }

        urlencode($content);

        return $intent.$content;
    }

    /**
     * Preparing hashtags for tweet.
     * @return string
     */
    private function prepareHashtags()
    {
        $tags = '';
        $hashtags = [
            self::HASHTAG,
            $this->hashtags,
        ];

        if ($this->hashtags == null) {
            $hashtag = explode(',', self::HASHTAG);
        } else {
            $hashtag = $hashtags;
        }

        foreach ($hashtag as $tag) {
            $tags .= ' '.$tag;
        }

        return urlencode($tags);
    }

    /**
     * Replacing of parts of the headline to hashtags for Social Media.
     * @return string
     */
    private function replaceHeadlines()
    {
        $search = [
            '/Formula 2\b/u',
            '/F3\b/u',
            '/Formula 3\b/u',
            '/GP3\b/u',
        ];

        $title = [
            '#F2',
            '#F3',
            '#FIAF3',
            '#GP3',
        ];

        $headline = (string) $this->title;

        $output = preg_replace($search, $title, $headline);

        return urlencode($output);
    }
}
