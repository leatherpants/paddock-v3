<?php

namespace App\paddock\Feed\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Feed\Models\FeedHistory.
 *
 * @property int $id
 * @property string $lang
 * @property \Illuminate\Support\Carbon $date
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $link
 * @property string $domain
 * @mixin \Eloquent
 */
class FeedHistory extends Model
{
    const HASHTAG = 'FerrariHistory';

    const LINK = 'https://twitter.com/intent/tweet?';

    protected $table = 'feed_history';

    protected $fillable = [
        'lang',
        'date',
        'title',
        'description',
        'image',
        'link',
        'domain',
    ];

    protected $dates = [
        'date',
    ];

    public $timestamps = false;

    /**
     * Output of flag due to Semantic UI.
     * @return mixed|string
     */
    public function getFlag()
    {
        if ($this->lang == 'en') {
            return 'england';
        } else {
            return $this->lang;
        }
    }

    /**
     * put various parts to a twitter link together.
     * @return string
     */
    public function getTweetLink()
    {
        // main part of link
        $intent = self::LINK;

        $content = 'text='.$this->prepareHashtags().' - ';

        // text of tweet
        if ($this->title) {
            $content .= $this->replaceHeadlines();
        }

        // link of tweet
        if ($this->link) {
            $content .= '&url='.$this->link;
        }

        urlencode($content);

        return $intent.$content;
    }

    /**
     * Preparing hashtags for tweet.
     * @return string
     */
    private function prepareHashtags()
    {
        return urlencode('#'.self::HASHTAG);
    }

    /**
     * Replacing of parts of the headline to hashtags for Social Media.
     * @return string
     */
    private function replaceHeadlines()
    {
        $search = [
            '/Ferrari\b/u',
            '/Scuderia\b/u',
        ];

        $title = [
            '#Ferrari',
            '#Scuderia',
        ];

        $headline = (string) $this->title;

        $output = preg_replace($search, $title, $headline);

        return urlencode($output);
    }
}
