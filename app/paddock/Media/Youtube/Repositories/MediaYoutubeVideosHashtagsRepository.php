<?php

namespace App\paddock\Media\Youtube\Repositories;

use App\paddock\Media\Youtube\Models\MediaYoutubeVideosHashtags;

class MediaYoutubeVideosHashtagsRepository
{
    /**
     * @var MediaYoutubeVideosHashtags
     */
    private $mediaYoutubeVideosHashtags;

    /**
     * MediaYoutubeVideosHashtagsRepository constructor.
     * @param MediaYoutubeVideosHashtags $mediaYoutubeVideosHashtags
     */
    public function __construct(MediaYoutubeVideosHashtags $mediaYoutubeVideosHashtags)
    {
        $this->mediaYoutubeVideosHashtags = $mediaYoutubeVideosHashtags;
    }
}
