<?php

namespace App\paddock\Media\Youtube\Repositories;

use App\paddock\Media\Youtube\Models\MediaYoutubeVideos;

class MediaYoutubeVideosRepository
{
    /**
     * Limit.
     */
    const LIMIT = 4;

    /**
     * @var MediaYoutubeVideos
     */
    private $model;

    /**
     * MediaYoutubeVideosRepository constructor.
     * @param MediaYoutubeVideos $mediaYoutubeVideos
     */
    public function __construct(MediaYoutubeVideos $mediaYoutubeVideos)
    {
        $this->model = $mediaYoutubeVideos;
    }

    /**
     * Get latest videos.
     * @return mixed
     */
    public function getLatestVideos()
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->take(self::LIMIT)
            ->get();
    }

    /**
     * Get latest video by YouTube.
     * @return mixed
     */
    public function getLatestYouTubeVideo()
    {
        return $this->model
            ->orderBy('id', 'DESC')
            ->first();
    }

    /**
     * Get video by ID.
     * @param string $youtube_id
     * @return mixed
     */
    public function getVideoByID(string $youtube_id)
    {
        return $this->model
            ->where('youtube_id', $youtube_id)
            ->first();
    }

    /**
     * Get all videos, who are stored by YouTube.
     * @return mixed
     */
    public function getVideos()
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
