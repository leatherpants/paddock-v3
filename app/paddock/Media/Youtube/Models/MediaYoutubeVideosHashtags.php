<?php

namespace App\paddock\Media\Youtube\Models;

use Illuminate\Database\Eloquent\Model;
use App\paddock\Hashtags\Models\Hashtags;

/**
 * App\paddock\Media\Youtube\Models\MediaYoutubeVideosHashtags.
 *
 * @property int $youtube_video_id
 * @property int $hashtag_id
 * @property-read \App\paddock\Hashtags\Models\Hashtags $hashtag
 * @mixin \Eloquent
 */
class MediaYoutubeVideosHashtags extends Model
{
    protected $table = 'media_youtube_videos_hashtags';

    public function hashtag()
    {
        return $this->belongsTo(Hashtags::class);
    }
}
