<?php

namespace App\paddock\Media\Youtube\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Media\Youtube\Models\MediaYoutubeVideos.
 *
 * @property int $id
 * @property string $youtube_id
 * @property string $title
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\paddock\Media\Youtube\Models\MediaYoutubeVideosHashtags[] $youtubeHashtags
 * @mixin \Eloquent
 */
class MediaYoutubeVideos extends Model
{
    protected $table = 'media_youtube_videos';

    public function youtubeHashtags()
    {
        return $this->hasMany(MediaYoutubeVideosHashtags::class, 'youtube_video_id');
    }

    public function titleShortner()
    {
        if (strpos($this->title, '|')) {
            $arr = explode('|', $this->title);

            return $arr[0];
        } elseif (strpos($this->title, ':')) {
            $arr = explode(':', $this->title);

            return $arr[1];
        } else {
            return $this->title;
        }
    }
}
