<?php

namespace App\paddock\Media\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Media\Gallery\Models\MediaGalleryPhotos.
 *
 * @property int $id
 * @property int $album_id
 * @property string $url
 * @mixin \Eloquent
 */
class MediaGalleryPhotos extends Model
{
    protected $table = 'media_gallery_photos';

    public $timestamps = false;
}
