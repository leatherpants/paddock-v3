<?php

namespace App\paddock\Media\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\Media\Gallery\Models\MediaGalleryAlbums.
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $cover_id
 * @mixin \Eloquent
 */
class MediaGalleryAlbums extends Model
{
    protected $table = 'media_gallery_albums';

    protected $fillable = [
        'name',
        'slug',
        'cover_id',
    ];

    public $timestamps = false;
}
