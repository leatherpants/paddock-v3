<?php

namespace App\paddock\Media\Gallery\Repositories;

use App\paddock\Media\Gallery\Models\MediaGalleryAlbums;

class MediaGalleryAlbumsRepository
{
    /**
     * Limit.
     */
    const LIMIT = 4;

    /**
     * @var MediaGalleryAlbums
     */
    private $model;

    /**
     * GalleryAlbumsRepository constructor.
     * @param MediaGalleryAlbums $galleryAlbums
     */
    public function __construct(MediaGalleryAlbums $galleryAlbums)
    {
        $this->model = $galleryAlbums;
    }

    /**
     * Get album by id.
     * @param int $id
     * @return mixed
     */
    public function getAlbumByID(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->first();
    }

    /**
     * Get all albums.
     * @return mixed
     */
    public function getAlbums()
    {
        return $this->model
            ->join('media_gallery_photos', 'media_gallery_photos.id', '=', 'media_gallery_albums.cover_id')
            ->orderBy('media_gallery_albums.id', 'DESC')
            ->get();
    }

    /**
     * Get latest albums.
     * @return mixed
     */
    public function getLatestAlbums()
    {
        return $this->model
            ->join('media_gallery_photos', 'media_gallery_photos.id', '=', 'media_gallery_albums.cover_id')
            ->orderBy('media_gallery_photos.id', 'DESC')
            ->take(self::LIMIT)
            ->get();
    }
}
