<?php

namespace App\paddock\Media\Gallery\Repositories;

use App\paddock\Media\Gallery\Models\MediaGalleryPhotos;

class MediaGalleryPhotosRepository
{
    /**
     * @var MediaGalleryPhotos
     */
    private $model;

    /**
     * MediaGalleryPhotosRepository constructor.
     * @param MediaGalleryPhotos $mediaGalleryPhotos
     */
    public function __construct(MediaGalleryPhotos $mediaGalleryPhotos)
    {
        $this->model = $mediaGalleryPhotos;
    }

    /**
     * Get images of an album.
     * @param int $album_id
     * @return mixed
     */
    public function getImagesByAlbumID(int $album_id)
    {
        return $this->model
            ->where('album_id', $album_id)
            ->get();
    }
}
