<?php

namespace App\paddock\SocialMedias\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\paddock\SocialMedias\Models\SocialMedias.
 *
 * @property int $id
 * @property string $name
 * @property string $avatar
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $youtube
 * @mixin \Eloquent
 */
class SocialMedias extends Model
{
    protected $table = 'social_medias';
}
