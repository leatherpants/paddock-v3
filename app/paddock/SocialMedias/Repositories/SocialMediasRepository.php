<?php

namespace App\paddock\SocialMedias\Repositories;

use App\paddock\SocialMedias\Models\SocialMedias;

class SocialMediasRepository
{
    /**
     * @var SocialMedias
     */
    private $model;

    /**
     * SocialMediasRepository constructor.
     * @param SocialMedias $socialMedias
     */
    public function __construct(SocialMedias $socialMedias)
    {
        $this->model = $socialMedias;
    }

    /**
     * Get all social media entries.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->get();
    }
}
