<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Countries\Requests\AddCountryRequest;
use App\paddock\Countries\Requests\EditCountryRequest;
use App\paddock\Countries\Repositories\CountriesRepository;

class CountriesController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * CountriesController constructor.
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(CountriesRepository $countriesRepository)
    {
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * Get countries page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.countries.index')
            ->with('countries', $countries);
    }

    /**
     * Get country form to add.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.countries.add');
    }

    /**
     * Store new countries.
     * @param AddCountryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddCountryRequest $request)
    {
        $data = [
            'code' => trim($request->code),
            'name' => trim($request->name),
        ];

        $this->countriesRepository->createCountry($data);

        return redirect()
            ->route('backend.countries');
    }

    /**
     * Get country by code.
     * @param string $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $code)
    {
        $country = $this->countriesRepository->getCountryByCode($code);

        return view('backend.countries.edit')
            ->with('country', $country);
    }

    /**
     * Update countries.
     * @param EditCountryRequest $request
     * @param string $country
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditCountryRequest $request, string $country)
    {
        $data = [
            'code' => trim($request->code),
            'name' => trim($request->name),
        ];

        $this->countriesRepository->editCountry($data, $country);

        return redirect()
            ->route('backend.countries');
    }
}
