<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Cars\Repositories\CarsRepository;
use App\paddock\Staffs\Repositories\StaffsRepository;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Countries\Repositories\CountriesRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;

class HomeController extends Controller
{
    /**
     * @var CarsRepository
     */
    private $carsRepository;

    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var StaffsRepository
     */
    private $staffsRepository;

    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * HomeController constructor.
     * @param CarsRepository $carsRepository
     * @param CountriesRepository $countriesRepository
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param SeasonsRepository $seasonsRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     * @param StaffsRepository $staffsRepository
     * @param TracksRepository $tracksRepository
     */
    public function __construct(
        CarsRepository $carsRepository,
        CountriesRepository $countriesRepository,
        GrandPrixsRepository $grandPrixsRepository,
        SeasonsRepository $seasonsRepository,
        SeasonsRacesRepository $seasonsRacesRepository,
        StaffsRepository $staffsRepository,
        TracksRepository $tracksRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->seasonsRepository = $seasonsRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->staffsRepository = $staffsRepository;
        $this->tracksRepository = $tracksRepository;
        $this->carsRepository = $carsRepository;
    }

    /**
     * Show backend page of paddock.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cars = $this->carsRepository->getCount();
        $countries = $this->countriesRepository->getCount();
        $grandprixs = $this->grandPrixsRepository->getCount();
        $seasons = $this->seasonsRepository->getCount();
        $seasonRaces = $this->seasonsRacesRepository->getCount();
        $staffs = $this->staffsRepository->getCount();
        $tracks = $this->tracksRepository->getCount();

        return view('backend.index')
            ->with('cars', $cars)
            ->with('countries', $countries)
            ->with('grandprixs', $grandprixs)
            ->with('seasons', $seasons)
            ->with('seasonRaces', $seasonRaces)
            ->with('staffs', $staffs)
            ->with('tracks', $tracks);
    }
}
