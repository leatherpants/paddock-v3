<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Languages\Repositories\LanguagesRepository;

class LanguagesController extends Controller
{
    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * LanguagesController constructor.
     * @param LanguagesRepository $languagesRepository
     */
    public function __construct(LanguagesRepository $languagesRepository)
    {
        $this->languagesRepository = $languagesRepository;
    }

    /**
     * Get all languages.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $languages = $this->languagesRepository->getAll();

        return view('backend.languages.index')
            ->with('languages', $languages);
    }
}
