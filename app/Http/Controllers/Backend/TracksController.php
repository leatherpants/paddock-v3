<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Tracks\Requests\AddTrackRequest;
use App\paddock\Tracks\Requests\EditTrackRequest;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Countries\Repositories\CountriesRepository;

class TracksController extends Controller
{
    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * TracksController constructor.
     * @param TracksRepository $tracksRepository
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(
        TracksRepository $tracksRepository,
        CountriesRepository $countriesRepository
    ) {
        $this->tracksRepository = $tracksRepository;
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * Get tracks page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tracks = $this->tracksRepository->getAll();

        return view('backend.tracks.index')
            ->with('tracks', $tracks);
    }

    /**
     * Get tracks form to add.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.tracks.add')
            ->with('countries', $countries);
    }

    /**
     * Store new track.
     * @param AddTrackRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddTrackRequest $request)
    {
        $data = [
            'country_id' => trim($request->country_id),
            'name' => trim($request->name),
            'slug' => trim(str_slug($request->name)),
            'timezone' => trim(str_slug($request->timezone)),
            'active' => ($request->has('active') === true) ? true : false,
        ];

        $this->tracksRepository->createTrack($data);

        return redirect()
            ->route('backend.tracks');
    }

    /**
     * Get track by slug.
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $slug)
    {
        $track = $this->tracksRepository->getTrackBySlug($slug);

        $countries = $this->countriesRepository->getAll();

        return view('backend.tracks.edit')
            ->with('track', $track)
            ->with('countries', $countries);
    }

    /**
     * Update track.
     * @param EditTrackRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditTrackRequest $request)
    {
        $data = [
            'id' => trim($request->id),
            'country_id' => trim($request->country_id),
            'name' => trim($request->name),
            'slug' => trim(str_slug($request->name)),
            'timezone' => trim(str_slug($request->timezone)),
            'active' => ($request->has('active') === true) ? true : false,
        ];

        $this->tracksRepository->editTrack($data);

        return redirect()
            ->route('backend.tracks');
    }
}
