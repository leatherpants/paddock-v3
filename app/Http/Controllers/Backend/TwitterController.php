<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Twitter\Repositories\TwitterAccountsRepository;

class TwitterController extends Controller
{
    /**
     * @var TwitterAccountsRepository
     */
    private $twitterAccountsRepository;

    /**
     * TwitterController constructor.
     * @param TwitterAccountsRepository $twitterAccountsRepository
     */
    public function __construct(TwitterAccountsRepository $twitterAccountsRepository)
    {
        $this->twitterAccountsRepository = $twitterAccountsRepository;
    }

    /**
     * Get Twitter index.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $accounts = $this->twitterAccountsRepository->getAll();

        return view('backend.twitter.index')
            ->with('accounts', $accounts);
    }
}
