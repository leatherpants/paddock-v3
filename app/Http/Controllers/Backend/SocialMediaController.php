<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\SocialMedias\Repositories\SocialMediasRepository;

class SocialMediaController extends Controller
{
    /**
     * @var SocialMediasRepository
     */
    private $socialMediasRepository;

    /**
     * SocialMediaController constructor.
     * @param SocialMediasRepository $socialMediasRepository
     */
    public function __construct(SocialMediasRepository $socialMediasRepository)
    {
        $this->socialMediasRepository = $socialMediasRepository;
    }

    /**
     * Get all entries of Social Media.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $entries = $this->socialMediasRepository->getAll();

        return view('backend.socialmedias.index')
            ->with('entries', $entries);
    }
}
