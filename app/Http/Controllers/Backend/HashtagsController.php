<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Hashtags\Repositories\HashtagsRepository;

class HashtagsController extends Controller
{
    /**
     * @var HashtagsRepository
     */
    private $hashtagsRepository;

    /**
     * HashtagsController constructor.
     * @param HashtagsRepository $hashtagsRepository
     */
    public function __construct(HashtagsRepository $hashtagsRepository)
    {
        $this->hashtagsRepository = $hashtagsRepository;
    }

    /**
     * Get all hashtags.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $hashtags = $this->hashtagsRepository->getAll();

        return view('backend.hashtags.index')
            ->with('hashtags', $hashtags);
    }
}
