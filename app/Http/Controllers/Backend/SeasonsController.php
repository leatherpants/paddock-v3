<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Seasons\Requests\AddSeasonRequest;
use App\paddock\Seasons\Requests\EditSeasonRequest;
use App\paddock\Seasons\Repositories\SeasonsRepository;

class SeasonsController extends Controller
{
    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * SeasonsController constructor.
     * @param SeasonsRepository $seasonsRepository
     */
    public function __construct(SeasonsRepository $seasonsRepository)
    {
        $this->seasonsRepository = $seasonsRepository;
    }

    /**
     * Get seasons page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $seasons = $this->seasonsRepository->getAll();

        return view('backend.seasons.index')
            ->with('seasons', $seasons);
    }

    /**
     * Get season form to add.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.seasons.add');
    }

    /**
     * Store new seasons.
     * @param AddSeasonRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddSeasonRequest $request)
    {
        $data = [
            'season' => trim($request->season),
        ];

        $this->seasonsRepository->createSeason($data);

        return redirect()
            ->route('backend.seasons');
    }

    /**
     * Get season by season.
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $season)
    {
        $season = $this->seasonsRepository->getSeasonBySeason($season);

        return view('backend.seasons.edit')
            ->with('season', $season);
    }

    /**
     * Update season.
     * @param EditSeasonRequest $request
     * @param int $season
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditSeasonRequest $request, int $season)
    {
        $data = [
            'season' => trim($request->season),
        ];

        $this->seasonsRepository->editSeason($data, $season);

        return redirect()
            ->route('backend.seasons');
    }
}
