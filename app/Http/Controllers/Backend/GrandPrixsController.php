<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\GrandPrixs\Requests\AddGrandPrixRequest;
use App\paddock\GrandPrixs\Requests\EditGrandPrixRequest;
use App\paddock\Countries\Repositories\CountriesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;

class GrandPrixsController extends Controller
{
    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * GrandPrixsController constructor.
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(
        GrandPrixsRepository $grandPrixsRepository,
        CountriesRepository $countriesRepository
    ) {
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * Get grand prixs page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $grandprixs = $this->grandPrixsRepository->getAll();

        return view('backend.grandprixs.index')
            ->with('grandprixs', $grandprixs);
    }

    /**
     * Get grand prixs form to add.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.grandprixs.add')
            ->with('countries', $countries);
    }

    /**
     * Store new grand prix.
     * @param AddGrandPrixRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddGrandPrixRequest $request)
    {
        $data = [
            'country_id' => trim($request->country_id),
            'name' => trim($request->name),
            'slug' => trim(str_slug($request->name)),
            'full_name' => trim($request->full_name),
            'hashtag' => '#'.trim($request->hashtag),
            'emoji' => trim($request->emoji),
            'active' => ($request->has('active') === true) ? true : false,
        ];

        $this->grandPrixsRepository->createGrandPrix($data);

        return redirect()
            ->route('backend.grandprixs');
    }

    /**
     * Get grand prix by slug.
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $slug)
    {
        $grandprix = $this->grandPrixsRepository->getGrandPrixBySlug($slug);

        $countries = $this->countriesRepository->getAll();

        return view('backend.grandprixs.edit')
            ->with('grandprix', $grandprix)
            ->with('countries', $countries);
    }

    /**
     * Update grand prix.
     * @param EditGrandPrixRequest $request
     * @param string $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditGrandPrixRequest $request, string $slug)
    {
        $grandprix = $this->grandPrixsRepository->getGrandPrixBySlug($slug);

        $data = [
            'id' => $grandprix->id,
            'country_id' => trim($request->country_id),
            'name' => trim($request->name),
            'slug' => trim(str_slug($request->name)),
            'full_name' => trim($request->full_name),
            'hashtag' => '#'.trim($request->hashtag),
            'emoji' => trim($request->emoji),
            'active' => ($request->has('active') === true) ? true : false,
        ];

        $this->grandPrixsRepository->editGrandPrix($data);

        return redirect()
            ->route('backend.grandprixs');
    }

    /**
     * Get list of all grand prix.
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function races(string $slug)
    {
        $grandprix = $this->grandPrixsRepository->getGrandPrixBySlug($slug);

        $races = $this->grandPrixsRepository->getGrandPrixRacesByID($grandprix->id);

        return view('backend.grandprixs.races')
            ->with('grandprix', $grandprix)
            ->with('races', $races);
    }
}
