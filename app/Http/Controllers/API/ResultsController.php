<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\paddock\Results\Repositories\ResultsTotalRepository;

class ResultsController extends Controller
{
    /**
     * @var ResultsTotalRepository
     */
    private $resultsTotalRepository;

    /**
     * ResultsController constructor.
     * @param ResultsTotalRepository $resultsTotalRepository
     */
    public function __construct(ResultsTotalRepository $resultsTotalRepository)
    {
        $this->resultsTotalRepository = $resultsTotalRepository;
    }

    /**
     * Get results of the season.
     * @param int $season
     * @return \Illuminate\Http\JsonResponse
     */
    public function season(int $season)
    {
        $laps = $this->resultsTotalRepository->getSeasonLaps($season);
        $podiums = $this->resultsTotalRepository->getSeasonPodiums($season);
        $points = $this->resultsTotalRepository->getSeasonPoints($season);
        $poles = $this->resultsTotalRepository->getSeasonPoles($season);
        $wins = $this->resultsTotalRepository->getSeasonWins($season);

        $data = [
            'laps'    => $laps,
            'podiums' => $podiums,
            'points'  => $points,
            'poles'   => $poles,
            'wins'    => $wins,
            'season'  => $season,
        ];

        return response()
            ->json($data, 200);
    }

    /**
     * Get total results of Scuderia Ferrari.
     * @return \Illuminate\Http\JsonResponse
     */
    public function total()
    {
        $totalLaps = $this->resultsTotalRepository->getTotalLaps();
        $totalPodiums = $this->resultsTotalRepository->getTotalPodiums();
        $totalPoints = $this->resultsTotalRepository->getTotalPoints();
        $totalPoles = $this->resultsTotalRepository->getTotalPoles();
        $totalWins = $this->resultsTotalRepository->getTotalWins();

        $total = [
            'laps'    => $totalLaps,
            'podiums' => $totalPodiums,
            'points'  => $totalPoints,
            'poles'   => $totalPoles,
            'wins'    => $totalWins,
        ];

        return response()
            ->json($total, 200);
    }
}
