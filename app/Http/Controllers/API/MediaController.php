<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\paddock\Media\Youtube\Repositories\MediaYoutubeVideosRepository;

class MediaController extends Controller
{
    /**
     * @var MediaYoutubeVideosRepository
     */
    private $mediaYoutubeVideosRepository;

    /**
     * MediaController constructor.
     * @param MediaYoutubeVideosRepository $mediaYoutubeVideosRepository
     */
    public function __construct(MediaYoutubeVideosRepository $mediaYoutubeVideosRepository)
    {
        $this->mediaYoutubeVideosRepository = $mediaYoutubeVideosRepository;
    }

    /**
     * Get latest video by youtube.
     * @return \Illuminate\Http\JsonResponse
     */
    public function latestYouTubeVideo()
    {
        $video = $this->mediaYoutubeVideosRepository->getLatestYouTubeVideo();

        $data = [
            'link' => route('media.youtube.videos'),
            'thumbnail' => 'https://img.youtube.com/vi/'.$video->youtube_id.'/0.jpg',
            'title' => $video->title,
        ];

        return response()
            ->json($data, 200);
    }
}
