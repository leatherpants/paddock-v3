<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;

class TimeZoneController extends Controller
{
    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * TimeZoneController constructor.
     * @param SeasonsRacesRepository $seasonsRacesRepository
     * @param SettingsRepository $settingsRepository
     * @param TracksRepository $tracksRepository
     */
    public function __construct(
        SeasonsRacesRepository $seasonsRacesRepository,
        SettingsRepository $settingsRepository,
        TracksRepository $tracksRepository
    ) {
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->settingsRepository = $settingsRepository;
        $this->tracksRepository = $tracksRepository;
    }

    /**
     * Get timezone of grand prix.
     * @return \Illuminate\Http\JsonResponse
     */
    public function away()
    {
        $season = $this->settingsRepository->getSettings('season')->value;

        $grandprix = $this->settingsRepository->getSettings('gp_id')->value;

        $gp = $this->seasonsRacesRepository->getCurrentRace((int) $season, (int) $grandprix);

        $track = $this->tracksRepository->getTrackByID($gp->track_id);

        $timezone = isset($track->timezone) ? $track->timezone : null;

        return response()
            ->json($timezone, 200);
    }

    /**
     * Get timezone of user.
     * @return \Illuminate\Http\JsonResponse
     */
    public function home()
    {
        $timezone = auth()->user()->timezone;

        return response()
            ->json($timezone, 200);
    }
}
