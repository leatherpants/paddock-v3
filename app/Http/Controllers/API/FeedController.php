<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedRepository;
use App\paddock\Feed\Repositories\FeedFDARepository;
use App\paddock\Feed\Repositories\FeedFerrariRepository;
use App\paddock\Feed\Repositories\FeedHistoryRepository;
use App\paddock\Feed\Repositories\FeedMotorsportRepository;

class FeedController extends Controller
{
    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @var FeedFDARepository
     */
    private $feedFDARepository;

    /**
     * @var FeedFerrariRepository
     */
    private $feedFerrariRepository;

    /**
     * @var FeedHistoryRepository
     */
    private $feedHistoryRepository;

    /**
     * @var FeedMotorsportRepository
     */
    private $feedMotorsportRepository;

    /**
     * FeedController constructor.
     * @param FeedRepository $feedRepository
     * @param FeedFDARepository $feedFDARepository
     * @param FeedFerrariRepository $feedFerrariRepository
     * @param FeedHistoryRepository $feedHistoryRepository
     * @param FeedMotorsportRepository $feedMotorsportRepository
     */
    public function __construct(
        FeedRepository $feedRepository,
        FeedFDARepository $feedFDARepository,
        FeedFerrariRepository $feedFerrariRepository,
        FeedHistoryRepository $feedHistoryRepository,
        FeedMotorsportRepository $feedMotorsportRepository
    ) {
        $this->feedRepository = $feedRepository;
        $this->feedFDARepository = $feedFDARepository;
        $this->feedFerrariRepository = $feedFerrariRepository;
        $this->feedHistoryRepository = $feedHistoryRepository;
        $this->feedMotorsportRepository = $feedMotorsportRepository;
    }

    /**
     * Collect stories from all feeds.
     * @return \Illuminate\Http\JsonResponse
     */
    public function activities()
    {
        $lang = auth()->user()->lang;

        $data = $this->feedRepository->feed($lang);

        $response = [
            'data' => $data,
        ];

        return response()
            ->json($response, 200);
    }

    /**
     * Collect latest stories of the fda feed.
     * @param string $lang
     * @return \Illuminate\Http\JsonResponse
     */
    public function fdaStories(string $lang)
    {
        $datas = [];

        $data = $this->feedFDARepository->getFeedByLang($lang);

        foreach ($data as $i => $info) {
            $datas[$i]['date'] = $info->created_at->format('d.m.Y');
            $datas[$i]['title'] = $info->title;
            $datas[$i]['image'] = $info->image;
            $datas[$i]['link'] = route('feed.fda.entry', ['date' => $info->created_at->format('Y-m-d')]);
        }

        $response = [
            'current_page' => $data->currentPage(),
            'data' => $datas,
            'first_page_url' => $data->previousPageUrl(),
            'last_page' => $data->lastPage(),
            'next_page_url' => $data->nextPageUrl(),
            'per_page' => $data->perPage(),
            'total' => $data->count(),
        ];

        return response()
            ->json($response, 200);
    }

    /**
     * Collect latest stories of the ferrari feed.
     * @param string $lang
     * @return \Illuminate\Http\JsonResponse
     */
    public function ferrariStories(string $lang)
    {
        $datas = [];

        $data = $this->feedFerrariRepository->getFeedByLang($lang);

        foreach ($data as $i => $info) {
            $datas[$i]['date'] = $info->created_at->timezone(auth()->user()->timezone)->format('d.m.Y');
            $datas[$i]['session'] = $info->prepareSession();
            $datas[$i]['title'] = $info->title;
            $datas[$i]['image'] = $info->image;
            $datas[$i]['link'] = $info->link;
            $datas[$i]['hashtag'] = $info->hashtags;
            $datas[$i]['hashtag_link'] = route('feed.ferrari.hashtag', ['hashtag' => substr($info->hashtags, 1)]);
            $datas[$i]['ferrari_link'] = route('feed.ferrari.entry', ['date' => $info->created_at->format('Y-m-d'), 'session' => $info->session]);
        }

        $response = [
            'current_page' => $data->currentPage(),
            'data' => $datas,
            'first_page_url' => $data->previousPageUrl(),
            'last_page' => $data->lastPage(),
            'next_page_url' => $data->nextPageUrl(),
            'per_page' => $data->perPage(),
            'total' => $data->count(),
        ];

        return response()
            ->json($response, 200);
    }

    /**
     * Collect latest stories of the history feed.
     * @param string $lang
     * @return \Illuminate\Http\JsonResponse
     */
    public function historyStories(string $lang)
    {
        $datas = [];

        $data = $this->feedHistoryRepository->getFeedByLang($lang);

        foreach ($data as $i => $info) {
            $datas[$i]['date'] = $info->date->format('d.m.Y');
            $datas[$i]['title'] = $info->title;
            $datas[$i]['image'] = $info->image;
            $datas[$i]['link'] = $info->link;
            $datas[$i]['history_link'] = route('feed.history.entry', ['date' => $info->date->format('Y-m-d')]);
        }

        $response = [
            'current_page' => $data->currentPage(),
            'data' => $datas,
            'first_page_url' => $data->previousPageUrl(),
            'last_page' => $data->lastPage(),
            'next_page_url' => $data->nextPageUrl(),
            'per_page' => $data->perPage(),
            'total' => $data->count(),
        ];

        return response()
            ->json($response, 200);
    }

    /**
     * Collect latest articles of the various feeds.
     * @return \Illuminate\Http\JsonResponse
     */
    public function latestArticles()
    {
        $lang = auth()->user()->lang;

        $result = $this->feedRepository->latestArticles($lang);

        $data = [
            'data' => $result,
            'link' => route('activities'),
        ];

        return response()
            ->json($data, 200);
    }

    /**
     * Collect latest stories of the motorsport feed.
     * @param string $lang
     * @return \Illuminate\Http\JsonResponse
     */
    public function motorsportStories(string $lang)
    {
        $datas = [];

        $data = $this->feedMotorsportRepository->getFeedByLang($lang);

        foreach ($data as $i => $info) {
            $datas[$i]['date'] = $info->created_at->format('d.m.Y');
            $datas[$i]['title'] = $info->title;
            $datas[$i]['image'] = $info->image;
            $datas[$i]['link'] = route('feed.motorsport.article', ['date' => $info->created_at->format('Y-m-d')]);
        }

        $response = [
            'current_page' => $data->currentPage(),
            'data' => $datas,
            'first_page_url' => $data->previousPageUrl(),
            'last_page' => $data->lastPage(),
            'next_page_url' => $data->nextPageUrl(),
            'per_page' => $data->perPage(),
            'total' => $data->count(),
        ];

        return response()
            ->json($response, 200);
    }
}
