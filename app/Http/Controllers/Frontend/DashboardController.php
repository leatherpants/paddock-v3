<?php

namespace App\Http\Controllers\Frontend;

use App\paddock\Results\Standings;
use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedFerrariRepository;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\Tests\Repositories\TestsResultsRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\Results\Repositories\ResultsTimingsRepository;
use App\paddock\Seasons\Repositories\SeasonsSessionsRepository;
use App\paddock\Reports\Repositories\ReportsTechnicalsRepository;
use App\paddock\Media\Youtube\Repositories\MediaYoutubeVideosRepository;

class DashboardController extends Controller
{
    /**
     * @var FeedFerrariRepository
     */
    private $feedFerrariRepository;

    /**
     * @var MediaYoutubeVideosRepository
     */
    private $mediaYoutubeVideosRepository;

    /**
     * @var ReportsTechnicalsRepository
     */
    private $reportsTechnicalsRepository;

    /**
     * @var ResultsTimingsRepository
     */
    private $resultsTimingsRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var SeasonsSessionsRepository
     */
    private $seasonsSessionsRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * @var Standings
     */
    private $standings;

    /**
     * @var TestsResultsRepository
     */
    private $testsResultsRepository;

    /**
     * DashboardController constructor.
     * @param FeedFerrariRepository $feedFerrariRepository
     * @param MediaYoutubeVideosRepository $mediaYoutubeVideosRepository
     * @param ReportsTechnicalsRepository $reportsTechnicalsRepository
     * @param ResultsTimingsRepository $resultsTimingsRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     * @param SeasonsSessionsRepository $seasonsSessionsRepository
     * @param SettingsRepository $settingsRepository
     * @param Standings $standings
     * @param TestsResultsRepository $testsResultsRepository
     */
    public function __construct(
        FeedFerrariRepository $feedFerrariRepository,
        MediaYoutubeVideosRepository $mediaYoutubeVideosRepository,
        ReportsTechnicalsRepository $reportsTechnicalsRepository,
        ResultsTimingsRepository $resultsTimingsRepository,
        SeasonsRacesRepository $seasonsRacesRepository,
        SeasonsSessionsRepository $seasonsSessionsRepository,
        SettingsRepository $settingsRepository,
        Standings $standings,
        TestsResultsRepository $testsResultsRepository
    ) {
        $this->feedFerrariRepository = $feedFerrariRepository;
        $this->mediaYoutubeVideosRepository = $mediaYoutubeVideosRepository;
        $this->reportsTechnicalsRepository = $reportsTechnicalsRepository;
        $this->resultsTimingsRepository = $resultsTimingsRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->seasonsSessionsRepository = $seasonsSessionsRepository;
        $this->settingsRepository = $settingsRepository;
        $this->standings = $standings;
        $this->testsResultsRepository = $testsResultsRepository;
    }

    /**
     * Show dashboard of paddock.
     * @return mixed
     */
    public function dashboard()
    {
        $season = $this->settingsRepository->getSettings('season')->value;

        $gp = $this->settingsRepository->getSettings('gp_id')->value;

        $race = $this->seasonsRacesRepository->getCurrentRace((int) $season, (int) $gp);

        $sessions = $this->seasonsSessionsRepository->getSessionsBySeasonAndGP((int) $season, (int) $gp);

        $standings = $this->standings->getTotalStandings((int) $season);

        $video = $this->mediaYoutubeVideosRepository->getLatestYouTubeVideo();

        return view('dashboard.dashboard')
            ->with('race', $race)
            ->with('sessions', $sessions)
            ->with('standings', $standings)
            ->with('video', $video);
    }

    /**
     * Show dashboard of the race.
     * @return mixed
     */
    public function race()
    {
        $user = auth()->user();

        $season = $this->settingsRepository->getSettings('season')->value;

        $gp = $this->settingsRepository->getSettings('gp_id')->value;

        $race = $this->seasonsRacesRepository->getCurrentRace((int) $season, (int) $gp);

        $feeds = $this->feedFerrariRepository->getFeedByHashtagAndLang($race->grandprix->hashtag, $user->lang);

        $pu = $this->reportsTechnicalsRepository->getTechnicalReportBySeasonAndGP((int) $season, (int) $gp);

        $sessions = $this->seasonsSessionsRepository->getSessionsBySeasonAndGP((int) $season, (int) $gp);

        $standings = $this->standings->getTotalStandings((int) $season);

        $times1 = $this->resultsTimingsRepository->getTimesBySeasonGPAndDriver((int) $season, (int) $gp, 76);

        $times2 = $this->resultsTimingsRepository->getTimesBySeasonGPAndDriver((int) $season, (int) $gp, 72);

        return view('dashboard.race')
            ->with('feeds', $feeds)
            ->with('pu', $pu)
            ->with('race', $race)
            ->with('sessions', $sessions)
            ->with('standings', $standings)
            ->with('times1', $times1)
            ->with('times2', $times2);
    }

    /**
     * Get testing dashboard.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function test()
    {
        $olders = $this->testsResultsRepository->getResultsBySeasonAndTestweek(2018, 1);
        $results = $this->testsResultsRepository->getResultsBySeasonAndTestweek(2018, 2);

        return view('dashboard.test')
            ->with('olders', $olders)
            ->with('results', $results);
    }
}
