<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Results\Repositories\ResultsStaffsRepository;
use App\paddock\Staffs\Repositories\StaffsRepository;

class StaffsController extends Controller
{
    /**
     * @var ResultsStaffsRepository
     */
    private $resultsStaffsRepository;

    /**
     * @var StaffsRepository
     */
    private $staffsRepository;

    /**
     * StaffsController constructor.
     * @param ResultsStaffsRepository $resultsStaffsRepository
     * @param StaffsRepository $staffsRepository
     */
    public function __construct(ResultsStaffsRepository $resultsStaffsRepository, StaffsRepository $staffsRepository)
    {
        $this->resultsStaffsRepository = $resultsStaffsRepository;
        $this->staffsRepository = $staffsRepository;
    }

    /**
     * Get all known staff by the Scuderia Ferrari.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $staffs = $this->staffsRepository->getAll();

        return view('staffs.index')
            ->with('staffs', $staffs);
    }

    /**
     * Get staff by slug.
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $slug)
    {
        $staff = $this->staffsRepository->getStaffBySlug($slug);

        // check, if staff exists
        if ($staff === null) {
            abort(404);
        }

        $podium = $this->resultsStaffsRepository->getPodiumsByStaffID($staff->id);
        $poles = $this->resultsStaffsRepository->getPolesByStaffID($staff->id);
        $points = $this->resultsStaffsRepository->getPointsByStaffID($staff->id);
        $races = $this->resultsStaffsRepository->getRacesByStaffID($staff->id);
        $wins = $this->resultsStaffsRepository->getWinsByStaffID($staff->id);

        $tpodium = $this->resultsStaffsRepository->getTeamPodiumsByStaffID($staff->id);
        $tpoles = $this->resultsStaffsRepository->getTeamPolesByStaffID($staff->id);
        $tpoints = $this->resultsStaffsRepository->getTeamPointsByStaffID($staff->id);
        $traces = $this->resultsStaffsRepository->getTeamRacesByStaffID($staff->id);
        $twins = $this->resultsStaffsRepository->getTeamWinsByStaffID($staff->id);

        return view('staffs.view')
            ->with('podium', $podium)
            ->with('poles', $poles)
            ->with('points', $points)
            ->with('races', $races)
            ->with('staff', $staff)
            ->with('tpodium', $tpodium)
            ->with('tpoles', $tpoles)
            ->with('tpoints', $tpoints)
            ->with('traces', $traces)
            ->with('twins', $twins)
            ->with('wins', $wins);
    }
}
