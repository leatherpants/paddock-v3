<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedMotorsportRepository;

class FeedMotorsportController extends Controller
{
    /**
     * @var FeedMotorsportRepository
     */
    private $feedMotorsportRepository;

    /**
     * FeedMotorsportController constructor.
     * @param FeedMotorsportRepository $feedMotorsportRepository
     */
    public function __construct(FeedMotorsportRepository $feedMotorsportRepository)
    {
        $this->feedMotorsportRepository = $feedMotorsportRepository;
    }

    /**
     * Get motorsport feed by the langasdas.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('feed.motorsport');
    }

    /**
     * Get article by date.
     * @param string $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article(string $date)
    {
        $article = $this->feedMotorsportRepository->getArticleByDate($date);

        if ($article === null) {
            abort(404);
        }

        $articles = $this->feedMotorsportRepository->getArticlesByDate($date);

        return view('feed.motorsport_entry')
            ->with('article', $article)
            ->with('articles', $articles);
    }
}
