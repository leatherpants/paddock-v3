<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Results\Repositories\ResultsRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;
use App\paddock\Results\Repositories\ResultsSeasonsRepository;
use App\paddock\Results\Repositories\ResultsGrandPrixRepository;

class ResultsController extends Controller
{
    /**
     * @var ResultsRepository
     */
    private $resultsRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var ResultsSeasonsRepository
     */
    private $resultsSeasonsRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var ResultsGrandPrixRepository
     */
    private $resultsGrandPrixRepository;

    /**
     * ResultsController constructor.
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param ResultsGrandPrixRepository $resultsGrandPrixRepository
     * @param ResultsRepository $resultsRepository
     * @param ResultsSeasonsRepository $resultsSeasonsRepository
     * @param SeasonsRepository $seasonsRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     */
    public function __construct(
        GrandPrixsRepository $grandPrixsRepository,
        ResultsGrandPrixRepository $resultsGrandPrixRepository,
        ResultsRepository $resultsRepository,
        ResultsSeasonsRepository $resultsSeasonsRepository,
        SeasonsRepository $seasonsRepository,
        SeasonsRacesRepository $seasonsRacesRepository
    ) {
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->resultsGrandPrixRepository = $resultsGrandPrixRepository;
        $this->resultsRepository = $resultsRepository;
        $this->resultsSeasonsRepository = $resultsSeasonsRepository;
        $this->seasonsRepository = $seasonsRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
    }

    /**
     * Get results overview.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $grandprixs = $this->resultsRepository->getAllResultsByGrandPrix();
        $season = 2018;

        return view('results.index')
            ->with('grandprixs', $grandprixs)
            ->with('season', $season);
    }

    /**
     * Get race and quali results of season and grand prix.
     * @param int $season
     * @param string $gp
     * @return mixed
     */
    public function result(int $season, string $gp)
    {
        $grandprix = $this->grandPrixsRepository->getGrandPrixBySlug($gp);

        $event = $this->seasonsRacesRepository->getRaceBySeasonAndGPID($season, $grandprix->id);

        if (!$event) {
            abort(404);
        }

        $raceresults = $this->resultsRepository->getRaceResultsOfSeasonAndGrandPrix($season, $grandprix->id);
        $qualiresults = $this->resultsRepository->getQualiResultsOfSeasonAndGrandPrix($season, $grandprix->id);

        return view('results.result')
            ->with('event', $event)
            ->with('qualiresults', $qualiresults)
            ->with('raceresults', $raceresults)
            ->with('season', $season);
    }

    /**
     * Get all F1 seasons for results.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function seasons()
    {
        $seasons = $this->seasonsRepository->getAll();

        return view('results.seasons')
            ->with('seasons', $seasons);
    }

    /**
     * Get races of F1 season.
     * @param int $season
     * @return mixed
     */
    public function season(int $season)
    {
        $races = $this->seasonsRacesRepository->getRacesBySeason($season);

        $laps = $this->resultsSeasonsRepository->getSeasonLaps($season);
        $podiums = $this->resultsSeasonsRepository->getSeasonPodiums($season);
        $points = $this->resultsSeasonsRepository->getSeasonPoints($season);
        $poles = $this->resultsSeasonsRepository->getSeasonPoles($season);
        $wins = $this->resultsSeasonsRepository->getSeasonWins($season);

        $stats = [
            'laps' => $laps,
            'podiums' => $podiums,
            'points' => $points,
            'poles' => $poles,
            'wins' => $wins,
        ];

        return view('results.seasons_races')
            ->with('races', $races)
            ->with('season', $season)
            ->with('stats', $stats);
    }

    /**
     * Get results of team by grand prix.
     * @param string $slug
     * @return mixed
     */
    public function view(string $slug)
    {
        $grandprix = $this->grandPrixsRepository->getGrandPrixBySlug($slug);

        $laps = $this->resultsGrandPrixRepository->getGPLaps($grandprix['id']);
        $podiums = $this->resultsGrandPrixRepository->getGPPodiums($grandprix['id']);
        $points = $this->resultsGrandPrixRepository->getGPPoints($grandprix['id']);
        $poles = $this->resultsGrandPrixRepository->getGPPoles($grandprix['id']);
        $wins = $this->resultsGrandPrixRepository->getGPWins($grandprix['id']);

        $results = $this->resultsGrandPrixRepository->getGPResultsForRace($grandprix['id']);

        $stats = [
            'laps' => $laps,
            'podiums' => $podiums,
            'points' => $points,
            'poles' => $poles,
            'wins' => $wins,
        ];

        return view('results.view')
            ->with('grandprix', $grandprix)
            ->with('results', $results)
            ->with('stats', $stats);
    }
}
