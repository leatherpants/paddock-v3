<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\paddock\Feed\Repositories\FeedRepository;
use App\paddock\Seasons\Repositories\SeasonsSessionsRepository;
use App\paddock\SocialMedias\Repositories\SocialMediasRepository;

class HomeController extends Controller
{
    const SEASON = 2018;

    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @var SeasonsSessionsRepository
     */
    private $seasonsSessionsRepository;

    /**
     * @var SocialMediasRepository
     */
    private $socialMediasRepository;

    /**
     * HomeController constructor.
     * @param FeedRepository $feedRepository
     * @param SeasonsSessionsRepository $seasonsSessionsRepository
     * @param SocialMediasRepository $socialMediasRepository
     */
    public function __construct(
        FeedRepository $feedRepository,
        SeasonsSessionsRepository $seasonsSessionsRepository,
        SocialMediasRepository $socialMediasRepository
    ) {
        $this->feedRepository = $feedRepository;
        $this->seasonsSessionsRepository = $seasonsSessionsRepository;
        $this->socialMediasRepository = $socialMediasRepository;
    }

    /**
     * Show index page of paddock.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        // Check, if session already exists
        if (Auth::check()) {
            return redirect()
                ->route('dashboard');
        }

        return view('index');
    }

    /**
     * Show activities.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activities()
    {
        return view('activities');
    }

    /**
     * Show sessions of the season.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sessions()
    {
        $sessions = $this->seasonsSessionsRepository->getSessionsBySeason(self::SEASON);

        return view('sessions')
            ->with('sessions', $sessions);
    }

    /**
     * Show social medias of various things.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function socialMedia()
    {
        $entries = $this->socialMediasRepository->getAll();

        return view('socialmedia')
            ->with('entries', $entries);
    }
}
