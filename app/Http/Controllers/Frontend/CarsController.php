<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Cars\Repositories\CarsRepository;

class CarsController extends Controller
{
    /**
     * @var CarsRepository
     */
    private $carsRepository;

    /**
     * CarsController constructor.
     * @param CarsRepository $carsRepository
     */
    public function __construct(CarsRepository $carsRepository)
    {
        $this->carsRepository = $carsRepository;
    }

    /**
     * Get all cars.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cars = $this->carsRepository->getAll();

        return view('cars.index')
            ->with('cars', $cars);
    }

    /**
     * Get car.
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function car(string $slug)
    {
        $car = $this->carsRepository->getCar($slug);

        if ($car === null) {
            abort(404);
        }

        return view('cars.car')
            ->with('car', $car);
    }
}
