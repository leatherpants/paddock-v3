<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Users\Repositories\UsersRepository;
use App\paddock\Media\Gallery\Repositories\MediaGalleryAlbumsRepository;
use App\paddock\Media\Gallery\Repositories\MediaGalleryPhotosRepository;
use App\paddock\Media\Youtube\Repositories\MediaYoutubeVideosRepository;

class MediaController extends Controller
{
    /**
     * @var MediaGalleryAlbumsRepository
     */
    private $mediaGalleryAlbumsRepository;

    /**
     * @var MediaGalleryPhotosRepository
     */
    private $mediaGalleryPhotosRepository;

    /**
     * @var MediaYoutubeVideosRepository
     */
    private $mediaYoutubeVideosRepository;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * MediaController constructor.
     * @param MediaGalleryAlbumsRepository $mediaGalleryAlbumsRepository
     * @param MediaGalleryPhotosRepository $mediaGalleryPhotosRepository
     * @param MediaYoutubeVideosRepository $mediaYoutubeVideosRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(
        MediaGalleryAlbumsRepository $mediaGalleryAlbumsRepository,
        MediaGalleryPhotosRepository $mediaGalleryPhotosRepository,
        MediaYoutubeVideosRepository $mediaYoutubeVideosRepository,
        UsersRepository $usersRepository
    ) {
        $this->mediaYoutubeVideosRepository = $mediaYoutubeVideosRepository;
        $this->mediaGalleryAlbumsRepository = $mediaGalleryAlbumsRepository;
        $this->mediaGalleryPhotosRepository = $mediaGalleryPhotosRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Get various media.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $galleries = $this->mediaGalleryAlbumsRepository->getLatestAlbums();

        $videos = $this->mediaYoutubeVideosRepository->getLatestVideos();

        return view('media.index')
            ->with('galleries', $galleries)
            ->with('videos', $videos);
    }

    /**
     * Get all galleries.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function galleries()
    {
        $galleries = $this->mediaGalleryAlbumsRepository->getAlbums();

        return view('media.gallery.albums')
            ->with('galleries', $galleries);
    }

    /**
     * Get all images of an album.
     * @param int $album_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function galleriesImages(int $album_id)
    {
        $album = $this->mediaGalleryAlbumsRepository->getAlbumByID($album_id);

        if ($album === null) {
            abort(404);
        }

        $images = $this->mediaGalleryPhotosRepository->getImagesByAlbumID($album_id);

        return view('media.gallery.images')
            ->with('album', $album)
            ->with('images', $images);
    }

    /**
     * Set background image for logged in user.
     * @param int $image_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setBackgroundImage(int $image_id)
    {
        $data = [
            'user_id' => auth()->user()->id,
            'background_image' => $image_id,
        ];

        $this->usersRepository->updateBackgroundImage($data);

        return redirect()
            ->back()
            ->with('save', trans('common.update_background_image'));
    }

    /**
     * Get all videos.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function youtubeVideos()
    {
        $videos = $this->mediaYoutubeVideosRepository->getVideos();

        return view('media.youtube.videos')
            ->with('videos', $videos);
    }

    /**
     * Get youtube video by id.
     * @param string $youtube_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function youtubeVideo(string $youtube_id)
    {
        $video = $this->mediaYoutubeVideosRepository->getVideoByID($youtube_id);

        return view('media.youtube.video')
            ->with('video', $video);
    }
}
