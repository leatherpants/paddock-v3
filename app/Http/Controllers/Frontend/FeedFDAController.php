<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\paddock\Feed\Repositories\FeedFDARepository;

class FeedFDAController extends Controller
{
    /**
     * @var FeedFDARepository
     */
    private $feedFDARepository;

    /**
     * FeedFDAController constructor.
     * @param FeedFDARepository $feedFDARepository
     */
    public function __construct(FeedFDARepository $feedFDARepository)
    {
        $this->feedFDARepository = $feedFDARepository;
    }

    /**
     * Get FDA article.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('feed.fda');
    }

    /**
     * Get ferrari entry.
     * @param string $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function entry(string $date)
    {
        $lang = (Auth::check()) ? auth()->user()->lang : '';

        $entry = $this->feedFDARepository->getEntryByDateAndLang($date, $lang);

        if ($entry === null) {
            abort(404);
        }

        $entries = $this->feedFDARepository->getEntryByDate($date);

        return view('feed.fda_entry')
            ->with('entry', $entry)
            ->with('entries', $entries);
    }
}
