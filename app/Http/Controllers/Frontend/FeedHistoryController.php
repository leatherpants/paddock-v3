<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\paddock\Feed\Repositories\FeedHistoryRepository;

class FeedHistoryController extends Controller
{
    /**
     * @var FeedHistoryRepository
     */
    private $feedHistoryRepository;

    /**
     * FeedHistoryController constructor.
     * @param FeedHistoryRepository $feedHistoryRepository
     */
    public function __construct(FeedHistoryRepository $feedHistoryRepository)
    {
        $this->feedHistoryRepository = $feedHistoryRepository;
    }

    /**
     * Get history article.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('feed.history');
    }

    /**
     * Get history entry by date.
     * @param string $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function entry(string $date)
    {
        $lang = (Auth::check()) ? auth()->user()->lang : '';

        $entry = $this->feedHistoryRepository->getEntryByDateAndLang($date, $lang);

        if ($entry === null) {
            abort(404);
        }

        $entries = $this->feedHistoryRepository->getEntriesByDate($date);

        return view('feed.history_entry')
            ->with('entry', $entry)
            ->with('entries', $entries);
    }
}
