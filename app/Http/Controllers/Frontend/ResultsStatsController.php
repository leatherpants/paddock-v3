<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Results\Repositories\ResultsStatisticsRepository;

class ResultsStatsController extends Controller
{
    /**
     * @var ResultsStatisticsRepository
     */
    private $resultsStatisticsRepository;

    /**
     * ResultsStatsController constructor.
     * @param ResultsStatisticsRepository $resultsStatisticsRepository
     */
    public function __construct(ResultsStatisticsRepository $resultsStatisticsRepository)
    {
        $this->resultsStatisticsRepository = $resultsStatisticsRepository;
    }

    /**
     * All stats.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $results = $this->resultsStatisticsRepository->getAllGrandPrixStats();
        $resultStats = $this->resultsStatisticsRepository->getAllGrandPrixResultStats();

        return view('results.stats.index')
            ->with('results', $results)
            ->with('resultStats', $resultStats);
    }

    /**
     * Wins of the team.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wins()
    {
        $wins = $this->resultsStatisticsRepository->getWins();

        return view('results.stats.wins')
            ->with('wins', $wins);
    }
}
