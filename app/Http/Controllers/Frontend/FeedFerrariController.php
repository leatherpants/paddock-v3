<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\paddock\Feed\Repositories\FeedFerrariRepository;

class FeedFerrariController extends Controller
{
    /**
     * @var FeedFerrariRepository
     */
    private $feedFerrariRepository;

    /**
     * FeedFerrariController constructor.
     * @param FeedFerrariRepository $feedFerrariRepository
     */
    public function __construct(FeedFerrariRepository $feedFerrariRepository)
    {
        $this->feedFerrariRepository = $feedFerrariRepository;
    }

    /**
     * Get ferrari article.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('feed.ferrari');
    }

    /**
     * Get ferrari entry.
     * @param string $date
     * @param int $session
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function entry(string $date, int $session)
    {
        $lang = (Auth::check()) ? auth()->user()->lang : '';

        $entry = $this->feedFerrariRepository->getEntryByDateAndSessionAndLang($date, $session, $lang);

        if ($entry === null) {
            abort(404);
        }

        $entries = $this->feedFerrariRepository->getEntryByDateAndSession($date, $session);

        return view('feed.ferrari_entry')
            ->with('entry', $entry)
            ->with('entries', $entries);
    }

    /**
     * Get Ferrari entries by hashtag.
     * @param string $hashtag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hashtag(string $hashtag)
    {
        $lang = (Auth::check()) ? auth()->user()->lang : '';

        $entries = $this->feedFerrariRepository->getFeedByHashtagAndLang($hashtag, $lang);

        return view('feed.ferrari_hashtags')
            ->with('entries', $entries)
            ->with('hashtag', $hashtag);
    }
}
