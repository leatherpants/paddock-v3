<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Events\Repositories\EventsRepository;

class EventsController extends Controller
{
    /**
     * @var EventsRepository
     */
    private $eventsRepository;

    /**
     * EventsController constructor.
     * @param EventsRepository $eventsRepository
     */
    public function __construct(EventsRepository $eventsRepository)
    {
        $this->eventsRepository = $eventsRepository;
    }

    /**
     * Events index.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lang = auth()->user()->lang;

        $events = $this->eventsRepository->getEventsByLang($lang);

        return view('events.index')
            ->with('events', $events);
    }

    /**
     * Get event of the day by staff id.
     * @param string $date
     * @param int $staff_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function event(string $date, int $staff_id)
    {
        $tweets = $this->eventsRepository->getEventsByDateAndDriver($date, $staff_id);

        return view('events.event')
            ->with('tweets', $tweets);
    }
}
