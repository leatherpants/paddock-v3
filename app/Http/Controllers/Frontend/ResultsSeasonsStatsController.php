<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Routing\Controller;
use App\paddock\Results\Repositories\ResultsStatisticsRepository;

class ResultsSeasonsStatsController extends Controller
{
    /**
     * @var ResultsStatisticsRepository
     */
    private $resultsStatisticsRepository;

    /**
     * ResultsSeasonsStatsController constructor.
     * @param ResultsStatisticsRepository $resultsStatisticsRepository
     */
    public function __construct(ResultsStatisticsRepository $resultsStatisticsRepository)
    {
        $this->resultsStatisticsRepository = $resultsStatisticsRepository;
    }

    /**
     * Wins of the season of the team.
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function seasonWins(int $season)
    {
        $wins = $this->resultsStatisticsRepository->getSeasonWins($season);

        return view('results.stats.wins')
            ->with('wins', $wins);
    }
}
