<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\paddock\Users\Requests\LoginRequest;

class AuthController extends Controller
{
    /**
     * Get login form.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getLogin()
    {
        // Check, if session already exists
        if (Auth::check()) {
            return redirect()
                ->route('dashboard');
        }

        return view('auth.login');
    }

    /**
     * Send request to login process.
     * @param LoginRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(LoginRequest $request)
    {
        $details = $request->only('email', 'password');

        if (auth()->attempt($details)) {
            return redirect()
                ->route('dashboard');
        }

        return redirect()->back()->withErrors(['error' => trans('error.wrong_email_or_password')]);
    }

    /**
     * Send request to logout process.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout(Request $request)
    {
        Auth::logout();

        $request->session()->flush();

        return redirect()
            ->route('index');
    }
}
