<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\paddock\Users\Repositories\UsersRepository;
use App\paddock\Users\Requests\UpdateUserSettingsRequest;
use App\paddock\Languages\Repositories\LanguagesRepository;

class SettingsController extends Controller
{
    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * SettingsController constructor.
     * @param LanguagesRepository $languagesRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(
        LanguagesRepository $languagesRepository,
        UsersRepository $usersRepository
    ) {
        $this->languagesRepository = $languagesRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Get settings page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $languages = $this->languagesRepository->getActive();

        return view('settings.index')
            ->with('languages', $languages);
    }

    /**
     * Updating user settings.
     * @param UpdateUserSettingsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(UpdateUserSettingsRequest $request)
    {
        $user_id = $request->user()->id;

        $data = [
            'id' => $user_id,
            'lang' => $request->input('lang'),
        ];

        $this->usersRepository->updateUserSettings($data);

        return redirect()
            ->route('settings')
            ->with('save', trans('common.update_settings'));
    }

    /**
     * Change language.
     * @param Request $request
     * @param string $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeLanguage(Request $request, string $lang)
    {
        $user = $request->user();
        $lang = in_array($lang, config('app.locales')) ? $lang : config('app.fallback_locale');

        if ($user) {
            $user->lang = $lang;
            $user->update();
        }

        cookie()->queue('lang', $lang);

        return redirect()
            ->back();
    }

    /**
     * Logged you out from other sessions.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logoutOthers()
    {
        $user = auth()->user()->password;

        Auth::logoutOtherDevices($user);

        return redirect()
            ->route('settings');
    }
}
