<?php

namespace App\Http\Controllers\Frontend;

use App\paddock\Results\Gaps;
use App\Http\Controllers\Controller;
use App\paddock\Staffs\Repositories\StaffsRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Reports\Repositories\ReportsTyresRepository;
use App\paddock\Seasons\Repositories\SeasonsFilesRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;
use App\paddock\Results\Repositories\ResultsTimingsRepository;
use App\paddock\Seasons\Repositories\SeasonsSessionsRepository;
use App\paddock\Reports\Repositories\ReportsTechnicalsRepository;

class SeasonsController extends Controller
{
    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var ReportsTechnicalsRepository
     */
    private $reportsTechnicalsRepository;

    /**
     * @var ReportsTyresRepository
     */
    private $reportsTyresRepository;

    /**
     * @var ResultsTimingsRepository
     */
    private $resultsTimingsRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * @var SeasonsFilesRepository
     */
    private $seasonsFilesRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var SeasonsSessionsRepository
     */
    private $seasonsSessionsRepository;

    /**
     * @var StaffsRepository
     */
    private $staffsRepository;

    /**
     * SeasonsController constructor.
     *
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param ReportsTechnicalsRepository $reportsTechnicalsRepository
     * @param ReportsTyresRepository $reportsTyresRepository
     * @param ResultsTimingsRepository $resultsTimingsRepository
     * @param SeasonsRepository $seasonsRepository
     * @param SeasonsFilesRepository $seasonsFilesRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     * @param SeasonsSessionsRepository $seasonsSessionsRepository
     * @param StaffsRepository $staffsRepository
     */
    public function __construct(
        GrandPrixsRepository $grandPrixsRepository,
        ReportsTechnicalsRepository $reportsTechnicalsRepository,
        ReportsTyresRepository $reportsTyresRepository,
        ResultsTimingsRepository $resultsTimingsRepository,
        SeasonsRepository $seasonsRepository,
        SeasonsFilesRepository $seasonsFilesRepository,
        SeasonsRacesRepository $seasonsRacesRepository,
        SeasonsSessionsRepository $seasonsSessionsRepository,
        StaffsRepository $staffsRepository
    ) {
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->reportsTechnicalsRepository = $reportsTechnicalsRepository;
        $this->reportsTyresRepository = $reportsTyresRepository;
        $this->resultsTimingsRepository = $resultsTimingsRepository;
        $this->seasonsRepository = $seasonsRepository;
        $this->seasonsFilesRepository = $seasonsFilesRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->seasonsSessionsRepository = $seasonsSessionsRepository;
        $this->staffsRepository = $staffsRepository;
    }

    /**
     * Get season races.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $currentSeason = 2018;

        $season = $this->seasonsRepository->getSeasonBySeason($currentSeason);

        $races = $this->seasonsRacesRepository->getRacesBySeason($currentSeason);

        return view('seasons.index')
            ->with('season', $season)
            ->with('races', $races);
    }

    /**
     * Get race by season and gp.
     * @param int $season
     * @param string $gp
     * @return mixed
     */
    public function race(int $season, string $gp)
    {
        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $gp);

        $techs = $this->reportsTechnicalsRepository->getTechnicalReportBySeasonAndGP($season, (int) $race['gp_id']);

        $tyres = $this->reportsTyresRepository->getSelectedSetsBySeasonAndGP($season, (int) $race['gp_id']);

        $files = $this->seasonsFilesRepository->getFilesBySeasonAndGPLimited($season, (int) $race['gp_id']);

        return view('seasons.race')
            ->with('files', $files)
            ->with('race', $race)
            ->with('techs', $techs)
            ->with('tyres', $tyres);
    }

    /**
     * Get files to race by season and gp.
     * @param int $season
     * @param string $gp
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function raceFiles(int $season, string $gp)
    {
        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $gp);

        $files = $this->seasonsFilesRepository->getFilesBySeasonAndGP($season, (int) $race['gp_id']);

        return view('seasons.race_files')
            ->with('files', $files)
            ->with('race', $race);
    }

    /**
     * Get sessions by season and gp.
     * @param int $season
     * @param string $gp
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function raceLaps(int $season, string $gp)
    {
        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $gp);

        $sessions = $this->seasonsSessionsRepository->getSessionsBySeasonAndGP($season, (int) $race['id']);

        return view('seasons.race_laps')
            ->with('race', $race)
            ->with('sessions', $sessions);
    }

    /**
     * Get times by season, gp and session.
     * @param int $season
     * @param string $gp
     * @param int $session
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function raceLive(int $season, string $gp, int $session)
    {
        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $gp);

        $driver1 = $this->staffsRepository->getStaffByID(76);
        $driver2 = $this->staffsRepository->getStaffByID(72);

        $grandprix = $this->grandPrixsRepository->getGrandPrixByID((int) $race['gp_id']);

        $driver1_c = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
            $season,
            (int) $race['gp_id'],
            $session,
            76
        );
        $driver2_c = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
            $season,
            (int) $race['gp_id'],
            $session,
            72
        );

        $driver1_l = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
            2017,
            (int) $race['gp_id'],
            $session,
            76
        );
        $driver2_l = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
            2017,
            (int) $race['gp_id'],
            $session,
            72
        );

        $data = [
            'driver1' => [
                    'part'      => $driver1['name'],
                    'string'    => 'Time Comparision '.$driver1['hashtag'].' #'.$grandprix['hashtag'].' '.$grandprix['emoji'].' '.$this->gp_session($session),
                    'this_year' => (is_null($driver1_c)) ? '' : $driver1_c->secintime(),
                    'last_year' => (is_null($driver1_l)) ? '' : $driver1_l->secintime(),
                    'gap'       => (is_null($driver1_l)) ? '' : Gaps::gapintime(
                        $driver1_c->laptime,
                        $driver1_l->laptime
                    ),
                ],
            'driver2' => [
                    'part'      => $driver2['name'],
                    'string'    => 'Time Comparision '.$driver2['hashtag'].' #'.$grandprix['hashtag'].' '.$grandprix['emoji'].' '.$this->gp_session($session),
                    'this_year' => (is_null($driver2_c)) ? '' : $driver2_c->secintime(),
                    'last_year' => (is_null($driver2_l)) ? '' : $driver2_l->secintime(),
                    'gap'       => (is_null($driver2_l)) ? '' : Gaps::gapintime($driver2_c->laptime, $driver2_l->laptime),
                ],
            'drivers' => [
                    'part'   => 'Team',
                    'string' => 'Time Comparision '.$driver1['hashtag'].' / '.$driver2['hashtag'].' #'.$grandprix['hashtag'].' '.$grandprix['emoji'].' '.$this->gp_session($session),
                    'time1'  => (is_null($driver1_c)) ? '' : $driver1_c->secintime(),
                    'time2'  => (is_null($driver2_c)) ? '' : $driver2_c->secintime(),
                    'gap'    => Gaps::team((int) $driver1_c['laptime'], (int) $driver2_c['laptime']),
                ],
        ];

        return view('seasons.race_live')
            ->with('data', $data)
            ->with('race', $race);
    }

    /**
     * @param int $season
     * @param string $gp
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function racePreview(int $season, string $gp)
    {
        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $gp);

        return view('seasons.race_preview')
            ->with('race', $race);
    }

    /**
     * Get tyre report by season and gp.
     * @param int $season
     * @param string $gp
     * @param int $staff
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reportTyres(int $season, string $gp, int $staff)
    {
        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $gp);

        $tyres = $this->reportsTyresRepository->getSelectedSetsBySeasonGPAndStaff($season, (int) $race['gp_id'], $staff);

        return view('reports.tyres')
            ->with('race', $race)
            ->with('tyres', $tyres);
    }

    /**
     * Output of the session of the grand prix.
     * @param int $session
     * @return string
     */
    private function gp_session(int $session)
    {
        $output = '';

        switch ($session) {
            case 1:
                $output = 'FP1';
                break;
            case 2:
                $output = 'FP2';
                break;
            case 3:
                $output = 'FP3';
                break;
            case 4:
                $output = 'Quali';
                break;
            case 5:
                $output = 'Race';
                break;
        }

        return $output;
    }
}
