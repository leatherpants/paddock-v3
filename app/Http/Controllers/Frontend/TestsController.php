<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Tests\Repositories\TestsResultsRepository;

class TestsController extends Controller
{
    /**
     * @var TestsResultsRepository
     */
    private $testsResultsRepository;

    /**
     * TestsController constructor.
     * @param TestsResultsRepository $testsResultsRepository
     */
    public function __construct(TestsResultsRepository $testsResultsRepository)
    {
        $this->testsResultsRepository = $testsResultsRepository;
    }

    /**
     * Get results by season and week.
     * @param int $season
     * @param int $week
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function result(int $season, int $week)
    {
        $results = $this->testsResultsRepository->getResultsBySeasonAndTestweek($season, $week);

        $generals = $this->testsResultsRepository->getGeneralResultsBySeasonAndTestweek($season, $week);

        return view('tests.result')
            ->with('generals', $generals)
            ->with('results', $results);
    }
}
