<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedFDARepository;
use App\paddock\Feed\Repositories\FeedFerrariRepository;
use App\paddock\Feed\Repositories\FeedHistoryRepository;
use App\paddock\Feed\Repositories\FeedMotorsportRepository;

class FeedController extends Controller
{
    /**
     * @var FeedFDARepository
     */
    private $feedFDARepository;

    /**
     * @var FeedFerrariRepository
     */
    private $feedFerrariRepository;

    /**
     * @var FeedHistoryRepository
     */
    private $feedHistoryRepository;

    /**
     * @var FeedMotorsportRepository
     */
    private $feedMotorsportRepository;

    /**
     * FeedController constructor.
     * @param FeedFDARepository $feedFDARepository
     * @param FeedFerrariRepository $feedFerrariRepository
     * @param FeedHistoryRepository $feedHistoryRepository
     * @param FeedMotorsportRepository $feedMotorsportRepository
     */
    public function __construct(
        FeedFDARepository $feedFDARepository,
        FeedFerrariRepository $feedFerrariRepository,
        FeedHistoryRepository $feedHistoryRepository,
        FeedMotorsportRepository $feedMotorsportRepository
    ) {
        $this->feedFDARepository = $feedFDARepository;
        $this->feedFerrariRepository = $feedFerrariRepository;
        $this->feedHistoryRepository = $feedHistoryRepository;
        $this->feedMotorsportRepository = $feedMotorsportRepository;
    }

    /**
     * Get index of the various feeds.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lang = auth()->user()->lang;

        $date = '-'.date('m').'-'.date('d');

        $fda = $this->feedFDARepository->getLatestEntry($lang);

        $ferrari = $this->feedFerrariRepository->getLatestEntry($lang);

        $history = $this->feedHistoryRepository->getLatestEntry($date, $lang);

        $motorsport = $this->feedMotorsportRepository->getLatestEntry($lang);

        return view('feed.index')
            ->with('fda', $fda)
            ->with('ferrari', $ferrari)
            ->with('history', $history)
            ->with('motorsport', $motorsport);
    }

    /**
     * Get form for meta request.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function meta()
    {
        return view('feed.meta');
    }

    /**
     * Read out meta tags of the url.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postMeta(Request $request)
    {
        $data = get_meta_tags($request->url);

        $tags = [
            'title'       => html_entity_decode($data['twitter:title'], ENT_QUOTES, 'UTF-8'),
            'description' => html_entity_decode($data['twitter:description'], ENT_QUOTES, 'UTF-8'),
            'image'       => $data['twitter:image'],
            'url'         => $request->input('url'),
        ];

        return redirect()
            ->back()
            ->with('tags', $tags);
    }
}
