<?php

namespace App\Http\Controllers\SQL;

use App\Http\Controllers\Controller;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;

class ResultsController extends Controller
{
    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * ResultsController constructor.
     * @param SeasonsRacesRepository $seasonsRacesRepository
     */
    public function __construct(SeasonsRacesRepository $seasonsRacesRepository)
    {
        $this->seasonsRacesRepository = $seasonsRacesRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $gps = \DB::table('seasons_races')
            ->whereNotExists(function ($query) {
                $query->select('*')
                    ->from('results')
                    ->whereRaw('results.season = seasons_races.season')
                    ->whereRaw('results.gp_id = seasons_races.gp_id');
            })
            ->join('grand_prixs', 'seasons_races.gp_id', '=', 'grand_prixs.id')
            ->groupBy('seasons_races.gp_id')
            ->get();

        return view('sql.index')
            ->with('gps', $gps);
    }

    /**
     * @param int $gp_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function gp(int $gp_id)
    {
        $gp = \DB::table('seasons_races')
            ->whereNotExists(function ($query) use ($gp_id) {
                $query->select('*')
                    ->from('results')
                ->whereRaw('results.season = seasons_races.season')
                ->where('results.gp_id', $gp_id);
            })
            ->where('seasons_races.gp_id', $gp_id)
            ->get();

        return view('sql.results')
            ->with('gp', $gp);
    }
}
