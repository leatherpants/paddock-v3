<?php

namespace App\Http\Controllers\SQL;

use App\Http\Controllers\Controller;

class StaffsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $staffs = \DB::table('staffs')->get();

        return view('sql.staffs')
            ->with('staffs', $staffs);
    }
}
