<?php

namespace App\Http\Middleware;

use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $appLocales = config('app.locales');
        $localeCookie = $request->cookie('lang');

        if ($localeCookie && in_array($localeCookie, $appLocales)) {
            $this->setLanguage($localeCookie);
        } else {
            app()->setLocale(config('app.locale'));

            $response = $next($request);

            return $response
                ->withCookie(cookie()->forever('lang', config('app.locale')));
        }

        $user = $request->user();

        if ($user) {
            $userLanguage = $user->lang;

            if (!empty($userLanguage) && in_array($userLanguage, $appLocales)) {
                $this->setLanguage($userLanguage);

                $response = $next($request);

                return $response
                    ->withCookie(cookie()->forever('lang', $userLanguage));
            }
        }

        return $next($request);
    }

    /**
     * Decider of language types for application and time.
     * @param array|string $lang
     * @throws \Exception
     */
    private function setLanguage($lang)
    {
        switch ($lang) {
            case 'de':
                app()->setLocale('de');
                setlocale(LC_TIME, 'de_DE');
                break;
            case 'en':
                app()->setLocale('en');
                setlocale(LC_TIME, 'en');
                break;
            case 'es':
                app()->setLocale('es');
                setlocale(LC_TIME, 'es_ES');
                break;
            case 'it':
                app()->setLocale('it');
                setlocale(LC_TIME, 'it_IT');
                break;
            case 'se':
                app()->setLocale('se');
                setlocale(LC_TIME, 'sv_SE');
                break;
            default:
                throw new \Exception('Locale not found');
        }
    }
}
