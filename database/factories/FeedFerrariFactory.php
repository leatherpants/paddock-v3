<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Feed\Models\FeedFerrari::class, function (Faker $faker) {
    return [
        'session' => $faker->randomDigit,
        'lang' => $faker->languageCode,
        'title' => $faker->title,
        'description' => $faker->text,
        'image' => $faker->imageUrl(),
        'link' => $faker->url,
        'domain' => $faker->domainName,
        'hashtags' => $faker->text,
    ];
});
