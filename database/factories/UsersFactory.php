<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'password' => bcrypt('secret'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'display_name' => $faker->name,
        'user_name' => str_slug($faker->name),
        'avatar' => '',
        'background_image' => '',
        'lang' => $faker->countryCode,
        'timezone' => 'Europe/Berlin',
    ];
});
