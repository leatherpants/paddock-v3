<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Users\Models\UsersSocials::class, function (Faker $faker) {
    return [
        'user_id' => $faker->uuid,
        'facebook_username' => $faker->userName,
        'twitter_username' => $faker->userName,
        'instagram_username' => $faker->userName,
    ];
});
