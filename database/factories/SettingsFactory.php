<?php

use Faker\Generator as Faker;

$factory->define(App\paddock\Settings\Models\Settings::class, function (Faker $faker) {
    return [
        'key' => $faker->text,
        'value' => $faker->randomDigit,
    ];
});
