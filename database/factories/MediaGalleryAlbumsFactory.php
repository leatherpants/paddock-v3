<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Media\Gallery\Models\MediaGalleryAlbums::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'slug' => $faker->slug,
        'cover_id' => $faker->randomDigit,
    ];
});
