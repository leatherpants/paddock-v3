<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Events\Models\Events::class, function (Faker $faker) {
    return [
        'staff_id' => $faker->uuid,
        'type' => $faker->text,
        'date' => $faker->date('Y-m-d'),
        'lang' => $faker->languageCode,
        'event_text' => $faker->text,
        'event_twitter' => urlencode($faker->text),
        'event_hashtags' => $faker->text,
        'event_link' => $faker->url,
    ];
});
