<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Languages\Models\Languages::class, function (Faker $faker) {
    return [
        'code' => $faker->languageCode,
        'name' => $faker->text,
        'active' => $faker->randomDigit,
        'news' => $faker->randomDigit,
    ];
});
