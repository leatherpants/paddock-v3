<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Feed\Models\FeedFDA::class, function (Faker $faker) {
    return [
        'lang' => $faker->languageCode,
        'title' => $faker->title,
        'description' => $faker->text,
        'image' => $faker->imageUrl(),
        'link' => $faker->url,
        'domain' => $faker->domainName,
        'hashtags' => $faker->text,
    ];
});
