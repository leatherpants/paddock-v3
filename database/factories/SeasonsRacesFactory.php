<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Seasons\Models\SeasonsRaces::class, function (Faker $faker) {
    return [
        'season' => $faker->year,
        'raceday' => $faker->date('Y-m-d'),
        'gp_id' => $faker->randomDigit,
        'track_id' => $faker->randomDigit,
    ];
});
