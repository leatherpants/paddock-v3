<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Media\Youtube\Models\MediaYoutubeVideos::class, function (Faker $faker) {
    return [
        'youtube_id' => $faker->randomDigit,
        'title' => $faker->title,
        'description' => $faker->text,
    ];
});
