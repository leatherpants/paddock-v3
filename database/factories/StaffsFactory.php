<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Staffs\Models\Staffs::class, function (Faker $faker) {
    return [
        'country_id' => $faker->countryCode,
        'name' => $faker->name,
        'slug' => $faker->slug,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'hashtag' => $faker->text,
        'dateofbirth' => $faker->date('Y-m-d'),
        'dateofdeath' => $faker->date('Y-m-d'),
    ];
});
