<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Tracks\Models\Tracks::class, function (Faker $faker) {
    return [
        'id' => $faker->randomDigit,
        'country_id' => $faker->countryCode,
        'name' => $faker->name,
        'slug' => $faker->slug,
        'timezone' => $faker->text,
        'active' => $faker->randomDigit,
    ];
});
