<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Feed\Models\FeedHistory::class, function (Faker $faker) {
    return [
        'lang' => $faker->languageCode,
        'date' => $faker->date('Y-m-d'),
        'title' => $faker->title,
        'description' => $faker->text,
        'image' => $faker->imageUrl(),
        'link' => $faker->url,
        'domain' => $faker->domainName,
    ];
});
