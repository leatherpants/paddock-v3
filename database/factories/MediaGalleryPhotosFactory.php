<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Media\Gallery\Models\MediaGalleryPhotos::class, function (Faker $faker) {
    return [
        'album_id' => $faker->randomDigit,
        'url' => $faker->imageUrl,
    ];
});
