<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [
                'code' => 'gb',
                'name' => 'Great Britian',
            ],
            [
                'code' => 'mc',
                'name' => 'Monaco',
            ],
            [
                'code' => 'us',
                'name' => 'United Staates',
            ],
            [
                'code' => 'ch',
                'name' => 'Switzerland',
            ],
            [
                'code' => 'be',
                'name' => 'Belgium',
            ],
            [
                'code' => 'fr',
                'name' => 'France',
            ],
            [
                'code' => 'it',
                'name' => 'Italy',
            ],
            [
                'code' => 'de',
                'name' => 'Germany',
            ],
            [
                'code' => 'es',
                'name' => 'Spain',
            ],
            [
                'code' => 'nl',
                'name' => 'Netherlands',
            ],
            [
                'code' => 'ar',
                'name' => 'Argentina',
            ],
            [
                'code' => 'ma',
                'name' => 'Morocco',
            ],
            [
                'code' => 'pt',
                'name' => 'Portugal',
            ],
            [
                'code' => 'za',
                'name' => 'South Africa',
            ],
            [
                'code' => 'mx',
                'name' => 'Mexico',
            ],
            [
                'code' => 'at',
                'name' => 'Austria',
            ],
            [
                'code' => 'ca',
                'name' => 'Canada',
            ],
            [
                'code' => 'se',
                'name' => 'Sweden',
            ],
            [
                'code' => 'br',
                'name' => 'Brazil',
            ],
            [
                'code' => 'jp',
                'name' => 'Japan',
            ],
            [
                'code' => 'sm',
                'name' => 'San Marino',
            ],
            [
                'code' => 'eu',
                'name' => 'Europe',
            ],
            [
                'code' => 'au',
                'name' => 'Australia',
            ],
            [
                'code' => 'hu',
                'name' => 'Hungary',
            ],
            [
                'code' => 'lu',
                'name' => 'Luxembourg',
            ],
            [
                'code' => 'my',
                'name' => 'Malaysia',
            ],
            [
                'code' => 'bh',
                'name' => 'Bahrain',
            ],
            [
                'code' => 'cn',
                'name' => 'China',
            ],
            [
                'code' => 'tr',
                'name' => 'Turkey',
            ],
            [
                'code' => 'sg',
                'name' => 'Singapore',
            ],
            [
                'code' => 'ae',
                'name' => 'United Arab Emirates',
            ],
            [
                'code' => 'kr',
                'name' => 'South Korea',
            ],
            [
                'code' => 'in',
                'name' => 'India',
            ],
            [
                'code' => 'ru',
                'name' => 'Russia',
            ],
            [
                'code' => 'az',
                'name' => 'Azerbaijan',
            ],
            [
                'code' => 'fi',
                'name' => 'Finnland',
            ],
            [
                'code' => 'vn',
                'name' => 'Vietnam',
            ],
        ]);
    }
}
