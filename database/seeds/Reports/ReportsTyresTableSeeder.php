<?php

use Illuminate\Database\Seeder;

class ReportsTyresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reports_tyres')->insert([
            [
                'season' => 2018,
                'race' => 1,
                'gp_id' => 28,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 7,
                'supersoft' => 3,
                'soft' => 3,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 1,
                'gp_id' => 28,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 7,
                'supersoft' => 3,
                'soft' => 3,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 2,
                'gp_id' => 33,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 7,
                'soft' => 4,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 2,
                'gp_id' => 33,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 7,
                'soft' => 4,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 3,
                'gp_id' => 34,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 8,
                'supersoft' => null,
                'soft' => 3,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 3,
                'gp_id' => 34,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 8,
                'supersoft' => null,
                'soft' => 3,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 4,
                'gp_id' => 41,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 10,
                'supersoft' => 2,
                'soft' => 1,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 4,
                'gp_id' => 41,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 10,
                'supersoft' => 1,
                'soft' => 2,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 5,
                'gp_id' => 9,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 7,
                'soft' => 4,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 5,
                'gp_id' => 9,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 7,
                'soft' => 4,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 6,
                'gp_id' => 2,
                'staff_id' => 76,
                'hypersoft' => 10,
                'ultrasoft' => 2,
                'supersoft' => 1,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 6,
                'gp_id' => 2,
                'staff_id' => 72,
                'hypersoft' => 10,
                'ultrasoft' => 2,
                'supersoft' => 1,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 7,
                'gp_id' => 19,
                'staff_id' => 76,
                'hypersoft' => 8,
                'ultrasoft' => 3,
                'supersoft' => 2,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 7,
                'gp_id' => 19,
                'staff_id' => 72,
                'hypersoft' => 8,
                'ultrasoft' => 3,
                'supersoft' => 2,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 8,
                'gp_id' => 6,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 9,
                'supersoft' => 3,
                'soft' => 1,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 8,
                'gp_id' => 6,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 9,
                'supersoft' => 2,
                'soft' => 2,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 9,
                'gp_id' => 18,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 9,
                'supersoft' => 2,
                'soft' => 2,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 9,
                'gp_id' => 18,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 9,
                'supersoft' => 3,
                'soft' => 1,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 10,
                'gp_id' => 1,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => null,
                'soft' => 8,
                'medium' => 4,
                'hard' => 1,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 10,
                'gp_id' => 1,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => null,
                'soft' => 8,
                'medium' => 4,
                'hard' => 1,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 11,
                'gp_id' => 8,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 7,
                'supersoft' => null,
                'soft' => 4,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 11,
                'gp_id' => 8,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 7,
                'supersoft' => null,
                'soft' => 5,
                'medium' => 1,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 12,
                'gp_id' => 29,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 9,
                'supersoft' => null,
                'soft' => 3,
                'medium' => 1,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 12,
                'gp_id' => 29,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 8,
                'supersoft' => null,
                'soft' => 3,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 13,
                'gp_id' => 5,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 7,
                'soft' => 4,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 13,
                'gp_id' => 5,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 7,
                'soft' => 5,
                'medium' => 1,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 14,
                'gp_id' => 7,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 10,
                'soft' => 1,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 14,
                'gp_id' => 7,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 10,
                'soft' => 2,
                'medium' => 1,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 15,
                'gp_id' => 36,
                'staff_id' => 76,
                'hypersoft' => 9,
                'ultrasoft' => 3,
                'supersoft' => null,
                'soft' => 1,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 15,
                'gp_id' => 36,
                'staff_id' => 72,
                'hypersoft' => 9,
                'ultrasoft' => 3,
                'supersoft' => null,
                'soft' => 1,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 16,
                'gp_id' => 40,
                'staff_id' => 76,
                'hypersoft' => 9,
                'ultrasoft' => 3,
                'supersoft' => null,
                'soft' => 1,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 16,
                'gp_id' => 40,
                'staff_id' => 72,
                'hypersoft' => 8,
                'ultrasoft' => 3,
                'supersoft' => null,
                'soft' => 2,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 17,
                'gp_id' => 24,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 10,
                'soft' => 2,
                'medium' => 1,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 17,
                'gp_id' => 24,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 10,
                'soft' => 2,
                'medium' => 1,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 18,
                'gp_id' => 15,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => 8,
                'supersoft' => 3,
                'soft' => 2,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 18,
                'gp_id' => 15,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => 7,
                'supersoft' => 3,
                'soft' => 3,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 19,
                'gp_id' => 17,
                'staff_id' => 76,
                'hypersoft' => 8,
                'ultrasoft' => 3,
                'supersoft' => 2,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 19,
                'gp_id' => 17,
                'staff_id' => 72,
                'hypersoft' => 8,
                'ultrasoft' => 3,
                'supersoft' => 2,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 20,
                'gp_id' => 20,
                'staff_id' => 76,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 9,
                'soft' => 3,
                'medium' => 1,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 20,
                'gp_id' => 20,
                'staff_id' => 72,
                'hypersoft' => null,
                'ultrasoft' => null,
                'supersoft' => 8,
                'soft' => 3,
                'medium' => 2,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 21,
                'gp_id' => 37,
                'staff_id' => 76,
                'hypersoft' => 8,
                'ultrasoft' => 2,
                'supersoft' => 3,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
            [
                'season' => 2018,
                'race' => 21,
                'gp_id' => 37,
                'staff_id' => 72,
                'hypersoft' => 8,
                'ultrasoft' => 2,
                'supersoft' => 3,
                'soft' => null,
                'medium' => null,
                'hard' => null,
                'superhard' => null,
            ],
        ]);
    }
}
