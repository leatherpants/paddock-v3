<?php

use Illuminate\Database\Seeder;

class SeasonsFilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seasons_files')->insert([
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'image',
                'name' => 'Selected Sets Per Driver',
                'file_name' => 'http://media.ferrari.granath/files/20859_04-AZ-Selected-Sets-Per-Driver-EN.jpg',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-17 17:09:17')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-17 17:09:17')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'PU Elements Used Per Driver',
                'file_name' => 'http://media.ferrari.granath/files/2018_azerbaijan_grand_prix_2018_pu_elements_used_per_driver_up_to_now_-_doc_4.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-26 13:16:17')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-26 13:16:17')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'Initial Scrutineering',
                'file_name' => 'http://media.ferrari.granath/files/2018_azerbaijan_grand_prix_2018_initial_scrutineering_-_doc_6.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-26 14:47:57')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-26 14:47:57')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'FP1 - Classification',
                'file_name' => 'http://media.ferrari.granath/files/2018_azerbaijan_grand_prix_2018_p1_classification_-_doc_8.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:17')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:17')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'FP1 - Lap Times',
                'file_name' => 'http://media.ferrari.granath/files/2018_04_aze_f1_p1_timing_firstpracticesessionlaptimes_v01.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:27')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:27')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'FP2 - Classification',
                'file_name' => 'http://media.ferrari.granath/files/2018_azerbaijan_grand_prix_2018_p2_classification_-_doc_9.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:17')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:17')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'FP2 - Lap Times',
                'file_name' => 'http://media.ferrari.granath/files/2018_04_aze_f1_p2_timing_secondpracticesessionlaptimes_v01.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:27')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-27 16:40:27')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'FP3 - Classification',
                'file_name' => 'http://media.ferrari.granath/files/2018_azerbaijan_grand_prix_2018_p3_classification_-_doc_16_1.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 12:14:51')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 12:14:51')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'FP3 - Lap Times',
                'file_name' => 'http://media.ferrari.granath/files/2018_04_aze_f1_p3_timing_thirdpracticesessionlaptimes_v01.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 12:14:51')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 12:14:51')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'Quali - Lap Times',
                'file_name' => 'http://media.ferrari.granath/files/2018_04_aze_f1_q0_timing_qualifyingsessionlaptimes_v01.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 15:12:11')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 15:12:11')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'pdf',
                'name' => 'Quali - Final Classification',
                'file_name' => 'http://media.ferrari.granath/files/2018_04_aze_f1_q0_timing_qualifyingsessionlaptimes_v01.pdf',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 18:58:06')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 18:58:06')),
            ],
            [
                'season' => 2018,
                'gp_id' => 41,
                'file_type' => 'image',
                'name' => 'Race Sets',
                'file_name' => 'http://media.ferrari.granath/files/21043_04-AZ-RaceSets.jpg',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 19:34:02')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-04-28 19:34:02')),
            ],
        ]);
    }
}
