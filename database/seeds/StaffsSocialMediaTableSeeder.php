<?php

use Illuminate\Database\Seeder;

class StaffsSocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('staffs_social_media')->insert([
            [
                'staff_id' => 72,
                'provider' => 2,
                'username' => 'kimimatiasraikkonen',
                'link' => 'kimimatiasraikkonen',
            ],
        ]);
    }
}
