<?php

use Illuminate\Database\Seeder;

class Results1951TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results')->insert([
            [
                'season' => 1951,
                'dateofresult' => '1951-05-26',
                'gp_id' => 4,
                'track_id' => 4,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-05-26',
                'gp_id' => 4,
                'track_id' => 4,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-05-26',
                'gp_id' => 4,
                'track_id' => 4,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-05-27',
                'gp_id' => 4,
                'track_id' => 4,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 42,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-05-27',
                'gp_id' => 4,
                'track_id' => 4,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 40,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-05-27',
                'gp_id' => 4,
                'track_id' => 4,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 18,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 12,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-16',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-16',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-16',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-17',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 36,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-17',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 36,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-17',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 8,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-30',
                'gp_id' => 6,
                'track_id' => 6,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-30',
                'gp_id' => 6,
                'track_id' => 6,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-06-30',
                'gp_id' => 6,
                'track_id' => 6,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-01',
                'gp_id' => 6,
                'track_id' => 6,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 42,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-01',
                'gp_id' => 6,
                'track_id' => 6,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 35,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-01',
                'gp_id' => 6,
                'track_id' => 6,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 74,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-01',
                'gp_id' => 6,
                'track_id' => 6,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 18,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 10,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-13',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-13',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-13',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-14',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 8,
                'laps' => 90,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-14',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 88,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-14',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 16,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 56,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-28',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-28',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-28',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-28',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-29',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 8,
                'laps' => 20,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-29',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 20,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-29',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 20,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-07-29',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 2,
                'laps' => 20,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-15',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-15',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-15',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-15',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-16',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 8,
                'laps' => 80,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-16',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 80,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-16',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 79,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-09-16',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 2,
                'laps' => 78,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-27',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-27',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-27',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-27',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-28',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jose-froilan-gonzalez')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 70,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-28',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'alberto-ascari')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 68,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-28',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'luigi-villoresi')->first()->id,
                'team' => 1,
                'place' => 11,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 48,
            ],
            [
                'season' => 1951,
                'dateofresult' => '1951-10-28',
                'gp_id' => 9,
                'track_id' => 9,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'piero-taruffi')->first()->id,
                'team' => 1,
                'place' => 14,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 30,
            ],
        ]);
    }
}
