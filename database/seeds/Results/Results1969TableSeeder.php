<?php

use Illuminate\Database\Seeder;

class Results1969TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results')->insert([
            [
                'season' => 1969,
                'dateofresult' => '1969-05-03',
                'gp_id' => 9,
                'track_id' => 33,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-05-04',
                'gp_id' => 9,
                'track_id' => 33,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 8,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 56,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-05-17',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-05-18',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 11,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 16,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-07-05',
                'gp_id' => 6,
                'track_id' => 27,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-07-06',
                'gp_id' => 6,
                'track_id' => 27,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 10,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 30,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-07-18',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-07-18',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 8,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-07-19',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 11,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 61,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-07-19',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'chris-amon')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 45,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-09-06',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-09-06',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'tino-brambilla')->first()->id,
                'team' => 1,
                'place' => 15,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-09-07',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 1,
                'laps' => 66,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-09-07',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'tino-brambilla')->first()->id,
                'team' => 1,
                'place' => 16,
                'dnf' => 0,
                'dns' => 1,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-09-19',
                'gp_id' => 19,
                'track_id' => 30,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 13,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-09-20',
                'gp_id' => 19,
                'track_id' => 30,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 37,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-10-04',
                'gp_id' => 15,
                'track_id' => 19,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-10-05',
                'gp_id' => 15,
                'track_id' => 19,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 2,
                'laps' => 101,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-10-18',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 15,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1969,
                'dateofresult' => '1969-10-19',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'pedro-rodriguez')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 63,
            ],
        ]);
    }
}
