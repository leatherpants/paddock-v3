<?php

use Illuminate\Database\Seeder;

class Results1989TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results')->insert([
            [
                'season' => 1989,
                'dateofresult' => '1989-03-25',
                'gp_id' => 20,
                'track_id' => 46,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 0,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-03-25',
                'gp_id' => 20,
                'track_id' => 46,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-03-26',
                'gp_id' => 20,
                'track_id' => 46,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 61,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-03-26',
                'gp_id' => 20,
                'track_id' => 46,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 25,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-05-06',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-05-07',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 22,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 30,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-05-27',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-05-27',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-05-28',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 17,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 43,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-05-28',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 22,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 16,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-03',
                'gp_id' => 15,
                'track_id' => 55,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-03',
                'gp_id' => 15,
                'track_id' => 55,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 8,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-04',
                'gp_id' => 15,
                'track_id' => 55,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 10,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 61,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-04',
                'gp_id' => 15,
                'track_id' => 55,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 18,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 31,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-17',
                'gp_id' => 19,
                'track_id' => 44,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-17',
                'gp_id' => 19,
                'track_id' => 44,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-18',
                'gp_id' => 19,
                'track_id' => 44,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 17,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 6,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-06-18',
                'gp_id' => 19,
                'track_id' => 44,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 25,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-08',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-08',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-09',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 80,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-09',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 23,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 29,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-15',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-15',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-16',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 64,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-16',
                'gp_id' => 1,
                'track_id' => 1,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 15,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 49,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-29',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-29',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-30',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 45,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-07-30',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 20,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 13,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-12',
                'gp_id' => 29,
                'track_id' => 53,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-12',
                'gp_id' => 29,
                'track_id' => 53,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-13',
                'gp_id' => 29,
                'track_id' => 53,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 77,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-13',
                'gp_id' => 29,
                'track_id' => 53,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 15,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 56,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-26',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-26',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-27',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 44,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-08-27',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 23,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 9,
            ],

            [
                'season' => 1989,
                'dateofresult' => '1989-09-09',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-09-09',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-09-10',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 53,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-09-10',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 14,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 41,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-09-30',
                'gp_id' => 9,
                'track_id' => 52,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-10-01',
                'gp_id' => 9,
                'track_id' => 52,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 73,
            ],

            [
                'season' => 1989,
                'dateofresult' => '1989-10-21',
                'gp_id' => 24,
                'track_id' => 54,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-10-21',
                'gp_id' => 24,
                'track_id' => 54,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-10-22',
                'gp_id' => 24,
                'track_id' => 54,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 13,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 43,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-10-22',
                'gp_id' => 24,
                'track_id' => 54,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 17,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 34,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-11-04',
                'gp_id' => 28,
                'track_id' => 51,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-11-04',
                'gp_id' => 28,
                'track_id' => 51,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 14,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-11-05',
                'gp_id' => 28,
                'track_id' => 51,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'nigel-mansell')->first()->id,
                'team' => 1,
                'place' => 14,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 17,
            ],
            [
                'season' => 1989,
                'dateofresult' => '1989-11-05',
                'gp_id' => 28,
                'track_id' => 51,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'gerhard-berger')->first()->id,
                'team' => 1,
                'place' => 21,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 6,
            ],
        ]);
    }
}
