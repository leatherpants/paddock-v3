<?php

use Illuminate\Database\Seeder;

class Results1970TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results')->insert([
            [
                'season' => 1970,
                'dateofresult' => '1970-04-18',
                'gp_id' => 9,
                'track_id' => 31,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-04-19',
                'gp_id' => 9,
                'track_id' => 31,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 15,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-05-09',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-05-10',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 15,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 11,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-06-06',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-06-06',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 8,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-06-07',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 28,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-06-07',
                'gp_id' => 5,
                'track_id' => 5,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 8,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 26,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-04',
                'gp_id' => 6,
                'track_id' => 27,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-04',
                'gp_id' => 6,
                'track_id' => 27,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 11,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-05',
                'gp_id' => 6,
                'track_id' => 27,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 14,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 35,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-05',
                'gp_id' => 6,
                'track_id' => 27,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 18,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 16,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-17',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-17',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-18',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 80,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-07-18',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 22,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 6,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-01',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-01',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-02',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 50,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-02',
                'gp_id' => 8,
                'track_id' => 34,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 30,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-15',
                'gp_id' => 18,
                'track_id' => 35,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-15',
                'gp_id' => 18,
                'track_id' => 35,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-15',
                'gp_id' => 18,
                'track_id' => 35,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-16',
                'gp_id' => 18,
                'track_id' => 35,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 60,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-16',
                'gp_id' => 18,
                'track_id' => 35,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 60,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-08-16',
                'gp_id' => 18,
                'track_id' => 35,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 59,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-05',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-05',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-05',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-06',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 68,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-06',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 13,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 25,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-06',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'ignazio-giunti')->first()->id,
                'team' => 1,
                'place' => 17,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 14,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-19',
                'gp_id' => 19,
                'track_id' => 32,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-19',
                'gp_id' => 19,
                'track_id' => 32,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-20',
                'gp_id' => 19,
                'track_id' => 32,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 90,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-09-20',
                'gp_id' => 19,
                'track_id' => 32,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 90,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-03',
                'gp_id' => 15,
                'track_id' => 21,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-03',
                'gp_id' => 15,
                'track_id' => 21,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-04',
                'gp_id' => 15,
                'track_id' => 21,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 107,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-04',
                'gp_id' => 15,
                'track_id' => 21,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 13,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 101,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-24',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-24',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-25',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'jacky-ickx')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 65,
            ],
            [
                'season' => 1970,
                'dateofresult' => '1970-10-25',
                'gp_id' => 17,
                'track_id' => 26,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 65,
            ],
        ]);
    }
}
