<?php

use Illuminate\Database\Seeder;

class Results1976TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results')->insert([
            [
                'season' => 1976,
                'dateofresult' => '1976-01-24',
                'gp_id' => 20,
                'track_id' => 38,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-01-24',
                'gp_id' => 20,
                'track_id' => 38,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-01-25',
                'gp_id' => 20,
                'track_id' => 38,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 40,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-01-25',
                'gp_id' => 20,
                'track_id' => 38,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 40,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-03-27',
                'gp_id' => 22,
                'track_id' => 42,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-03-27',
                'gp_id' => 22,
                'track_id' => 42,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-03-28',
                'gp_id' => 22,
                'track_id' => 42,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 80,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-03-28',
                'gp_id' => 22,
                'track_id' => 42,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 80,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-01',
                'gp_id' => 9,
                'track_id' => 31,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-01',
                'gp_id' => 9,
                'track_id' => 31,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-02',
                'gp_id' => 9,
                'track_id' => 31,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 75,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-02',
                'gp_id' => 9,
                'track_id' => 31,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 11,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 72,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-15',
                'gp_id' => 5,
                'track_id' => 39,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-15',
                'gp_id' => 5,
                'track_id' => 39,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-16',
                'gp_id' => 5,
                'track_id' => 39,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 70,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-16',
                'gp_id' => 5,
                'track_id' => 39,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 70,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-29',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-29',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-30',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 78,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-05-30',
                'gp_id' => 2,
                'track_id' => 2,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 14,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 73,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-06-12',
                'gp_id' => 21,
                'track_id' => 40,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-06-12',
                'gp_id' => 21,
                'track_id' => 40,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 11,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-06-13',
                'gp_id' => 21,
                'track_id' => 40,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 72,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-06-13',
                'gp_id' => 21,
                'track_id' => 40,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 1,
                'laps' => 72,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-03',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-03',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-04',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 24,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 17,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-04',
                'gp_id' => 6,
                'track_id' => 36,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 25,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 8,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-17',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-17',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-18',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 1,
                'dnf' => 0,
                'dns' => 0,
                'points' => 9,
                'laps' => 76,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-18',
                'gp_id' => 1,
                'track_id' => 24,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 25,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 36,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-31',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-07-31',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-08-01',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 9,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 14,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-08-01',
                'gp_id' => 8,
                'track_id' => 8,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 24,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-09-11',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-09-11',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'carlos-reutemann')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-09-11',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 9,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-09-12',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 2,
                'dnf' => 0,
                'dns' => 0,
                'points' => 6,
                'laps' => 52,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-09-12',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 4,
                'dnf' => 0,
                'dns' => 0,
                'points' => 3,
                'laps' => 52,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-09-12',
                'gp_id' => 7,
                'track_id' => 7,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'carlos-reutemann')->first()->id,
                'team' => 1,
                'place' => 9,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 52,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-02',
                'gp_id' => 19,
                'track_id' => 30,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-02',
                'gp_id' => 19,
                'track_id' => 30,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 12,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-03',
                'gp_id' => 19,
                'track_id' => 30,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 6,
                'dnf' => 0,
                'dns' => 0,
                'points' => 1,
                'laps' => 80,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-03',
                'gp_id' => 19,
                'track_id' => 30,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 8,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 80,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-09',
                'gp_id' => 23,
                'track_id' => 21,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-09',
                'gp_id' => 23,
                'track_id' => 21,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 14,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-10',
                'gp_id' => 23,
                'track_id' => 21,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 4,
                'laps' => 59,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-10',
                'gp_id' => 23,
                'track_id' => 21,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 58,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-23',
                'gp_id' => 24,
                'track_id' => 43,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 3,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-23',
                'gp_id' => 24,
                'track_id' => 43,
                'session_id' => 2,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 7,
                'dnf' => 0,
                'dns' => 0,
                'points' => 0,
                'laps' => 0,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-24',
                'gp_id' => 24,
                'track_id' => 43,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'clay-regazzoni')->first()->id,
                'team' => 1,
                'place' => 5,
                'dnf' => 0,
                'dns' => 0,
                'points' => 2,
                'laps' => 72,
            ],
            [
                'season' => 1976,
                'dateofresult' => '1976-10-24',
                'gp_id' => 24,
                'track_id' => 43,
                'session_id' => 1,
                'staff_id' => \App\paddock\Staffs\Models\Staffs::where('slug', 'niki-lauda')->first()->id,
                'team' => 1,
                'place' => 23,
                'dnf' => 1,
                'dns' => 0,
                'points' => 0,
                'laps' => 2,
            ],
        ]);
    }
}
