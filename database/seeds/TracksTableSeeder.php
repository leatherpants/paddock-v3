<?php

use Illuminate\Database\Seeder;

class TracksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tracks')->insert([
            [
                'country_id' => 'gb',
                'name' => 'Silverstone Circuit',
                'slug' => str_slug('Silverstone Circuit'),
                'timezone' => 'Europe/London',
                'active' => 1,
            ],
            [
                'country_id' => 'mc',
                'name' => 'Circuit de Monaco',
                'slug' => str_slug('Circuit de Monaco'),
                'timezone' => 'Europe/Monaco',
                'active' => 1,
            ],
            [
                'country_id' => 'us',
                'name' => 'Indianapolis Motor Speedway',
                'slug' => str_slug('Indianapolis Motor Speedway'),
                'timezone' => 'America/Indiana/Indianapolis',
                'active' => 0,
            ],
            [
                'country_id' => 'ch',
                'name' => 'Bremgarten',
                'slug' => str_slug('Bremgarten'),
                'timezone' => 'Europe/Zurich',
                'active' => 0,
            ],
            [
                'country_id' => 'be',
                'name' => 'Circuit de Spa-Francorchamps',
                'slug' => str_slug('Circuit de Spa-Francorchamps'),
                'timezone' => 'Europe/Brussels',
                'active' => 1,
            ],
            [
                'country_id' => 'fr',
                'name' => 'Circuit de Reims-Gueux',
                'slug' => str_slug('Circuit de Reims-Gueux'),
                'timezone' => 'Europe/Paris',
                'active' => 0,
            ],
            [
                'country_id' => 'it',
                'name' => 'Autodromo Nazionale Monza',
                'slug' => str_slug('Autodromo Nazionale Monza'),
                'timezone' => 'Europe/Rome',
                'active' => 1,
            ],
            [
                'country_id' => 'de',
                'name' => 'Nürburgring',
                'slug' => str_slug('Nürburgring'),
                'timezone' => 'Europe/Berlin',
                'active' => 0,
            ],
            [
                'country_id' => 'es',
                'name' => 'Circuit de Pedralbes',
                'slug' => str_slug('Circuit de Pedralbes'),
                'timezone' => 'Europe/Madrid',
                'active' => 0,
            ],
            [
                'country_id' => 'fr',
                'name' => 'Circuit de Rouen-Les-Essarts',
                'slug' => str_slug('Circuit de Rouen-Les-Essarts'),
                'timezone' => 'Europe/Paris',
                'active' => 0,
            ],
            [
                'country_id' => 'nl',
                'name' => 'Circuit Park Zandvoort',
                'slug' => str_slug('Circuit Park Zandvoort'),
                'timezone' => 'Europe/Amsterdam',
                'active' => 0,
            ],
            [
                'country_id' => 'ar',
                'name' => 'Autódromo Juan y Oscar Gálvez',
                'slug' => str_slug('Autódromo Juan y Oscar Gálvez'),
                'timezone' => 'America/Argentina/Buenos_Aires',
                'active' => 0,
            ],
            [
                'country_id' => 'gb',
                'name' => 'Aintree Motor Racing Circuit',
                'slug' => str_slug('Aintree Motor Racing Circuit'),
                'timezone' => 'Europe/London',
                'active' => 0,
            ],
            [
                'country_id' => 'it',
                'name' => 'Circuito di Pescara',
                'slug' => str_slug('Circuito di Pescara'),
                'timezone' => 'Europe/Rome',
                'active' => 0,
            ],
            [
                'country_id' => 'pt',
                'name' => 'Circuito da Boavista',
                'slug' => str_slug('Circuito da Boavista'),
                'timezone' => 'Europe/Lisbon',
                'active' => 0,
            ],
            [
                'country_id' => 'ma',
                'name' => 'Ain-Diab Circuit',
                'slug' => str_slug('Ain-Diab Circuit'),
                'timezone' => 'Africa/Casablanca',
                'active' => 0,
            ],
            [
                'country_id' => 'de',
                'name' => 'Avus',
                'slug' => str_slug('Avus'),
                'timezone' => 'Europe/Berlin',
                'active' => 0,
            ],
            [
                'country_id' => 'pt',
                'name' => 'Circuito de Monsanto',
                'slug' => str_slug('Circuito de Monsanto'),
                'timezone' => 'Europe/Lisbon',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Sebring International Raceway',
                'slug' => str_slug('Sebring International Raceway'),
                'timezone' => 'America/New_York',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Riverside International Raceway',
                'slug' => str_slug('Riverside International Raceway'),
                'timezone' => 'America/Los_Angeles',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Watkins Glen International',
                'slug' => str_slug('Watkins Glen International'),
                'timezone' => 'America/New_York',
                'active' => 0,
            ],
            [
                'country_id' => 'za',
                'name' => 'Prince George Circuit',
                'slug' => str_slug('Prince George Circuit'),
                'timezone' => 'Africa/Johannesburg',
                'active' => 0,
            ],
            [
                'country_id' => 'mx',
                'name' => 'Magdalena Mixhuca Circuit',
                'slug' => str_slug('Magdalena Mixhuca Circuit'),
                'timezone' => 'America/Mexico_City',
                'active' => 0,
            ],
            [
                'country_id' => 'gb',
                'name' => 'Brands Hatch',
                'slug' => str_slug('Brands Hatch'),
                'timezone' => 'Europe/London',
                'active' => 0,
            ],
            [
                'country_id' => 'at',
                'name' => 'Zeltweg',
                'slug' => str_slug('Zeltweg'),
                'timezone' => 'Europe/Vienna',
                'active' => 0,
            ],
            [
                'country_id' => 'mx',
                'name' => 'Autódromo Hermanos Rodríguez',
                'slug' => str_slug('Autódromo Hermanos Rodríguez'),
                'timezone' => 'America/Mexico_City',
                'active' => 1,
            ],
            [
                'country_id' => 'fr',
                'name' => 'Circuit Clermont-Ferrand',
                'slug' => str_slug('Circuit Clermont-Ferrand'),
                'timezone' => 'Europe/Paris',
                'active' => 0,
            ],
            [
                'country_id' => 'za',
                'name' => 'Kyalami Racing Circuit',
                'slug' => str_slug('Kyalami Racing Circuit'),
                'timezone' => 'Africa/Johannesburg',
                'active' => 0,
            ],
            [
                'country_id' => 'fr',
                'name' => 'Bugatti Circuit',
                'slug' => str_slug('Bugatti Circuit'),
                'timezone' => 'Europe/Paris',
                'active' => 0,
            ],
            [
                'country_id' => 'ca',
                'name' => 'Mosport Park',
                'slug' => str_slug('Mosport Park'),
                'timezone' => 'America/Toronto',
                'active' => 0,
            ],
            [
                'country_id' => 'es',
                'name' => 'Circuito del Jarama',
                'slug' => str_slug('Circuito del Jarama'),
                'timezone' => 'Europe/Madrid',
                'active' => 0,
            ],
            [
                'country_id' => 'ca',
                'name' => 'Circuit Mont-Tremblant',
                'slug' => str_slug('Circuit Mont-Tremblant'),
                'timezone' => 'America/Montreal',
                'active' => 0,
            ],
            [
                'country_id' => 'es',
                'name' => 'Montjuïc circuit',
                'slug' => str_slug('Montjuïc circuit'),
                'timezone' => 'Europe/Madrid',
                'active' => 0,
            ],
            [
                'country_id' => 'de',
                'name' => 'Hockenheimring',
                'slug' => str_slug('Hockenheimring'),
                'timezone' => 'Europe/Berlin',
                'active' => 1,
            ],
            [
                'country_id' => 'at',
                'name' => 'Österreichring',
                'slug' => str_slug('Österreichring'),
                'timezone' => 'Europe/Vienna',
                'active' => 0,
            ],
            [
                'country_id' => 'fr',
                'name' => 'Le Castellet',
                'slug' => str_slug('Le Castellet'),
                'timezone' => 'Europe/Paris',
                'active' => 1,
            ],
            [
                'country_id' => 'be',
                'name' => 'Nivelles-Baulers',
                'slug' => str_slug('Nivelles-Baulers'),
                'timezone' => 'Europe/Brussels',
                'active' => 0,
            ],
            [
                'country_id' => 'br',
                'name' => 'Autódromo José Carlos Pace',
                'slug' => str_slug('Autódromo José Carlos Pace'),
                'timezone' => 'America/Sao_Paulo',
                'active' => 1,
            ],
            [
                'country_id' => 'be',
                'name' => 'Circuit Zolder',
                'slug' => str_slug('Circuit Zolder'),
                'timezone' => 'Europe/Brussels',
                'active' => 0,
            ],
            [
                'country_id' => 'se',
                'name' => 'Anderstorp Raceway',
                'slug' => str_slug('Anderstorp Raceway'),
                'timezone' => 'Europe/Stockholm',
                'active' => 0,
            ],
            [
                'country_id' => 'fr',
                'name' => 'Circuit de Dijon-Prenois',
                'slug' => str_slug('Circuit de Dijon-Prenois'),
                'timezone' => 'Europe/Paris',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Long Beach',
                'slug' => str_slug('Long Beach'),
                'timezone' => 'America/Los_Angeles',
                'active' => 0,
            ],
            [
                'country_id' => 'jp',
                'name' => 'Fuji International Speedway',
                'slug' => str_slug('Fuji International Speedway'),
                'timezone' => 'Asia/Tokyo',
                'active' => 0,
            ],
            [
                'country_id' => 'ca',
                'name' => 'Circuit Gilles Villeneuve',
                'slug' => str_slug('Circuit Gilles Villeneuve'),
                'timezone' => 'America/Montreal',
                'active' => 1,
            ],
            [
                'country_id' => 'it',
                'name' => 'Autodromo Enzo e Dino Ferrari',
                'slug' => str_slug('Autodromo Enzo e Dino Ferrari'),
                'timezone' => 'Europe/Rome',
                'active' => 0,
            ],
            [
                'country_id' => 'br',
                'name' => 'Autódromo Internacional Nelson Piquet',
                'slug' => str_slug('Autódromo Internacional Nelson Piquet'),
                'timezone' => 'America/Sao_Paulo',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Caesars Palace',
                'slug' => str_slug('Caesars Palace'),
                'timezone' => 'America/Los_Angeles',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Detroit Street Circuit',
                'slug' => str_slug('Detroit Street Circuit'),
                'timezone' => 'America/New_York',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Fair Park',
                'slug' => str_slug('Fair Park'),
                'timezone' => 'America/Chicago',
                'active' => 0,
            ],
            [
                'country_id' => 'pt',
                'name' => 'Autódromo do Estoril',
                'slug' => str_slug('Autódromo do Estoril'),
                'timezone' => 'Europe/Lisbon',
                'active' => 0,
            ],
            [
                'country_id' => 'au',
                'name' => 'Adelaide Street Circuit',
                'slug' => str_slug('Adelaide Street Circuit'),
                'timezone' => 'Australia/Adelaide',
                'active' => 0,
            ],
            [
                'country_id' => 'es',
                'name' => 'Circuito de Jerez',
                'slug' => str_slug('Circuito de Jerez'),
                'timezone' => 'Europe/Madrid',
                'active' => 0,
            ],
            [
                'country_id' => 'hu',
                'name' => 'Hungaroring',
                'slug' => str_slug('Hungaroring'),
                'timezone' => 'Europe/Budapest',
                'active' => 1,
            ],
            [
                'country_id' => 'jp',
                'name' => 'Suzuka International Racing Course',
                'slug' => str_slug('Suzuka International Racing Course'),
                'timezone' => 'Asia/Tokyo',
                'active' => 1,
            ],
            [
                'country_id' => 'us',
                'name' => 'Phoenix street circuit',
                'slug' => str_slug('Phoenix street circuit'),
                'timezone' => 'America/Phoenix',
                'active' => 0,
            ],
            [
                'country_id' => 'fr',
                'name' => 'Circuit de Nevers Magny-Cours',
                'slug' => str_slug('Circuit de Nevers Magny-Cours'),
                'timezone' => 'Europe/Paris',
                'active' => 0,
            ],
            [
                'country_id' => 'es',
                'name' => 'Circuit de Barcelona-Catalunya',
                'slug' => str_slug('Circuit de Barcelona-Catalunya'),
                'timezone' => 'Europe/Madrid',
                'active' => 1,
            ],
            [
                'country_id' => 'gb',
                'name' => 'Donington Park',
                'slug' => str_slug('Donington Park'),
                'timezone' => 'Europe/London',
                'active' => 0,
            ],
            [
                'country_id' => 'jp',
                'name' => 'TI Circuit Aida',
                'slug' => str_slug('TI Circuit Aida'),
                'timezone' => 'Asia/Tokyo',
                'active' => 0,
            ],
            [
                'country_id' => 'au',
                'name' => 'Albert Park',
                'slug' => str_slug('Albert Park'),
                'timezone' => 'Australia/Melbourne',
                'active' => 1,
            ],
            [
                'country_id' => 'at',
                'name' => 'Red Bull Ring',
                'slug' => str_slug('Red Bull Ring'),
                'timezone' => 'Europe/Vienna',
                'active' => 1,
            ],
            [
                'country_id' => 'my',
                'name' => 'Sepang International Circuit',
                'slug' => str_slug('Sepang International Circuit'),
                'timezone' => 'Asia/Kuala_Lumpur',
                'active' => 0,
            ],
            [
                'country_id' => 'bh',
                'name' => 'Bahrain International Circuit',
                'slug' => str_slug('Bahrain International Circuit'),
                'timezone' => 'Asia/Bahrain',
                'active' => 1,
            ],
            [
                'country_id' => 'cn',
                'name' => 'Shanghai International Circuit',
                'slug' => str_slug('Shanghai International Circuit'),
                'timezone' => 'Asia/Shanghai',
                'active' => 1,
            ],
            [
                'country_id' => 'tr',
                'name' => 'Istanbul Park',
                'slug' => str_slug('Istanbul Park'),
                'timezone' => 'Europe/Istanbul',
                'active' => 0,
            ],
            [
                'country_id' => 'es',
                'name' => 'Valencia Street Circuit',
                'slug' => str_slug('Valencia Street Circuit'),
                'timezone' => 'Europe/Madrid',
                'active' => 0,
            ],
            [
                'country_id' => 'sg',
                'name' => 'Marina Bay Street Circuit',
                'slug' => str_slug('Marina Bay Street Circuit'),
                'timezone' => 'Asia/Singapore',
                'active' => 1,
            ],
            [
                'country_id' => 'ae',
                'name' => 'Yas Marina Circuit',
                'slug' => str_slug('Yas Marina Circuit'),
                'timezone' => 'Asia/Dubai',
                'active' => 1,
            ],
            [
                'country_id' => 'kr',
                'name' => 'Korean International Circuit',
                'slug' => str_slug('Korean International Circuit'),
                'timezone' => 'Asia/Seoul',
                'active' => 0,
            ],
            [
                'country_id' => 'in',
                'name' => 'Buddh International Circuit',
                'slug' => str_slug('Buddh International Circuit'),
                'timezone' => 'Asia/Kolkata',
                'active' => 0,
            ],
            [
                'country_id' => 'us',
                'name' => 'Circuit of the Americas',
                'slug' => str_slug('Circuit of the Americas'),
                'timezone' => 'America/Chicago',
                'active' => 1,
            ],
            [
                'country_id' => 'ru',
                'name' => 'Sochi Autodrom',
                'slug' => str_slug('Sochi Autodrom'),
                'timezone' => 'Asia/Tbilisi',
                'active' => 1,
            ],
            [
                'country_id' => 'az',
                'name' => 'Baku City Circuit',
                'slug' => str_slug('Baku City Circuit'),
                'timezone' => 'Asia/Baku',
                'active' => 1,
            ],
            [
                'country_id' => 'vn',
                'name' => 'Vietnam street circuit',
                'slug' => str_slug('Vietnam street circuit'),
                'timezone' => 'Asia/Ho_Chi_Minh',
                'active' => 0,
            ],
        ]);
    }
}
