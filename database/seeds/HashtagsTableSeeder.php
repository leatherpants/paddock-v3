<?php

use Illuminate\Database\Seeder;

class HashtagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hashtags')->insert([
            [
                'title' => 'BringYourPassion',
                'slug' => str_slug('BringYourPassion'),
            ],
            [
                'title' => 'ForzaFerrari',
                'slug' => str_slug('ForzaFerrari'),
            ],
            [
                'title' => 'F1Testing',
                'slug' => str_slug('F1Testing'),
            ],
            [
                'title' => 'SF71H',
                'slug' => str_slug('SF71H'),
            ],
            [
                'title' => 'Kimi7',
                'slug' => str_slug('Kimi7'),
            ],
            [
                'title' => 'Seb5',
                'slug' => str_slug('Seb5'),
            ],
            [
                'title' => 'BritishGP',
                'slug' => str_slug('BritishGP'),
            ],
            [
                'title' => 'F12018',
                'slug' => str_slug('F12018'),
            ],
            [
                'title' => 'AusGP',
                'slug' => str_slug('AusGP'),
            ],
            [
                'title' => 'F12017',
                'slug' => str_slug('F12017'),
            ],
            [
                'title' => 'Ferrari',
                'slug' => str_slug('Ferrari'),
            ],
            [
                'title' => 'BahrainGP',
                'slug' => str_slug('BahrainGP'),
            ],
            [
                'title' => 'F12010',
                'slug' => str_slug('F12010'),
            ],
            [
                'title' => 'ChineseGP',
                'slug' => str_slug('ChineseGP'),
            ],
            [
                'title' => 'F12006',
                'slug' => str_slug('F12006'),
            ],
            [
                'title' => 'AzerbaijanGP',
                'slug' => str_slug('AzerbaijanGP'),
            ],
            [
                'title' => 'SpanishGP',
                'slug' => str_slug('SpanishGP'),
            ],
            [
                'title' => 'F11996',
                'slug' => str_slug('F11996'),
            ],
            [
                'title' => 'F12000',
                'slug' => str_slug('F12000'),
            ],
            [
                'title' => 'F12013',
                'slug' => str_slug('F12013'),
            ],
            [
                'title' => 'MonacoGP',
                'slug' => str_slug('MonacoGP'),
            ],
            [
                'title' => 'F12016',
                'slug' => str_slug('F12016'),
            ],
            [
                'title' => 'CanadianGP',
                'slug' => str_slug('CanadianGP'),
            ],
            [
                'title' => 'F12001',
                'slug' => str_slug('F12001'),
            ],
            [
                'title' => 'F11995',
                'slug' => str_slug('F11995'),
            ],
            [
                'title' => 'FrenchGP',
                'slug' => str_slug('FrenchGP'),
            ],
            [
                'title' => 'F12008',
                'slug' => str_slug('F12008'),
            ],
            [
                'title' => 'AustrianGP',
                'slug' => str_slug('AustrianGP'),
            ],
            [
                'title' => 'F12015',
                'slug' => str_slug('F12015'),
            ],
            [
                'title' => 'GermanGP',
                'slug' => str_slug('GermanGP'),
            ],
            [
                'title' => 'HungarianGP',
                'slug' => str_slug('HungarianGP'),
            ],
            [
                'title' => 'BelgianGP',
                'slug' => str_slug('BelgianGP'),
            ],
            [
                'title' => 'F11998',
                'slug' => str_slug('F11998'),
            ],
            [
                'title' => 'ItalianGP',
                'slug' => str_slug('ItalianGP'),
            ],
            [
                'title' => 'SingaporeGP',
                'slug' => str_slug('SingaporeGP'),
            ],
            [
                'title' => 'BeyondTheGrid',
                'slug' => str_slug('BeyondTheGrid'),
            ],
            [
                'title' => 'RussianGP',
                'slug' => str_slug('RussianGP'),
            ],
            [
                'title' => 'JapaneseGP',
                'slug' => str_slug('JapaneseGP'),
            ],
            [
                'title' => 'USGP',
                'slug' => str_slug('USGP'),
            ],
            [
                'title' => 'MexicoGP',
                'slug' => str_slug('MexicoGP'),
            ],
            [
                'title' => 'BrazilGP',
                'slug' => str_slug('BrazilGP'),
            ],
            [
                'title' => 'AbuDhabiGP',
                'slug' => str_slug('AbuDhabiGP'),
            ],
        ]);
    }
}
