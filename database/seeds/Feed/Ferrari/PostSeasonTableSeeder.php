<?php

use Illuminate\Database\Seeder;

class PostSeasonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feed_ferrari')->insert([
            [
                'session' => 1,
                'lang' => 'en',
                'title' => 'Happy Holidays from Ferrari',
                'description' => 'The future is just around the corner',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/e8baa9f4f4175adbdfd42199777fde36.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-holidays-from-ferrari/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'de',
                'title' => 'Frohe Feiertage von Ferrari',
                'description' => '',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/e8baa9f4f4175adbdfd42199777fde36.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-holidays-from-ferrari/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'it',
                'title' => 'Buone feste da Ferrari',
                'description' => '',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/e8baa9f4f4175adbdfd42199777fde36.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/it/buone-feste-da-ferrari-2018-2/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'se',
                'title' => 'Lyckliga helgdagar från Ferrari',
                'description' => '',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/e8baa9f4f4175adbdfd42199777fde36.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-holidays-from-ferrari/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'es',
                'title' => 'Felices vacaciones desde Ferrari',
                'description' => '',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/e8baa9f4f4175adbdfd42199777fde36.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-holidays-from-ferrari/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-24 20:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'en',
                'title' => 'Happy New Year from Ferrari',
                'description' => 'All set for 2019',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/ef0bbffaf15354216d21163cdc89567c.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-new-year-from-ferrari-2019/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'de',
                'title' => 'Frohes neues Jahr von Ferrari',
                'description' => '',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/ef0bbffaf15354216d21163cdc89567c.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-new-year-from-ferrari-2019/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'it',
                'title' => 'Buon anno da Ferrari',
                'description' => 'Pronti per il 2019',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/ef0bbffaf15354216d21163cdc89567c.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/it/buon-anno-da-ferrari-2019/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'se',
                'title' => 'Gott nytt år från Ferrari',
                'description' => '',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/ef0bbffaf15354216d21163cdc89567c.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-new-year-from-ferrari-2019/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
            ],
            [
                'session' => 1,
                'lang' => 'es',
                'title' => 'Feliz año nuevo de Ferrari',
                'description' => '',
                'image' => 'https://static.formula1.ferrari.com/imgresize-cache/ef0bbffaf15354216d21163cdc89567c.jpg',
                'domain' => 'formula1.ferrari.com',
                'link' => 'https://formula1.ferrari.com/en/happy-new-year-from-ferrari-2019/',
                'hashtags' => '',
                'created_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2018-12-31 17:00:00')),
            ],
        ]);
    }
}
