<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
            [
                'name' => 'SF70H',
                'slug' => str_slug('SF70H'),
            ],
            [
                'name' => 'SF71H',
                'slug' => str_slug('SF71H'),
            ],
        ]);
    }
}
