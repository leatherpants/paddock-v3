<?php

use Illuminate\Database\Seeder;

class UsersSocialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_socials')->insert([
            [
                'user_id' => 1,
                'facebook_username' => 'ScuderiaFerrari',
                'twitter_username' => 'Scuderiaferrari',
                'instagram_username' => 'scuderiaferrari',
                'created_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
            ],
            [
                'user_id' => 2,
                'facebook_username' => null,
                'twitter_username' => null,
                'instagram_username' => null,
                'created_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
            ],
            [
                'user_id' => 3,
                'facebook_username' => 'sandra.granath.3',
                'twitter_username' => 'Sandraagranath',
                'instagram_username' => 'sandragranath',
                'created_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
            ],
        ]);
    }
}
