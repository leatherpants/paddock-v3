<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1945-01-01',
                'lang' => 'en',
                'event_text' => 'Ferrari\'s 6 times Grand Prix winner Jacky Ickx was born in 1945 and turns 73 today.',
                'event_twitter' => urlencode('Ferrari\'s 6 times Grand Prix winner Jacky Ickx was born in 1945 and turns 73 today.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1945-01-01',
                'lang' => 'de',
                'event_text' => 'Ferrari\'s 6-facher Grand Prix Sieger Jacky Ickx wurde 1945 geboren und wird heute 73 Jahre alt.',
                'event_twitter' => urlencode('Ferrari\'s 6-facher Grand Prix Sieger Jacky Ickx wurde 1945 geboren und wird heute 73 Jahre alt.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1945-01-01',
                'lang' => 'it',
                'event_text' => 'Jacky Ickx, vincitore del 6 ° Gran Premio della Ferrari, è nato nel 1945 e compie oggi 73 anni.',
                'event_twitter' => urlencode('Jacky Ickx, vincitore del 6 ° Gran Premio della Ferrari, è nato nel 1945 e compie oggi 73 anni.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1945-01-01',
                'lang' => 'se',
                'event_text' => 'Ferrari s 6 gånger Grand Prix vinnare Jacky Ickx föddes 1945 och vänder 73 i dag.',
                'event_twitter' => urlencode('Ferrari s 6 gånger Grand Prix vinnare Jacky Ickx föddes 1945 och vänder 73 i dag.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1945-01-01',
                'lang' => 'es',
                'event_text' => 'El seis veces ganador del Gran Premio de Ferrari, Jacky Ickx, nació en 1945 y hoy cumple 73 años.',
                'event_twitter' => urlencode('El seis veces ganador del Gran Premio de Ferrari, Jacky Ickx, nació en 1945 y hoy cumple 73 años.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1969-01-03',
                'lang' => 'en',
                'event_text' => 'Michael Schumacher, the most successful driver of #Ferrari, turns 49.',
                'event_twitter' => urlencode('Michael Schumacher, the most successful driver of #Ferrari, turns 49.'),
                'event_hashtags' => 'FerrariHistory,KeepFightingMichael',
                'event_link' => 'http://formula1.ferrari.com/en/michael-schumacher/',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1969-01-03',
                'lang' => 'de',
                'event_text' => 'Michael Schumacher, der erfolgreichste Fahrer von #Ferrari, wird 49 Jahre alt.',
                'event_twitter' => urlencode('Michael Schumacher, der erfolgreichste Fahrer von #Ferrari, wird 49 Jahre alt.'),
                'event_hashtags' => 'FerrariHistory,KeepFightingMichael',
                'event_link' => 'http://formula1.ferrari.com/en/michael-schumacher/',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1969-01-03',
                'lang' => 'it',
                'event_text' => 'Michael Schumacher, il pilota di maggior successo della #Ferrari, compie 49 anni.',
                'event_twitter' => urlencode('Michael Schumacher, il pilota di maggior successo della #Ferrari, compie 49 anni.'),
                'event_hashtags' => 'FerrariHistory,KeepFightingMichael',
                'event_link' => 'http://formula1.ferrari.com/it/michael-schumacher-2/',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1969-01-03',
                'lang' => 'se',
                'event_text' => 'Michael Schumacher, den mest framgångsrika föraren av #Ferrari, blir 49.',
                'event_twitter' => urlencode('Michael Schumacher, den mest framgångsrika föraren av #Ferrari, blir 49.'),
                'event_hashtags' => 'FerrariHistory,KeepFightingMichael',
                'event_link' => 'http://formula1.ferrari.com/en/michael-schumacher/',
            ],
            [
                'staff_id' => '1',
                'type' => 'birthday',
                'date' => '1969-01-03',
                'lang' => 'es',
                'event_text' => 'Michael Schumacher, el piloto más exitoso de #Ferrari, cumple 49 años.',
                'event_twitter' => urlencode('Michael Schumacher, el piloto más exitoso de #Ferrari, cumple 49 años.'),
                'event_hashtags' => 'FerrariHistory,KeepFightingMichael',
                'event_link' => 'http://formula1.ferrari.com/en/michael-schumacher/',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1920-01-05',
                'lang' => 'en',
                'event_text' => 'In 1952 at the Swiss GP and Italian GP, André Simon drove for Ferrari. #OnThisDay, he was born in Paris in 1920.',
                'event_twitter' => urlencode('In 1952 at the Swiss GP and Italian GP, André Simon drove for Ferrari. #OnThisDay, he was born in Paris in 1920.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1920-01-05',
                'lang' => 'de',
                'event_text' => '1952 fuhr André Simon beim Schweizer GP und beim italienischen GP für Ferrari. #AnDiesenTag wurde er 1920 in Paris geboren.',
                'event_twitter' => urlencode('1952 fuhr André Simon beim Schweizer GP und beim italienischen GP für Ferrari. #AnDiesenTag wurde er 1920 in Paris geboren.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1920-01-05',
                'lang' => 'it',
                'event_text' => 'Nel 1952 al GP di Svizzera e al GP d\'Italia, André Simon guidava per la Ferrari. #InQuestoGiorno, è nato a Parigi nel 1920.',
                'event_twitter' => urlencode('Nel 1952 al GP di Svizzera e al GP d\'Italia, André Simon guidava per la Ferrari. #InQuestoGiorno, è nato a Parigi nel 1920.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1920-01-05',
                'lang' => 'se',
                'event_text' => 'År 1952 körde André Simon för Ferrari i den schweiziska GP och italienska GP. #PåDennaDag föddes han i Paris 1920.',
                'event_twitter' => urlencode('År 1952 körde André Simon för Ferrari i den schweiziska GP och italienska GP. #PåDennaDag föddes han i Paris 1920.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1920-01-05',
                'lang' => 'es',
                'event_text' => 'En 1952 en el GP de Suiza y el GP de Italia, André Simon condujo para Ferrari. #EnEsteDía, nació en París en 1920.',
                'event_twitter' => urlencode('En 1952 en el GP de Suiza y el GP de Italia, André Simon condujo para Ferrari. #EnEsteDía, nació en París en 1920.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'dead',
                'date' => '1971-01-10',
                'lang' => 'en',
                'event_text' => 'Ignazio Giunti, an Italian racing driver, was killed in 1971 during the 1000 km Buenos Aires.

One year before he completed several races in F1 for the Scuderia Ferrari.',
                'event_twitter' => urlencode('Ignazio Giunti, an Italian racing driver, was killed in 1971 during the 1000 km Buenos Aires.

One year before he completed several races in F1 for the Scuderia Ferrari.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'dead',
                'date' => '1971-01-10',
                'lang' => 'de',
                'event_text' => 'Ignazio Giunti, ein italienischer Rennfahrer, wurde während der 1000 km von Buenos Aires im Jahr 1971 getötet.

Ein Jahr zuvor, absolvierte er mehrere Rennen in der Formel 1 für die Scuderia Ferrari.',
                'event_twitter' => urlencode('Ignazio Giunti, ein italienischer Rennfahrer, wurde während der 1000 km von Buenos Aires im Jahr 1971 getötet.

Ein Jahr zuvor, absolvierte er mehrere Rennen in der Formel 1 für die Scuderia Ferrari.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'dead',
                'date' => '1971-01-10',
                'lang' => 'it',
                'event_text' => 'Ignazio Giunti, un pilota automobilistico italiano, è stato ucciso del 1971 sul circuito di Buenos Aires durante la 1000 km.

Un anno prima ha completato diverse gare in F1 per la Scuderia Ferrari.',
                'event_twitter' => urlencode('Ignazio Giunti, un pilota automobilistico italiano, è stato ucciso del 1971 sul circuito di Buenos Aires durante la 1000 km.

Un anno prima ha completato diverse gare in F1 per la Scuderia Ferrari.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'dead',
                'date' => '1971-01-10',
                'lang' => 'se',
                'event_text' => 'Ignazio Giunti, en italiensk racerförare, dödades 1971 i Buenos Aires 1000 km.

Ett år innan avslutade han flera raser i F1 för Scuderia Ferrari.',
                'event_twitter' => urlencode('Ignazio Giunti, en italiensk racerförare, dödades 1971 i Buenos Aires 1000 km.

Ett år innan avslutade han flera raser i F1 för Scuderia Ferrari.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'dead',
                'date' => '1971-01-10',
                'lang' => 'es',
                'event_text' => 'Ignazio Giunti, un piloto de automovilismo italiano, murió en el año 1971 durante los 1000 km de Buenos Aires.

Un año antes, completó varias carreras en F1 para la Scuderia Ferrari.',
                'event_twitter' => urlencode('Ignazio Giunti, un piloto de automovilismo italiano, murió en el año 1971 durante los 1000 km de Buenos Aires.

Un año antes, completó varias carreras en F1 para la Scuderia Ferrari.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1924-01-12',
                'lang' => 'en',
                'event_text' => 'Olivier Gendebien was born in Brussels in 1924 and was part of the Scuderia Ferrari in 1956 as well as from 1958 to 1961.',
                'event_twitter' => urlencode('Olivier Gendebien was born in Brussels in 1924 and was part of the Scuderia Ferrari in 1956 as well as from 1958 to 1961.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1924-01-12',
                'lang' => 'de',
                'event_text' => 'Olivier Gendebien wurde 1924 in Brüssel geboren und war 1956 Teil der Scuderia Ferrari sowie von 1958 bis 1961.',
                'event_twitter' => urlencode('Olivier Gendebien wurde 1924 in Brüssel geboren und war 1956 Teil der Scuderia Ferrari sowie von 1958 bis 1961.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1924-01-12',
                'lang' => 'it',
                'event_text' => 'Olivier Gendebien è nato a Bruxelles nel 1924 e ha fatto parte della Scuderia Ferrari nel 1956 e dal 1958 al 1961.',
                'event_twitter' => urlencode('Olivier Gendebien è nato a Bruxelles nel 1924 e ha fatto parte della Scuderia Ferrari nel 1956 e dal 1958 al 1961.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1924-01-12',
                'lang' => 'se',
                'event_text' => 'Olivier Gendebien föddes i Bryssel 1924 och var en del av Scuderia Ferrari 1956 samt 1958-1961.',
                'event_twitter' => urlencode('Olivier Gendebien föddes i Bryssel 1924 och var en del av Scuderia Ferrari 1956 samt 1958-1961.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
            [
                'staff_id' => 1,
                'type' => 'birthday',
                'date' => '1924-01-12',
                'lang' => 'es',
                'event_text' => 'Olivier Gendebien nació en Bruselas en 1924 y formó parte de la Scuderia Ferrari en 1956, así como de 1958 a 1961.',
                'event_twitter' => urlencode('Olivier Gendebien nació en Bruselas en 1924 y formó parte de la Scuderia Ferrari en 1956, así como de 1958 a 1961.'),
                'event_hashtags' => 'FerrariHistory',
                'event_link' => '',
            ],
        ]);
    }
}
