<?php

use Illuminate\Database\Seeder;

class SeasonsRaces1990TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seasons_races')->insert([
            [
                'season' => 1990,
                'raceday' => '1990-03-11',
                'gp_id' => 15,
                'track_id' => 55,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-03-25',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-05-13',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-05-27',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-06-10',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-06-24',
                'gp_id' => 17,
                'track_id' => 26,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-07-08',
                'gp_id' => 6,
                'track_id' => 36,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-07-15',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-07-29',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-08-12',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-08-26',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-09-09',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-09-23',
                'gp_id' => 13,
                'track_id' => 50,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-09-30',
                'gp_id' => 9,
                'track_id' => 52,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-10-21',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1990,
                'raceday' => '1990-11-04',
                'gp_id' => 28,
                'track_id' => 51,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-03-10',
                'gp_id' => 15,
                'track_id' => 55,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-03-24',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-04-28',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-05-12',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-06-02',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-06-16',
                'gp_id' => 17,
                'track_id' => 26,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-07-07',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-07-14',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-07-28',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-08-11',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-08-25',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-09-08',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-09-22',
                'gp_id' => 13,
                'track_id' => 50,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-09-29',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-10-20',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1991,
                'raceday' => '1991-11-03',
                'gp_id' => 28,
                'track_id' => 51,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-03-01',
                'gp_id' => 16,
                'track_id' => 28,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-03-22',
                'gp_id' => 17,
                'track_id' => 26,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-04-05',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-05-03',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-05-17',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-05-31',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-06-14',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-07-05',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-07-12',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-07-26',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-08-16',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-08-30',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-09-13',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-09-27',
                'gp_id' => 13,
                'track_id' => 50,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-10-25',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1992,
                'raceday' => '1992-11-08',
                'gp_id' => 28,
                'track_id' => 51,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-03-14',
                'gp_id' => 16,
                'track_id' => 28,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-03-28',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-04-11',
                'gp_id' => 27,
                'track_id' => 58,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-04-25',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-05-09',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-05-23',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-06-13',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-07-04',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-07-11',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-07-25',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-08-15',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-08-29',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-09-12',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-09-26',
                'gp_id' => 13,
                'track_id' => 50,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-10-24',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1993,
                'raceday' => '1993-11-07',
                'gp_id' => 28,
                'track_id' => 51,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-03-27',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-04-17',
                'gp_id' => 30,
                'track_id' => 59,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-05-01',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-05-15',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-05-29',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-06-12',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-07-03',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-07-10',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-07-31',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-08-14',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-08-28',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-09-11',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-09-25',
                'gp_id' => 13,
                'track_id' => 50,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-10-16',
                'gp_id' => 27,
                'track_id' => 52,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-11-06',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1994,
                'raceday' => '1994-11-13',
                'gp_id' => 28,
                'track_id' => 51,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-03-26',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-04-09',
                'gp_id' => 11,
                'track_id' => 12,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-04-30',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-05-14',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-05-28',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-06-11',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-07-02',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-07-16',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-07-30',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-08-13',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-08-27',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-09-10',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-09-24',
                'gp_id' => 13,
                'track_id' => 50,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-10-01',
                'gp_id' => 27,
                'track_id' => 8,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-10-22',
                'gp_id' => 30,
                'track_id' => 59,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-10-29',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1995,
                'raceday' => '1995-11-12',
                'gp_id' => 28,
                'track_id' => 51,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-03-10',
                'gp_id' => 28,
                'track_id' => 60,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-03-31',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-04-07',
                'gp_id' => 11,
                'track_id' => 12,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-04-28',
                'gp_id' => 27,
                'track_id' => 8,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-05-05',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-05-19',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-06-02',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-06-16',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-06-30',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-07-14',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-07-28',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-08-11',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-08-25',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-09-08',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-09-22',
                'gp_id' => 13,
                'track_id' => 50,
            ],
            [
                'season' => 1996,
                'raceday' => '1996-10-13',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-03-09',
                'gp_id' => 28,
                'track_id' => 60,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-03-30',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-04-13',
                'gp_id' => 11,
                'track_id' => 12,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-04-27',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-05-11',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-05-25',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-06-15',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-06-29',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-07-13',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-07-27',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-08-10',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-08-24',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-09-07',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-09-21',
                'gp_id' => 18,
                'track_id' => 61,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-09-28',
                'gp_id' => 31,
                'track_id' => 8,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-10-12',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1997,
                'raceday' => '1997-10-26',
                'gp_id' => 27,
                'track_id' => 52,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-03-08',
                'gp_id' => 28,
                'track_id' => 60,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-03-29',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-04-12',
                'gp_id' => 11,
                'track_id' => 12,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-04-26',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-05-10',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-05-24',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-06-07',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-06-28',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-07-12',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-07-26',
                'gp_id' => 18,
                'track_id' => 61,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-08-02',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-08-16',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-08-30',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-09-13',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-09-27',
                'gp_id' => 31,
                'track_id' => 8,
            ],
            [
                'season' => 1998,
                'raceday' => '1998-11-01',
                'gp_id' => 24,
                'track_id' => 54,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-03-07',
                'gp_id' => 28,
                'track_id' => 60,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-04-11',
                'gp_id' => 20,
                'track_id' => 38,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-05-02',
                'gp_id' => 25,
                'track_id' => 45,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-05-16',
                'gp_id' => 2,
                'track_id' => 2,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-05-30',
                'gp_id' => 9,
                'track_id' => 57,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-06-13',
                'gp_id' => 19,
                'track_id' => 44,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-06-27',
                'gp_id' => 6,
                'track_id' => 56,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-07-11',
                'gp_id' => 1,
                'track_id' => 1,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-07-25',
                'gp_id' => 18,
                'track_id' => 61,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-08-01',
                'gp_id' => 8,
                'track_id' => 34,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-08-15',
                'gp_id' => 29,
                'track_id' => 53,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-08-29',
                'gp_id' => 5,
                'track_id' => 5,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-09-12',
                'gp_id' => 7,
                'track_id' => 7,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-09-26',
                'gp_id' => 27,
                'track_id' => 8,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-10-17',
                'gp_id' => 32,
                'track_id' => 62,
            ],
            [
                'season' => 1999,
                'raceday' => '1999-10-31',
                'gp_id' => 24,
                'track_id' => 54,
            ],
        ]);
    }
}
