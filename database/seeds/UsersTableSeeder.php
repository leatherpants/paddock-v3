<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'email' => 'forzaferrari@paddock.de',
                'password' => bcrypt('adlershof'),
                'first_name' => 'Forza',
                'last_name' => 'Ferrari',
                'display_name' => 'Forza Ferrari',
                'user_name' => str_slug('Forza Ferrari'),
                'avatar' => 'http://media.ferrari.granath/users/forza-ferrari.jpg',
                'background_image' => 488,
                'lang' => 'en',
                'timezone' => 'Asia/Dubai',
                'isAdmin' => 1,
                'created_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
            ],
            [
                'email' => 'jessikagranath@paddock.de',
                'password' => bcrypt('adlershof'),
                'first_name' => 'Jessika',
                'last_name' => 'Granath',
                'display_name' => 'Jessika Granath',
                'user_name' => str_slug('Jessika Granath'),
                'avatar' => 'http://media.ferrari.granath/users/jessika-granath.jpg',
                'background_image' => 488,
                'lang' => 'en',
                'timezone' => 'Asia/Dubai',
                'isAdmin' => 1,
                'created_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
            ],
            [
                'email' => 'sandragranath@paddock.de',
                'password' => bcrypt('7!Ftatr3T6kud$9O'),
                'first_name' => 'Sandra',
                'last_name' => 'Granath',
                'display_name' => 'Sandra Granath',
                'user_name' => str_slug('Sandra Granath'),
                'avatar' => 'http://media.ferrari.granath/users/sandra-granath.jpg',
                'background_image' => 488,
                'lang' => 'en',
                'timezone' => 'Asia/Dubai',
                'isAdmin' => 1,
                'created_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
                'updated_at' => date('Y-m-d H:i:s', strtotime('2017-11-27 12:00:00')),
            ],
        ]);
    }
}
