<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'key' => 'season',
                'value' => 2018,
            ],
            [
                'key' => 'gp_id',
                'value' => null,
            ],
        ]);
    }
}
