<?php

use Illuminate\Database\Seeder;

class TwitterAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('twitter_accounts')->insert([
            [
                'twitter_id' => 134557140,
                'name' => 'ForzaFerrari',
                'screen_name' => 'ForzaFerrari',
                'profile_image_url_https' => null,
                'active' => 1,
                'owner' => 1,
            ],
            [
                'twitter_id' => 815334792317300738,
                'name' => 'ForzaFerrariDE',
                'screen_name' => 'ForzaFerrariDE',
                'profile_image_url_https' => null,
                'active' => 1,
                'owner' => 1,
            ],
            [
                'twitter_id' => 815336154966986756,
                'name' => 'ForzaFerrariEN',
                'screen_name' => 'ForzaFerrariEN',
                'profile_image_url_https' => null,
                'active' => 1,
                'owner' => 1,
            ],
            [
                'twitter_id' => 815337028330131456,
                'name' => 'ForzaFerrariIT',
                'screen_name' => 'ForzaFerrariIT',
                'profile_image_url_https' => null,
                'active' => 1,
                'owner' => 1,
            ],
            [
                'twitter_id' => 826873432335851521,
                'name' => 'ForzaFerrariSE',
                'screen_name' => 'ForzaFerrariSE',
                'profile_image_url_https' => null,
                'active' => 1,
                'owner' => 1,
            ],
            [
                'twitter_id' => 870131780648566784,
                'name' => 'ForzaFerrariES',
                'screen_name' => 'ForzaFerrariES',
                'profile_image_url_https' => null,
                'active' => 1,
                'owner' => 1,
            ],
            [
                'twitter_id' => 108247668,
                'name' => 'Scuderia Ferrari',
                'screen_name' => 'ScuderiaFerrari',
                'profile_image_url_https' => null,
                'active' => 1,
                'owner' => 1,
            ],
        ]);
    }
}
