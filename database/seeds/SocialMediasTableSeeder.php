<?php

use Illuminate\Database\Seeder;

class SocialMediasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social_medias')->insert([
            [
                'name' => 'Scuderia Ferrari',
                'avatar' => 'http://media.ferrari.granath/logos/social/ferrari.jpg',
                'website' => 'http://formula1.ferrari.com/en/',
                'facebook' => 'https://www.facebook.com/ScuderiaFerrari/',
                'twitter' => 'https://twitter.com/Scuderiaferrari/',
                'instagram' => 'https://www.instagram.com/scuderiaferrari/',
                'youtube' => 'https://www.youtube.com/user/ferrariworld/',
            ],
            [
                'name' => 'F1',
                'avatar' => 'http://media.ferrari.granath/logos/social/f1.jpg',
                'website' => 'https://www.formula1.com/',
                'facebook' => 'https://www.facebook.com/Formula1/',
                'twitter' => 'https://twitter.com/F1/',
                'instagram' => 'https://www.instagram.com/F1/',
                'youtube' => 'https://www.youtube.com/F1/',
            ],
            [
                'name' => 'Kimi Räikkönen',
                'avatar' => 'http://media.ferrari.granath/logos/social/kimi.jpg',
                'website' => 'https://www.kimiraikkonen.com/',
                'facebook' => '',
                'twitter' => '',
                'instagram' => 'https://www.instagram.com/kimimatiasraikkonen/',
                'youtube' => '',
            ],
            [
                'name' => 'Pirelli',
                'avatar' => 'http://media.ferrari.granath/logos/social/pirelli.jpg',
                'website' => 'https://news.pirelli.com/global/en-ww/allnews',
                'facebook' => 'https://www.facebook.com/PirelliMotorsport/',
                'twitter' => 'https://twitter.com/pirellisport/',
                'instagram' => 'https://www.instagram.com/pirelli_motorsport/',
                'youtube' => '',
            ],
        ]);
    }
}
