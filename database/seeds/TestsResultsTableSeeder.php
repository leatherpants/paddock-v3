<?php

use Illuminate\Database\Seeder;

class TestsResultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tests_results')->insert([
            [
                'season' => 2018,
                'week' => 1,
                'day' => 1,
                'dateoftest' => '2018-02-26',
                'track_id' => 57,
                'staff_id' => 72,
                'pos' => 3,
                'laptime' => 80506,
                'laps' => 80,
            ],
            [
                'season' => 2018,
                'week' => 1,
                'day' => 2,
                'dateoftest' => '2018-02-27',
                'track_id' => 57,
                'staff_id' => 76,
                'pos' => 1,
                'laptime' => 79673,
                'laps' => 98,
            ],
            [
                'season' => 2018,
                'week' => 1,
                'day' => 3,
                'dateoftest' => '2018-02-28',
                'track_id' => 57,
                'staff_id' => 72,
                'pos' => 4,
                'laptime' => 0,
                'laps' => 0,
            ],
            [
                'season' => 2018,
                'week' => 1,
                'day' => 4,
                'dateoftest' => '2018-03-01',
                'track_id' => 57,
                'staff_id' => 76,
                'pos' => 2,
                'laptime' => 80241,
                'laps' => 120,
            ],
            [
                'season' => 2018,
                'week' => 2,
                'day' => 1,
                'dateoftest' => '2018-03-06',
                'track_id' => 57,
                'staff_id' => 76,
                'pos' => 5,
                'laptime' => 80396,
                'laps' => 171,
            ],
            [
                'season' => 2018,
                'week' => 2,
                'day' => 2,
                'dateoftest' => '2018-03-07',
                'track_id' => 57,
                'staff_id' => 76,
                'pos' => 3,
                'laptime' => 79541,
                'laps' => 66,
            ],
            [
                'season' => 2018,
                'week' => 2,
                'day' => 2,
                'dateoftest' => '2018-03-07',
                'track_id' => 57,
                'staff_id' => 72,
                'pos' => 4,
                'laptime' => 80242,
                'laps' => 49,
            ],
            [
                'season' => 2018,
                'week' => 2,
                'day' => 3,
                'dateoftest' => '2018-03-08',
                'track_id' => 57,
                'staff_id' => 76,
                'pos' => 1,
                'laptime' => 77182,
                'laps' => 188,
            ],
            [
                'season' => 2018,
                'week' => 2,
                'day' => 4,
                'dateoftest' => '2018-03-09',
                'track_id' => 57,
                'staff_id' => 72,
                'pos' => 2,
                'laptime' => 77221,
                'laps' => 155,
            ],
        ]);
    }
}
