<?php

use Illuminate\Database\Seeder;

class MediaGalleryPhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_gallery_photos')->insert([
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'sf71h')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a94af1f9-2c93-4ee5-a56a-38234d9463e2?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'sf71h')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/359e6ccb-316f-4078-8ece-1372cede1de8?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'sf71h')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/df6e133c-f598-4206-ab78-00ef167b4b1a?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'sf71h')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/1a743e9a-47ad-4379-9e10-628e587f9738?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'sf71h')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b38a7a9a-b3d8-47a5-abae-2d7782d3db5b?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'sf71h')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/52ed376b-0d67-4359-b051-9b3c4cadbb88?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/107c3f03-66cc-43d2-a1d6-cb2dc6f0f280?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/76388425-ef9c-44a6-b0e5-605c0206a9fa?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c3eec5ac-980e-46a0-92da-15729ae8890b?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ec00f2f2-0024-4361-8086-ad185e1f1e79?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2ce546ef-753f-481c-8373-25268fc14042?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/99963414-4f4d-4199-b41d-8e299efbfc89?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7e4111a0-74ae-4c9d-b57e-2b8d3ce773d7?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d4884dea-20bf-429d-a6a4-3f055d917c73?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-4')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c8be9019-e61b-4e34-b15f-b1aeafe21b56?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-4')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x892/3983c05c-887e-4312-b59f-3d9fdc68e247?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-4')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1511x892/80bffa84-e6bb-4a75-ad1e-ef69121e2231?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'barcelona-test-2018-day-4')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1511x892/8ae95af7-389f-4924-970c-ad50463c7963?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-5')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/57b4ca71-db3b-47b6-bb16-d5df1ad41fb9?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-5')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x892/1c244bd3-1809-4ef6-9e50-c39c585acb49?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-5')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1112x892/d947b7ee-77f5-4d31-9a78-6c1c81331c86?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-5')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1112x892/558d4e9c-7cb8-479a-ba82-092928c11940?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-5')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1112x892/34344aff-8df1-4d53-a822-26c866b7e216?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-6')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/676e8b2b-9b43-4de7-b65a-61e3d4be6606?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-6')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/928ae5fe-149e-4919-aaaf-a0dda85182f3?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-6')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/38827712-ef63-4c58-a300-a0cd706bbf14?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-6')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/23985255-bb24-4543-9f42-bb0b9645a01e?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-7')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3a39fffd-7e60-4b07-b819-56df8c2c8146?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-7')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/61061203-0b94-4c0a-81ab-8c1944b0d48c?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-7')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0448a89a-13c9-4815-8c52-1798fd807df2?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-7')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7bf5dd5d-76bc-4507-8c2d-66d9e4eb8918?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-8')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ec9d3263-5e70-41be-a14c-8e2fdd279b92?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-8')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/76e7fc5b-d8e9-450f-b705-1bc188b21179?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-8')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ca9f8595-e202-455f-9d5f-d9e147d3d5c7?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'test-barcelona-2018-day-8')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2d483d9f-8fa4-4d58-b60f-50368d2d7be0?v=3',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-wednesday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/880c1fe8-837c-4f19-adbb-2414bc4c77ec?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-wednesday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5f47db50-518b-42dc-b0a9-142152e6b5f3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-wednesday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1403x909/4e7a4954-b3c5-4775-8392-cad02ebba75f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-wednesday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1403x909/c13aa966-ce85-4cab-8a5d-ae65478bdc96?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/62bee1d7-598e-4a5f-a9c9-3a4b9003003c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8b5c9544-5d23-45e5-9d7f-79092c541b78?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1403x926/21dbf6c0-c261-4ec3-a3fa-aefded8a7876?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1403x926/9312e8da-41fd-4d0d-afaa-6207d5b17d71?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/e6d085ad-601d-4a11-a763-0d81a0508465?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/a4f2484e-8819-4698-808d-532c61976e8f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/d99d532a-2e4f-4eeb-a4c7-f241baa7c833?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/c8b37b99-c49f-4c03-9bc2-0405145cbf87?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/626f8058-733e-4b06-b8d3-4c2d0520e3e5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x926/def0b9b2-dd1f-4187-8fac-5de4c2ca649b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4d88a30c-13d4-45f1-9d07-aef8fee9322f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8da11fdb-f17c-4c06-866d-97220a1bdc6b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4a72feab-6a14-43eb-8cae-034211fcf70f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bf8e8437-1488-47b0-bff4-bfbfcfe91343?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/1ad84e7d-0aa7-4849-a582-1b687c1d69b5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7483117e-e929-464b-9b82-090811006d2c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7e256ecc-cd83-4147-8b4f-4f95f998437b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/570cc509-e040-4669-ab42-c5bdf33682e3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c125797e-d307-4312-8c1a-d4fc5be41625?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2f99dd0c-7dc9-4033-bbc7-f8b1fb65e9ed?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/986d779f-3c1d-45ef-aa0f-871ffad5ae66?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5278a5a2-be53-45fe-99b5-02dd906e3cd1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'australian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5ddb1c51-f43d-4317-8a5f-8d9989ce9aba?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e10a16af-07c1-45e9-8fe1-88f2b6ba1bfe?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9b4a1b44-ce8c-4a5b-a998-a10763b8640a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/58e4ea3d-503d-4724-91f4-1a8c8bc0f190?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5867ce3f-9baf-4df0-b8c9-1f5ed5ff3677?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f4776c26-5af2-4daa-9186-643f2357f290?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c23d57b9-855b-4aac-b2e3-70f72c3ae4af?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/fe6a72c2-4f1d-4608-8607-c81528713124?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a636472c-0406-4ef5-b105-5b7bd70728f2?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/deb1fb9b-5e63-4091-960e-7d03cbc664bf?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3b6bd544-6366-4285-a576-96f4a8ca6d78?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/613c5296-924c-4dc2-b960-4b80ca7729c7?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6b32c9e1-dd8e-4a03-897d-149a74696372?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ad240b41-0044-4bdf-8d23-55e1826ab6e1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x892/630eb236-69a2-450f-ad10-0dad3ffb16c9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1105x892/d2af20fb-6543-4ad3-91e3-1fb8f0552761?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1105x892/d901605f-7f07-4f3d-b759-cf9a7b6ec1d2?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1105x892/0af43733-8b6f-41e3-b602-6b4f17409a9c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1105x892/e1edd08b-8205-471e-92ec-e2fe6794b774?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4835e9d4-b41c-4b46-8285-d36753e7af52?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9b6c5217-5922-4c94-91d1-d7eb1688229c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/42b81223-e216-45f9-8047-af47d8107840?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f3cc2fe2-b1c8-4c93-99f1-2dd090488b93?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/497c1d1b-4d7e-4f2f-aafb-b82e387c347c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/69e7ec1a-aae3-4e76-ad92-a262fc517953?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7bcf6824-bd57-4e29-9477-4dee17d793f1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'bahrain-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/72ffc723-71a4-4ff3-8618-10dcc3c82197?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c8d867e0-60f4-46de-8516-78e6ed9cb9f1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e0f13333-049b-4d03-8b6d-273ceaca0327?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3a15e66f-4d6d-4a77-b453-7a624089ca61?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9d936fdd-2a3d-4477-8e37-3e61f28f4dd6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a59dad77-18fd-43ca-b258-6563bdb988e1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/50c20fa0-bc28-422d-a15b-2f1ff53cbcfe?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/96703c1f-7982-420d-b754-62527709cf0f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bd958d55-b1af-4424-b38b-d046ef274161?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b2efa3cf-9c4c-402e-b32c-0534cf3c2ac4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a7b75e0c-9fc7-4d61-a38d-1339c4e2c1ce?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ebd43ad6-fd1c-46d2-981d-8a580c201b01?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a6ee349f-aa04-4d00-a1e0-460fca429d54?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/00a36dc1-6dfa-4fbd-8edd-56cae78c1bd7?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5fd27546-c7fc-4703-a284-2bcea27c4a0e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c39f93cb-4e68-44f9-857b-00ac650f4ec3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7ba06740-f930-4ae3-9675-4514e9abde73?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5b530de9-ace9-409e-8350-73c6e8d20ed9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/85f5a61d-4ed3-41ab-ab74-e63896c47a16?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/fa946aec-0a30-40d2-a3c0-fd3739d53d4b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/1ef20811-6949-4d8d-afb5-dbc9b5ef191a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/787f826e-7672-40f0-a582-21fc0bc2c1f3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ba431f53-de75-47a8-b783-947a7bc2545f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/147ba6e9-accb-42d8-9814-466793ba5665?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5ce765d3-b865-49fd-9847-606e57da6da8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/18770243-3aab-43df-a7bd-d226c0c11b6c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'chinese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/dba7c7c3-23c2-4f03-b457-6ecf10533bfa?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/371cf916-f977-4897-a3b1-ea608d66956f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x892/874636d8-afc0-4700-8f7f-406d18062201?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x892/e71be4fc-7c23-4a9e-adae-d94f5037c514?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1348x909/af37eca9-b38f-4e5b-a8a9-b803bf58c09f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1348x909/02d4f497-24b3-40cd-b8a6-d18ca3d96b1b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1348x909/cb660f7b-dd7e-49d6-afbd-fe0879ca954b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1348x909/14c710d9-cf63-4057-ba10-cdc6cd0b0fe6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a592df3a-e23e-4fde-a804-08a158060432?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1920x949/a2a6e7f6-6d92-433a-8dde-e74b4d929931?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c1575283-70d5-44f5-828a-e0b8e7de22ca?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2bdf7ecd-3971-4c72-9ef8-201ad0c65493?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7107f241-23ea-43d0-ae35-6f1423816dce?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b50d3954-d6d1-46be-b817-73d85e1579c9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a83ade6d-c281-4df5-bb79-7a89eb90aba4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c68fafa1-33c8-4f08-9a24-1de2c1d9c7ae?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f8b292c3-58cb-4901-8b30-784def12e602?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/49e222e5-ef0d-4a7d-a199-efbcc1cd37e0?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x892/dca10db2-c3c5-471a-a9bd-5a9a075a1222?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1042x892/f54d98a7-9ab1-494c-aa2d-5ff63f387ada?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1042x892/bb51c5ff-6304-41d0-bb39-e25d0bcf6cf4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'azerbaijan-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1042x892/f86548b9-810e-4f96-a347-bf276885db39?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a0c70196-6a67-4c75-8dfe-123d5f35d574?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/40b0064d-a7bc-4d7d-8725-d95a6c71d156?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a892220f-cb76-4d72-8e1a-a94528cb104e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4937fc67-b63b-4889-8761-90fc5c9512d6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f757dfe8-d536-43c2-81d1-4ecd317efeea?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6cca2290-105b-4677-b69d-9bd9ead60f0f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/534f0dde-88e4-441a-bca2-f940af816b53?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/27baa131-58ad-4d59-a019-ba886c982b13?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/df199cf7-7215-49ce-9d47-8e1a6f16ebc4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/acb1face-b65c-4fec-86e2-6512016ed872?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c7fea921-b3b5-48af-a473-bda102a1cb45?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/50a380bc-721a-4f2a-82f0-d2eae1cbfc1d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ef5b5327-bf12-4ddf-9a18-cc0f39ff5f88?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0ea4e3fc-2402-4a97-9226-85f600fc1dfd?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6430d67d-d2fe-490b-9400-5159f4e01f2d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/823c886a-1be1-4afc-8034-bd7d98849440?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/175ce82f-9d32-49fd-8834-8e5d7fb4374c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f1731728-a7d3-4c16-9428-39768f3aed89?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7c860c7b-798b-4e94-be44-6e025314787c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4f967359-b8f9-47fe-9337-14332cd393ff?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2aedeaa6-d671-4cdd-a5d1-829bba9a06e6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/293d1ae8-b80c-490c-835b-033449a4da5c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2431b4cb-295b-456e-a9e6-916182049a05?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bfb883a9-6d5b-4f98-92ff-1903feda2563?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/94989778-4d8e-4661-9aa5-c1ce8c158ebe?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2b8b5701-2e50-4cf4-a787-c81cd9115954?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'spanish-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a0262407-4c34-41b9-af8f-f1a050b41bed?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-wednesday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a435a754-3073-4df3-85b0-544c0179efee?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-wednesday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/70f8152e-1f44-4230-9c11-8500a24c5c5a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-wednesday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c7047e95-d053-4d99-a05e-fcf41b981d9d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/19e87d32-e7b2-431b-b502-77ef094a5838?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e80892a9-966a-474e-a34d-cabe436dc643?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c783c172-01c2-47bd-8b4c-2d5939d41bc9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b906bed0-77b8-4f9d-8a8a-7e8fa31a5156?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a228c7fb-550c-41f6-a40b-2daf80e5e75d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/243778ee-435a-4de7-8c9d-4e792dd96824?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f5d4cc98-f143-4328-aab5-4bcd78123273?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/860182e6-5946-4086-a298-a4699df0dcba?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9b6ffc5e-fa84-46c0-97ac-d087fb7b2d50?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d636efd4-7074-4c2a-b512-fd8e8e47f96d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/44e22d55-222b-44bc-a2f7-3100b56f879b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/cbecac51-321d-4df2-aaa1-794154dc968b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/cedaf5b5-2c86-4c75-a110-3a04731eaf54?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6624c512-b15e-4316-a2fa-20538e245343?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5cbcd84b-5018-48a0-aa96-8dc4b24abf97?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f50ec8c5-c6d2-4c25-983c-2b624c89d358?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f8d38696-2b91-42ed-90f2-a1dcc335c362?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/26962d19-f212-4247-ab4a-17ff7bcc2059?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'monaco-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5ce7dcec-5475-46ab-8998-3096738c778f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5b3d43ca-d322-4932-96f9-132594f68671?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6c51b40d-4cc0-4de5-b432-e1f9ff63bb56?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7852bcd3-d6da-4f51-ac82-2e5329d3aa72?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/04a099d1-32e5-4374-8483-97ded80bba69?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/95343a59-a868-4ab9-922c-6b653dabee02?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d0617f7c-aaed-47e0-8180-ec4733092a64?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/14603eda-0136-496a-96ed-65cf37827471?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/90850d12-d3bd-4bad-8c54-dc0685561084?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7a9211f6-e455-48ff-be19-b54dc67a070c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7352125f-0134-4f4b-8f3b-ead0c88e1037?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f28c25fa-c6cc-4eae-b89b-77b327ed982a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bef1471c-05ff-4a8e-92a5-ae381ca46d3f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2c2ac7be-e2b4-4ccd-b6a4-544c3e52c691?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8e30f3ed-a029-488d-b836-0ee11316ed59?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3cfc6b38-9029-4e5e-aeaf-1428cdca23fa?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8635c9e5-0d4f-4cff-8b1e-090bb3743a79?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7b565234-17fd-44f8-802f-914e0931f47e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/184ab95b-f4a4-43f6-b2a8-c721b571cdcd?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/db61d20a-4b6e-423e-bb72-b00e696eecaf?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'canadian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2d241282-4841-4ec9-be91-56074b0cfa97?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/90f4b0f4-2f65-46ed-839f-430cea0cb3bc?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9d5ca867-7830-4d27-9b76-53f34f802f18?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/892a8643-6a46-4b79-ae9c-d7649948815d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bde94b03-8454-44f9-a142-f0da24136a63?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bc854c4d-3504-4887-b619-9c99154f9672?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ef347470-e84e-4618-bdbf-c6c85f8d24c6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/599966fd-9fcb-4b97-a2f7-b6c438e710bc?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d79841b5-16c7-403b-9bfd-ff23d09d0cc0?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/03491713-134c-4c4d-b88a-91db1715be1b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b36fbb30-7df4-40f0-834e-33f534ec4e1e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e141db74-f19a-4781-823f-dca1ea8605bf?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/eb40a9ac-6533-4770-bdf5-310bb36fed20?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0dfe6e50-bacf-4d6c-8f44-a124ed7c2316?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ae27d78c-72e9-4d3a-bf73-98d1d9cdf215?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c46f1d7b-1ad7-4fbc-8539-f7a94f89f0fd?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/35f523da-5fd0-4bf7-8c62-ed956d5556d5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0cebb33c-53c0-41fc-abaf-b64d1c5a7d35?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d002c156-c7c3-4846-80d3-749a7b176551?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c30ec6dd-46cb-4a6b-826d-d4cd7a58096e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c2d3e862-8267-49a7-b9a7-11b9fc529ce0?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/dbbeec01-d00e-4f4a-a6ee-f95d4cee6376?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'french-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5db90700-7db3-4bc8-bfa7-983ae9992e73?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/909110d4-ba28-4a02-8a46-36d99e67b378?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4ca246df-174a-4f13-a14c-96b2bfd60d80?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/105dd970-a0f6-462c-b18f-e3a0958504f6?v=2',
            ],

            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/73d752b1-71fe-4478-8ecf-2f32fe6b6494?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e2dd71fc-5736-43ca-afdb-ff9807977d12?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6e52709f-dbdd-4f96-b4d4-e751f2c2cdd3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/dfa7d210-6de1-4fb8-bb97-5ced4a8787ef?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/63fa7485-ce90-494e-964f-b13ddc415a23?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/564d7cc6-c8c7-4250-a9f0-0409e54bc8d8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/fa6aa3f0-5d65-4ba0-8027-032c5f5ea285?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/fe79839e-ab4e-4390-84e3-dd6fde9a917d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e41144e3-b29c-44dd-82e1-d2d23b61c7ec?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/536a7d9d-c3d6-4521-aa3e-1145ebd1fc93?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/16fc7f38-699e-47c8-897c-23319c764b67?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6d475ca0-4e7b-4e34-8ecb-b5a900c400f1?v=2',
            ],

            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/55f75fcd-1a1c-4f97-a4ad-7102bb04bc6d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/82a41ab2-6672-4805-90d5-93871a270163?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ce4af8cb-6280-48ca-a5b5-bb985c76db73?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/dcf23cc0-47e2-4e66-9ec0-0c0c710284b9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'austrian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3021880c-cf1c-4a3b-aaf2-971167c8afcc?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/40750c5e-0785-464f-a1c6-2e07978c1423?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b7906f9f-2fb7-4751-ba39-266b49d060de?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/81ab6d58-60ae-4ac3-b264-6ff8dbe87ddd?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/580f69cf-e660-42b1-84da-4ef5e85108a9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/52ad500b-d0c6-462a-982e-bc77eeee0fb6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ed5863f0-fece-4625-ad1c-bd6bb4eafae0?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f32e2018-512c-4af0-9d1b-0c4e6c591e43?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3d0e9e47-bb96-4c0c-a6e7-37974725a690?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c906be68-6ca2-4e9d-a29f-7c478a83a76d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/163fd0b5-e056-4b6f-ba58-1d52c40f91d1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2173d776-1af5-4b59-b622-e77fd625c122?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/390e6f6c-c0ea-4d05-97d3-d328276fbe93?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9b467afa-1c1d-43a2-8f9e-40806c88fea6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/381cd2b9-64fe-4eb1-85ef-3d6a6f2a01bb?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/73ce9deb-47d0-4e82-b58d-98c197efc99e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c19a46b2-ab94-4afc-99f1-5eff94eb9912?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/52a16ee4-c5e4-4a80-8ecf-161d06c8781f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0b9e670b-c206-405a-960f-e2224e4853b8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/148ba87c-fcea-4ce7-90d9-642a5c6ae737?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'british-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/46746c30-7744-4508-945a-17e8cd1d5a81?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/19e7bf10-3525-4703-81bb-7d98d7a324b8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e06194fa-cf41-41b4-b6ae-e2503c80b2b9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b84b33f4-5256-4f26-a61f-2f7ec7e88691?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/018e3f23-77ef-446e-9339-c8f3f8620ce4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2e4277f0-8bca-4c65-9f0b-e2427f415839?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/856c62c4-d21a-4aa6-bd7c-a80113c48b19?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/79bfee49-672c-4e0c-a263-ab6bd80ec225?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7517e2da-4e76-4e19-b898-f6f5b3cbb83a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8ac43f16-eaa7-42c8-b974-39e99f3790a5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/12164700-2130-4d7e-9cb4-d1915df2263c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5089ccb4-f3a0-4e22-8911-612b1331190d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/de3e13ad-ca1b-4481-8415-93510a45868e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a46756ba-edc4-4a21-96bc-b7948113ae59?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e2b6e832-6741-4065-b6a6-2bd8cd280017?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e10f4861-1768-4085-95aa-67ae80b5cac2?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/49d11b77-33fe-442b-b8a4-460665f75506?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5ac26fc7-8ac1-460d-8748-547fefcc478c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b432971c-650f-44b1-9614-8029f2f7185a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'german-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bab3610e-59f1-4541-b59d-fe2ba809cfd8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6f9e3b40-5a44-4321-8ab7-ad194bce3817?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/08be4758-5b11-4995-9ba4-ecf071f76742?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/87e67ca5-17f8-4bec-aa8d-479f7aea4a3a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6ba1dbcd-158a-40d4-9e84-0caa66a93d25?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/654ca30c-c38f-43d0-a6b2-11ae0d82a926?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c2deef77-f7b6-41f3-9606-864bd8bcecfa?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3efeddd9-e90c-49e4-a121-896845b53411?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0f1caa23-bc3c-4805-8bd9-6c08706de2f4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bd1cfd32-3688-4833-a7ab-d6e177361e32?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/299cdd81-e095-4881-b29d-2248003e602e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2faddcb3-81ca-4dea-84a7-147fdaf55715?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/490a41f9-056e-48f2-9063-b4b541c2a835?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/91e821e6-0753-44c4-9148-c8e410ba813e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7957c88f-46bf-410f-9799-c24ca31a3a83?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/cbf2c3a4-dbb8-4f03-bb39-4c52310fc3c3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6fe31d81-9d2c-4188-b16d-497e10e05705?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2ecb83bd-a615-4d67-b2a3-056daa971f9f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/be109ac1-4557-4da9-93f9-f8d2778c3d9b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-1')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/08f21f2e-4228-444e-a519-f150821359b1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9668987e-df07-4b4d-a96e-a55c8e696078?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/33ac04e7-83d5-4e0d-8096-f262734659ca?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/37c89dfe-5845-4dc4-905e-9b9e62f592b5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/abcd9a05-a1fc-471b-82bf-537517fb22c7?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'hungarian-test-2018-day-2')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3569dc5c-a0ab-4696-bf31-25bf73b0d2ae?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/60d42863-8265-498b-831d-a0d560213ec1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/36f83e91-6978-409c-bf26-4aae81f08738?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4314993c-f1bd-454c-a730-9d08f27cf98c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/eca82bce-1d85-4fcf-936c-180bd8de3ed2?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d092058a-8664-420c-8186-6d1f65de17c8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c5925366-764c-4b03-8c09-c6dbf48b2e1f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/19107563-f0e1-4794-8361-f9a2ea86546b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6dee8444-2259-4794-be01-6b11892396a8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/902bb868-531a-4022-b1dc-ca9f929e59b6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b98a204e-1d57-4312-b92e-370d13d7bd76?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bafa2353-7e79-4d67-be74-8ad1466a9a32?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/157d773d-647f-4543-9f26-4fba7a36cf2d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c279a257-208c-4b97-b70a-18de3f7f511a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e0dd7d6d-4e31-4fa1-acb7-13e16fe627f3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/315f7d7c-0c82-461d-917d-11acbd8dc96a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/47b04e50-cde1-4c73-a834-048ec3317f13?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bdf7e105-c1e1-437a-8ed0-4b59e3a16e21?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'belgian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e3c3f24b-8640-421b-8ac1-0f1013047938?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6be49318-aefa-4f90-acbd-be56c3d8f0f1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c4a2c91a-4dff-4b0c-abb0-89d821b87a5e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/dd7ac98d-5548-4aa8-8cf1-07ad71be50d3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/85eb4390-d81c-4879-8c87-38feadcba886?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a03ae1dd-3a4a-42fd-9084-fcd46b532ef6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/cc0caca6-0df6-4a87-a77f-f303b3093a1a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/dfd73474-b582-422b-a0f3-e248170650ec?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5e50630f-09b2-41ca-99c1-b6caf5779e99?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/aceb80ca-a310-4ed9-bf1b-95fcb9f3c03e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e7e02371-720a-4fc7-b9b4-ef475a1fdf0c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/29f39fe9-b9ea-406d-b711-c400e9820e22?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/65a65ebe-f6a4-4d7d-93b6-aa3aa6d42a28?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/bc5ae89c-8a1a-476e-b199-a0eb050db9d7?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ec42b357-d2be-40d1-a50d-80f30aba6f7b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/1fbad08f-70a8-4f94-be0a-aed3f77428eb?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/973c81ee-7c6c-405b-8ed0-d700e05365ba?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/92cdd6f1-4217-484e-965d-6e2be28d9e02?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/38269d43-cca3-4f0c-ab7b-bc4bcecfbae8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2393efbc-c158-4037-b5fa-f7c523df4f44?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8e8fe318-1ed5-4918-aa95-07f57bc2798a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c71aeb3f-94ad-4cb8-b1d5-9b64b2b001ea?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/1fe70fb4-81f2-4c6e-ae92-070c2bf63497?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f7a10d0e-cd60-4b9d-a428-dc5b3da77510?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2ddf31f0-db6f-4057-b229-69ae0e71df96?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'italian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/53866111-4073-41c5-a2f4-f420610a68ab?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/21b6ec12-1d36-426d-ae2b-710711a35583?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9ae19bf6-33e9-4a3f-ba80-54a673185cdc?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/94087ee3-5c18-49b1-bcbd-304119792e50?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/abf75fb3-f687-4b76-82cf-33d1c3ab7721?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/98770012-2c12-4d45-a914-424cb90c2811?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8c06d9ce-a5fc-4ce2-9fbd-aea9ea146a0e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2b4dc6f1-5b5a-4a13-9ea7-45e354d8e746?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ea55253e-c9d8-4b30-acdf-9dcf45e33e79?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/51117190-b707-4f79-8f35-1d83d02eba7d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8e28d541-6bfc-4a56-bd0b-622f74ab6484?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/418e4482-27df-4391-b03e-4f8a462bc5b1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/27b83984-3125-47f2-85b7-2e9bca6b6fec?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9f59d9fd-658d-4ce5-b1db-a2da9e070a1d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a154bc2e-5fc9-42d7-8d80-57203ec3fd7b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a7897949-d626-4789-a70d-492e9112c6e3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0614dee6-4157-4c7e-80ce-bb3aefe75da3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/50a4e2d2-b5b1-4130-a1f0-b5afee7c7bc2?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'singapore-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c580f8c6-d319-439e-9715-e8ea4c797521?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2a350d6d-1670-4576-bdd4-5e6b7d88f08d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/29ed75d8-edaa-4ee2-ac40-01d4d8e94532?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/efcfedd7-ab82-4cf6-86be-3e49828ef77d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a55fa04f-a1ee-4a33-b4d1-5b7e140f2cc6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c57af420-8ab2-4fe3-8b40-3f1f0f653d76?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/60d50fbd-b74d-40ae-8c2d-705eb9c5a0df?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b4377767-28c5-44e1-b95c-9b20e1e511ca?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/44baa389-ec92-4032-81bd-6e1ea2fa29ca?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/31ede7c0-f3c3-4fee-97f5-23f9c3759d0a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f6463316-e2b9-4000-a415-2527d1e83027?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6e17d75a-6ff9-4705-b9a3-7a4a3d9c2dd0?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0bf41537-cd75-4e62-9252-c4f8aa6f3eff?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/cc14414c-5361-4c7b-87c4-43893c81b371?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b5da06f1-a28e-410e-9e77-b12818f692d8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/2effa51a-02a2-4ea5-a436-f97fd0bb7051?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/149aad78-7d57-4f93-a689-e0b4e0a54923?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/02a3fc31-e534-44ed-9813-c5a27be873c5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a97b13e6-2360-4007-91a0-d7be00fb83b5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/cc76ae11-3491-4d31-9f32-b76f4f95d4bf?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'russian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7f43633c-4a01-433f-956e-c3230cefeacb?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3724593c-2f99-41e1-856e-4637e12d41a5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9f40c302-04e4-488e-9d6d-51f54d2dc9f2?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/1641f02f-a1b0-41c7-bb16-d31aece12cee?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/aa57a12f-8b47-496c-93ee-0b8709fd7548?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7fbb11ed-c0f1-4ff5-9c94-4b41eaad556b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b5653de5-01f7-468a-90cb-f56019f621a9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f73177a9-ecac-450e-8d50-17d8e232324c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/798f56ca-cde8-4bdf-87bd-a9cb3cc3829a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/852317a4-f67b-4b15-b180-fa4fd9059a0b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a19175ec-8080-4bea-a05f-ece1e2da0f1f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/74c78c49-d24c-475b-9040-a011a85abbc6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/eae96a34-478f-489a-aa28-d41f98cd06d9?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/29e42cfb-709c-4334-9e44-c83ec5da9905?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/85d63bc1-b6da-49c6-bc97-f5bb259e8187?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e6839ddd-89e1-4c40-84b7-c4c564dfbb30?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7bbaaf0d-9e47-4d1f-9bff-a07f71f575d4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/304eefee-104a-4a18-a32b-5c50469d0b20?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'japanese-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/78c6589f-2217-4a83-97c9-7d2dbc1deaf3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f67dcc7f-0fc5-493c-9bd5-36357aadf9bd?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f1559fe7-d14d-4d40-89eb-b853ce7de13b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b13a2b37-cf48-4325-b6dc-a33c5b8c8105?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e58fd8e3-cfe6-44a0-b78e-728620fcf9e6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e4f24337-fd86-4fdb-b084-a6d87e4288f4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/29a1d864-b450-4210-82a5-ea09ebd80f37?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4ba287cb-f1e6-4594-a90f-3ea36b21fe94?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f0336b6b-38e3-41f7-896d-031f7e81e2cb?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/37ba079e-9f69-462f-9efa-97cefc701135?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/27527c8f-2ba1-4808-a3a2-a4d3d332a2e2?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5feea77d-b1d5-4579-8431-d4b71f2022e1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9a2f34dd-ee4e-4d21-833a-0b4bb99ecd04?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/24f9e7d4-4135-4bcb-b82b-52925f26b45f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/eabf868d-f933-4de0-839a-0b3cf37d7b6c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9a745328-dbba-472c-81e3-481202a4f456?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/0a091776-4d17-43fa-a7cd-0d0205ceda98?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/fa62ff8b-71e7-47a1-ab75-daf1069d4cbe?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f5421c3f-4d9e-45da-b7dd-099994241ce5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d9ae4fb3-d2ea-49fc-aa5c-116c864d69ae?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/24653e51-200e-4feb-9366-28b3e6f9cd7f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'united-states-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9f0b8616-a498-4dee-aafc-77f3351a3495?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/55720d57-f76b-453a-b9d4-7ab06f77a90a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/fdc71310-20bb-41fc-8c2d-1da30d2be0c6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/593b6b45-5fbf-48cf-8672-ba5f1067f101?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/be4fd32b-4966-46fc-87f2-7f963fb29259?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9c52b06e-ac14-4a2b-b1f1-af1ebc477200?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6ba1c53b-7173-4c84-95bc-a9ac52c12bed?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/71f8ba64-1701-4923-b6d1-76ded28d07b5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/9ed7007d-11e5-422e-93be-9584743a29d7?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/16bc2680-e821-484a-840c-3f813895e90e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4c920254-3b23-4c65-a34b-1c3bd015dae8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f6065981-24d9-4048-b532-7481b939d965?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/02c96505-6f72-4e0e-8a66-f01ff3361e3f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/63a00d00-1d51-4d3e-a78b-24e648060758?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/00f903c3-8bc4-4578-a6f2-9ab094107ae1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6bfe7583-7f07-4718-83d1-39d60b841916?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/30ef6e8c-3c92-4e81-858e-a0e7e4f5bd27?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7b0aec27-8452-4447-9315-a0a81cc70467?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'mexican-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/079e164b-b3ff-4981-8bcb-404d6c5108b8?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b3223d1c-dd44-4771-87ff-d973c04edca3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c013fce5-dbba-421f-9450-f3da4c4ab4fe?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/fe10a6a8-8ca8-4fba-880b-485682d5588c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/e34614c6-0b77-4d68-af0e-17cfd3380aaf?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/179c5adc-7331-4419-9a9f-a5e01ff1fb72?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7be6c3c6-1311-4247-8d58-ed4c8b41f61d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f74f47c2-ffd5-4df9-83fd-1569f192753e?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ba2b0187-3012-4de1-8d7b-7153c1d061f7?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/4c8f0923-c607-437a-bc4d-05186376c77a?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f298ce90-d369-47d3-b7bf-f5550a932051?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/68efa959-4b74-4340-9613-8a4989c220da?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d51526a3-cdbd-424e-807d-91a895b0ef24?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/8603dbce-9d22-4fba-b5ac-1555af7eb1d0?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ffda8a51-706b-4aab-b82b-ee64fc8972e5?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b9432a53-d6e5-4fa6-a216-7b5b66e3163d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/7d399f5c-65b4-4f39-a10a-5a4e24b8cda4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f130b9b5-1068-4ba7-8af9-d43b84c3167d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3a0b2d20-90d5-42c3-b883-7167f284ca5b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/3c3fcc1d-6efa-4507-ba97-13cc332856e1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ae5dfeb2-18ab-43e3-8dac-ca4c596a9494?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'brazilian-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/facfad2f-e31d-4687-966e-5791da7b3910?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6e1573ad-6a62-4183-ade0-34549aaa40ce?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/6c0faac8-24a6-4d82-9b35-de9617f81f53?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-thursday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/56ac4fff-5834-4063-8a35-051d916e21d1?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/264484bc-245e-49bb-8704-5d7e68f39161?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/a08aac78-588c-4b4e-b684-28bab9b2cf44?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/67a01c40-1589-47da-bfd3-0ea317534a8c?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/44d28ae4-229c-4aad-a3d2-4ff8f8c153d3?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/adc174cb-24c7-4e64-a966-51c479b9dc15?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-friday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/b5e1e32a-e79f-463e-ae34-de7ca18ec182?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/ce173f8c-abf3-40c6-8812-15e5a7650304?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/f784047f-8fa5-40c4-a2d5-401fbe5fe86b?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/92da9e39-3a45-487e-9073-84a5ec0bb395?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/415faa62-8d61-48b9-8192-1f4f0af0b92f?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/024eeab4-8679-42c7-a6d0-6da543a806b6?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-saturday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d6715d47-0b1c-4064-92db-5a3fd8b1e491?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/5b75d577-9368-4b06-b1c3-331dc2a8ad3d?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/dbcd5b69-6047-42c3-981f-051445571783?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/c000a2cd-bed7-42fb-adb7-e24493beed29?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/d1c255cb-e5bf-4645-8dce-482458eeddc4?v=2',
            ],
            [
                'album_id' => \App\paddock\Media\Gallery\Models\MediaGalleryAlbums::where('slug', 'abu-dhabi-gp-sunday')->first()->id,
                'url' => 'http://ferrari-view.4me.it/api/xcontents/resources/delivery/getThumbnail/ferrari/1903x909/63a08713-e3d1-493d-a837-7d1182a273f0?v=2',
            ],
        ]);
    }
}
