<?php

use Illuminate\Database\Seeder;

class MediaGalleryAlbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_gallery_albums')->insert([
            [
                'name' => 'SF71H',
                'slug' => str_slug('SF71H'),
                'cover_id' => 5,
            ],
            [
                'name' => 'Barcelona Test 2018 - Day 1',
                'slug' => str_slug('Barcelona Test 2018 - Day 1'),
                'cover_id' => 10,
            ],
            [
                'name' => 'Barcelona Test 2018 - Day 2',
                'slug' => str_slug('Barcelona Test 2018 - Day 2'),
                'cover_id' => 12,
            ],
            [
                'name' => 'Barcelona Test 2018 - Day 4',
                'slug' => str_slug('Barcelona Test 2018 - Day 4'),
                'cover_id' => 15,
            ],
            [
                'name' => 'Test Barcelona 2018 - Day 5',
                'slug' => str_slug('Test Barcelona 2018 - Day 5'),
                'cover_id' => 23,
            ],
            [
                'name' => 'Test Barcelona 2018 - Day 6',
                'slug' => str_slug('Test Barcelona 2018 - Day 6'),
                'cover_id' => 24,
            ],
            [
                'name' => 'Test Barcelona 2018 - Day 7',
                'slug' => str_slug('Test Barcelona 2018 - Day 7'),
                'cover_id' => 29,
            ],
            [
                'name' => 'Test Barcelona 2018 - Day 8',
                'slug' => str_slug('Test Barcelona 2018 - Day 8'),
                'cover_id' => 34,
            ],
            [
                'name' => 'Australian GP - Wednesday',
                'slug' => str_slug('Australian GP - Wednesday'),
                'cover_id' => 36,
            ],
            [
                'name' => 'Australian GP - Thursday',
                'slug' => str_slug('Australian GP - Thursday'),
                'cover_id' => 40,
            ],
            [
                'name' => 'Australian GP - Friday',
                'slug' => str_slug('Australian GP - Friday'),
                'cover_id' => 44,
            ],
            [
                'name' => 'Australian GP - Saturday',
                'slug' => str_slug('Australian GP - Saturday'),
                'cover_id' => 52,
            ],
            [
                'name' => 'Australian GP - Sunday',
                'slug' => str_slug('Australian GP - Sunday'),
                'cover_id' => 60,
            ],
            [
                'name' => 'Bahrain GP - Thursday',
                'slug' => str_slug('Bahrain GP - Thursday'),
                'cover_id' => 63,
            ],
            [
                'name' => 'Bahrain GP - Friday',
                'slug' => str_slug('Bahrain GP - Friday'),
                'cover_id' => 67,
            ],
            [
                'name' => 'Bahrain GP - Saturday',
                'slug' => str_slug('Bahrain GP - Saturday'),
                'cover_id' => 76,
            ],
            [
                'name' => 'Bahrain GP - Sunday',
                'slug' => str_slug('Bahrain GP - Sunday'),
                'cover_id' => 83,
            ],
            [
                'name' => 'Chinese GP - Thursday',
                'slug' => str_slug('Chinese GP - Thursday'),
                'cover_id' => 89,
            ],
            [
                'name' => 'Chinese GP - Friday',
                'slug' => str_slug('Chinese GP - Friday'),
                'cover_id' => 94,
            ],
            [
                'name' => 'Chinese GP - Saturday',
                'slug' => str_slug('Chinese GP - Saturday'),
                'cover_id' => 102,
            ],
            [
                'name' => 'Chinese GP - Sunday',
                'slug' => str_slug('Chinese GP - Sunday'),
                'cover_id' => 108,
            ],
            [
                'name' => 'Azerbaijan GP - Thursday',
                'slug' => str_slug('Azerbaijan GP - Thursday'),
                'cover_id' => 115,
            ],
            [
                'name' => 'Azerbaijan GP - Friday',
                'slug' => str_slug('Azerbaijan GP - Friday'),
                'cover_id' => 118,
            ],
            [
                'name' => 'Azerbaijan GP - Saturday',
                'slug' => str_slug('Azerbaijan GP - Saturday'),
                'cover_id' => 124,
            ],
            [
                'name' => 'Azerbaijan GP - Sunday',
                'slug' => str_slug('Azerbaijan GP - Sunday'),
                'cover_id' => 131,
            ],
            [
                'name' => 'Spanish GP - Thursday',
                'slug' => str_slug('Spanish GP - Thursday'),
                'cover_id' => 136,
            ],
            [
                'name' => 'Spanish GP - Friday',
                'slug' => str_slug('Spanish GP - Friday'),
                'cover_id' => 140,
            ],
            [
                'name' => 'Spanish GP - Saturday',
                'slug' => str_slug('Spanish GP - Saturday'),
                'cover_id' => 149,
            ],
            [
                'name' => 'Spanish GP - Sunday',
                'slug' => str_slug('Spanish GP - Sunday'),
                'cover_id' => 159,
            ],
            [
                'name' => 'Monaco GP - Wednesday',
                'slug' => str_slug('Monaco GP - Wednesday'),
                'cover_id' => 163,
            ],
            [
                'name' => 'Monaco GP - Thursday',
                'slug' => str_slug('Monaco GP - Thursday'),
                'cover_id' => 166,
            ],
            [
                'name' => 'Monaco GP - Friday',
                'slug' => str_slug('Monaco GP - Friday'),
                'cover_id' => 171,
            ],
            [
                'name' => 'Monaco GP - Saturday',
                'slug' => str_slug('Monaco GP - Saturday'),
                'cover_id' => 173,
            ],
            [
                'name' => 'Monaco GP - Sunday',
                'slug' => str_slug('Monaco GP - Sunday'),
                'cover_id' => 179,
            ],
            [
                'name' => 'Canadian GP - Thursday',
                'slug' => str_slug('Canadian GP - Thursday'),
                'cover_id' => 185,
            ],
            [
                'name' => 'Canadian GP - Friday',
                'slug' => str_slug('Canadian GP - Friday'),
                'cover_id' => 187,
            ],
            [
                'name' => 'Canadian GP - Saturday',
                'slug' => str_slug('Canadian GP - Saturday'),
                'cover_id' => 193,
            ],
            [
                'name' => 'Canadian GP - Sunday',
                'slug' => str_slug('Canadian GP - Sunday'),
                'cover_id' => 198,
            ],
            [
                'name' => 'French GP - Thursday',
                'slug' => str_slug('French GP - Thursday'),
                'cover_id' => 205,
            ],
            [
                'name' => 'French GP - Friday',
                'slug' => str_slug('French GP - Friday'),
                'cover_id' => 209,
            ],
            [
                'name' => 'French GP - Saturday',
                'slug' => str_slug('French GP - Saturday'),
                'cover_id' => 215,
            ],
            [
                'name' => 'French GP - Sunday',
                'slug' => str_slug('French GP - Sunday'),
                'cover_id' => 222,
            ],
            [
                'name' => 'Austrian GP - Thursday',
                'slug' => str_slug('Austrian GP - Thursday'),
                'cover_id' => 227,
            ],
            [
                'name' => 'Austrian GP - Friday',
                'slug' => str_slug('Austrian GP - Friday'),
                'cover_id' => 230,
            ],
            [
                'name' => 'Austrian GP - Saturday',
                'slug' => str_slug('Austrian GP - Saturday'),
                'cover_id' => 237,
            ],
            [
                'name' => 'Austrian GP - Sunday',
                'slug' => str_slug('Austrian GP - Sunday'),
                'cover_id' => 242,
            ],
            [
                'name' => 'British GP - Thursday',
                'slug' => str_slug('British GP - Thursday'),
                'cover_id' => 247,
            ],
            [
                'name' => 'British GP - Friday',
                'slug' => str_slug('British GP - Friday'),
                'cover_id' => 249,
            ],
            [
                'name' => 'British GP - Saturday',
                'slug' => str_slug('British GP - Saturday'),
                'cover_id' => 255,
            ],
            [
                'name' => 'British GP - Sunday',
                'slug' => str_slug('British GP - Sunday'),
                'cover_id' => 260,
            ],
            [
                'name' => 'German GP - Thursday',
                'slug' => str_slug('German GP - Thursday'),
                'cover_id' => 267,
            ],
            [
                'name' => 'German GP - Friday',
                'slug' => str_slug('German GP - Friday'),
                'cover_id' => 269,
            ],
            [
                'name' => 'German GP - Saturday',
                'slug' => str_slug('German GP - Saturday'),
                'cover_id' => 275,
            ],
            [
                'name' => 'German GP - Sunday',
                'slug' => str_slug('German GP - Sunday'),
                'cover_id' => 280,
            ],
            [
                'name' => 'Hungarian GP - Friday',
                'slug' => str_slug('Hungarian GP - Friday'),
                'cover_id' => 286,
            ],
            [
                'name' => 'Hungarian GP - Saturday',
                'slug' => str_slug('Hungarian GP - Saturday'),
                'cover_id' => 290,
            ],
            [
                'name' => 'Hungarian GP - Sunday',
                'slug' => str_slug('Hungarian GP - Sunday'),
                'cover_id' => 296,
            ],
            [
                'name' => 'Hungarian Test 2018 - Day 1',
                'slug' => str_slug('Hungarian Test 2018 - Day 1'),
                'cover_id' => 300,
            ],
            [
                'name' => 'Hungarian Test 2018 - Day 2',
                'slug' => str_slug('Hungarian Test 2018 - Day 2'),
                'cover_id' => 305,
            ],
            [
                'name' => 'Belgian GP - Thursday',
                'slug' => str_slug('Belgian GP - Thursday'),
                'cover_id' => 310,
            ],
            [
                'name' => 'Belgian GP - Friday',
                'slug' => str_slug('Belgian GP - Friday'),
                'cover_id' => 313,
            ],
            [
                'name' => 'Belgian GP - Saturday',
                'slug' => str_slug('Belgian GP - Saturday'),
                'cover_id' => 317,
            ],
            [
                'name' => 'Belgian GP - Sunday',
                'slug' => str_slug('Belgian GP - Sunday'),
                'cover_id' => 324,
            ],
            [
                'name' => 'Italian GP - Thursday',
                'slug' => str_slug('Italian GP - Thursday'),
                'cover_id' => 328,
            ],
            [
                'name' => 'Italian GP - Friday',
                'slug' => str_slug('Italian GP - Friday'),
                'cover_id' => 330,
            ],
            [
                'name' => 'Italian GP - Saturday',
                'slug' => str_slug('Italian GP - Saturday'),
                'cover_id' => 338,
            ],
            [
                'name' => 'Italian GP - Sunday',
                'slug' => str_slug('Italian GP - Sunday'),
                'cover_id' => 345,
            ],
            [
                'name' => 'Singapore GP - Thursday',
                'slug' => str_slug('Singapore GP - Thursday'),
                'cover_id' => 353,
            ],
            [
                'name' => 'Singapore GP - Friday',
                'slug' => str_slug('Singapore GP - Friday'),
                'cover_id' => 356,
            ],
            [
                'name' => 'Singapore GP - Saturday',
                'slug' => str_slug('Singapore GP - Saturday'),
                'cover_id' => 362,
            ],
            [
                'name' => 'Singapore GP - Sunday',
                'slug' => str_slug('Singapore GP - Sunday'),
                'cover_id' => 366,
            ],
            [
                'name' => 'Russian GP - Friday',
                'slug' => str_slug('Russian GP - Friday'),
                'cover_id' => 371,
            ],
            [
                'name' => 'Russian GP - Saturday',
                'slug' => str_slug('Russian GP - Saturday'),
                'cover_id' => 377,
            ],
            [
                'name' => 'Russian GP - Sunday',
                'slug' => str_slug('Russian GP - Sunday'),
                'cover_id' => 383,
            ],
            [
                'name' => 'Japanese GP - Thursday',
                'slug' => str_slug('Japanese GP - Thursday'),
                'cover_id' => 391,
            ],
            [
                'name' => 'Japanese GP - Friday',
                'slug' => str_slug('Japanese GP - Friday'),
                'cover_id' => 392,
            ],
            [
                'name' => 'Japanese GP - Saturday',
                'slug' => str_slug('Japanese GP - Saturday'),
                'cover_id' => 398,
            ],
            [
                'name' => 'Japanese GP - Sunday',
                'slug' => str_slug('Japanese GP - Sunday'),
                'cover_id' => 403,
            ],
            [
                'name' => 'United States GP - Thursday',
                'slug' => str_slug('United States GP - Thursday'),
                'cover_id' => 409,
            ],
            [
                'name' => 'United States GP - Friday',
                'slug' => str_slug('United States GP - Friday'),
                'cover_id' => 413,
            ],
            [
                'name' => 'United States GP - Saturday',
                'slug' => str_slug('United States GP - Saturday'),
                'cover_id' => 417,
            ],
            [
                'name' => 'United States GP - Sunday',
                'slug' => str_slug('United States GP - Sunday'),
                'cover_id' => 423,
            ],
            [
                'name' => 'Mexican GP - Thursday',
                'slug' => str_slug('Mexican GP - Thursday'),
                'cover_id' => 430,
            ],
            [
                'name' => 'Mexican GP - Friday',
                'slug' => str_slug('Mexican GP - Friday'),
                'cover_id' => 432,
            ],
            [
                'name' => 'Mexican GP - Saturday',
                'slug' => str_slug('Mexican GP - Saturday'),
                'cover_id' => 438,
            ],
            [
                'name' => 'Mexican GP - Sunday',
                'slug' => str_slug('Mexican GP - Sunday'),
                'cover_id' => 444,
            ],
            [
                'name' => 'Brazilian GP - Thursday',
                'slug' => str_slug('Brazilian GP - Thursday'),
                'cover_id' => 448,
            ],
            [
                'name' => 'Brazilian GP - Friday',
                'slug' => str_slug('Brazilian GP - Friday'),
                'cover_id' => 450,
            ],
            [
                'name' => 'Brazilian GP - Saturday',
                'slug' => str_slug('Brazilian GP - Saturday'),
                'cover_id' => 456,
            ],
            [
                'name' => 'Brazilian GP - Sunday',
                'slug' => str_slug('Brazilian GP - Sunday'),
                'cover_id' => 462,
            ],
            [
                'name' => 'Abu Dhabi GP - Thursday',
                'slug' => str_slug('Abu Dhabi GP - Thursday'),
                'cover_id' => 469,
            ],
            [
                'name' => 'Abu Dhabi GP - Friday',
                'slug' => str_slug('Abu Dhabi GP - Friday'),
                'cover_id' => 472,
            ],
            [
                'name' => 'Abu Dhabi GP - Saturday',
                'slug' => str_slug('Abu Dhabi GP - Saturday'),
                'cover_id' => 478,
            ],
            [
                'name' => 'Abu Dhabi GP - Sunday',
                'slug' => str_slug('Abu Dhabi GP - Sunday'),
                'cover_id' => 484,
            ],
        ]);
    }
}
