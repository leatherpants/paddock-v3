<?php

use Illuminate\Database\Seeder;

class MediaYoutubeVideosHashtagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_youtube_videos_hashtags')->insert([
            [
                'youtube_video_id' => 1,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BritishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 2,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 2,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AzerbaijanGP')->first()->id,
            ],
            [
                'youtube_video_id' => 2,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 6,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 6,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AzerbaijanGP')->first()->id,
            ],
            [
                'youtube_video_id' => 6,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 8,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 8,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ChineseGP')->first()->id,
            ],
            [
                'youtube_video_id' => 8,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 9,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 9,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 9,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 11,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 11,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 12,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 12,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 13,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 13,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 14,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 14,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 15,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 15,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 16,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 16,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 16,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 17,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 17,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 18,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F1Testing')->first()->id,
            ],
            [
                'youtube_video_id' => 18,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 19,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 19,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AusGP')->first()->id,
            ],
            [
                'youtube_video_id' => 20,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AusGP')->first()->id,
            ],
            [
                'youtube_video_id' => 20,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 21,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AusGP')->first()->id,
            ],
            [
                'youtube_video_id' => 21,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 21,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 21,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 22,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 22,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 22,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AusGP')->first()->id,
            ],
            [
                'youtube_video_id' => 22,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 23,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BahrainGP')->first()->id,
            ],
            [
                'youtube_video_id' => 23,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12010')->first()->id,
            ],
            [
                'youtube_video_id' => 24,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BahrainGP')->first()->id,
            ],
            [
                'youtube_video_id' => 24,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 25,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BahrainGP')->first()->id,
            ],
            [
                'youtube_video_id' => 25,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 25,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 25,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 26,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 26,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 26,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BahrainGP')->first()->id,
            ],
            [
                'youtube_video_id' => 26,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 27,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ChineseGP')->first()->id,
            ],
            [
                'youtube_video_id' => 27,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12006')->first()->id,
            ],
            [
                'youtube_video_id' => 28,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ChineseGP')->first()->id,
            ],
            [
                'youtube_video_id' => 28,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 29,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ChineseGP')->first()->id,
            ],
            [
                'youtube_video_id' => 29,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 29,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 29,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 30,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 30,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 30,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ChineseGP')->first()->id,
            ],
            [
                'youtube_video_id' => 30,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 31,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AzerbaijanGP')->first()->id,
            ],
            [
                'youtube_video_id' => 31,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 32,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AzerbaijanGP')->first()->id,
            ],
            [
                'youtube_video_id' => 32,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 32,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 32,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 33,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AzerbaijanGP')->first()->id,
            ],
            [
                'youtube_video_id' => 33,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 33,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 33,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 33,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 34,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 34,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F11996')->first()->id,
            ],
            [
                'youtube_video_id' => 35,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 35,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12000')->first()->id,
            ],
            [
                'youtube_video_id' => 36,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 36,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12013')->first()->id,
            ],
            [
                'youtube_video_id' => 37,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 37,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 38,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 38,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 39,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 39,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 39,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 39,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 40,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SpanishGP')->first()->id,
            ],
            [
                'youtube_video_id' => 40,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 40,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 40,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 40,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 41,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MonacoGP')->first()->id,
            ],
            [
                'youtube_video_id' => 41,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12016')->first()->id,
            ],
            [
                'youtube_video_id' => 41,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 41,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 42,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MonacoGP')->first()->id,
            ],
            [
                'youtube_video_id' => 42,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 42,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 42,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 43,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MonacoGP')->first()->id,
            ],
            [
                'youtube_video_id' => 43,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 43,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 43,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 44,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MonacoGP')->first()->id,
            ],
            [
                'youtube_video_id' => 44,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 45,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MonacoGP')->first()->id,
            ],
            [
                'youtube_video_id' => 45,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 45,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 45,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 46,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MonacoGP')->first()->id,
            ],
            [
                'youtube_video_id' => 46,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 46,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 46,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 46,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 47,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 47,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F11995')->first()->id,
            ],
            [
                'youtube_video_id' => 48,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 48,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12001')->first()->id,
            ],
            [
                'youtube_video_id' => 49,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 49,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12013')->first()->id,
            ],
            [
                'youtube_video_id' => 50,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 50,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12016')->first()->id,
            ],
            [
                'youtube_video_id' => 50,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 51,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 51,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 51,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 52,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 52,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 53,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 53,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 53,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 53,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 54,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 54,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'CanadianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 54,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 54,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 54,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 55,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 55,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 55,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 55,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'FrenchGP')->first()->id,
            ],
            [
                'youtube_video_id' => 56,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'FrenchGP')->first()->id,
            ],
            [
                'youtube_video_id' => 56,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12008')->first()->id,
            ],
            [
                'youtube_video_id' => 57,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'FrenchGP')->first()->id,
            ],
            [
                'youtube_video_id' => 57,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 57,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => 57,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 57,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => 58,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AustrianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 58,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12015')->first()->id,
            ],
            [
                'youtube_video_id' => 59,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AustrianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 59,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12001')->first()->id,
            ],
            [
                'youtube_video_id' => 60,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AustrianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 60,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => 61,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => 61,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => 61,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AustrianGP')->first()->id,
            ],
            [
                'youtube_video_id' => 61,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'JNmfS9PSeYg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'JNmfS9PSeYg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'JNmfS9PSeYg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'JNmfS9PSeYg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'JNmfS9PSeYg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AustrianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9ESAYRZ5DMc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BritishGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9ESAYRZ5DMc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F11995')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9R1wEevHfwU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BritishGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9R1wEevHfwU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9R1wEevHfwU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9R1wEevHfwU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 't0WHNqwDjXc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BritishGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 't0WHNqwDjXc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Qg-AK5B9yvY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Qg-AK5B9yvY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Qg-AK5B9yvY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BritishGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Qg-AK5B9yvY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '_HkmYvKVx58')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BritishGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '_HkmYvKVx58')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '_HkmYvKVx58')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '_HkmYvKVx58')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '_HkmYvKVx58')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FnPpE2_N2dc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BritishGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FnPpE2_N2dc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FnPpE2_N2dc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FnPpE2_N2dc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FnPpE2_N2dc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'DwXOMAExnJM')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'DwXOMAExnJM')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'DwXOMAExnJM')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'GermanGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'DwXOMAExnJM')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'gp-i8vtgzJE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'gp-i8vtgzJE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'gp-i8vtgzJE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'GermanGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'gp-i8vtgzJE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'gp-i8vtgzJE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'pU72heP0uH0')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'HungarianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'pU72heP0uH0')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '5ob8zPojhE4')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '5ob8zPojhE4')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '5ob8zPojhE4')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'HungarianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '5ob8zPojhE4')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'swnvZggX0uo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'swnvZggX0uo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'swnvZggX0uo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'swnvZggX0uo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'swnvZggX0uo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'HungarianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'GGlNrHEiCa4')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BelgianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'GGlNrHEiCa4')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'fUkTSzPkiq8')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BelgianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'fUkTSzPkiq8')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F11998')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'HhnqhpaLFKY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BelgianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'HhnqhpaLFKY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'HhnqhpaLFKY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'HhnqhpaLFKY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'GXg1sGWpLlQ')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'GXg1sGWpLlQ')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'GXg1sGWpLlQ')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'GXg1sGWpLlQ')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BelgianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'GXg1sGWpLlQ')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '1LOPzsVsaA0')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ItalianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '1LOPzsVsaA0')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'grjFLrUIDf0')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ItalianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'grjFLrUIDf0')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd0J1oW3Yg64')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd0J1oW3Yg64')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd0J1oW3Yg64')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd0J1oW3Yg64')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ItalianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FcKJkWBjPVY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FcKJkWBjPVY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'FcKJkWBjPVY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9d1ppvK9npY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9d1ppvK9npY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9d1ppvK9npY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9d1ppvK9npY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9d1ppvK9npY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'ItalianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'MmVyfTUCbNU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'MmVyfTUCbNU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12010')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'MmVyfTUCbNU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SingaporeGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'wAOMOmsqpUY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'wAOMOmsqpUY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'wAOMOmsqpUY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SingaporeGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xwCdDyNdcec')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xwCdDyNdcec')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xwCdDyNdcec')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xwCdDyNdcec')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SingaporeGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'U3ebbDRYPgw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BeyondTheGrid')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'U3ebbDRYPgw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hm-AmaCaSOs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hm-AmaCaSOs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hm-AmaCaSOs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hm-AmaCaSOs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'SingaporeGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hm-AmaCaSOs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'TUAPI3t_9Kg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12015')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'TUAPI3t_9Kg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'TUAPI3t_9Kg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'TUAPI3t_9Kg')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'RussianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xtHtegExhXY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xtHtegExhXY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xtHtegExhXY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'RussianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '8Hh1tTaL43U')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '8Hh1tTaL43U')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '8Hh1tTaL43U')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '8Hh1tTaL43U')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'RussianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'jnRCF08PwIU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'jnRCF08PwIU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'jnRCF08PwIU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'jnRCF08PwIU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'jnRCF08PwIU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'RussianGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hr6g7nLgoPc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12000')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hr6g7nLgoPc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'hr6g7nLgoPc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'JapaneseGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9zMFhYEQNlU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9zMFhYEQNlU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9zMFhYEQNlU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', '9zMFhYEQNlU')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'JapaneseGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'F4T97cQm2iE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'F4T97cQm2iE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'F4T97cQm2iE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'JapaneseGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'ieYgD11lNGE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'ieYgD11lNGE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'ieYgD11lNGE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'ieYgD11lNGE')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'JapaneseGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'cRTmuPOhaIA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'cRTmuPOhaIA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'cRTmuPOhaIA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'cRTmuPOhaIA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'cRTmuPOhaIA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'JapaneseGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'aljfbhxtdyc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'aljfbhxtdyc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'aljfbhxtdyc')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'USGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xUaE8Rc4Bbk')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xUaE8Rc4Bbk')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xUaE8Rc4Bbk')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'xUaE8Rc4Bbk')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'USGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'PNwPpqaLE60')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'PNwPpqaLE60')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'PNwPpqaLE60')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'PNwPpqaLE60')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'PNwPpqaLE60')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'USGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'yalwjy6vRWA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'yalwjy6vRWA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'yalwjy6vRWA')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MexicoGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qoZME5Rcurw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qoZME5Rcurw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qoZME5Rcurw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qoZME5Rcurw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MexicoGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd1-VK12FZhs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd1-VK12FZhs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd1-VK12FZhs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd1-VK12FZhs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'd1-VK12FZhs')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'MexicoGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'YYOCmR5Lvbw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'YYOCmR5Lvbw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'YYOCmR5Lvbw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BrazilGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qphDZygiPoo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qphDZygiPoo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qphDZygiPoo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'qphDZygiPoo')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BrazilGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'o5pdLO9irzY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'o5pdLO9irzY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'o5pdLO9irzY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'o5pdLO9irzY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'o5pdLO9irzY')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BrazilGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'BXREg5yPf9A')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12017')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'BXREg5yPf9A')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'BXREg5yPf9A')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AbuDhabiGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Sn49RMe9imw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Sn49RMe9imw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Sn49RMe9imw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'Sn49RMe9imw')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AbuDhabiGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'KMcSGDJoS7o')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'KMcSGDJoS7o')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Seb5')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'KMcSGDJoS7o')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'F12018')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'KMcSGDJoS7o')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'KMcSGDJoS7o')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'AbuDhabiGP')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'O6Pw7IGoX0w')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Kimi7')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 'O6Pw7IGoX0w')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 't15tKnsUJrk')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'BeyondTheGrid')->first()->id,
            ],
            [
                'youtube_video_id' => \App\paddock\Media\Youtube\Models\MediaYoutubeVideos::where('youtube_id', 't15tKnsUJrk')->first()->id,
                'hashtag_id' => \App\paddock\Hashtags\Models\Hashtags::where('title', 'Ferrari')->first()->id,
            ],
        ]);
    }
}
