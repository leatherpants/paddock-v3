<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            [
                'code' => 'en',
                'name' => 'english',
                'active' => true,
                'news' => true,
            ],
            [
                'code' => 'de',
                'name' => 'german',
                'active' => true,
                'news' => true,
            ],
            [
                'code' => 'es',
                'name' => 'spanish',
                'active' => true,
                'news' => true,
            ],
            [
                'code' => 'se',
                'name' => 'swedish',
                'active' => true,
                'news' => true,
            ],
            [
                'code' => 'it',
                'name' => 'italian',
                'active' => true,
                'news' => true,
            ],
        ]);
    }
}
