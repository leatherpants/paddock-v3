<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(GrandPrixsTableSeeder::class);
        $this->call(TracksTableSeeder::class);
        $this->call(SeasonsTableSeeder::class);
        $this->call(SeasonsSessions2018TableSeeder::class);
        $this->call(StaffsTableSeeder::class);
        $this->call(StaffsSocialMediaTableSeeder::class);
        $this->call(HashtagsTableSeeder::class);
        $this->call(CarsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UsersSocialsTableSeeder::class);
        $this->call(SocialMediasTableSeeder::class);
        $this->call(TwitterAccountsTableSeeder::class);

        $this->call(ReportsTyresTableSeeder::class);
        $this->call(ReportsTechnicalsTableSeeder::class);

        $this->call(SeasonsRaces1950TableSeeder::class);
        $this->call(SeasonsRaces1960TableSeeder::class);
        $this->call(SeasonsRaces1970TableSeeder::class);
        $this->call(SeasonsRaces1980TableSeeder::class);
        $this->call(SeasonsRaces1990TableSeeder::class);
        $this->call(SeasonsRaces2000TableSeeder::class);
        $this->call(SeasonsRaces2010TableSeeder::class);

        $this->call(SeasonsFilesTableSeeder::class);

        $this->call(EventsTableSeeder::class);

        $this->call(FeedHistoryJanuaryTableSeeder::class);
        $this->call(FeedHistoryFebruaryTableSeeder::class);
        $this->call(FeedHistoryMarchTableSeeder::class);
        $this->call(FeedHistoryAprilTableSeeder::class);
        $this->call(FeedHistoryMayTableSeeder::class);
        $this->call(FeedHistoryJuneTableSeeder::class);

        $this->call(FeedMotorsportTableSeeder::class);

        $this->call(FeedFDATableSeeder::class);

        //Ferrari Feed
        $this->call(PreSeasonTableSeeder::class);
        $this->call(F1TestingPreSeasonTableSeeder::class);
        $this->call(AustralianGPTableSeeder::class);
        $this->call(BahrainGPTableSeeder::class);
        $this->call(ChineseGPTableSeeder::class);
        $this->call(AzerbaijanGPTableSeeder::class);
        $this->call(SpanishGPTableSeeder::class);
        $this->call(MonacoGPTableSeeder::class);
        $this->call(CanadianGPTableSeeder::class);
        $this->call(FrenchGPTableSeeder::class);
        $this->call(AustrianGPTableSeeder::class);
        $this->call(BritishGPTableSeeder::class);
        $this->call(GermanGPTableSeeder::class);
        $this->call(HungarianGPTableSeeder::class);
        $this->call(BelgianGPTableSeeder::class);
        $this->call(ItalianGPTableSeeder::class);
        $this->call(SingaporeGPTableSeeder::class);
        $this->call(RussianGPTableSeeder::class);
        $this->call(JapaneseGPTableSeeder::class);
        $this->call(USGPTableSeeder::class);
        $this->call(MexicoGPTableSeeder::class);
        $this->call(BrazilGPTableSeeder::class);
        $this->call(AbuDhabiGPTableSeeder::class);
        $this->call(PostSeasonTableSeeder::class);

        $this->call(MediaYoutubeVideosTableSeeder::class);
        $this->call(MediaYoutubeVideosHashtagsTableSeeder::class);

        $this->call(MediaGalleryAlbumsTableSeeder::class);
        $this->call(MediaGalleryPhotosTableSeeder::class);

        $this->call(Results1950TableSeeder::class);
        $this->call(Results1951TableSeeder::class);
        $this->call(Results1952TableSeeder::class);
        $this->call(Results1953TableSeeder::class);
        $this->call(Results1954TableSeeder::class);
        $this->call(Results1955TableSeeder::class);
        $this->call(Results1956TableSeeder::class);
        $this->call(Results1957TableSeeder::class);
        $this->call(Results1958TableSeeder::class);
        $this->call(Results1959TableSeeder::class);
        $this->call(Results1960TableSeeder::class);
        $this->call(Results1961TableSeeder::class);
        $this->call(Results1962TableSeeder::class);
        $this->call(Results1963TableSeeder::class);
        $this->call(Results1964TableSeeder::class);
        $this->call(Results1965TableSeeder::class);
        $this->call(Results1966TableSeeder::class);
        $this->call(Results1967TableSeeder::class);
        $this->call(Results1968TableSeeder::class);
        $this->call(Results1969TableSeeder::class);
        $this->call(Results1970TableSeeder::class);
        $this->call(Results1971TableSeeder::class);
        $this->call(Results1972TableSeeder::class);
        $this->call(Results1973TableSeeder::class);
        $this->call(Results1974TableSeeder::class);
        $this->call(Results1975TableSeeder::class);
        $this->call(Results1976TableSeeder::class);
        $this->call(Results1977TableSeeder::class);
        $this->call(Results1978TableSeeder::class);
        $this->call(Results1979TableSeeder::class);
        $this->call(Results1980TableSeeder::class);
        $this->call(Results1981TableSeeder::class);
        $this->call(Results1982TableSeeder::class);
        $this->call(Results1983TableSeeder::class);
        $this->call(Results1984TableSeeder::class);
        $this->call(Results1985TableSeeder::class);
        $this->call(Results1986TableSeeder::class);
        $this->call(Results1987TableSeeder::class);
        $this->call(Results1988TableSeeder::class);
        $this->call(Results1989TableSeeder::class);
        $this->call(Results1990TableSeeder::class);
        $this->call(Results1991TableSeeder::class);
        $this->call(Results1992TableSeeder::class);
        $this->call(Results1993TableSeeder::class);
        $this->call(Results1994TableSeeder::class);
        $this->call(Results1995TableSeeder::class);
        $this->call(Results1996TableSeeder::class);
        $this->call(Results1997TableSeeder::class);
        $this->call(Results1998TableSeeder::class);
        $this->call(Results1999TableSeeder::class);
        $this->call(Results2000TableSeeder::class);
        $this->call(Results2001TableSeeder::class);
        $this->call(Results2002TableSeeder::class);
        $this->call(Results2003TableSeeder::class);
        $this->call(Results2004TableSeeder::class);
        $this->call(Results2005TableSeeder::class);
        $this->call(Results2006TableSeeder::class);
        $this->call(Results2007TableSeeder::class);
        $this->call(Results2008TableSeeder::class);
        $this->call(Results2009TableSeeder::class);
        $this->call(Results2010TableSeeder::class);
        $this->call(Results2011TableSeeder::class);
        $this->call(Results2012TableSeeder::class);
        $this->call(Results2013TableSeeder::class);
        $this->call(Results2014TableSeeder::class);
        $this->call(Results2015TableSeeder::class);
        $this->call(Results2016TableSeeder::class);
        $this->call(Results2017TableSeeder::class);
        $this->call(Results2018TableSeeder::class);

        $this->call(ResultsTimings2017TableSeeder::class);
        $this->call(ResultsTimings2018TableSeeder::class);

        $this->call(TestsResultsTableSeeder::class);
    }
}
