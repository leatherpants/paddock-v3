<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaYoutubeVideosHashtagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_youtube_videos_hashtags', function (Blueprint $table) {
            $table->unsignedInteger('youtube_video_id');
            $table->foreign('youtube_video_id')->references('id')->on('media_youtube_videos')->onDelete('cascade');
            $table->unsignedInteger('hashtag_id');
            $table->foreign('hashtag_id')->references('id')->on('hashtags')->onDelete('cascade');

            $table->unique(['youtube_video_id', 'hashtag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_youtube_videos_hashtags');
    }
}
