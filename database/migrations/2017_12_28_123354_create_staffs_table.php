<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('country_id', 5);
            $table->string('name');
            $table->string('slug');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('hashtag')->nullable();
            $table->date('dateofbirth')->nullable();
            $table->date('dateofdeath')->nullable();

            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
