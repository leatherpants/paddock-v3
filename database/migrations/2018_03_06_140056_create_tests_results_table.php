<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests_results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->foreign('season')->references('season')->on('seasons')->onDelete('cascade');
            $table->unsignedTinyInteger('week');
            $table->unsignedTinyInteger('day');
            $table->date('dateoftest');
            $table->unsignedSmallInteger('track_id');
            $table->foreign('track_id')->references('id')->on('tracks')->onDelete('cascade');
            $table->unsignedSmallInteger('staff_id');
            $table->foreign('staff_id')->references('id')->on('staffs')->onDelete('cascade');
            $table->unsignedTinyInteger('pos');
            $table->unsignedInteger('laptime');
            $table->unsignedTinyInteger('laps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests_results');
    }
}
