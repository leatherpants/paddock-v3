<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->foreign('season')->references('season')->on('seasons')->onDelete('cascade');
            $table->unsignedTinyInteger('gp_id');
            $table->foreign('gp_id')->references('id')->on('grand_prixs')->onDelete('cascade');
            $table->unsignedTinyInteger('session_id');
            $table->dateTime('start');
            $table->dateTime('end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasons_sessions');
    }
}
