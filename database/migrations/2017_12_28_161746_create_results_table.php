<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->foreign('season')->references('season')->on('seasons')->onDelete('cascade');
            $table->date('dateofresult');
            $table->unsignedTinyInteger('gp_id');
            $table->foreign('gp_id')->references('id')->on('grand_prixs')->onDelete('cascade');
            $table->unsignedSmallInteger('track_id');
            $table->foreign('track_id')->references('id')->on('tracks')->onDelete('cascade');
            $table->unsignedTinyInteger('session_id');
            $table->unsignedSmallInteger('staff_id');
            $table->foreign('staff_id')->references('id')->on('staffs')->onDelete('cascade');
            $table->boolean('team');
            $table->unsignedTinyInteger('place');
            $table->boolean('dnf');
            $table->boolean('dns');
            $table->boolean('dsq');
            $table->decimal('points', 5, 2);
            $table->unsignedTinyInteger('laps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
