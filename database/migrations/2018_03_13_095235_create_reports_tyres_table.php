<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTyresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_tyres', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->foreign('season')->references('season')->on('seasons')->onDelete('cascade');
            $table->unsignedTinyInteger('race');
            $table->unsignedTinyInteger('gp_id');
            $table->foreign('gp_id')->references('id')->on('grand_prixs')->onDelete('cascade');
            $table->unsignedSmallInteger('staff_id');
            $table->foreign('staff_id')->references('id')->on('staffs')->onDelete('cascade');
            $table->unsignedTinyInteger('hypersoft')->nullable();
            $table->unsignedTinyInteger('ultrasoft')->nullable();
            $table->unsignedTinyInteger('supersoft')->nullable();
            $table->unsignedTinyInteger('soft')->nullable();
            $table->unsignedTinyInteger('medium')->nullable();
            $table->unsignedTinyInteger('hard')->nullable();
            $table->unsignedTinyInteger('superhard')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_tyres');
    }
}
