<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTechnicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_technicals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->foreign('season')->references('season')->on('seasons')->onDelete('cascade');
            $table->unsignedTinyInteger('race');
            $table->unsignedTinyInteger('gp_id');
            $table->foreign('gp_id')->references('id')->on('grand_prixs')->onDelete('cascade');
            $table->unsignedSmallInteger('staff_id');
            $table->foreign('staff_id')->references('id')->on('staffs')->onDelete('cascade');
            $table->unsignedTinyInteger('ice');
            $table->unsignedTinyInteger('tc');
            $table->unsignedTinyInteger('mguh');
            $table->unsignedTinyInteger('mguk');
            $table->unsignedTinyInteger('es');
            $table->unsignedTinyInteger('ce');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_technicals');
    }
}
