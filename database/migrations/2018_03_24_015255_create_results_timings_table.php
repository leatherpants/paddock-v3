<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results_timings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->foreign('season')->references('season')->on('seasons')->onDelete('cascade');
            $table->unsignedTinyInteger('gp_id');
            $table->foreign('gp_id')->references('id')->on('grand_prixs')->onDelete('cascade');
            $table->unsignedTinyInteger('session');
            $table->unsignedSmallInteger('staff_id');
            $table->foreign('staff_id')->references('id')->on('staffs')->onDelete('cascade');
            $table->unsignedInteger('laptime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results_timings');
    }
}
