<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedMotorsportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_motorsport', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang', 5);
            $table->string('title');
            $table->string('description');
            $table->string('image');
            $table->string('link');
            $table->string('domain');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_motorsport');
    }
}
